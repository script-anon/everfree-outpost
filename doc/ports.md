Ports used by Outpost components:

 * `8888`: game server (websocket)
 * `8889`: HTTP server for client/launcher files
 * `8890`: control socket (Windows only)
 * `8891`: repl socket (Windows only)
 * `8892`: logging server, for debugging
 * `8893`: admin interface web server (listens only on localhost)
