# Vault pack

"Vaults" are small hand-made map sections that get included in the larger
randomly-generated forest.  They're currently used to set up the starting area
and to spawn random treasure for players to find.

Vaults can be edited using [Tiled](https://www.mapeditor.org/).  This pack
contains Tiled map files for each vault and tileset files containing all the
terrain and structure types that exist in the current version of the game.

To begin, open one of the `.tmx` files in Tiled.

## Layers and tilesets

Each vault has two layers: a tile layer and an object layer.  The tile layer
sets the terrain type for each ground tile in the vault, using the tiles from
the `blocks` tileset.  The object layer contains a tile object for each
structure in the vault, using tiles from the `structures` tileset.

To edit the terrain, select the tile layer and either draw new `blocks` tiles
with the Stamp Brush tool (B) or erase existing terrain with the Eraser tool
(E).

To edit structures, select the object layer and either place new `structures`
tile objects with the Insert Tile tool (T), or move and delete structures with
the Select Objects tool (S).

Be sure to use only `blocks` tiles on the tile layer and only `structures`
tiles on the object layer.  Mixing the two will cause errors when compiling the
vault for use in-game.
