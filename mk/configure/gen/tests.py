import os

from minitemplate import template

def tests(i, test_exes):
    basename = os.path.basename

    data_files = (
        '$b_data2/server_data.bin',
        '$b_data2/client_data.bin',
        '$b_data2/preboot.py',
        '$b_data2/server_test_data.bin',
        '$b_data2/client_test_data.bin',
        '$b_data2/preboot_test.py',
    )

    return template(r'''
        rule run_test
            command = $python3 $root/util/run_test.py $log $stamp $in
            description = CHECK $in

        %for test in test_exes
        build $b_tests/reports/%{basename(test)}.log $
              $b_tests/stamp/%{basename(test)}.passed: run_test %test $
              | %{' '.join(data_files)}
            log = $b_tests/reports/%{basename(test)}.log
            stamp = $b_tests/stamp/%{basename(test)}.passed
        %end
    
        build check: phony $
            %for test in test_exes% build $b_tests/stamp/%{basename(test)}.passed %end%
    ''', **locals())

