import os

from minitemplate import template

from configure.util import join, cond, walk_dir


def rules(i, r):
    r.rule('c_asm_obj', 'CC',
            join(
                'clang',
                '$in', '-c', '-o', '$out',
                '--target=wasm32-unknown-unknown-wasm',
                '-MMD -MF $out.d',
                cond(i.debug, '-ggdb', '-O3'),
                '$flags',
                ),
            depfile='$out.d')

def lib(i, r, name, extra_flags=''):
    flags = extra_flags
    flags += ' -I$root/src/lib%s' % name
    flags += ' -I$root/src/lib%s/include' % name

    for path in walk_dir(os.path.join(i.root_dir, 'src/lib%s' % name)):
        if path.endswith('.c'):
            src_file = os.path.join('$root/src/lib%s' % name, path)
            obj_file = '$b_asmjs/lib%s/%s.o' % (name, path)

            r.c_asm_obj.build((obj_file,), (src_file,), flags=flags)
            r.file('c_asm_lib%s' % name, obj_file)
