from collections import defaultdict, deque
import os
import re

from configure.util import join, cond, maybe
from minitemplate import template


class RustCrate:
    def __init__(self,
            name,
            deps,
            src_file=None,
            # Non-outpost libraries used by this crate.  Should be `None`
            # everywhere except in the definitions of the external crates
            # themselves, which are used to build asm versions.
            external_deps=None,
            # Syntax plugins used by this crate.
            plugin_deps=(),
            # Internal (outpost) C libraries used by this crate.
            c_deps=(),
            # Dependencies that are specific to asm targets
            asm_deps=(),
            # Dependencies that are specific to native targets
            native_deps=(),
            force_optimize=False,
            extra_flags='',
            test=True,
            ):
        self.name = name
        self.deps = deps
        self.src_file = src_file or '$root/src/lib%s/lib.rs' % name
        if external_deps is not None:
            external_deps = tuple(external_deps)
        self.external_deps = external_deps
        self.plugin_deps = tuple(plugin_deps)
        self.c_deps = tuple(c_deps)
        self.asm_deps = tuple(asm_deps)
        self.native_deps = tuple(native_deps)
        self.force_optimize = force_optimize
        self.extra_flags = extra_flags
        self.test = test


def RustTest(name, deps, **kwargs):
    if 'src_file' not in kwargs:
        kwargs['src_file'] = '$root/tests/%s.rs' % name
    default_deps = ('test_paths',)
    return RustCrate('test_%s' % name, deps + default_deps, **kwargs)



def rules(i, r):
    r.var('rust_opt_flags_debug', '-g')
    r.var('rust_opt_flags_release', '-C opt-level=3')
    r.var('rust_opt_flags',
            '$rust_opt_flags_debug' if i.debug else '$rust_opt_flags_release')
    # LTO only applies to certain crate types
    r.var('rust_lto_flags', '' if i.debug else '-C lto')

    # As of rustc 1.26.2, opt-level=0 triggers a segfault inside LLVM's
    # wasm backend.  We set opt-level=1 by default here, which will be
    # overridden by the opt-level=3 in $rustflags_release on release
    # builds.
    r.var('rust_opt_flags_asm_debug', '-g -C opt-level=1')
    r.var('rust_opt_flags_asm_release', '-C opt-level=3')
    r.var('rust_opt_flags_asm',
            '$rust_opt_flags_asm_debug' if i.debug else '$rust_opt_flags_asm_release')
    r.var('rust_lto_flags_asm', '' if i.debug else '-C lto')


    compile_base = join(
            i.rustc_feature_env,
            '$rustc',
            '--emit link,dep-info',
            '-L $b_native -L $b_native/rust-libs -L $b_native/rust-plugins',
            maybe('-L %s', i.rust_extra_libdir),
            '--crate-name=$crate_name',
            '$in',
            '$flags',
            *('--extern %s' % x for x in i.rust_externs)
            )

    def add_rule(name, short_name, command, **kwargs):
        if 'depfile' in kwargs:
            # rustc outputs depfiles with a line for the main target, a line
            # for the depfile itself, and lines for each source file (with
            # empty deps).  Ninja expects to see only the first line, so we cut
            # off the rest.
            command += ' && sed -i -e 2q %s' % kwargs['depfile']
        r.rule(name, short_name, command, **kwargs)

    add_rule('rust_native_rlib', 'RUSTC',
            join(compile_base, '--crate-type=rlib', '--out-dir=$b_native/rust-libs'),
            depfile='$b_native/rust-libs/$crate_name.d',
            output_patterns=('$b_native/rust-libs/lib*.rlib',),
            required_args=('crate_name',))

    add_rule('rust_native_dylib', 'RUSTC',
            join(compile_base, '--crate-type=dylib', '--out-dir=$b_native/rust-libs'),
            depfile='$b_native/rust-libs/$crate_name.d',
            output_patterns=('$b_native/rust-libs/lib*$_so',),
            required_args=('crate_name',))

    add_rule('rust_native_proc_macro', 'RUSTC',
            join(compile_base, '--crate-type=proc-macro',
                '--out-dir=$b_native/rust-plugins'),
            depfile='$b_native/rust-plugins/$crate_name.d',
            output_patterns=('$b_native/rust-plugins/lib*$_so',),
            required_args=('crate_name',))

    add_rule('rust_native_bin', 'RUSTC',
            join(compile_base, '--crate-type=bin', '--out-dir=$b_native',
                '$rust_lto_flags'),
            depfile='$b_native/$crate_name.d',
            output_patterns=('$b_native/*$_exe',),
            required_args=('crate_name',))

    test_boot_py = os.path.join(i.root_dir, 'tests', 'boot.py')
    add_rule('rust_native_test', 'RUSTC',
            join(
                'OUTPOST_SERVER_DATA=../../data2/server_test_data.bin',
                'OUTPOST_CLIENT_DATA=../../data2/client_test_data.bin',
                'OUTPOST_SERVER_FULL_DATA=../../data2/server_data.bin',
                'OUTPOST_CLIENT_FULL_DATA=../../data2/client_data.bin',
                'OUTPOST_BOOT_PY=%s' % test_boot_py,
                compile_base,
                '--out-dir $b_tests/rust',
                '--crate-type=lib',
                '--test'),
            depfile='$b_tests/rust/$crate_name.d',
            output_patterns=('$b_tests/rust/*$_exe',),
            required_args=('crate_name',),
            implicit_deps=(
                '$b_data2/server_data.bin',
                '$b_data2/client_data.bin',
                '$b_data2/server_test_data.bin',
                '$b_data2/client_test_data.bin',
                '$root/tests/boot.py',
                ))

    add_rule('rust_native_cdylib', 'RUSTC',
            join(compile_base, '--crate-type=cdylib', '--out-dir=$b_native'),
            depfile='$b_native/$crate_name.d',
            output_patterns=('$b_native/*$_so',),
            required_args=('crate_name',))

    add_rule('rust_native_staticlib', 'RUSTC',
            join(compile_base, '--crate-type=staticlib', '--out-dir=$b_native',
                '$rust_lto_flags'),
            depfile='$b_native/$crate_name.d',
            output_patterns=('$b_native/*$_a',),
            required_args=('crate_name',))


    compile_base_asm = join(
            i.rustc_feature_env,
            '$rustc',
            '--emit link,dep-info',
            '--cfg asmjs',
            '--cfg \'feature="no_std"\'',
            '--target=wasm32-unknown-unknown',
            '-L $b_asmjs -L $b_asmjs/rust-libs -L $b_asmjs/rust-libs-opt',
            '-L $b_native/rust-plugins',
            '--crate-name=$crate_name',
            '$in',
            '$flags',
            )

    add_rule('rust_asm_rlib', 'RUSTC',
            join(compile_base_asm, '--crate-type=rlib', '--out-dir=$b_asmjs/rust-libs'),
            depfile='$b_asmjs/rust-libs/$crate_name.d',
            output_patterns=('$b_asmjs/rust-libs/lib*.rlib',),
            required_args=('crate_name',))

    add_rule('rust_asm_rlib_opt', 'RUSTC',
            join(compile_base_asm, '--crate-type=rlib', '--out-dir=$b_asmjs/rust-libs-opt'),
            depfile='$b_asmjs/rust-libs-opt/$crate_name.d',
            output_patterns=('$b_asmjs/rust-libs-opt/lib*.rlib',),
            required_args=('crate_name',))

    add_rule('rust_asm_wasm', 'RUSTC',
            join(compile_base_asm, '--crate-type=cdylib', '--out-dir=$b_asmjs',
                '-C linker=lld', '$rust_lto_flags_asm'),
            depfile='$b_asmjs/$crate_name.d',
            output_patterns=('$b_asmjs/*.wasm',),
            required_args=('crate_name',))


ESCAPE_RE = re.compile(r'[.\\^$*]')

def path_template(f):
    regex_str = ESCAPE_RE.sub(lambda m: '\\' + m.group(), f)
    regex = re.compile(regex_str.replace('%%', '(?P<name>.*)'))
    path = lambda d: f.replace('%%', d)
    return regex, path

ASM_RLIB_RE, asm_rlib_path = path_template('$b_asmjs/rust-libs/lib%%.rlib')
ASM_RLIB_OPT_RE, asm_rlib_opt_path = path_template('$b_asmjs/rust-libs-opt/lib%%.rlib')
ASM_WASM_RE, asm_wasm_path = path_template('$b_asmjs/%%.wasm')

NATIVE_RLIB_RE, native_rlib_path = path_template('$b_native/rust-libs/lib%%.rlib')
NATIVE_DYLIB_RE, native_dylib_path = path_template('$b_native/rust-libs/lib%%$_so')
NATIVE_PROC_MACRO_RE, native_proc_macro_path = path_template('$b_native/rust-plugins/lib%%$_so')
NATIVE_BIN_RE, native_bin_path = path_template('$b_native/%%$_exe')
NATIVE_CDYLIB_RE, native_cdylib_path = path_template('$b_native/lib%%$_so')
NATIVE_STATICLIB_RE, native_staticlib_path = path_template('$b_native/lib%%$_a')
NATIVE_TEST_RE, native_test_path = path_template('$b_tests/rust/%%$_exe')

def register_providers(r, crates):
    def provide_asm_rlib(out, m, force_optimize=False):
        name = m.group('name')
        c = crates[name]

        src_file = c.src_file
        deps = c.deps + c.asm_deps
        external_deps = c.external_deps
        plugin_deps = c.plugin_deps
        assert c.c_deps == (), 'c deps not supported for non-terminal asm libs'

        opt_flags = cond(force_optimize or c.force_optimize,
                '$rust_opt_flags_asm_release', '$rust_opt_flags_asm')
        flags = join(opt_flags, c.extra_flags)

        if external_deps is None:
            # This is the list of libraries we provide by default to asm-compiled
            # crates - a small subset of our full (native) rust dependencies.
            external_deps = ('std', 'bitflags', 'log')


        dep_files = []

        for d in deps:
            
            dep_files.append(r.require(asm_rlib_path(d)))

        for d in external_deps:
            dep_files.append(r.require(asm_rlib_opt_path(d)))

        for d in plugin_deps:
            dep_files.append(r.require(native_proc_macro_path(d)))


        rule = r.rust_asm_rlib if not force_optimize else r.rust_asm_rlib_opt
        rule.build((out,), (src_file,),
                implicit_inputs=dep_files,
                crate_name=name,
                flags=flags)

    r.provider(ASM_RLIB_RE,
            lambda out, m: provide_asm_rlib(out, m, force_optimize=False))
    r.provider(ASM_RLIB_OPT_RE,
            lambda out, m: provide_asm_rlib(out, m, force_optimize=True))


    def provide_asm_wasm(out, m):
        name = m.group('name')
        c = crates[name]

        src_file = c.src_file
        deps = c.deps + c.asm_deps
        external_deps = c.external_deps
        plugin_deps = c.plugin_deps
        c_deps = c.c_deps

        opt_flags = cond(c.force_optimize,
                '$rust_opt_flags_asm_release', '$rust_opt_flags_asm')
        flags = join(opt_flags, c.extra_flags)

        if external_deps is None:
            # This is the list of libraries we provide by default to asm-compiled
            # crates - a small subset of our full (native) rust dependencies.
            external_deps = ('std', 'bitflags', 'log', 'panic_abort')

        # Every asm library depends on asmrt (which contains memcpy etc.)
        c_deps += ('asmrt',)


        dep_files = []

        for d in deps:
            dep_files.append(r.require(asm_rlib_path(d)))

        for d in external_deps:
            dep_files.append(r.require(asm_rlib_opt_path(d)))

        for d in plugin_deps:
            dep_files.append(r.require(native_proc_macro_path(d)))

        for d in c_deps:
            objs = r.filesets['c_asm_lib%s' % d]
            dep_files.extend(objs)
            flags += ''.join(' -C link-arg=%s' % o for o in objs)

        r.rust_asm_wasm.build((out,), (src_file,),
                implicit_inputs=dep_files,
                crate_name=name,
                flags=flags)

    r.provider(ASM_WASM_RE, provide_asm_wasm)


    def provide_native_rlib(out, m):
        name = m.group('name')
        c = crates[name]

        src_file = c.src_file
        deps = c.deps + c.native_deps
        assert c.external_deps is None
        plugin_deps = c.plugin_deps
        assert c.c_deps == ()       # NYI

        opt_flags = cond(c.force_optimize, '$rust_opt_flags_release', '$rust_opt_flags')
        flags = join(opt_flags, c.extra_flags)


        dep_files = []

        for d in deps:
            dep_files.append(r.require(native_rlib_path(d)))

        for d in plugin_deps:
            dep_files.append(r.require(native_proc_macro_path(d)))


        r.rust_native_rlib.build((out,), (c.src_file,),
                implicit_inputs=dep_files,
                crate_name=c.name,
                flags=flags)

    r.provider(NATIVE_RLIB_RE, provide_native_rlib)


    def provide_native_dylib(out, m):
        name = m.group('name')
        c = crates[name]

        src_file = c.src_file
        deps = c.deps + c.native_deps
        assert c.external_deps is None
        plugin_deps = c.plugin_deps
        assert c.c_deps == ()       # NYI

        opt_flags = cond(c.force_optimize, '$rust_opt_flags_release', '$rust_opt_flags')
        flags = join(opt_flags, '-C prefer-dynamic', c.extra_flags)


        dep_files = []

        for d in deps:
            dep_files.append(r.require(native_dylib_path(d)))

        for d in plugin_deps:
            dep_files.append(r.require(native_proc_macro_path(d)))


        r.rust_native_dylib.build((out,), (c.src_file,),
                implicit_inputs=dep_files,
                crate_name=c.name,
                flags=flags)

    r.provider(NATIVE_DYLIB_RE, provide_native_dylib)


    def provide_native_proc_macro(out, m):
        name = m.group('name')
        c = crates[name]

        src_file = c.src_file
        deps = c.deps + c.native_deps
        assert c.external_deps is None
        plugin_deps = c.plugin_deps
        assert c.c_deps == ()       # NYI

        opt_flags = cond(c.force_optimize, '$rust_opt_flags_release', '$rust_opt_flags')
        flags = join(opt_flags, '-C prefer-dynamic', c.extra_flags)


        dep_files = []

        for d in deps:
            dep_files.append(r.require(native_dylib_path(d)))

        for d in plugin_deps:
            dep_files.append(r.require(native_proc_macro_path(d)))


        r.rust_native_proc_macro.build((out,), (c.src_file,),
                implicit_inputs=dep_files,
                crate_name=c.name,
                flags=flags)

    r.provider(NATIVE_PROC_MACRO_RE, provide_native_proc_macro)


    def provide_native_bin(out, m):
        name = m.group('name')
        c = crates[name]

        src_file = c.src_file
        deps = c.deps + c.native_deps
        assert c.external_deps is None
        plugin_deps = c.plugin_deps
        assert c.c_deps == ()       # NYI

        opt_flags = cond(c.force_optimize, '$rust_opt_flags_release', '$rust_opt_flags')
        flags = join(opt_flags, c.extra_flags)


        dep_files = []

        for d in deps:
            dep_files.append(r.require(native_rlib_path(d)))

        for d in plugin_deps:
            dep_files.append(r.require(native_proc_macro_path(d)))


        r.rust_native_bin.build((out,), (c.src_file,),
                implicit_inputs=dep_files,
                crate_name=c.name,
                flags=flags)

    r.provider(NATIVE_BIN_RE, provide_native_bin)


    def provide_native_cdylib(out, m):
        name = m.group('name')
        c = crates[name]

        src_file = c.src_file
        deps = c.deps + c.native_deps
        assert c.external_deps is None
        plugin_deps = c.plugin_deps
        assert c.c_deps == ()       # NYI

        opt_flags = cond(c.force_optimize, '$rust_opt_flags_release', '$rust_opt_flags')
        flags = join(opt_flags, c.extra_flags)


        dep_files = []

        for d in deps:
            dep_files.append(r.require(native_rlib_path(d)))

        for d in plugin_deps:
            dep_files.append(r.require(native_proc_macro_path(d)))


        r.rust_native_cdylib.build((out,), (c.src_file,),
                implicit_inputs=dep_files,
                crate_name=c.name,
                flags=flags)

    r.provider(NATIVE_CDYLIB_RE, provide_native_cdylib)


    def provide_native_staticlib(out, m):
        name = m.group('name')
        c = crates[name]

        src_file = c.src_file
        deps = c.deps + c.native_deps
        assert c.external_deps is None
        plugin_deps = c.plugin_deps
        assert c.c_deps == ()       # NYI

        opt_flags = cond(c.force_optimize, '$rust_opt_flags_release', '$rust_opt_flags')
        flags = join(opt_flags, c.extra_flags)


        dep_files = []

        for d in deps:
            dep_files.append(r.require(native_rlib_path(d)))

        for d in plugin_deps:
            dep_files.append(r.require(native_proc_macro_path(d)))


        r.rust_native_staticlib.build((out,), (c.src_file,),
                implicit_inputs=dep_files,
                crate_name=c.name,
                flags=flags)

    r.provider(NATIVE_STATICLIB_RE, provide_native_staticlib)


    def provide_native_test(out, m):
        name = m.group('name')
        c = crates[name]

        src_file = c.src_file
        deps = c.deps + c.native_deps
        assert c.external_deps is None
        plugin_deps = c.plugin_deps
        assert c.c_deps == ()       # NYI

        opt_flags = cond(c.force_optimize, '$rust_opt_flags_release', '$rust_opt_flags')
        flags = join(opt_flags, c.extra_flags)


        dep_files = []

        for d in deps:
            dep_files.append(r.require(native_rlib_path(d)))

        for d in plugin_deps:
            dep_files.append(r.require(native_proc_macro_path(d)))


        r.rust_native_test.build((out,), (c.src_file,),
                implicit_inputs=dep_files,
                crate_name=c.name,
                flags=flags)

    r.provider(NATIVE_TEST_RE, provide_native_test)
