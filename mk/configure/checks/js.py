import os

from configure.checks.context import ConfigError

def configure(ctx):
    ctx.detect('uglifyjs', 'uglifyjs',
            ('uglifyjs',), chk_uglifyjs)

def requirements(ctx):
    return ('uglifyjs',)

def chk_uglifyjs(ctx, uglify):
    output = ctx.run_output(uglify, ('--version',))
    if output is None:
        raise ConfigError('not found')

    return True
