import os
import sys
import textwrap

boot_py, game_py, out_file = sys.argv[1:]

with open(out_file, 'w') as f:
    f.write(textwrap.dedent('''
        GAME_PY_PATH = {game_py!r}

        __file__ = {boot_py!r}
        with open(__file__, 'r') as f:
            code = compile(f.read(), __file__, 'exec')
        exec(code, globals(), locals())
    ''').format(game_py=game_py, boot_py=boot_py))
