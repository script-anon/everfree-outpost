uniform sampler2D color_tex;
uniform sampler2D light_tex;
uniform vec2 screen_size;

varying vec2 tex_coord;


void main(void) {
    vec4 baseColor = texture2D(color_tex, tex_coord);

    vec4 lightColor = texture2D(light_tex, tex_coord);
    vec4 mainColor = baseColor * lightColor;

    vec3 color = mainColor.rgb;
    gl_FragColor = vec4(color, 1.0);
}
