varying vec2 tex_coord;

uniform float target;
uniform vec2 xy;
uniform float center_x;

#include "colorspace.inc"

void main(void) {
    float x_dist = (gl_FragCoord.x - 0.5) - center_x;
    float y_dist = (gl_FragCoord.y - 0.5) - target;

    bool in_cross = abs(x_dist + y_dist) <= 1.01 || abs(x_dist - y_dist) <= 1.01;

    if (abs(y_dist) <= 0.01 && !in_cross) {
        gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
    } else if (abs(y_dist) <= 1.01 && !in_cross) {
        gl_FragColor = vec4(0.8, 0.8, 0.8, 1.0);
    } else {
        vec3 lch_uv = vec3(tex_coord.y, xy.y, xy.x);
        vec3 luv = lch_uv_to_luv(lch_uv);
        vec3 xyz = luv_to_xyz(luv);
        vec3 linear_rgb = xyz_to_linear_rgb(xyz);
        vec3 srgb = linear_rgb_to_srgb(linear_rgb);

        if (!srgb_in_gamut(srgb)) {
            gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);
        } else {
            gl_FragColor = vec4(srgb, 1.0);
        }
    }
}
