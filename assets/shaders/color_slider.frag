varying vec2 tex_coord;

uniform float target;
uniform float channel;
uniform vec3 color;

#include "colorspace.inc"

void main(void) {
    float x_dist = abs(gl_FragCoord.x - 0.5 - target);
    if (x_dist <= 0.5) {
        gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
    } else if (x_dist <= 1.5) {
        gl_FragColor = vec4(0.8, 0.8, 0.8, 1.0);
    } else {
        vec3 c = color;
        if (channel == 0.0) {
            c.r = tex_coord.x;
        } else if (channel == 1.0) {
            c.g = tex_coord.x;
        } else {
            c.b = tex_coord.x;
        }
        gl_FragColor = vec4(c, 1.0);
    }
}
