use std::env;
use std::fs::File;
use std::io::{self, BufReader, SeekFrom};
use std::io::prelude::*;
use std::mem;
use std::slice;
use std::str::FromStr;



unsafe fn as_buf_mut<'a, T>(x: &'a mut T) -> &'a mut [u8] {
    slice::from_raw_parts_mut(x as *mut T as *mut u8,
                              mem::size_of::<T>())
}


struct FuncInfo {
    start: u64,
    end: u64,
    name: String,
}

impl FuncInfo {
    fn new(start: u64, end: u64) -> FuncInfo {
        FuncInfo {
            start,
            end,
            name: String::new(),
        }
    }
}

struct WasmReader<R> {
    r: R,
    offset: u64,

    num_imported_funcs: usize,
    funcs: Vec<FuncInfo>,
}

impl<R: Read + Seek> WasmReader<R> {
    fn new(r: R) -> WasmReader<R> {
        WasmReader {
            r,
            offset: 0,
            num_imported_funcs: 0,
            funcs: Vec::new(),
        }
    }

    unsafe fn read_raw<T: Default>(&mut self) -> io::Result<T> {
        let mut buf = T::default();
        unsafe { self.r.read_exact(as_buf_mut(&mut buf))? };
        self.offset += mem::size_of::<T>() as u64;
        Ok(buf)
    }

    fn read_byte(&mut self) -> io::Result<u8> {
        unsafe { self.read_raw::<u8>() }
    }

    fn skip(&mut self, delta: i64) -> io::Result<()> {
        self.r.seek(SeekFrom::Current(delta as i64))?;
        self.offset += delta as u64;
        Ok(())
    }

    fn skip_to(&mut self, offset: u64) -> io::Result<()> {
        let delta = offset as i64 - self.offset as i64;
        self.skip(delta)
    }

    fn read_uleb128(&mut self) -> io::Result<u64> {
        let mut acc = 0;
        let mut shift = 0;
        loop {
            let b = self.read_byte()?;
            acc |= ((b & 0x7f) as u64) << shift;
            if b & 0x80 == 0 {
                break;
            }
            shift += 7;
        }
        Ok(acc)
    }

    fn read_leb128(&mut self) -> io::Result<i64> {
        let mut acc = 0;
        let mut shift = 0;
        loop {
            let b = self.read_byte()?;
            acc |= ((b & 0x7f) as i64) << shift;
            if b & 0x80 == 0 {
                break;
            }
            shift += 7;
        }
        if shift < 64 {
            // Sign extend
            acc = acc << (64 - shift) >> (64 - shift);
        }
        Ok(acc)
    }

    fn read_u32(&mut self) -> io::Result<u32> {
        self.read_uleb128().map(|x| x as u32)
    }

    fn read_vector<T, F: Fn(&mut Self) -> io::Result<T>>(&mut self, f: F) -> io::Result<Vec<T>> {
        let len = self.read_u32()?;
        let mut v = Vec::with_capacity(len as usize);
        for _ in 0 .. len {
            v.push(f(self)?);
        }
        Ok(v)
    }

    fn read_name(&mut self) -> io::Result<String> {
        let bytes = self.read_vector(|s| s.read_byte())?;
        String::from_utf8(bytes)
            .map_err(|e| io::Error::new(io::ErrorKind::InvalidData,
                                        format!("utf-8 error: {}", e)))
    }


    fn read_func_span(&mut self) -> io::Result<(u64, u64)> {
        let size = self.read_u32()?;
        let start = self.offset;
        let end = start + size as u64;
        self.skip_to(end);
        Ok((start, end))
    }

    fn read_names(&mut self, end: u64) -> io::Result<()> {
        while self.offset < end {
            let subsec_type = self.read_byte()?;
            let size = self.read_u32()?;
            if subsec_type != 1 {
                // We only care about function names.
                self.skip(size as i64);
                continue;
            }

            let entries = self.read_vector(|s| {
                let idx = s.read_u32()?;
                let name = s.read_name()?;
                Ok((idx, name))
            })?;

            for (idx, name) in entries {
                let idx = idx as usize;
                if idx < self.num_imported_funcs {
                    continue;
                }
                let idx = idx - self.num_imported_funcs;
                if idx >= self.funcs.len() {
                    eprintln!("warning: namemap entry refers to nonexistent function {} ({:?})",
                              idx, name);
                    continue;
                }

                self.funcs[idx].name = name;
            }
        }

        Ok(())
    }


    fn read_module(&mut self) -> io::Result<()> {
        let magic = unsafe { self.read_raw::<[u8; 4]>()? };
        let version = unsafe { self.read_raw::<u32>()? };
        if &magic != b"\0asm" || version != 1 {
            return Err(io::Error::new(io::ErrorKind::InvalidData,
                                      "bad magic and/or version"));
        }

        loop {
            let sec_type = match self.read_byte() {
                Ok(x) => x,
                Err(ref e) if e.kind() == io::ErrorKind::UnexpectedEof => break,
                Err(e) => return Err(e),
            };
            let size = self.read_u32()?;
            let start = self.offset;
            let end = start + size as u64;

            match sec_type {
                0 => {  // custom
                    let name = self.read_name()?;
                    let size = size - (self.offset - start) as u32;
                    if &name == "name" {
                        self.read_names(end)?;
                    }
                },

                2 => {
                    let imports = self.read_vector(|s| {
                        let _m = s.read_name()?;
                        let _n = s.read_name()?;
                        match s.read_byte()? {
                            0 => {
                                let _ty = s.read_u32()?;
                            },
                            _ => unimplemented!("non-function imports"),
                        };
                        Ok(())
                    })?;
                    self.num_imported_funcs = imports.len();
                    eprintln!("found {} import decls", imports.len());
                },

                3 => {
                    let count = self.read_u32()?;
                    eprintln!("found {} function decls", count);
                },

                10 => { // code
                    let func_spans = self.read_vector(|s| s.read_func_span())?;
                    eprintln!("found {} function bodies", func_spans.len());
                    self.funcs = func_spans.into_iter()
                        .map(|(start, end)| FuncInfo::new(start, end))
                        .collect();
                },

                _ => {},
            }

            self.skip_to(end);
        }

        Ok(())
    }
}

fn io_main() -> io::Result<()> {
    let path = env::args().nth(1).unwrap();

    let mut wr = WasmReader::new(File::open(&path)?);
    wr.read_module()?;

    for (i, f) in wr.funcs.iter_mut().enumerate() {
        if f.name.len() == 0 {
            f.name = format!("${}", i);
        }
    }

    for addr_str in env::args().skip(2) {
        let addr = u64::from_str(&addr_str).unwrap();
        for f in &wr.funcs {
            if addr >= f.start && addr < f.end {
                println!("{:10}: {}", addr, f.name);
            }
        }
    }

    Ok(())
}

fn main() {
    io_main().unwrap()
}
