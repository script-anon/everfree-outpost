
# Nix script for setting up the build environment.  The upper portion (above
# `mkShell`) defines custom nix packages, for dependencies that aren't in the
# main `nixpkgs` repo, or for cases where we need a specific version or build
# configuration.  The lower portion gives the actual dependencies needed to
# build Everfree Outpost, some of which are the custom packages defined
# previously.

with builtins;

let pkgs = import ./pinned-nixpkgs.nix {};
    inherit (pkgs) stdenv lib fetchurl fetchzip callPackage buildEnv;

    # LLVM and Clang, configured to enable the WebAssembly target.
    #llvmWasm = import ./llvm-wasm.nix pkgs;


    # The rust compiler.  Using a pinned nixpkgs ensures we always get the same version.
    rustc = (pkgs.rustc.override rec {
        version = "1.33.0";
        rustPlatform = callPackage ./rust-bootstrap-platform-1.32.nix {};
        src = fetchurl {
            url = "https://static.rust-lang.org/dist/rustc-${version}-src.tar.gz";
            sha256 = "152x91mg7bz4ygligwjb05fgm1blwy2i70s2j03zc9jiwvbsh0as";
        };
    }).overrideAttrs (old: {
        patches = [ ./rustc-wasm-linker-no-export-dynamic.patch ];
    });

    rustSrc = pkgs.runCommand "unpack-rust-src-${rustc.version}" {
        src = rustc.src;
    } ''
        tar -xzf ${rustc.src}
        mv rustc-${rustc.version}-src $out
    '';


    inherit (import ./rust-libs.nix (pkgs // { inherit rustc; }))
        rustDep buildRustLibDir;

    # Rust library dependencies.  The first argument to `rustDep` is the
    # package name and version.  `features = [ ... ]` indicates Cargo features
    # that need to be enabled.
    rustDepDrvs = rec {
        bitflags = rustDep "bitflags-0.7.0" {
            sha256 = "1hr72xg5slm0z4pxs2hiy4wcyx3jva70h58b7mid8l0a4c8f7gn5";
        };
        libc = rustDep "libc-0.2.8" {
            sha256 = "103i6v93lm1jc8r2mkj5y9p62ss9ygr05zdr81dyh11nph8syh7n";
        };
        rand = rustDep "rand-0.3.14" {
            sha256 = "0bjzb2ngjkgc1kmc7gzw2b8imv2d3fkg0x2qd5qw0bl9f7h6km2p";
            dependencies = [ libc ];
        };
        rustc-serialize = rustDep "rustc-serialize-0.3.24" {
            sha256 = "0rfk6p66mqkd3g36l0ddlv2rvnp1mp3lrq5frq9zz5cbnz5pmmxn";
        };
        time = rustDep "time-0.1.34" {
            sha256 = "08qxwig2w7wh9p9pf35ygzs2ny9xdv4x3qhasxrcyz6xf3iahsr7";
            dependencies = [ libc ];
        };

        linked-hash-map = rustDep "linked-hash-map-0.0.9" {
            sha256 = "0cg5p6m9ka9isg370p5sszs81h3gg8wvy9xv86fk6nc45jiqdzaf";
        };

        unicode-xid = rustDep "unicode-xid-0.1.0" {
            sha256 = "05wdmwlfzxhq3nhsxn6wx4q8dhxzzfb9szsz6wiw092m1rjj01zj";
        };
        proc-macro2 = (rustDep "proc-macro2-0.4.27" {
            sha256 = "1cp4c40p3hwn2sz72ssqa62gp5n8w4gbamdqvvadzp5l7gxnq95i";
            features = [ "proc-macro" "span-locations" ];
            dependencies = [ unicode-xid ];
        }).overrideAttrs (old: {
            # build.rs relies on the RUSTC env var, which is not set by
            # buildRustCrate.
            RUSTC = "${rustc}/bin/rustc";
        });
        syn = rustDep "syn-0.15.29" {
            sha256 = "0wrd6awgc6f1iwfn2v9fvwyd2yddgxdjv9s106kvwg1ljbw3fajw";
            features = [ "clone-impls" "full" "parsing" "printing" "proc-macro" "fold" ];
            dependencies = [ proc-macro2 unicode-xid quote ];
        };
        quote = rustDep "quote-0.6.11" {
            sha256 = "0agska77z58cypcq4knayzwx7r7n6m756z1cz9cp2z4sv0b846ga";
            dependencies = [ proc-macro2 ];
        };

        python3-sys = lib.overrideDerivation
            (rustDep "python3-sys-0.1.3" {
                sha256 = "01z3dbqwfraxgkwi83n6ckxidgaw1fcrgbpmwmva3fsyx8jm0niw";
                dependencies = [ libc ];
                buildDependencies = [ regex ];
                buildInputs = [ pkgs.python3 ];
                features = [ "python-3" "pep-384" ];
                # As of Python 3.7, threads are always enabled.  That means
                # there's no longer a WITH_THREAD option for build.rs to
                # detect.  (The "python-3-7" feature enables the same things as
                # WITH_THREAD, but we'd like to keep supporting older Python
                # versions as well.)
                extraRustcOpts = [ "--cfg py_sys_config=\\\"WITH_THREAD\\\"" ];
            })
            # build.rs relies on CARGO_FEATURE_XYZ env vars, which
            # buildRustCrate does not yet support
            (_: {
                CARGO_FEATURE_PYTHON_3 = "1";
                CARGO_FEATURE_PEP_384 = "1";
            });

        log = rustDep "log-0.3.5" {
            sha256 = "00279rcly8c6w7w4y92gsxl146m2rxmh1k9s8hz14y7gbi1j0ig4";
            features = [ "use_std" ];
            dependencies = [ libc ];
        };
        env_logger = rustDep "env_logger-0.3.2" {
            sha256 = "03l06frgpbl990z20a91lp9w9vir6ikgmi6pxh0zn1havsy7waqk";
            dependencies = [ log regex ];
        };

        regex = rustDep "regex-0.1.55" {
            sha256 = "00lb300gvm5w0353pvkmic17h2g8gw0i25q2adyy6h2ad25i8imq";
            dependencies = [ aho-corasick memchr regex-syntax utf8-ranges ];
        };
        regex-syntax = rustDep "regex-syntax-0.2.5" {
            sha256 = "0b1al9ilxv7ldnbbax0alkdmcjh0h5xn3md4sy2s9wxxdxs27kbd";
        };
        aho-corasick = rustDep "aho-corasick-0.5.1" {
            sha256 = "1bg4asdxilfbsawm9fj6946dlidsi0jqr7i52vz78q6i80n397dz";
            dependencies = [ memchr ];
        };
        memchr = rustDep "memchr-0.1.10" {
            sha256 = "0whcrwnmqn5w4zb6sq2kpabyx93k8q8bn963rf4v5c07fsx7qj8j";
            dependencies = [ libc ];
        };
        utf8-ranges = rustDep "utf8-ranges-0.1.3" {
            sha256 = "1cj548a91a93j8375p78qikaiam548xh84cb0ck8y119adbmsvbp";
        };

        num = rustDep "num-0.1.36" {
            sha256 = "1p1vyczf75a39vxcid8d5w1jmjybd0akgn3s3vmrjslhv9sfvqb3";
            dependencies = [ num-integer num-iter num-traits ];
        };
        num-integer = rustDep "num-integer-0.1.32" {
            sha256 = "0l52mizdca5967vnsscwahwg27aklih52cznmvlrx7sina87jcg0";
            dependencies = [ num-traits ];
        };
        num-iter = rustDep "num-iter-0.1.32" {
            sha256 = "14a4710b037jih43fxav1ahzh2k419vsz9ym5pkrcciynczr4niv";
            dependencies = [ num-traits num-integer ];
        };
        num-traits = rustDep "num-traits-0.1.36" {
            sha256 = "1wzf0jd77dbagjqk9ygd6dy1729zg7gj7flf3sa54n7qvfhcwi79";
        };

        sdl2 = rustDep "sdl2-0.29.0" {
            sha256 = "1f3va8g5ng5xv2mnbdi3ayg3lg08yidrwgqhy2j47xjishg8bihk";
            libPath = "src/sdl2/lib.rs";
            dependencies = [ bitflags lazy_static libc num rand sdl2-sys ];
        };
        sdl2-sys = rustDep "sdl2-sys-0.27.2" {
            sha256 = "0igj45rf6zik21rasl1fsy1j7x798kxq1lvlqc9szky7bx1p60kl";
            dependencies = [ libc ];
        };
        lazy_static = rustDep "lazy_static-0.2.2" {
            sha256 = "006gi7k0a5s67hwd6csfqddzq7pxwwgasyxch0hdhghjk9bj11yz";
        };

        gl_generator = rustDep "gl_generator-0.5.2" {
            sha256 = "1cdk2nv9g9p1p4837m9qjpqb5k7k52yvvzjmvfb90phxgzgdfk48";
            libPath = "lib.rs";
            dependencies = [ xml khronos_api log ];
        };
        xml = rustDep "xml-rs-0.3.6" {
            libName = "xml";
            sha256 = "1g1cclib7fj900m4669vxlz45lxcq0m36g7cd8chl494c2xsgj15";
            dependencies = [ bitflags ];
        };
        khronos_api = rustDep "khronos_api-1.0.0" {
            sha256 = "0rx0ck2kn3xp1d17v4jwdqyb1bf33i7m1gvablhnzyg9mx3g1ycm";
        };

        png = rustDep "png-0.6.2" {
            sha256 = "03i78w5jbvk9y6babfrh7h0akvg81pcyyhniilv24z5v0vh5jvjs";
            dependencies = [ inflate num-iter bitflags ];
        };
        inflate = rustDep "inflate-0.1.1" {
            sha256 = "112kh9hjcjjxdybl032mdhpwnr3qxw8j0ch6hwanwpcf3gz42g1h";
        };
    };

    rustDeps = buildRustLibDir (attrValues rustDepDrvs);

    bitflagsSrc = rustDepDrvs.bitflags.src;
    logSrc = rustDepDrvs.log.src;


    # Static build of libsodium (nixpkgs only provides dynamic)
    libsodium = lib.overrideDerivation pkgs.libsodium
        (_: { configureFlags = [ "--enable-static" ]; });


    elfLoader = import ./elf-loader.nix pkgs;


in pkgs.mkShell {
    buildInputs = attrValues {
        # Standard build environment, similar to Debian's `build-essential`.
        inherit (pkgs) stdenv;
        glibc_out = pkgs.glibc.out;
        glibc_static = pkgs.glibc.static;

        # Custom packages defined above
        inherit rustc libsodium;
        #inherit (llvmWasm) llvm lld;
        # `llvmWasm.clang` is a Nix-wrapped version of clang that adds a bunch
        # of extra compiler flags.  `llvmWasm.clang.cc` is the real underlying
        # `clang` package.
        #clang = llvmWasm.clang.cc;

        # Standard packages
        inherit (pkgs)
            ninja
            python3 optipng
            SDL2 boost websocketpp ncurses;
        inherit (pkgs.haskellPackages) pandoc;
        inherit (pkgs.python3Packages) pillow pyyaml numpy scipy;

        # Run-time dependencies
        inherit (pkgs.python3Packages) bcrypt tornado pynacl flask requests Wand;

        # `patchelf` is only needed when building under Nix.
        inherit (pkgs) patchelf;

        inherit (pkgs.nodePackages) uglify-js;

        inherit (pkgs.llvmPackages_7) llvm lld compiler-rt;
    };

    # Additional dependencies, whose installation paths get added to the
    # environment.
    inherit
        # Auto-generated library directory, containing compiled versions of all
        # the Rust libraries defined above.
        rustDeps
        # Unpacked source code for the Rust compiler
        rustSrc
        # Unpacked source code for Rust libraries (the same versions defined
        # above).
        bitflagsSrc logSrc;
    #compiler_rt_wasm = llvmWasm.compiler-rt-wasm;
    #inherit (pkgs.llvmPackages_7) llvm lld compiler-rt;
    #inherit (pkgs) rustc;

    # The contents of `$OUTPOST_CONFIGURE_ARGS` gets appended to every
    # `./configure` invocation.  Here, we add flags specifying the locations of
    # certain dependencies.
    OUTPOST_CONFIGURE_ARGS =
        "--rust-extra-libdir=${rustDeps} " +
        "--rust-home=${rustSrc} " +
        "--bitflags-home=${bitflagsSrc} " +
        "--log-home=${logSrc} " +
        #"--compiler-rt-wasm-lib=${llvmWasm.compiler-rt-wasm}/lib/linux/libclang_rt.builtins-wasm32.a " +
        (if elfLoader == null then "" else
            "--nix-patch-elf-loader=" + elfLoader + " ");

    # By default, Nix's gcc and clang wrappers add a bunch of hardening flags
    # to every compilation.  We don't want that behavior.
    hardeningDisable = [ "all" ];
}
