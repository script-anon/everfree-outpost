var crypto_wasm = window.OUTPOST_CRYPTO_WASM;

const EVENT_NONE = 0;
const EVENT_RECV = 1;
const EVENT_OUTGOING = 2;
const EVENT_HANDSHAKE_FINISHED = 3;
const EVENT_ERROR = 4;
const EVENT_FATAL_ERROR = 5;

const CHANNEL_BINDING_TOKEN_LEN = 32;

function OutpostEncryptedSocket(url, protocols) {
    this.inner = null;

    this.wasm = null;
    this.mem = null;
    // Pointer to the OccProtocol object.
    this.proto = null;
    // Pointer to a temporary buffer used for functions with multiple return
    // values (out-pointers).
    this.ret_ptr = null;
    // The Uint32Array located at `ret_ptr`.
    this.ret_arr = null;

    // The channel binding token for this connection.  Also serves to indicate
    // whether the handshake has finished - it remains `null` until then.
    this.cb_token = null;

    this.onopen = null;     // onopen()
    this.onmessage = null;  // onmessage(Uint8Array)
    this.onerror = null;    // onerror(exception)
    this.onclose = null;    // onclose()

    this._instantiateWasm().then(() => {
        this.inner = new WebSocket(url, protocols);
        this.inner.binaryType = 'arraybuffer';
        this.inner.onopen = (e) => this._onopen(e);
        this.inner.onmessage = (e) => this._onmessage(e);
        this.inner.onerror = (e) => this._onerror(e);
        this.inner.onclose = (e) => this._onclose(e);
    }).catch((e) => {
        this._die(e);
    });
}

window.OutpostEncryptedSocket = OutpostEncryptedSocket;


OutpostEncryptedSocket.prototype._buffer = function(ptr, len) {
    return new Uint8Array(this.mem.buffer, ptr, len);
};


OutpostEncryptedSocket.prototype._instantiateWasm = function() {
    let imports = {
        'env': {
            'randombytes': (ptr, len) => {
                window.crypto.getRandomValues(this._buffer(ptr, len));
            },

            'sodium_misuse': () => {
                throw 'crypto primitive misuse';
            },

            'abort': () => {
                throw 'abort';
            },

            'rust_wasm_syscall': () => {
                return 0;
            },
        },
    };

    return WebAssembly.instantiate(crypto_wasm, imports).then((inst) => {
        this.wasm = inst.exports;
        this.mem = inst.exports['memory'];

        // TODO: figure out how to make LLVM allocate less stack (currently it
        // allocates about 1MB)

        this.proto = this.wasm['occ_protocol_new'](1);

        this.ret_ptr = this.wasm['buffer_new'](16);
        this.ret_arr = new Uint32Array(this.mem.buffer, this.ret_ptr, 4);
    });
};

OutpostEncryptedSocket.prototype._onopen = function(e) {
    try {
        this.wasm['occ_protocol_open'](this.proto);
        this._processEvents();
    } catch (e) {
        this._die(e);
    }
};

OutpostEncryptedSocket.prototype._onmessage = function(e) {
    try {
        let msg = new Uint8Array(e.data);
        let msg_len = msg.length;
        let msg_ptr = this.wasm['buffer_new'](msg_len);
        this._buffer(msg_ptr, msg_len).set(msg);

        this.wasm['occ_protocol_incoming'](this.proto, msg_ptr, msg_len);
        this.wasm['buffer_delete'](msg_ptr, msg_len);
        this._processEvents();
    } catch (e) {
        this._die(e);
    }
};

OutpostEncryptedSocket.prototype._onerror = function(e) {
    this._die(e.error);
};

OutpostEncryptedSocket.prototype._onclose = function(e) {
    if (this.cb_token == null) {
        // Closed while still in the handshake
        this._die('server disconnected during handshake');
        return;
    }

    if (this.onclose != null) {
        this.onclose({});
    }
};

OutpostEncryptedSocket.prototype.send = function(msg) {
    try {
        let msg_len = msg.length;
        let msg_ptr = this.wasm['buffer_new'](msg_len);
        this._buffer(msg_ptr, msg_len).set(msg);

        this.wasm['occ_protocol_send'](this.proto, msg_ptr, msg_len);
        this.wasm['buffer_delete'](msg_ptr, msg_len);
        this._processEvents();
    } catch (e) {
        this._die(e);
    }
};


OutpostEncryptedSocket.prototype._processEvents = function() {
    let kind = 0;
    while ((kind = this.wasm['occ_protocol_next_event'](this.proto)) != EVENT_NONE) {
        switch (kind) {
            case EVENT_RECV: {
                let msg_ptr = this.wasm['occ_protocol_recv_data'](this.proto, this.ret_ptr);
                let msg_len = this.ret_arr[0];
                let msg = this._buffer(msg_ptr, msg_len);
                if (this.onmessage) {
                    this.onmessage({data: msg.slice().buffer});
                }
                break;
            }

            case EVENT_OUTGOING: {
                let msg_ptr = this.wasm['occ_protocol_outgoing_data'](this.proto, this.ret_ptr);
                let msg_len = this.ret_arr[0];
                let msg = this._buffer(msg_ptr, msg_len);

                this.inner.send(msg);
                break;
            }

            case EVENT_HANDSHAKE_FINISHED: {
                let cb_token_ptr = this.wasm[
                    'occ_protocol_handshake_finished_channel_binding_token'](this.proto);
                let cb_token = this._buffer(cb_token_ptr, CHANNEL_BINDING_TOKEN_LEN);
                this.cb_token = cb_token.slice();

                if (this.onopen) {
                    this.onopen({});
                }
                break;
            }

            case EVENT_ERROR:
            case EVENT_FATAL_ERROR:
            {
                let msg_ptr = this.wasm['occ_protocol_error_message'](this.proto, this.ret_ptr);
                let msg_len = this.ret_arr[0];
                let msg = this._buffer(msg_ptr, msg_len);

                let s = '';
                for (let b of msg) {
                    s += String.fromCharCode(b);
                }
                this._die(s);
                break;
            }

            default:
                throw 'unknown event kind ' + kind;
        }
    }
};


OutpostEncryptedSocket.prototype._die = function(exc) {
    console.log('encsocket dies:', exc);

    if (this.onerror != null) {
        this.onerror({error: exc});
    }

    if (this.inner != null) {
        this.inner.close();
        this.inner = null;
    }

    this.wasm = null;
    this.mem = null;
};
