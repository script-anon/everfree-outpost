use server_types::*;
use std::collections::HashMap;
use std::ops::{Index, IndexMut};
use common::util::StrResult;

use super::id_map::{self, IdMap};

pub use super::id_map::Reserved;



#[derive(Clone)]
pub struct StableIdMap<K, V> {
    map: IdMap<K, V>,
    stable_ids: HashMap<StableId, K>,
    next_id: StableId,
}

pub trait IntrusiveStableId {
    fn get_stable_id(&self) -> StableId;
    fn set_stable_id(&mut self, StableId);
}

macro_rules! impl_IntrusiveStableId {
    ($ty:ty, $field:ident) => {
        impl $crate::maps::IntrusiveStableId for $ty {
            fn get_stable_id(&self) -> ::server_types::StableId {
                self.$field
            }

            fn set_stable_id(&mut self, stable_id: ::server_types::StableId) {
                self.$field = stable_id;
            }
        }
    };
}

impl<K: ObjectId, V: IntrusiveStableId> StableIdMap<K, V> {
    pub fn new() -> StableIdMap<K, V> {
        StableIdMap::new_starting_from(1)
    }

    pub fn new_starting_from(next_id: StableId) -> StableIdMap<K, V> {
        // Can't start from zero, because that's reserved (NO_STABLE_ID)
        assert!(next_id > 0);
        StableIdMap {
            map: IdMap::new(),
            stable_ids: HashMap::new(),
            next_id: next_id,
        }
    }

    pub fn len(&self) -> usize {
        self.map.len()
    }

    pub fn next_stable_id(&self) -> StableId {
        self.next_id
    }

    pub fn set_next_stable_id(&mut self, next_id: StableId) {
        assert!(next_id > 0);
        self.next_id = next_id;
    }

    pub fn next_id(&self) -> K {
        self.map.next_id()
    }

    pub fn next_n_ids(&mut self, n: usize) -> Vec<K> {
        self.map.next_n_ids(n)
    }

    pub fn insert_as(&mut self, transient_id: K, v: V) -> StrResult<()> {
        let stable_id = v.get_stable_id();
        if stable_id != NO_STABLE_ID && self.stable_ids.contains_key(&stable_id) {
            fail!("stable_id conflict");
        }

        let ok = self.map.insert_as(transient_id, v);
        if !ok {
            fail!("id conflict");
        }

        if stable_id != NO_STABLE_ID {
            self.stable_ids.insert(stable_id, transient_id);
        }
        Ok(())
    }

    /// Try to insert an object into the map, returning its newly-assigned transient object ID.
    /// If the object has a stable ID that collides with a stable ID already in the map, the object
    /// is not inserted and the method returns `None`.
    pub fn insert(&mut self, v: V) -> Option<K> {
        let transient_id = self.next_id();
        self.insert_as(transient_id, v).ok()?;
        Some(transient_id)
    }

    pub fn remove(&mut self, transient_id: K) -> Option<V> {
        let opt_val = self.map.remove(transient_id);

        if let Some(ref val) = opt_val {
            let stable_id = val.get_stable_id();
            if stable_id != NO_STABLE_ID {
                self.stable_ids.remove(&stable_id);
            }
        }

        opt_val
    }

    pub fn reserve_ids(&mut self, n: usize) -> Vec<Reserved<K>> {
        self.map.reserve_ids(n)
    }

    /// TODO: currently requires reserved keys to be passed to `insert_reserved` in the same order
    /// they were returned by `reserve_ids`
    pub fn insert_reserved(&mut self, _k: Reserved<K>, v: V) -> K {
        let stable_id = v.get_stable_id();
        assert!(stable_id == NO_STABLE_ID || !self.stable_ids.contains_key(&stable_id),
                "stable ID is already present");

        let transient_id = self.map.insert(v);

        if stable_id != NO_STABLE_ID {
            self.stable_ids.insert(stable_id, transient_id);
        }

        transient_id
    }

    pub fn set_stable_id(&mut self, transient_id: K, stable_id: StableId) -> StrResult<()> {
        let v = match self.map.get_mut(transient_id) {
            Some(x) => x,
            None => fail!("transient_id is not present in the map"),
        };
        if v.get_stable_id() != NO_STABLE_ID {
            fail!("value already has a stable_id");
        }
        if self.stable_ids.contains_key(&stable_id) {
            fail!("stable_id is already in use");
        }

        if stable_id != NO_STABLE_ID {
            v.set_stable_id(stable_id);
            self.stable_ids.insert(stable_id, transient_id);
        }
        Ok(())
    }

    pub fn pin(&mut self, transient_id: K) -> Stable<K> {
        self.try_pin(transient_id).expect("no entry found for key")
    }

    pub fn try_pin(&mut self, transient_id: K) -> Option<Stable<K>> {
        let val = unwrap_or!(self.map.get_mut(transient_id), return None);

        if val.get_stable_id() != NO_STABLE_ID {
            return Some(Stable::new(val.get_stable_id()));
        }

        let stable_id = self.next_id;
        self.next_id += 1;

        val.set_stable_id(stable_id);
        self.stable_ids.insert(stable_id, transient_id);

        Some(Stable::new(stable_id))
    }

    pub fn unpin(&mut self, transient_id: K) {
        let val = &mut self.map[transient_id];

        let stable_id = val.get_stable_id();
        if stable_id == NO_STABLE_ID {
            return;
        }

        val.set_stable_id(NO_STABLE_ID);
        self.stable_ids.remove(&stable_id);
    }

    pub fn contains(&self, transient_id: K) -> bool {
        self.map.contains(transient_id)
    }

    pub fn contains_stable(&self, stable_id: Stable<K>) -> bool {
        let stable_id = stable_id.val;
        self.stable_ids.contains_key(&stable_id)
    }

    pub fn get_id(&self, stable_id: Stable<K>) -> Option<K> {
        let stable_id = stable_id.val;
        self.stable_ids.get(&stable_id).map(|&x| x)
    }

    pub fn get(&self, transient_id: K) -> Option<&V> {
        self.map.get(transient_id)
    }

    pub fn get_mut(&mut self, transient_id: K) -> Option<&mut V> {
        self.map.get_mut(transient_id)
    }

    pub fn get_stable(&self, stable_id: Stable<K>) -> Option<&V> {
        self.get_id(stable_id).and_then(|k| self.get(k))
    }

    pub fn get_stable_mut(&mut self, stable_id: Stable<K>) -> Option<&mut V> {
        let k = unwrap_or!(self.get_id(stable_id), return None);
        self.get_mut(k)
    }

    /// Free up the transient IDs of recently removed items so they can be reused.
    pub fn recycle_ids(&mut self) {
        self.map.recycle_ids();
    }

    pub fn iter(&self) -> Iter<K, V> {
        self.map.iter()
    }
}

impl<K, V> Index<K> for StableIdMap<K, V>
        where K: ObjectId,
              V: IntrusiveStableId {
    type Output = V;

    fn index(&self, index: K) -> &V {
        self.get(index).expect("no entry found for key")
    }
}

impl<K, V> IndexMut<K> for StableIdMap<K, V>
        where K: ObjectId,
              V: IntrusiveStableId {
    fn index_mut(&mut self, index: K) -> &mut V {
        self.get_mut(index).expect("no entry found for key")
    }
}

pub type Iter<'a, K, V> = id_map::Iter<'a, K, V>;
