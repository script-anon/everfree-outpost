use server_types::*;
use std::rc::Rc;
use common::movement::InputBits;

use objects::*;


pub trait Ext {
    type Update: Update;
    type Components: Components<Self::Update>;
}

#[allow(unused_variables)]
pub trait Components<U> {
    fn destroy_client(&mut self, id: ClientId, update: &U) {}
    fn destroy_entity(&mut self, id: EntityId, update: &U) {}
    fn destroy_inventory(&mut self, id: InventoryId, update: &U) {}
    fn destroy_plane(&mut self, id: PlaneId, update: &U) {}
    fn destroy_terrain_chunk(&mut self, id: TerrainChunkId, update: &U) {}
    fn destroy_structure(&mut self, id: StructureId, update: &U) {}
}

pub trait Update {
    // Presence of `old` arguments is basically dictated by the needs of clients.  It's easy to add
    // them to additional methods if needed.
    for_each_obj! {
        fn $create_obj(&self, id: $ObjId);
        fn $destroy_obj(&self, id: $ObjId, old: $Obj);
        fn $obj_flags(&self, id: $ObjId, old: $ObjFlags);
        fn $obj_stable_id(&self, id: $ObjId, old: StableId);

        // Called when `world.$objs.next_id` changes.
        fn $next_obj_stable_id(&self, old: StableId);
    }

    fn client_name(&self, id: ClientId, old: &String);
    fn client_pawn(&self, id: ClientId, old: Option<EntityId>);
    fn client_current_input(&self, id: ClientId, old: InputBits);

    fn entity_activity(&self, id: EntityId, act: &Activity, start: Time);
    fn entity_plane(&self, id: EntityId, old: PlaneId);
    fn entity_appearance(&self, id: EntityId, old: &Appearance);
    fn entity_attachment(&self, id: EntityId, old: EntityAttachment);

    fn inventory_contents(&self, id: InventoryId, old: &[Item]);
    fn inventory_attachment(&self, id: InventoryId, old: InventoryAttachment);

    fn terrain_chunk_blocks(&self, id: TerrainChunkId, old: &BlockChunk);

    fn structure_template(&self, id: StructureId, old: TemplateId);
    fn structure_attachment(&self, id: StructureId, old: StructureAttachment);
}



macro_rules! dispatch {
    ($(
        fn $name:ident (&self $(, $arg:ident : $ArgTy:ty)*);
        )*) => {
        $(
            fn $name(&self, $($arg: $ArgTy,)*) {
                (**self).$name($($arg,)*)
            }
        )*
    };
}

impl<T: Update> Update for Rc<T> {
    for_each_obj! {
        dispatch! {
            fn $create_obj(&self, id: $ObjId);
            fn $destroy_obj(&self, id: $ObjId, old: $Obj);
            fn $obj_flags(&self, id: $ObjId, old: $ObjFlags);
            fn $obj_stable_id(&self, id: $ObjId, old: StableId);
            fn $next_obj_stable_id(&self, old: StableId);
        }
    }

    dispatch! {
        fn client_name(&self, id: ClientId, old: &String);
        fn client_pawn(&self, id: ClientId, old: Option<EntityId>);
        fn client_current_input(&self, id: ClientId, old: InputBits);

        fn entity_activity(&self, id: EntityId, act: &Activity, start: Time);
        fn entity_plane(&self, id: EntityId, old: PlaneId);
        fn entity_appearance(&self, id: EntityId, old: &Appearance);
        fn entity_attachment(&self, id: EntityId, old: EntityAttachment);

        fn inventory_contents(&self, id: InventoryId, old: &[Item]);
        fn inventory_attachment(&self, id: InventoryId, old: InventoryAttachment);

        fn terrain_chunk_blocks(&self, id: TerrainChunkId, old: &BlockChunk);

        fn structure_template(&self, id: StructureId, old: TemplateId);
        fn structure_attachment(&self, id: StructureId, old: StructureAttachment);
    }
}
