use server_types::*;
use std::collections::hash_set;
use common::movement::InputBits;

use Ext;
use ext::Update;
use objects::*;
use object_ref::{ObjectRef, ObjectMut};
use world::{EntitiesById, InventoriesById};


simple_getters! {
    object_type = Client;
    obj_ident = obj;

    fn uid: u32 = obj.stable_id as u32;
    fn name: &str = &obj.name;
    fn pawn_id: Option<EntityId> = obj.pawn;
    fn current_input: InputBits = obj.current_input;
}

// Setters and modifiers

impl<'a, E: Ext> ObjectMut<'a, E, Client> {
    pub fn set_name(&mut self, val: String) {
        self.world.update.client_name(self.id, &self.obj().name);
        self.obj_mut().name = val;
    }

    pub fn set_current_input(&mut self, val: InputBits) {
        self.world.update.client_current_input(self.id, self.obj().current_input);
        self.obj_mut().current_input = val;
    }

    pub fn modify_current_input<F: FnOnce(&mut InputBits) -> R, R>(&mut self, f: F) -> R {
        self.world.update.client_current_input(self.id, self.obj().current_input);
        f(&mut self.obj_mut().current_input)
    }
}

// Non-mut methods.
//
// Would be nice to have a macro to generate both copies, but that's probably infeasible since they
// have slightly different lifetime requirements.

impl<'a, E: Ext> ObjectRef<'a, E, Client> {
    pub fn pawn(&self) -> Option<ObjectRef<'a, E, Entity>> {
        self.pawn_id().and_then(|eid| self.world().get_entity(eid))
    }

    pub fn child_entities(&self) -> EntitiesById<'a, E, hash_set::Iter<'a, EntityId>> {
        EntitiesById::new(self.world(), self.obj().child_entities.iter())
    }

    pub fn child_inventories(&self) -> InventoriesById<'a, E, hash_set::Iter<'a, InventoryId>> {
        InventoriesById::new(self.world(), self.obj().child_inventories.iter())
    }
}

impl<'a, E: Ext> ObjectMut<'a, E, Client> {
    pub fn pawn(&self) -> Option<ObjectRef<E, Entity>> {
        self.pawn_id().and_then(|eid| self.world().get_entity(eid))
    }

    pub fn child_entities(&self) -> EntitiesById<E, hash_set::Iter<EntityId>> {
        EntitiesById::new(self.world(), self.obj().child_entities.iter())
    }

    pub fn child_inventories(&self) -> InventoriesById<E, hash_set::Iter<InventoryId>> {
        InventoriesById::new(self.world(), self.obj().child_inventories.iter())
    }
}

// Mut methods.

impl<'a, E: Ext> ObjectMut<'a, E, Client> {
    pub fn pawn_mut(&mut self) -> Option<ObjectMut<E, Entity>> {
        let eid = unwrap_or!(self.pawn_id(), return None);
        self.world_mut().get_entity_mut(eid)
    }

    /// Sets an entity as the pawn of this client.  The entity must exist and must already be a
    /// child of this client (`entity.attachment == Client(self.id)`.
    pub fn set_pawn(&mut self, eid: EntityId) {
        assert!(self.obj().child_entities.contains(&eid),
                "set_pawn: entity is not a child of the client");
        self.world.update.client_pawn(self.id, self.obj().pawn);
        self.obj_mut().pawn = Some(eid);
    }

    pub fn clear_pawn(&mut self) {
        self.world.update.client_pawn(self.id, self.obj().pawn);
        self.obj_mut().pawn = None;
    }
}
