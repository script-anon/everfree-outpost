use server_types::*;
use std::collections::HashMap;
use std::vec;

use World;
use ext::Ext;


expand_objs! {
    /// Helper struct for assigning IDs to imported objects.  When importing objects from a remote
    /// source that uses different (transient) IDs, such as a save file, use an `IdRemapper` to
    /// assign a fresh local ID to each remote object.
    ///
    /// Recommended usage:
    ///
    ///  1. Pass IDs of all newly imported objects to `add_ids`.  (Don't pass IDs for objects that
    ///     already exist but are to be modified by the import.)
    ///  2. For each newly imported object, `w.try_create_obj_at(mapper.map(old_id), obj)`.
    ///
    /// This procedure guarantees that there will be no (transient) ID conflicts in the creation
    /// step.  Adding other `w.create_obj` calls between (1) and (2) invalidates the guarantee.
    /// Also, if any objects are omitted during step (2), their corresponding IDs should be removed
    /// from the `IdRemapper` to avoid future conflicts.
    pub struct IdRemapper {
        $( $obj_ids: HashMap<$ObjId, $ObjId>, )*
    }

    impl IdRemapper {
        pub fn new() -> IdRemapper {
            IdRemapper {
                $( $obj_ids: HashMap::new(), )*
            }
        }

        pub fn assign_ids<E, F, I>(&mut self, w: &mut World<E>, mut ids: F)
                where E: Ext,
                      F: FnMut() -> I,
                      I: IntoIterator<Item=AnyId> {
            // Count number of fresh IDs needed for each object type.
            struct FreshCount { $( $obj: usize, )* };
            let mut fresh_count = FreshCount { $( $obj: 0, )* };

            for id in ids() {
                match id {
                    $( AnyId::$Obj(_) => {
                        fresh_count.$obj += 1;
                    }, )*
                }
            }

            // Actually obtain the IDs and add them to the map.
            struct FreshIds { $( $obj: vec::IntoIter<$ObjId>, )* };
            let mut fresh_ids = FreshIds {
                $( $obj: w.$objs.next_n_ids(fresh_count.$obj).into_iter(), )*
            };
            $( self.$obj_ids.reserve(fresh_count.$obj); )*

            for id in ids() {
                match id {
                    $( AnyId::$Obj(id) => {
                        if self.$obj_ids.contains_key(&id) {
                            error!("{:?} reused before old object was destroyed", id);
                            // Leave old mapping in place.  Caller will probably hit an error
                            // trying to insert, but at least we won't lose track of the old
                            // object.  (This should never happen anyway.)
                            continue;
                        }
                        self.$obj_ids.insert(id, fresh_ids.$obj.next().unwrap());
                    }, )*
                }
            }

            $( assert!(fresh_ids.$obj.next().is_none()); )*
        }

        pub fn remove_ids<I>(&mut self, ids: I)
                where I: IntoIterator<Item=AnyId> {
            for id in ids {
                match id {
                    $( AnyId::$Obj(id) => {
                        let ok = self.$obj_ids.remove(&id).is_some();
                        if !ok {
                            error!("tried to remove nonexistent mapping for {:?}", id);
                        }
                    }, )*
                }
            }
        }

        pub fn insert(&mut self, old_id: AnyId, new_id: AnyId) {
            match (old_id, new_id) {
                $( (AnyId::$Obj(old_id), AnyId::$Obj(new_id)) =>
                    self.$insert_obj(old_id, new_id), )*
                _ => panic!("mismatched ID types"),
            }
        }

        pub fn remove(&mut self, old_id: AnyId) -> bool {
            match old_id {
                $( AnyId::$Obj(old_id) => self.$remove_obj(old_id), )*
            }
        }

        pub fn contains(&self, id: AnyId) -> bool {
            match id {
                $( AnyId::$Obj(id) => self.$obj_ids.contains_key(&id), )*
            }
        }

        pub fn map(&self, id: AnyId) -> Option<AnyId> {
            match id {
                $( AnyId::$Obj(id) => self.$obj_ids.get(&id).map(|&x| AnyId::$Obj(x)), )*
            }
        }

        $(
            pub fn $insert_obj(&mut self, old_id: $ObjId, new_id: $ObjId) {
                if self.$obj_ids.contains_key(&old_id) {
                    error!("{:?} reused before old object was destroyed", old_id);
                    return;
                }
                self.$obj_ids.insert(old_id, new_id);
            }

            pub fn $remove_obj(&mut self, old_id: $ObjId) -> bool {
                self.$obj_ids.remove(&old_id).is_some()
            }

            pub fn $contains_obj(&self, id: $ObjId) -> bool {
                self.$obj_ids.contains_key(&id)
            }

            pub fn $map_obj(&self, id: $ObjId) -> Option<$ObjId> {
                self.$obj_ids.get(&id).map(|&x| x)
            }
        )*
    }
}
