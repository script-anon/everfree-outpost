#[macro_export]
macro_rules! define_obj_list_world {
    () => {
        ::syntax_exts::define_expand_objs_list! {
            world_objs = {
                client, clients;
                entity, entities;
                inventory, inventories;
                plane, planes;
                terrain_chunk, terrain_chunks;
                structure, structures;
            }
        }
    };
}

define_obj_list_world!();

#[macro_export]
macro_rules! expand_objs {
    ($($args:tt)*) => { ::syntax_exts::expand_objs_named! { [[world_objs]] $($args)* } };
}

#[macro_export]
macro_rules! for_each_obj {
    ($($args:tt)*) => { ::syntax_exts::for_each_obj_named! { [[world_objs]] $($args)* } };
}
