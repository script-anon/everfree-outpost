#![crate_name = "world"]

extern crate common;
#[macro_use] extern crate log;
extern crate physics;
extern crate server_bundle;
extern crate server_config;
extern crate server_extra;
extern crate server_types;
extern crate server_world_types;
extern crate syntax_exts;


use server_types::*;
use server_extra::Extra;

#[macro_use] mod macros;
#[macro_use] pub mod maps;

pub mod ext;
pub mod import;
pub mod objects;
pub mod object_ref;
pub mod world;


use maps::StableIdMap;
pub use ext::Ext;


#[derive(Clone)]
#[repr(C)]
pub struct World<E: Ext> {
    extra: Extra,

    clients: StableIdMap<ClientId, objects::Client>,
    entities: StableIdMap<EntityId, objects::Entity>,
    inventories: StableIdMap<InventoryId, objects::Inventory>,
    planes: StableIdMap<PlaneId, objects::Plane>,
    terrain_chunks: StableIdMap<TerrainChunkId, objects::TerrainChunk>,
    structures: StableIdMap<StructureId, objects::Structure>,

    pub components: E::Components,
    pub update: E::Update,
}
