#![crate_name = "server_config"]
#![feature(
    thread_local,
)]

#[macro_use] extern crate log;
extern crate rand;
extern crate rustc_serialize;

extern crate common;
extern crate physics;
extern crate server_types;

pub use data::Data;
pub use data_tls::{data, with_tls_data};
pub use storage::Storage;

pub mod data;
pub mod data_tls;
pub mod storage;
