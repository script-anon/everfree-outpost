use std::ptr;

use data::Data;


#[thread_local]
static mut DATA: *const Data = ptr::null();


/// Get access to the thread-local `Data` object.
///
/// ## Safety Warning
///
/// It is unsafe to use the returned reference after the caller itself has returned.  Don't move
/// the reference into any kind of long-lived data structure.  Typically, no data structure should
/// contain references to `Data` or any data item (`Block`, etc.), though it's okay to look up a
/// data item and pass it to another function (assuming that function is also well-behaved).
///
/// It is also unsafe to call this outside the dynamic extent of `with_tls_data()`.  Normally this
/// is not a problem because `with_tls_data()` should be entered soon after program startup.
#[inline(always)]
pub fn data<'a>() -> &'a Data {
    unsafe { &*DATA }
}

pub fn with_tls_data<F: FnOnce() -> R, R>(data: &Data, f: F) -> R {
    unsafe { DATA = data; }
    let r = f();
    unsafe { DATA = ptr::null(); }
    r
}

pub fn has_data() -> bool {
    unsafe { !DATA.is_null() }
}

/// Directly set the current thread-local `Data` pointer.  The caller is responsible for clearing
/// the data pointer before the referenced object goes out of scope.
pub unsafe fn set_data(data: *const Data) {
    DATA = data;
}
