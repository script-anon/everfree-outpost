import builtins
import re
import textwrap


BLOCK_FOR = re.compile(r'^[ \t]*%(for [^%\n]*)$', re.MULTILINE)
BLOCK_IF = re.compile(r'^[ \t]*%(if [^%\n]*)$', re.MULTILINE)
BLOCK_ELIF = re.compile(r'^[ \t]*%(elif [^%\n]*)$', re.MULTILINE)
BLOCK_ELSE = re.compile(r'^[ \t]*%(else)[ \t]*$', re.MULTILINE)
BLOCK_END = re.compile(r'^[ \t]*%(end)[ \t]*$', re.MULTILINE)
BLOCK_BLOCK = re.compile(r'^([ \t]*)%block ([^%\n]*)$', re.MULTILINE)
BLOCK_IMPORT = re.compile(r'^[ \t]*%(import [^%;\n]*)$', re.MULTILINE)
BLOCK_FROM = re.compile(r'^[ \t]*%(from [^%;\n]*)$', re.MULTILINE)
BLOCK_SET = re.compile(r'^[ \t]*%set ([^;\n]*)$', re.MULTILINE)

FOR_PARTS = re.compile(r'for ([a-zA-Z0-9_]*(?: *, *[a-zA-Z0-9_]*)*) in (.*)$')

PLACEHOLDER = re.compile(r'(%([a-zA-Z0-9_]+)\b|%{([^}\n]*)})')

def lines(s):
    i = 0
    while i < len(s):
        end = s.find('\n', i)
        if end == -1:
            end = len(s)
        else:
            end += 1

        yield i, end
        i = end


# Note that this doesn't include `block`.  Inline `%block` directives don't
# make much sense.
PREP_RE = re.compile(r'%(for|if|elif|else|end)\b[^%\n]*%')

class TemplateRender(object):
    def __init__(self, s, kwargs):
        # Turn inline %if/%for into multiline ones
        def repl(m):
            return '%\n' + m.group(0)[:-1] + '\n'
        s = PREP_RE.sub(repl, s)

        self.s = s
        self.args = kwargs

    def render(self):
        out = ''
        depth = 0
        header = None
        header_line = 0
        blocks = []
        for line_num, (start, end) in enumerate(lines(self.s)):
            m = None
            def match(r):
                nonlocal m
                m = r.match(self.s, start)
                return m

            if match(BLOCK_FOR) or match(BLOCK_IF):
                if depth == 0:
                    header = m
                    header_line = line_num
                    blocks = []
                depth += 1
            elif match(BLOCK_ELSE) or match(BLOCK_ELIF):
                if depth == 0:
                    raise ValueError('bad syntax: stray %r on line %d' %
                            (m.group(1), line_num + 1))
                elif depth == 1:
                    blocks.append((header, header_line, header.end() + 1, m.start()))
                    header_line = line_num
                    header = m
            elif match(BLOCK_END):
                if depth == 0:
                    raise ValueError('bad syntax: stray %r on line %d' %
                            (m.group(1), line_num + 1))
                elif depth == 1:
                    blocks.append((header, header_line, header.end() + 1, m.start()))
                    out += self._do_block(blocks)
                    plain_start = m.end() + 1
                depth -= 1
            elif match(BLOCK_BLOCK):
                if depth == 0:
                    ind = m.group(1)
                    expr = m.group(2)
                    text = self._eval(expr)
                    out += textwrap.indent(text, ind)
            elif match(BLOCK_IMPORT) or match(BLOCK_FROM) or match(BLOCK_SET):
                if depth == 0:
                    self._exec(m.group(1))
            else:
                if depth == 0:
                    out += self._do_plain(start, end)

        if depth != 0:
            raise ValueError('bad syntax: unclosed %r on line %d' %
                    (header.group(1), header_line + 1))

        return out

    def _do_block(self, parts):
        h, h_line, start, end = parts[0]
        if h.group(1).startswith('for'):
            if len(parts) != 1:
                raise ValueError('bad syntax: unclosed %%for on line %d' % h_line)
            m = FOR_PARTS.match(h.group(1))
            if not m:
                raise ValueError('bad syntax: invalid %%for on line %d' % h_line)

            var_names = [v.strip() for v in m.group(1).split(',')]
            collection = self._eval(m.group(2))
            dct = self.args.copy()
            out = ''
            for x in collection:
                if len(var_names) == 1:
                    dct[var_names[0]] = x
                else:
                    if len(var_names) != len(x):
                        raise ValueError('line %d: wrong number of values to unpack' % h_line)
                    for name, val in zip(var_names, x):
                        dct[name] = val
                out += TemplateRender(self.s[start:end], dct).render()
            return out
        else:   # `%if`
            for i, (h, h_line, start, end) in enumerate(parts):
                if h.group(1) == 'else' and i != len(parts) - 1:
                    raise ValueError('bad syntax: more cases after %%else on line %d' % h_line)
            for h, h_line, start, end in parts:
                if h.group(1) == 'else':
                    go = True
                else:
                    cond = h.group(1).partition(' ')[2]
                    go = self._eval(cond)
                if go:
                    return TemplateRender(self.s[start:end], self.args).render()
            return ''

    def _do_plain(self, start, end):
        def repl(m):
            try:
                expr = m.group(2) or m.group(3)
                if expr:
                    val = self._eval(expr)
                    if not isinstance(val, str):
                        raise TypeError('expected str, %s found' % (type(val),))
                    return val
                else:
                    # Use '%{}' to produce a literal '%'
                    return '%'
            except Exception as e:
                raise RuntimeError('error evaluating placeholder %s' % m.group()) from e
        line = self.s[start:end]
        if line.endswith('%\n'):
            line = line[:-2]
        return PLACEHOLDER.sub(repl, line)

    def _eval(self, expr):
        return eval(expr, {'__builtins__': builtins}, self.args)

    def _exec(self, expr):
        return exec(expr, {'__builtins__': builtins}, self.args)

def template(s, args_dict=None, **kwargs):
    if args_dict is not None and len(kwargs) > 0:
        args = dict(args_dict, **kwargs)
    else:
        args = args_dict or kwargs

    s = textwrap.dedent(s).strip('\n') + '\n'
    return TemplateRender(s, args).render()
