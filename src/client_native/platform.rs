use std::collections::HashMap;
use std::str::FromStr;
use std::sync::mpsc::Sender;
use time::{self, Timespec};

use client::Time;
use client::platform::{self, ConfigKey, Cursor};
use common::proto::game::Request;


pub struct Platform {
    config: Config,
    to_server: Sender<Request>,
    time_base: Timespec,
}

impl Platform {
    pub fn new(to_server: Sender<Request>) -> Platform {
        Platform {
            config: Config::new(),
            to_server: to_server,
            time_base: time::get_time(),
        }
    }
}

impl platform::Platform for Platform {
    type Config = Config;
    fn config(&self) -> &Config { &self.config }
    fn config_mut(&mut self) -> &mut Config { &mut self.config }

    fn set_cursor(&mut self, _cursor: Cursor) {
        // TODO - NYI
    }

    fn send_message(&mut self, msg: Request) {
        self.to_server.send(msg).unwrap();
    }

    fn get_time(&self) -> Time {
        let delta = time::get_time() - self.time_base;
        let timespec = Timespec::new(0, 0) + delta;
        (timespec.sec as Time * 1000) + (timespec.nsec / 1000000) as Time

    }
}

pub struct Config {
    map: HashMap<ConfigKey, String>,
}

impl Config {
    pub fn new() -> Config {
        Config {
            map: HashMap::new(),
        }
    }
}

impl platform::Config for Config {
    fn get_int(&self, key: ConfigKey) -> i32 {
        self.map.get(&key)
            .and_then(|s| FromStr::from_str(s).ok())
            .unwrap_or(0)
    }

    fn set_int(&mut self, key: ConfigKey, value: i32) {
        self.set_str(key, &value.to_string())
    }

    fn get_str(&self, key: ConfigKey) -> String {
        match self.map.get(&key) {
            Some(x) => x.clone(),
            None => String::new(),
        }
    }

    fn set_str(&mut self, key: ConfigKey, value: &str) {
        self.map.insert(key, value.to_owned());
    }

    fn clear(&mut self, key: ConfigKey) {
        self.map.remove(&key);
    }
}
