#!/bin/sh

cd "$(dirname "$(dirname "$0")")"

export RUST_BACKTRACE=1
[ -z "$RUST_LOG" ] && export RUST_LOG=info
python3 bin/wrapper.py
