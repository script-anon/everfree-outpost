use std::borrow::ToOwned;
use std::io;
use std::mem;
use std::ops::{Deref, DerefMut};
use std::slice;
use std::vec::Vec;

use common_types::*;


/// Newtype wrapper around `Vec<u8>`.  The purpose is to distinguish byte arrays from sequences of
/// numbers (that happen to be represented as u8) during un/marshalling.
#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub struct Bytes(pub Vec<u8>);

macro_rules! bytes_wrapper_fn {
    ($name:ident, $args:tt) => {
        bytes_wrapper_fn!($name, $args -> ());
    };
    ($name:ident, (&self $(, $arg:ident : $ArgTy:ty)*) -> $RetTy:ty) => {
        pub fn $name(&self, $( $arg: $ArgTy, )*) -> $RetTy {
            (self.0).$name($($arg,)*)
        }
    };
    ($name:ident, (&mut self $(, $arg:ident : $ArgTy:ty)*) -> $RetTy:ty) => {
        pub fn $name(&mut self, $( $arg: $ArgTy, )*) -> $RetTy {
            (self.0).$name($($arg,)*)
        }
    };
    ($name:ident, (self $(, $arg:ident : $ArgTy:ty)*) -> $RetTy:ty) => {
        pub fn $name(self, $( $arg: $ArgTy, )*) -> $RetTy {
            (self.0).$name($($arg,)*)
        }
    };
}

macro_rules! bytes_wrappers {
    ($(
        fn $name:ident $args:tt $(-> $RetTy:ty)*;
    )*) => {
        impl Bytes {
            $(
                bytes_wrapper_fn!($name, $args $(-> $RetTy)*);
            )*
        }
    };
}

impl Bytes {
    pub fn new() -> Bytes {
        Bytes(Vec::new())
    }

    pub fn with_capacity(cap: usize) -> Bytes {
        Bytes(Vec::with_capacity(cap))
    }

    pub fn from_slice(b: &[u8]) -> Bytes {
        Bytes(b.to_owned())
    }
}

// We provide wrapper methods instead of a Deref impl because we don't want to auto-convert Bytes
// to Vec<u8>.
bytes_wrappers! {
    fn len(&self) -> usize;
    fn is_empty(&self) -> bool;
    fn push(&mut self, val: u8);
    fn pop(&mut self) -> Option<u8>;
    fn clear(&mut self);

    fn extend_from_slice(&mut self, b: &[u8]);
}

impl Deref for Bytes {
    type Target = [u8];
    fn deref(&self) -> &[u8] {
        &self.0 as &[u8]
    }
}

impl DerefMut for Bytes {
    fn deref_mut(&mut self) -> &mut [u8] {
        &mut self.0 as &mut [u8]
    }
}


/// Marker trait for types that can be safely manipulated as a string of bytes.  This means a type
/// `T` can implement `ByteCast` only if it has no safety-critical invariants, so that any sequence of
/// bytes of appropriate length can safely be interpreted as a `T`.  For example, the following
/// types may implement bytes:
///
///  - All primitive integer and floating point types implement `ByteCast`.  Any sequence of 4 bytes
///    is a valid `u32`, for example.
///  - A tuple or struct containing only `ByteCast` elements can implement `ByteCast`.
///
/// And these cannot:
///
///  - Safe pointers (`&T`, `Box<T>`) do not implement `ByteCast` because they maintain the invariant
///    that the pointer is non-null (among others).
///  - `bool` does not implement `ByteCast` because only 0 and 1 are legal values.
///  - No `enum` can implement `ByteCast`, because the discriminant is restricted to a particular
///    range of values.
///  - Types with destructors cannot implement `ByteCast` since they may have a drop flag, whose
///    invariants are unspecified (but probably it must not be zero).
pub unsafe trait ByteCast: Copy { }

macro_rules! bytecast_primitive_impls {
    ($($ty:ty),*) => {
        $( unsafe impl ByteCast for $ty { } )*
    }
}

bytecast_primitive_impls!(
    u8, u16, u32, u64, usize,
    i8, i16, i32, i64, isize,
    f32, f64,
    ()
    // NB: `bool` is not `ByteCast` because it's 8 bits wide but only `0` and `1` are valid.
);

macro_rules! bytecast_tuple_impl {
    () => {};
    ($($name:ident)*) => {
        unsafe impl<$($name: ByteCast),*> ByteCast for ($($name,)*) { }
    };
}
bytecast_tuple_impl!(A);
bytecast_tuple_impl!(A B);
bytecast_tuple_impl!(A B C);
bytecast_tuple_impl!(A B C D);
bytecast_tuple_impl!(A B C D E);
bytecast_tuple_impl!(A B C D E F);
bytecast_tuple_impl!(A B C D E F G);
bytecast_tuple_impl!(A B C D E F G H);
bytecast_tuple_impl!(A B C D E F G H I);
bytecast_tuple_impl!(A B C D E F G H I J);
bytecast_tuple_impl!(A B C D E F G H I J K);
bytecast_tuple_impl!(A B C D E F G H I J K L);

macro_rules! bytecast_array_impls {
    ($($n:expr),*) => {
        $(
            unsafe impl<T: ByteCast> ByteCast for [T; $n] { }
        )*
    };
}
bytecast_array_impls!(
    0, 1, 2, 3, 4, 5, 6, 7,
    8, 9, 10, 11, 12, 13, 14, 15
    );

unsafe impl ByteCast for V2 {}
unsafe impl ByteCast for V3 {}
unsafe impl<V: ByteCast> ByteCast for Region<V> {}

unsafe impl ByteCast for ClientId {}
unsafe impl ByteCast for EntityId {}
unsafe impl ByteCast for InventoryId {}
unsafe impl ByteCast for PlaneId {}
unsafe impl ByteCast for TerrainChunkId {}
unsafe impl ByteCast for StructureId {}


pub trait WriteBytes {
    unsafe fn write_as_bytes<T: Copy>(&mut self, x: T) -> io::Result<()>;
    fn write_bytes<T: ByteCast>(&mut self, x: T) -> io::Result<()>;
    fn write_bytes_slice<T: ByteCast>(&mut self, x: &[T]) -> io::Result<()>;
}

impl<W: io::Write> WriteBytes for W {
    unsafe fn write_as_bytes<T: Copy>(&mut self, x: T) -> io::Result<()> {
        let slice = slice::from_raw_parts(&x as *const T as *const u8,
                                          mem::size_of::<T>());
        self.write_all(slice)
    }

    fn write_bytes<T: ByteCast>(&mut self, x: T) -> io::Result<()> {
        unsafe { self.write_as_bytes(x) }
    }

    fn write_bytes_slice<T: ByteCast>(&mut self, x: &[T]) -> io::Result<()> {
        unsafe {
            let slice = slice::from_raw_parts(x.as_ptr() as *const u8,
                                              x.len() * mem::size_of::<T>());
            self.write_all(slice)
        }
    }
}

pub trait ReadBytes {
    unsafe fn read_as_bytes<T: Copy>(&mut self) -> io::Result<T>;
    fn read_bytes<T: ByteCast>(&mut self) -> io::Result<T>;
    fn read_bytes_slice<T: ByteCast>(&mut self, x: &mut [T]) -> io::Result<()>;
}

impl<R: io::Read> ReadBytes for R {
    unsafe fn read_as_bytes<T: Copy>(&mut self) -> io::Result<T> {
        let mut x = mem::zeroed();
        let slice = slice::from_raw_parts_mut(&mut x as *mut T as *mut u8,
                                              mem::size_of::<T>());
        try!(self.read_exact(slice));
        Ok(x)
    }

    fn read_bytes<T: ByteCast>(&mut self) -> io::Result<T> {
        unsafe { self.read_as_bytes() }
    }

    fn read_bytes_slice<T: ByteCast>(&mut self, x: &mut [T]) -> io::Result<()> {
        unsafe {
            let slice = slice::from_raw_parts_mut(x.as_mut_ptr() as *mut u8,
                                                  x.len() * mem::size_of::<T>());
            try!(self.read_exact(slice));
        }
        Ok(())
    }
}
