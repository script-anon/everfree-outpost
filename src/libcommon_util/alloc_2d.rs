use std::prelude::v1::*;
use std::cmp;
use std::iter;
use common_types::{V2, Region, scalar};


/// 2D spatial allocator.  Tracks allocated subregions of a fixed-size space.
///
/// The current implementation only supports allocations of size up to 16x16, though the overall
/// space can have any size.
pub struct Alloc2D {
    size: V2,
    in_use: Box<[u32]>,
}

impl Alloc2D {
    pub fn new(size: V2) -> Alloc2D {
        assert!(size.x >= 0 && size.y >= 0);
        let w = size.x as usize;
        let h = size.y as usize;

        let line_words = (w + 31) / 32;
        let words = h * line_words;
        let mut in_use = iter::repeat(0).take(words).collect::<Vec<_>>().into_boxed_slice();

        // Mark the padding bits
        let padding_bits = 32 * line_words - w;
        if padding_bits > 0 {
            let padding_mask = ((1 << padding_bits) - 1) << (32 - padding_bits);
            for y in 0 .. h {
                let x = line_words - 1;
                in_use[y * line_words + x] = padding_mask;
            }
        }

        Alloc2D {
            size: size,
            in_use: in_use,
        }
    }

    pub fn size(&self) -> V2 {
        self.size
    }

    fn line(&self, y: usize) -> &[u32] {
        let w = (self.size.x as usize + 31) / 32;
        if y * w >= self.in_use.len() {
            return &[];
        }
        &self.in_use[y * w .. (y + 1) * w]
    }

    fn line_mut(&mut self, y: usize) -> &mut [u32] {
        let w = (self.size.x as usize + 31) / 32;
        if y * w >= self.in_use.len() {
            return &mut [];
        }
        &mut self.in_use[y * w .. (y + 1) * w]
    }

    fn extract(&self, x0: usize, x1: usize, y: usize) -> u32 {
        extract(self.line(y), x0, x1)
    }

    fn find_alloc(&self, size: V2) -> Option<V2> {
        if size.x < 0 || size.y < 0 {
            return None;
        }
        let w = self.size.x as usize;
        let h = self.size.y as usize;
        let aw = size.x as usize;
        let ah = size.y as usize;

        if aw == 0 || ah == 0 {
            return Some(scalar(0));
        }
        // Note that `ah` doesn't need to be less than 32.  Only `aw` is used to build a bit mask.
        if aw > 32 || aw > w || ah > h {
            return None;
        }

        let mask = (-1_i32 as u32) >> (32 - aw);
        let max_x = w - aw + 1;
        let max_y = h - ah + 1;
        for y in 0 .. max_y {
            // Scan through the current line, looking for a place where `mask` is all zeros.
            let mut buf = 0;
            let mut left = 0;
            for x in 0 .. max_x {
                // Fill the buffer with enough bits so we can check against `mask`.
                if left < aw {
                    buf |= self.extract(x + left, x + 32, y) << left;
                    left = 32;
                }

                if buf & mask == 0 {
                    // Try to allocate here.
                    let mut ok = true;
                    for dy in 1 .. ah {
                        if self.extract(x, x + aw, y + dy) & mask != 0 {
                            ok = false;
                            break;
                        }
                    }
                    if ok {
                        return Some(V2::new(x as i32, y as i32));
                    }
                }

                buf >>= 1;
                left -= 1;
            }
        }

        None
    }

    pub fn alloc(&mut self, size: V2) -> Option<Region<V2>> {
        let pos = unwrap_or!(self.find_alloc(size), return None);
        let bounds = Region::sized(size) + pos;

        for y in bounds.min.y .. bounds.max.y {
            update(self.line_mut(y as usize),
                   bounds.min.x as usize,
                   bounds.max.x as usize,
                   -1_i32 as u32);
        }
        Some(bounds)
    }

    pub fn free(&mut self, bounds: Region<V2>) {
        for y in bounds.min.y .. bounds.max.y {
            update(self.line_mut(y as usize),
                   bounds.min.x as usize,
                   bounds.max.x as usize,
                   0);
        }
    }
}

fn extract(words: &[u32], b0: usize, b1: usize) -> u32 {
    let w0 = b0 / 32;
    let word_slice = if w0 <= words.len() { &words[w0..] } else { &[] };

    let mut out = 0;
    let mut pos = 0;
    let mut start = b0 % 32;
    for &data in word_slice {
        out |= (data >> start) << pos;
        pos += 32 - start;
        start = 0;
        if pos >= cmp::min(32, b1 - b0) {
            break;
        }
    }

    if b1 - b0 < 32 {
        out &= (1 << (b1 - b0)) - 1;
    }

    out
}

fn update(words: &mut [u32], b0: usize, b1: usize, val: u32) {
    let w0 = b0 / 32;
    let word_slice = if w0 <= words.len() { &mut words[w0..] } else { &mut [] };

    let mut val = val;
    let mut left = b1 - b0;
    let mut start = b0 % 32;
    for data in word_slice {
        let mask = if left < 32 { (1 << left) - 1 } else { -1_i32 as u32 };
        *data &= !(mask << start);
        *data |= (val & mask) << start;

        if start != 0 {
            val >>= 32 - start;
        } else {
            val = 0;
        }

        if left > 32 - start {
            left -= 32 - start;
        } else {
            break;
        }

        start = 0;
    }
}


#[cfg(test)]
mod test {
    #[test]
    fn extract_test() {
        use super::extract;

        let words = [0x76543210, 0xfdecba98];
        assert_eq!(extract(&words, 0, 32), 0x76543210);
        assert_eq!(extract(&words, 0, 16), 0x3210);
        assert_eq!(extract(&words, 0, 9), 0x10);
        assert_eq!(extract(&words, 16, 32), 0x7654);
        assert_eq!(extract(&words, 20, 32), 0x765);
        assert_eq!(extract(&words, 22, 32), 0x765 >> 2);
        assert_eq!(extract(&words, 32, 32), 0);
        assert_eq!(extract(&words, 0, 0), 0);
        assert_eq!(extract(&words, 24, 40), 0x9876);
        assert_eq!(extract(&words, 60, 68), 0xf);
        assert_eq!(extract(&words, 32, 64), 0xfdecba98);
    }

    #[test]
    fn update_test() {
        use super::update;

        let mut words = [0x76543210, 0xfdecba98];
        update(&mut words, 0, 32, 0x11111111);
        assert_eq!(words, [0x11111111, 0xfdecba98]);

        let mut words = [0x76543210, 0xfdecba98];
        update(&mut words, 0, 16, 0x11111111);
        assert_eq!(words, [0x76541111, 0xfdecba98]);

        let mut words = [0x76543210, 0xfdecba98];
        update(&mut words, 16, 32, 0x11111111);
        assert_eq!(words, [0x11113210, 0xfdecba98]);

        let mut words = [0x76543210, 0xfdecba98];
        update(&mut words, 0, 9, 0x11111111);
        assert_eq!(words, [0x76543311, 0xfdecba98]);

        let mut words = [0x76543210, 0xfdecba98];
        update(&mut words, 22, 32, 0x3ff);
        assert_eq!(words, [0xffd43210, 0xfdecba98]);

        let mut words = [0x76543210, 0xfdecba98];
        update(&mut words, 28, 36, 0x11111111);
        assert_eq!(words, [0x16543210, 0xfdecba91]);

        let mut words = [0x76543210, 0xfdecba98];
        update(&mut words, 60, 68, 0x11111111);
        assert_eq!(words, [0x76543210, 0x1decba98]);

        let mut words = [0x76543210, 0xfdecba98];
        update(&mut words, 12, 52, 0x11111111);
        assert_eq!(words, [0x11111210, 0xfde00111]);
    }

    #[test]
    fn alloc_2d_test() {
        use common_types::{V2, Region, scalar};
        use super::Alloc2D;

        let bounds = Region::sized(60);
        let mut a = Alloc2D::new(scalar(60));

        let r1 = a.alloc(V2::new(30, 50)).unwrap();
        assert!(bounds.contains_region(r1));

        let r2 = a.alloc(V2::new(30, 50)).unwrap();
        assert!(bounds.contains_region(r2));
        assert!(!r2.overlaps(r1));

        let r3 = a.alloc(V2::new(20, 10)).unwrap();
        assert!(bounds.contains_region(r3));
        assert!(!r3.overlaps(r1));
        assert!(!r3.overlaps(r2));

        assert_eq!(a.alloc(V2::new(30, 50)), None);

        a.free(r1);
        let r4 = a.alloc(V2::new(30, 50)).unwrap();
        assert!(bounds.contains_region(r4));
        // OK to overlap r1, which is now freed.
        assert!(!r4.overlaps(r2));
        assert!(!r4.overlaps(r3));
    }
}
