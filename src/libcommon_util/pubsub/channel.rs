use btree_multimap::BTreeMultiMap;
use super::Name;


/// A slightly unusual publish/subscribe system.
///
/// Each subscriber can associate itself with any number of channels.  Each publisher can associate
/// itself with channels in the same way.  When a publisher sends a message, each subscriber that
/// is associated with at least one of its channels will receive a single copy of the message.
/// Furthermore, when a publisher/subscriber becomes un/associated with a channel, it is notified
/// of the identities of all the subscribers/publishers currently associated with that channel.
#[derive(Debug)]
pub struct PubSub<P: Name, C: Name, S: Name> {
    /// Mapping from channels to publishers.  When adding new subscribers, we check this mapping to
    /// see which publishers are already on the same channel.
    chan_pub: BTreeMultiMap<C, P>,
    /// Mapping from channels to subscribers.  When adding new publishers, we check this mapping to
    /// see which subscribers are already on the same channel.
    chan_sub: BTreeMultiMap<C, S>,
    /// Mapping from publishers to all subscribers that are on the same channel.
    pub_sub: BTreeMultiMap<P, S>,
}

impl<P: Name, C: Name, S: Name> PubSub<P, C, S> {
    pub fn new() -> PubSub<P, C, S> {
        PubSub {
            chan_pub: BTreeMultiMap::new(),
            chan_sub: BTreeMultiMap::new(),
            pub_sub: BTreeMultiMap::new(),
        }
    }

    pub fn is_subscribed(&self, subscriber: S, channel: C) -> bool {
        self.chan_sub.contains(channel, subscriber)
    }

    pub fn publish<F>(&mut self, publisher: P, channel: C, mut f: F)
            where F: FnMut(&P, &C, &S) {
        if !self.chan_pub.insert(channel.clone(), publisher.clone()) {
            return;
        }

        for subscriber in self.chan_sub.get_distinct(channel.clone()) {
            if self.pub_sub.insert(publisher.clone(), subscriber.clone()) {
                f(&publisher, &channel, subscriber);
            }
        }
    }

    pub fn unpublish<F>(&mut self, publisher: P, channel: C, mut f: F)
            where F: FnMut(&P, &C, &S) {
        if !self.chan_pub.remove(channel.clone(), publisher.clone()) {
            return;
        }

        for subscriber in self.chan_sub.get_distinct(channel.clone()) {
            if self.pub_sub.remove(publisher.clone(), subscriber.clone()) {
                f(&publisher, &channel, subscriber);
            }
        }
    }

    pub fn subscribe<F>(&mut self, subscriber: S, channel: C, mut f: F)
            where F: FnMut(&P, &C, &S) {
        if !self.chan_sub.insert(channel.clone(), subscriber.clone()) {
            return;
        }

        for publisher in self.chan_pub.get_distinct(channel.clone()) {
            if self.pub_sub.insert(publisher.clone(), subscriber.clone()) {
                f(publisher, &channel, &subscriber);
            }
        }
    }

    pub fn unsubscribe<F>(&mut self, subscriber: S, channel: C, mut f: F)
            where F: FnMut(&P, &C, &S) {
        if !self.chan_sub.remove(channel.clone(), subscriber.clone()) {
            return;
        }

        for publisher in self.chan_pub.get_distinct(channel.clone()) {
            if self.pub_sub.remove(publisher.clone(), subscriber.clone()) {
                f(publisher, &channel, &subscriber);
            }
        }
    }


    pub fn message<F>(&self, publisher: &P, mut f: F)
            where F: FnMut(&P, &S) {
        for subscriber in self.pub_sub.get_distinct(publisher.clone()) {
            f(publisher, subscriber);
        }
    }


    pub fn subscribe_publisher<F>(&mut self, subscriber: S, publisher: P, mut f: F)
            where F: FnMut(&P, &S) {
        if self.pub_sub.insert(publisher.clone(), subscriber.clone()) {
            f(&publisher, &subscriber);
        }
    }

    pub fn unsubscribe_publisher<F>(&mut self, subscriber: S, publisher: P, mut f: F)
            where F: FnMut(&P, &S) {
        if self.pub_sub.remove(publisher.clone(), subscriber.clone()) {
            f(&publisher, &subscriber);
        }
    }

    pub fn channel_message<F>(&self, channel: &C, mut f: F)
            where F: FnMut(&C, &S) {
        for subscriber in self.chan_sub.get_distinct(channel.clone()) {
            f(channel, subscriber);
        }
    }
}
