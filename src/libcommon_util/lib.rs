#![crate_name = "common_util"]

extern crate common_types;
extern crate syntax_exts;

use std::hash::{Hash, Hasher};
#[allow(deprecated)] use std::hash::SipHasher;
use std::io;
use std::iter;
use std::mem;
use std::slice;

pub use self::alloc_2d::Alloc2D;
pub use self::bit_slice::BitSlice;
pub use self::btree_multimap::{BTreeMultiMap, BTreeMultiMap1};
pub use self::bytes::{Bytes, ByteCast};
pub use self::convert::Convert;
pub use self::coroutine::Coroutine;
pub use self::region_tree::RegionTree;
pub use self::small_vec::SmallVec;
pub use self::small_set::SmallSet;
pub use self::str_error::{StrError, StrResult};
pub use self::str_error::{StringError, StringResult};
pub use self::v3::{GridOrdered, ZOrdered};

#[macro_use] pub mod coroutine;
#[macro_use] pub mod str_error;
pub mod alloc_2d;
pub mod bit_slice;
pub mod btree_multimap;
pub mod bytes;
pub mod convert;
pub mod pubsub;
pub mod region_tree;
pub mod small_set;
pub mod small_vec;
pub mod v3;


#[macro_export]
macro_rules! warn_on_err {
    ($e:expr) => {
        match $e {
            Ok(_) => {},
            Err(e) => warn!("{}: {}",
                            stringify!($e),
                            ::std::error::Error::description(&e)),
        }
    };
}

#[macro_export]
macro_rules! err_on_err {
    ($e:expr) => {
        match $e {
            Ok(_) => {},
            Err(e) => error!("{}: {}",
                             stringify!($e),
                             ::std::error::Error::description(&e)),
        }
    };
}


pub unsafe fn transmute_slice<'a, T, U>(x: &'a [T]) -> &'a [U] {
    slice::from_raw_parts(x.as_ptr() as *const U,
                          x.len() * mem::size_of::<T>() / mem::size_of::<U>())
}

pub unsafe fn transmute_slice_mut<'a, T, U>(x: &'a mut [T]) -> &'a mut [U] {
    slice::from_raw_parts_mut(x.as_mut_ptr() as *mut U,
                              x.len() * mem::size_of::<T>() / mem::size_of::<U>())
}


/// Filter a `Vec<T>` in-place, not preserving order.
pub fn filter_in_place<T, F: FnMut(&T) -> bool>(vec: &mut Vec<T>, mut f: F) {
    let mut i = 0;
    while i < vec.len() {
        if f(&vec[i]) {
            i += 1;
        } else {
            vec.swap_remove(i);
        }
    }
}

/// Return the index of the first element of `xs` that is equal to `x`.
pub fn index_of<T: PartialEq>(xs: &[T], x: &T) -> Option<usize> {
    for (i, x0) in xs.iter().enumerate() {
        if x0 == x {
            return Some(i);
        }
    }
    None
}

/// Remove the first element of a vector that is equal to `x`.  Returns `true` if an element was
/// removed.  Does not preserve order.
pub fn remove_first<T: PartialEq>(vec: &mut Vec<T>, x: &T) -> bool {
    if let Some(i) = index_of(vec, x) {
        vec.swap_remove(i);
        true
    } else {
        false
    }
}


pub unsafe fn write_vec<W: io::Write, T>(w: &mut W, v: &Vec<T>) -> io::Result<()> {
    write_array(w, v)
}

pub unsafe fn read_vec<R: io::Read, T>(r: &mut R) -> io::Result<Vec<T>> {
    use self::bytes::ReadBytes;
    let len = try!(r.read_bytes::<u32>()) as usize;
    let mut v = Vec::with_capacity(len);
    v.set_len(len);
    try!(r.read_exact(transmute_slice_mut(&mut v)));
    Ok(v)
}

pub unsafe fn write_array<W: io::Write, T>(w: &mut W, v: &[T]) -> io::Result<()> {
    use self::bytes::WriteBytes;
    try!(w.write_bytes(v.len().to_u32().unwrap()));
    try!(w.write_all(transmute_slice(v)));
    Ok(())
}

pub unsafe fn read_array<R: io::Read, T>(r: &mut R) -> io::Result<Box<[T]>> {
    read_vec(r).map(|v| v.into_boxed_slice())
}


pub fn make_array<T: Copy>(init: T, len: usize) -> Box<[T]> {
    iter::repeat(init).take(len).collect::<Vec<_>>().into_boxed_slice()
}

pub fn make_array_with<T, F: FnMut() -> T>(len: usize, mut f: F) -> Box<[T]> {
    let mut v = Vec::with_capacity(len);
    for _ in 0 .. len {
        v.push(f());
    }
    v.into_boxed_slice()
}


/// Downcast a `Box<[T]>` to `Box<[T; N]>`.  This function is unsafe because we can't actually
/// restrict `A` to be `[T; N]`.
pub unsafe fn size_array<T, A>(mut arr: Box<[T]>) -> Box<A> {
    let target_len = mem::size_of::<A>() / mem::size_of::<T>();
    assert!(arr.len() == target_len,
            "size_array: len mismatch: {} != {}", arr.len(), target_len);
    let ptr = arr.as_mut_ptr() as *mut A;
    mem::forget(arr);
    Box::from_raw(ptr)
}

pub fn view_bytes<T: ByteCast>(s: &[T]) -> &[u8] {
    unsafe { transmute_slice(s) }
}

pub fn from_bytes<T: ByteCast>(b: &[u8]) -> T {
    unsafe {
        // Make sure copy_from_slice doesn't panic, as that would drop `buf` uninitialized.
        assert!(b.len() == mem::size_of::<T>());

        let mut buf = mem::uninitialized();
        {
            let buf_slice = slice::from_raw_parts_mut(&mut buf as *mut T as *mut u8,
                                                      mem::size_of::<T>());
            buf_slice.copy_from_slice(&b);
        }
        buf
    }
}


#[allow(deprecated)]    // for SipHasher
pub fn hash<H: ?Sized+Hash>(x: &H) -> u64 {
    let mut sip = SipHasher::new();
    x.hash(&mut sip);
    sip.finish()
}


