use error::{Error, Result};
use primitives::*;

macro_rules! die {
    ($msg:expr) => {
        return Err(Error::Noise($msg));
    }
}

fn concat(a: &[u8], b: &[u8]) -> Vec<u8> {
    a.iter().cloned()
        .chain(b.iter().cloned())
        .collect()
}

pub fn hmac_hash(key: &[u8; HASH_LEN],
                 data: &[u8]) -> HashOutput {
    let mut k_padded = [0; HASH_BLOCK_LEN];
    assert!(HASH_BLOCK_LEN >= HASH_LEN);
    k_padded[.. HASH_LEN].copy_from_slice(key);
    let k_padded = k_padded;

    let mut k_xor_ipad = k_padded;
    for b in k_xor_ipad.iter_mut() {
        *b ^= 0x36;
    }
    let k_xor_ipad = k_xor_ipad;
    // H(K XOR ipad, text)
    let first_hash = hash(&concat(&k_xor_ipad, data));

    let mut k_xor_opad = k_padded;
    for b in k_xor_opad.iter_mut() {
        *b ^= 0x5c;
    }
    let k_xor_opad = k_xor_opad;
    // H(K XOR opad, first_hash)
    let second_hash = hash(&concat(&k_xor_opad, first_hash.bytes()));

    second_hash
}

// Specialized to `num_outputs == 2`, because that's the only value we actually use.
pub fn hkdf(chaining_key: &[u8; HASH_LEN],
            input_key_material: &[u8]) -> (HashOutput, HashOutput) {
    let temp_key = hmac_hash(chaining_key, input_key_material);

    let output1 = hmac_hash(temp_key.bytes(), &[0x01]);
    let output2 = hmac_hash(temp_key.bytes(), &concat(output1.bytes(), &[0x02]));
    (output1, output2)
}


pub struct CipherState {
    k: Option<CipherKey>,
    n: u64,
}

impl CipherState {
    pub fn initialize_key(key: Option<CipherKey>) -> CipherState {
        CipherState {
            k: key,
            n: 0,
        }
    }

    pub fn has_key(&self) -> bool {
        self.k.is_some()
    }

    pub fn set_nonce(&mut self, nonce: u64) {
        self.n = nonce;
    }

    pub fn encrypt_with_ad(&mut self, ad: &[u8], plaintext: &[u8]) -> Result<Box<[u8]>> {
        if let Some(ref k) = self.k {
            let ciphertext = cipher_encrypt(k, self.n, ad, plaintext)?;
            self.n = self.n.checked_add(1)
                .expect("nonce overflow");
            Ok(ciphertext)
        } else {
            Ok(plaintext.to_owned().into_boxed_slice())
        }
    }

    pub fn decrypt_with_ad(&mut self, ad: &[u8], ciphertext: &[u8]) -> Result<Box<[u8]>> {
        if let Some(ref k) = self.k {
            let plaintext = cipher_decrypt(k, self.n, ad, ciphertext)?;
            self.n = self.n.checked_add(1)
                .expect("nonce overflow");
            Ok(plaintext)
        } else {
            Ok(ciphertext.to_owned().into_boxed_slice())
        }
    }

    pub fn rekey(&mut self) -> Result<()> {
        assert!(self.k.is_some());
        self.k = Some(cipher_rekey(self.k.as_ref().unwrap())?);
        Ok(())
    }
}


pub struct SymmetricState {
    cipher: CipherState,
    ck: [u8; HASH_LEN],
    h: HashOutput,
}

impl SymmetricState {
    pub fn initialize_symmetric(protocol_name: &[u8]) -> SymmetricState {
        let h =
            if protocol_name.len() <= HASH_LEN {
                HashOutput::from_bytes_unhashed(protocol_name)
            } else {
                hash(protocol_name)
            };

        let ck = h.bytes().clone();

        let cipher = CipherState::initialize_key(None);

        SymmetricState { cipher, ck, h }
    }

    pub fn mix_key(&mut self, input_key_material: &[u8]) {
        let (ck, temp_k) = hkdf(&self.ck, input_key_material);
        self.ck = ck.bytes().clone();

        let temp_k = CipherKey::from_hash_truncate(&temp_k);
        self.cipher = CipherState::initialize_key(Some(temp_k));
    }

    pub fn mix_hash(&mut self, data: &[u8]) {
        self.h = hash(&concat(self.h.bytes(), data));
    }

    // `mix_key_and_hash` not implemented.  It's used only for handling pre-shared symmetric keys.

    pub fn get_handshake_hash(&self) -> HashOutput {
        self.h.clone()
    }

    pub fn encrypt_and_hash(&mut self, plaintext: &[u8]) -> Result<Box<[u8]>> {
        let ciphertext = self.cipher.encrypt_with_ad(self.h.bytes(), plaintext)?;
        self.mix_hash(&ciphertext);
        Ok(ciphertext)
    }

    pub fn decrypt_and_hash(&mut self, ciphertext: &[u8]) -> Result<Box<[u8]>> {
        let plaintext = self.cipher.decrypt_with_ad(self.h.bytes(), ciphertext)?;
        self.mix_hash(&ciphertext);
        Ok(plaintext)
    }

    pub fn split(&self) -> (CipherState, CipherState) {
        let (temp_k1, temp_k2) = hkdf(&self.ck, &[]);

        let temp_k1 = CipherKey::from_hash_truncate(&temp_k1);
        let temp_k2 = CipherKey::from_hash_truncate(&temp_k2);

        let c1 = CipherState::initialize_key(Some(temp_k1));
        let c2 = CipherState::initialize_key(Some(temp_k2));
        (c1, c2)
    }
}


pub struct HandshakeState {
    symm: SymmetricState,
    e: DHPublicKey,
    e_priv: DHPrivateKey,
    re: Option<DHPublicKey>,

    initiator: bool,
    state: u8,
}

//  *0*
// -> e
//  *1*
// <- e, ee
//  *2*

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum HandshakeAction {
    Send,
    Recv,
    Done,
}

static NOISE_PROTOCOL_NAME: &[u8] = b"Noise_NN_25519_ChaChaPoly_SHA256";

impl HandshakeState {
    pub fn new(initiator: bool) -> Result<HandshakeState> {
        let (e, e_priv) = dh_generate_keypair()?;

        let mut symm = SymmetricState::initialize_symmetric(NOISE_PROTOCOL_NAME);
        // MixHash(prologue)
        symm.mix_hash(&[]);

        Ok(HandshakeState {
            symm, e, e_priv,
            re: None,
            initiator: initiator,
            state: 0,
        })
    }

    pub fn next_action(&self) -> HandshakeAction {
        match (self.initiator, self.state) {
            (true, 0) => HandshakeAction::Send,
            (true, 1) => HandshakeAction::Recv,
            (true, 2) => HandshakeAction::Done,

            (false, 0) => HandshakeAction::Recv,
            (false, 1) => HandshakeAction::Send,
            (false, 2) => HandshakeAction::Done,

            _ => panic!("unhandled handshake error"),
        }
    }

    pub fn send_message(&mut self) -> Result<Box<[u8]>> {
        match (self.initiator, self.state) {
            (true, 0) => {
                self.state = 1;
                let mut output = Vec::with_capacity(DH_LEN);

                // -> e
                output.extend_from_slice(self.e.bytes());
                self.symm.mix_hash(self.e.bytes());

                output.extend_from_slice(&self.symm.encrypt_and_hash(&[])?);
                Ok(output.into_boxed_slice())
            },

            (false, 1) => {
                self.state = 2;
                let mut output = Vec::with_capacity(DH_LEN + CIPHER_ABYTES);

                // <- e, ee
                output.extend_from_slice(self.e.bytes());
                self.symm.mix_hash(self.e.bytes());

                let re = self.re.as_ref().unwrap();
                self.symm.mix_key(dh(&self.e, &self.e_priv, re)?.bytes());

                output.extend_from_slice(&self.symm.encrypt_and_hash(&[])?);
                Ok(output.into_boxed_slice())
            },

            _ => panic!("send_message: invalid handshake state"),
        }
    }


    pub fn recv_message(&mut self, msg: &[u8]) -> Result<()> {
        match (self.initiator, self.state) {
            (true, 1) => {
                self.state = 2;

                // <- e, ee
                if msg.len() < DH_LEN {
                    die!("message too short (expecting DH public key, initiator state 1)");
                }
                let (re, msg) = msg.split_at(DH_LEN);
                self.re = Some(DHPublicKey::from_bytes(re));
                let re = self.re.as_ref().unwrap();
                self.symm.mix_hash(re.bytes());

                self.symm.mix_key(dh(&self.e, &self.e_priv, re)?.bytes());

                let payload = self.symm.decrypt_and_hash(msg)?;
                if payload.len() != 0 {
                    die!("nonempty payload (initiator state 1)");
                }
            },

            (false, 0) => {
                self.state = 1;

                // -> e
                if msg.len() < DH_LEN {
                    die!("message too short (expecting DH public key, responder state 0)");
                }
                let (re, msg) = msg.split_at(DH_LEN);
                self.re = Some(DHPublicKey::from_bytes(re));
                let re = self.re.as_ref().unwrap();
                self.symm.mix_hash(re.bytes());

                let payload = self.symm.decrypt_and_hash(msg)?;
                if payload.len() != 0 {
                    die!("nonempty payload (responder state 0)");
                }
            },

            _ => panic!("recv_message: invalid handshake state"),
        }

        Ok(())
    }

    pub fn finish(&self) -> (CipherState, CipherState, HashOutput) {
        assert!(self.state == 2);
        let (c_send, c_recv) = self.symm.split();
        let h = self.symm.get_handshake_hash();
        if self.initiator {
            (c_send, c_recv, h)
        } else {
            (c_recv, c_send, h)
        }
    }
}
