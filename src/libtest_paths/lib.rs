macro_rules! env_const {
    ($name:ident) => {
        pub const $name: &str = env!(concat!("OUTPOST_", stringify!($name)));
    };
}

env_const!(TEST_SERVER_DATA);
env_const!(TEST_CLIENT_DATA);
env_const!(TEST_BOOT_PY);
env_const!(FULL_SERVER_DATA);
env_const!(FULL_CLIENT_DATA);
env_const!(FULL_BOOT_PY);
