use std::cmp::{min, max};
use std::fmt;
use std::ops::{Add, Sub, Mul, Div, Rem, Neg, Shl, Shr, BitAnd, BitOr, BitXor, Not};

use super::{V3, V2, Vn};


#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub struct Region<V=V3> {
    pub min: V,
    pub max: V,
}

pub type Region2 = Region<V2>;
pub type Region3 = Region<V3>;

impl<V: Copy+fmt::Debug> fmt::Debug for Region<V> {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        (self.min, self.max).fmt(f)
    }
}

impl<V: Vn> Region<V> {
    // Constructors

    #[inline]
    pub fn new(min: V, max: V) -> Region<V> {
        Region { min: min, max: max }
    }

    #[inline]
    pub fn empty() -> Region<V> {
        Region { min: 0.into(), max: 0.into() }
    }

    #[inline]
    pub fn around(center: V, radius: i32) -> Region<V> {
        Region::new(center - radius.into(),
                    center + radius.into())
    }

    #[inline]
    pub fn sized(size: impl Into<V>) -> Region<V> {
        Region::new(0.into(), size.into())
    }

    // Basic inspection

    #[inline]
    pub fn is_empty(&self) -> bool {
        <V as Vn>::fold_axes(false, |a, e| e || self.min.get(a) >= self.max.get(a))
    }

    #[inline]
    pub fn size(&self) -> V {
        self.max - self.min
    }

    #[inline]
    pub fn volume(&self) -> i32 {
        let size = self.size();
        <V as Vn>::fold_axes(1, |a, v| v * size.get(a))
    }

    // Region ops

    #[inline]
    pub fn join(&self, other: Region<V>) -> Region<V> {
        Region::new(self.min.zip(other.min, |a, b| min(a, b)),
                    self.max.zip(other.max, |a, b| max(a, b)))
    }

    #[inline]
    pub fn intersect(&self, other: Region<V>) -> Region<V> {
        Region::new(self.min.zip(other.min, |a, b| max(a, b)),
                    self.max.zip(other.max, |a, b| min(a, b)))
    }

    #[inline]
    pub fn overlaps(&self, other: Region<V>) -> bool {
        let inter = self.intersect(other);
        <V as Vn>::fold_axes(true, |a, over| over && inter.min.get(a) < inter.max.get(a))
    }

    #[inline]
    pub fn overlaps_inclusive(&self, other: Region<V>) -> bool {
        let inter = self.intersect(other);
        <V as Vn>::fold_axes(true, |a, over| over && inter.min.get(a) <= inter.max.get(a))
    }

    #[inline]
    pub fn contains_region(&self, other: Region<V>) -> bool {
        <V as Vn>::fold_axes(true, |a, over| {
            over &&
            self.min.get(a) <= other.min.get(a) &&
            other.max.get(a) <= self.max.get(a)
        })
    }

    #[inline]
    pub fn expand(&self, amount: V) -> Region<V> {
        Region::new(self.min - amount, self.max + amount)
    }

    #[inline]
    pub fn div_round(&self, rhs: i32) -> Region<V> {
        Region::new(self.min / rhs.into(),
                    (self.max + (rhs - 1).into()) / rhs.into())
    }

    #[inline]
    pub fn div_round_signed(&self, rhs: i32) -> Region<V> {
        Region::new(self.min.div_floor(rhs),
                    (self.max + (rhs - 1).into()).div_floor(rhs))
    }

    // Point ops

    #[inline]
    pub fn index(&self, point: V) -> usize {
        let offset = point - self.min;
        let size = self.size();
        <V as Vn>::fold_axes((0, 1), |a, (sum, mul)| {
            (sum + offset.get(a) as usize * mul,
             mul * size.get(a) as usize)
        }).0
    }

    #[inline]
    pub fn from_index(&self, idx: usize) -> V {
        let size = self.size();
        <V as Vn>::unfold(idx, |a, acc| {
            let len = size.get(a) as usize;
            let x = self.min.get(a) + (acc % len) as i32;
            let acc = acc / len;
            (x, acc)
        }).0
    }

    #[inline]
    pub fn contains(&self, point: V) -> bool {
        <V as Vn>::fold_axes(true, |a, cur| {
            cur &&
            point.get(a) >= self.min.get(a) &&
            point.get(a) <  self.max.get(a)
        })
    }

    #[inline]
    pub fn contains_inclusive(&self, point: V) -> bool {
        <V as Vn>::fold_axes(true, |a, cur| {
            cur &&
            point.get(a) >= self.min.get(a) &&
            point.get(a) <= self.max.get(a)
        })
    }

    #[inline]
    pub fn clamp_point(&self, point: V) -> V {
        <V as Vn>::from_fn(|a| max(self.min.get(a),
                               min(self.max.get(a) - 1,
                                   point.get(a))))
    }

    // Iteration

    #[inline]
    pub fn points(&self) -> RegionPoints<V> {
        if self.is_empty() {
            RegionPoints::empty()
        } else {
            RegionPoints::new(self.min, self.max)
        }
    }

    #[inline]
    pub fn points_inclusive(&self) -> RegionPoints<V> {
        Region::new(self.min, self.max.map(|x| x + 1)).points()
    }
}

impl Region<V3> {
    #[inline]
    pub fn with_zs(&self, min_z: i32, max_z: i32) -> Region<V3> {
        Region::new(self.min.with_z(min_z), self.max.with_z(max_z))
    }

    #[inline]
    pub fn flatten(&self, depth: i32) -> Region<V3> {
        self.with_zs(self.min.z, self.min.z + depth)
    }

    #[inline]
    pub fn reduce(&self) -> Region<V2> {
        Region::new(self.min.reduce(),
                    self.max.reduce())
    }
}

impl Region<V2> {
    #[inline]
    pub fn extend(&self, min: i32, max: i32) -> Region<V3> {
        Region::new(self.min.extend(min),
                    self.max.extend(max))
    }

    #[inline]
    pub fn align_x(&self, other: Region<V2>, align: Align) -> Region<V2> {
        self.align(other, align, Align::None)
    }

    #[inline]
    pub fn align_y(&self, other: Region<V2>, align: Align) -> Region<V2> {
        self.align(other, Align::None, align)
    }

    #[inline]
    pub fn align(&self, other: Region<V2>, align_x: Align, align_y: Align) -> Region<V2> {
        let delta_x = align_delta(align_x,
                                  self.min.x, self.max.x,
                                  other.min.x, other.max.x);
        let delta_y = align_delta(align_y,
                                  self.min.y, self.max.y,
                                  other.min.y, other.max.y);
        *self + V2::new(delta_x, delta_y)
    }

    /// Compute an inner rect from left/right/top/bottom inset amounts.  A negative inset value
    /// means to inset from the opposite edge instead.  For example:
    ///  * `left = 10`: Left edge of the new rect is 10 units right of the left edge of `self`
    ///  * `left = -10`: Left edge of the new rect is 10 units *left* of the *right* edge of `self`
    #[inline]
    pub fn inset(&self, x0: i32, x1: i32, y0: i32, y1: i32) -> Region<V2> {
        let new_min =
            V2::new(if x0 >= 0 { self.min.x + x0 } else { self.max.x - (-x0) },
                    if y0 >= 0 { self.min.y + y0 } else { self.max.y - (-y0) });
        let new_max =
            V2::new(if x1 >= 0 { self.max.x - x1 } else { self.min.x + (-x1) },
                    if y1 >= 0 { self.max.y - y1 } else { self.min.y + (-y1) });
        Region::new(new_min, new_max)
    }
}

pub enum Align {
    Start,
    End,
    Center,
    None,
}

fn align_delta(align: Align, min1: i32, max1: i32, min2: i32, max2: i32) -> i32 {
    match align {
        Align::Start => min2 - min1,
        Align::End => max2 - max1,
        Align::Center => ((min2 + max2) - (min1 + max1)) >> 1,
        Align::None => 0,
    }
}

macro_rules! impl_Region_binop {
    ($op:ident, $method:ident) => {
        impl<V: Vn+Copy> $op<V> for Region<V> {
            type Output = Region<V>;
            #[inline]
            fn $method(self, other: V) -> Region<V> {
                Region::new(<V as Vn>::$method(self.min, other),
                            <V as Vn>::$method(self.max, other))
            }
        }

        impl<V: Vn+Copy> $op<i32> for Region<V> {
            type Output = Region<V>;
            #[inline]
            fn $method(self, other: i32) -> Region<V> {
                Region::new(<V as Vn>::$method(self.min, other.into()),
                            <V as Vn>::$method(self.max, other.into()))
            }
        }
    };
}

macro_rules! impl_Region_unop {
    ($op:ident, $method:ident) => {
        impl<V: Vn+Copy> $op for Region<V> {
            type Output = Region<V>;
            #[inline]
            fn $method(self) -> Region<V> {
                Region::new(<V as Vn>::$method(self.min),
                            <V as Vn>::$method(self.max))
            }
        }
    };
}

macro_rules! impl_Region_shift_op {
    ($op:ident, $method:ident) => {
        impl<V: Vn+Copy> $op<usize> for Region<V> {
            type Output = Region<V>;
            #[inline]
            fn $method(self, amount: usize) -> Region<V> {
                Region::new(<V as Vn>::$method(self.min, amount),
                            <V as Vn>::$method(self.max, amount))
            }
        }
    };
}

macro_rules! impl_Region_ops {
    () => {
        impl_Region_binop!(Add, add);
        impl_Region_binop!(Sub, sub);
        impl_Region_binop!(Mul, mul);
        impl_Region_binop!(Div, div);
        impl_Region_binop!(Rem, rem);
        impl_Region_unop!(Neg, neg);

        impl_Region_shift_op!(Shl, shl);
        impl_Region_shift_op!(Shr, shr);

        impl_Region_binop!(BitAnd, bitand);
        impl_Region_binop!(BitOr, bitor);
        impl_Region_binop!(BitXor, bitxor);
        impl_Region_unop!(Not, not);
    };
}

impl_Region_ops!();


#[derive(Clone, Copy)]
pub struct RegionPoints<V> {
    cur: V,
    min: V,
    max: V,
}

impl<V: Vn> RegionPoints<V> {
    #[inline]
    pub fn empty() -> RegionPoints<V> {
        RegionPoints {
            cur: 0.into(),
            min: 0.into(),
            max: 0.into(),
        }
    }

    #[inline]
    pub fn new(min: V, max: V) -> RegionPoints<V> {
        let mut first = true;
        let start = min.map(|x| {
            if first {
                first = false;
                x - 1
            } else {
                x
            }
        });
        RegionPoints {
            cur: start,
            min: min,
            max: max,
        }
    }
}

impl<V: Vn+Copy> Iterator for RegionPoints<V> {
    type Item = V;

    #[inline]
    fn next(&mut self) -> Option<V> {
        let (new, carry) = <V as Vn>::unfold(true, |a, carry| {
            if !carry {
                (self.cur.get(a), false)
            } else {
                let new_val = self.cur.get(a) + 1;
                if new_val < self.max.get(a) {
                    (new_val, false)
                } else {
                    (self.min.get(a), true)
                }
            }
        });
        self.cur = new;
        if carry {
            None
        } else {
            Some(new)
        }
    }
}
