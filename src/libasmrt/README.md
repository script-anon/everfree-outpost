This library provides the minimal subset of `libc` functionality required for
freestanding environments.  Specifically, it provides `memcpy`, `memmove`, and
`memset`, which are called by certain LLVM intrinsics.

Files in this directory are copied from `musl` (version 1.1.19) and are
distributed under the the `musl` license.  See the `COPYRIGHT` file for
details.
