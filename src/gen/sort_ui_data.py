import json
import re
import sys

def main(data_json, enum_json, enum_name, out_json):
    with open(data_json) as f:
        data = json.load(f)

    with open(enum_json) as f:
        enums = json.load(f)

    sorted_data = [data[k] for k in enums[enum_name]]

    with open(out_json, 'w') as f:
        json.dump(sorted_data, f)

if __name__ == '__main__':
    data_json, enum_json, enum_name, out_json = sys.argv[1:]
    main(data_json, enum_json, enum_name, out_json)

