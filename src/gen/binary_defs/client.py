from binary_defs.context import *


# Increment when an existing section's format is changed
VER_MINOR = 8


FILES = (
    'blocks',
    'terrains',
    'items',
    'structures',
    'recipes',
    'sprites',
    'pony_layers',
    'extras',
    'ui_cards',
    'ui_borders',
    'fonts',
    'inventory_layouts',
    )


EXT_INFO = Vector('B', 4)


BLOCK = Struct((
    Field('flags',          Scalar('H')),
    Field('display_flags',  Scalar('B')),
    Field('terrain',        Scalar('B'),    default=0),
    Field('structure',      Scalar('I'),    default=0),
    ))

def convert_blocks(ctx, blocks):
    parts = [x.copy() for x in parts]
    for x in parts:
        if x.get('anim_oneshot', False):
            x['anim_length'] = -x['anim_length']
    ctx.convert(b'StrcPart', STRUCTURE_PART, parts)


TERRAIN = Struct((
    Field('priority',       Scalar('B')),
    Field('has_alt',        Scalar('B')),
    Field('index',          Scalar('H')),
    ))


ITEM = Struct((
    Field('name',           String()),
    Field('ui_name',        String()),
    Field('desc',           String()),
    Field('image_pos',      Vector('H', 2)),
    Field('flags',          Scalar('H')),
    ))


STRUCTURE_VERTEX = Vector('h', 3)

STRUCTURE_PART = Struct((
    Field('verts',          Sequence(b'StrcVert', STRUCTURE_VERTEX, idx_ty='H')),
    Field('offset',         Vector('h', 2)),
    Field('sheet',          Scalar('B')),
    Field('flags',          Scalar('B'),    default=0),

    Field('anim_length',    Scalar('b'), 0),
    Field('anim_rate',      Scalar('B'), 0),
    Field('anim_step',      Scalar('H'), 0),
    ))

@STRUCTURE_PART.adjustment
def adjust_structure_part(part):
    if part.get('anim_oneshot', False):
        part['anim_length'] = -part['anim_length']

STRUCTURE = Struct((
    Field('size',           Vector('B', 3)),
    Field('layer',          Scalar('B')),
    Field('shape',          Sequence(b'StrcShap', Scalar('H'),
        idx_ty='H', offset_only=True)),
    Field('parts',          Sequence(b'StrcPart', STRUCTURE_PART, idx_ty='H')),
    Field('flags',          Scalar('B'),    default=0),

    Field('draw_mode',      Scalar('B'),    default=0),
    Field('draw_args',      Vector('B', 4), default=(0, 0, 0, 0)),
    Field('swizzle',        Sequence(b'StrcSwzl', Scalar('B'), idx_ty='H'), default=()),
    Field('depth_mask',     Sequence(b'StrcDpth', Scalar('B'), idx_ty='H')),

    Field('light_pos',      Vector('B', 3), default=(0, 0, 0)),
    Field('light_color',    Vector('B', 3), default=(0, 0, 0)),
    Field('light_radius',   Scalar('H'),    default=0),

    Field('ext_info',       Sequence(b'ExtnInfo', EXT_INFO, idx_ty='H'), default=()),
    ))


SPRITE = Struct((
    Field('src_pos',        Vector('h', 2)),
    Field('dest_pos',       Vector('h', 2)),
    Field('size',           Vector('H', 2)),
    Field('sheet',          Scalar('B')),

    Field('anim_length',    Scalar('b'), 0),
    Field('anim_rate',      Scalar('B'), 0),
    Field('anim_step',      Scalar('H'), 0),
    ))

@SPRITE.adjustment
def adjust_structure_part(part):
    if part.get('anim_oneshot', False):
        part['anim_length'] = -part['anim_length']

LAYER_SPRITE = Struct((
    Field('sprite',         Scalar('H')),
    Field('flip',           Scalar('B')),
    ))

PONY_LAYER = Struct((
    Field('anims',          Sequence(b'PonySprt', LAYER_SPRITE, idx_ty='H')),
    ))


RECIPE_ITEM = Vector('H', 2)

RECIPE = Struct((
    Field('ui_name',        String()),
    Field('inputs',         Sequence(b'RcpeItms', RECIPE_ITEM, idx_ty='H')),
    Field('outputs',        Sequence(b'RcpeItms', RECIPE_ITEM, idx_ty='H')),
    Field('ability',        Scalar('H')),
    Field('time',           Scalar('H')),
    Field('crafting_class', Scalar('B')),
    ))


DAY_NIGHT_PHASE = Struct((
    Field('start_time',     Scalar('H')),
    Field('end_time',       Scalar('H')),
    Field('start_color',    Scalar('B')),
    Field('end_color',      Scalar('B')),
    ))

DAY_NIGHT_COLOR = Vector('B', 3)

def convert_day_night(ctx, j):
    cut_0 = 0
    colors = [(255, 255, 255)]
    cut_1 = len(colors)
    colors.extend(reversed(j['sunset']))
    cut_2 = len(colors)
    colors.extend(j['sunrise'])
    cut_3 = len(colors)

    phases = [
            dict(   # day
                start_time=j['day_start'],
                end_time=j['day_end'],
                start_color=cut_0,
                end_color=cut_1),
            dict(   # sunset
                start_time=j['day_end'],
                end_time=j['night_start'],
                start_color=cut_1,
                end_color=cut_2 - 1),
            dict(   # night
                start_time=j['night_start'],
                end_time=j['night_end'],
                start_color=cut_2 - 1,
                end_color=cut_2),
            dict(   # sunrise
                start_time=j['night_end'],
                end_time=24000,
                start_color=cut_2,
                end_color=cut_3 - 1),
            ]

    ctx.convert(b'DyNtPhas', DAY_NIGHT_PHASE, phases)
    ctx.convert(b'DyNtColr', DAY_NIGHT_COLOR, colors)


UI_CARD = Struct((
    Field('pos',            Vector('H', 2)),
    Field('size',           Vector('H', 2)),
))

UI_BORDER = Struct((
    Field('pos',            Vector('H', 2)),
    Field('size',           Vector('H', 2)),
    Field('inset',          Vector('B', 4)),
))


FONT_SPAN = Struct((
    Field('last_char',      Scalar('I')),
    Field('len',            Scalar('H')),
    Field('xs',             Sequence(b'FontXs\0\0', Scalar('H'), idx_ty='H',
        offset_only=True)),
    Field('pos',            Vector('H', 2)),
    ))

FONT = Struct((
    Field('spans',          Sequence(b'FontSpan', FONT_SPAN, idx_ty='H')),
    Field('height',         Scalar('B')),
    Field('spacing',        Scalar('B')),
    Field('space_width',    Scalar('B')),
    ))


INVENTORY_LAYOUT_SLOT = Struct((
    Field('display_pos',    Vector('H', 2)),
    Field('image_src',      Vector('H', 2)),
    ))

INVENTORY_LAYOUT = Struct((
    Field('size',           Vector('H', 2)),
    Field('slots',          Sequence(b'InvLSlot', INVENTORY_LAYOUT_SLOT, idx_ty='H')),
    ))



def convert_extras(ctx, j):
    # XPonLayr - pony_layer_table
    ctx.convert(b'XPonLayr', Scalar('B'), j['pony_layer_table'])

    # XPhysAnm - physics_anim_table
    arr = j['physics_anim_table']
    for i in range(len(arr)):
        if arr[i] is None:
            arr[i] = [0] * 8
    ctx.convert(b'XPhysAnm', Vector('H', 8), arr)

    # XAnimDir - anim_dir_table
    max_idx = max(int(k) for k in j['anim_dir_table'].keys())
    lst = [255] * (max_idx + 1)
    for k,v in j['anim_dir_table'].items():
        lst[int(k)] = v
    ctx.convert(b'XAnimDir', Scalar('B'), lst)

    # XSpcAnim - special anims
    ctx.convert(b'XSpcAnim', Scalar('H'), [
        j['default_anim'],
        j['editor_anim'],
        j['activity_none_anim'],
        ])

    # XSpcLayr - special layers
    ctx.convert(b'XSpcLayr', Scalar('B'), [
        j['activity_layer'],
        ])

    # XSpcGrfx - special graphics
    ctx.convert(b'XSpcGrfx', Scalar('H'), [
        j['activity_bubble_graphics'],
        ])



def convert(ctx, defs):
    ctx.init_intern_table(b'Strings\0', 1)
    ctx.init_intern_table(b'ExtnInfo', EXT_INFO.size())
    ctx.init_intern_table(b'RcpeItms', RECIPE_ITEM.size())
    ctx.init_intern_table(b'StrcShap', 2)
    ctx.init_intern_table(b'StrcSwzl', 1)
    ctx.init_intern_table(b'StrcDpth', 1)
    ctx.init_intern_table(b'StrcVert', STRUCTURE_VERTEX.size())
    ctx.init_sequence(b'StrcPart', STRUCTURE_PART.size())
    ctx.init_sequence(b'FontXs\0\0', Scalar('H').size())
    ctx.init_sequence(b'FontSpan', FONT_SPAN.size())
    ctx.init_sequence(b'InvLSlot', INVENTORY_LAYOUT_SLOT.size())
    ctx.init_sequence(b'PonySprt', LAYER_SPRITE.size())

    ctx.convert(b'Blocks\0\0', BLOCK, defs['blocks'])
    ctx.convert(b'Terrains', TERRAIN, defs['terrains'])
    ctx.convert(b'Items\0\0\0', ITEM, defs['items'])
    ctx.convert(b'StrcDefs', STRUCTURE, defs['structures'])
    ctx.convert(b'Sprites\0', SPRITE, defs['sprites'])
    ctx.convert(b'PonyLayr', PONY_LAYER, defs['pony_layers'])
    ctx.convert(b'RcpeDefs', RECIPE, defs['recipes'])
    convert_day_night(ctx, defs['extras']['day_night'])
    ctx.convert(b'UICard\0\0', UI_CARD, defs['ui_cards'])
    ctx.convert(b'UIBorder', UI_BORDER, defs['ui_borders'])
    ctx.convert(b'Fonts\0\0\0', FONT, defs['fonts'])
    ctx.convert(b'InvLay\0\0', INVENTORY_LAYOUT, defs['inventory_layouts'])
    convert_extras(ctx, defs['extras'])

    ctx.build_index(b'Item', (x['name'] for x in defs['items']))
