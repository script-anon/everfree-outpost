from binary_defs.context import *


# Increment when an existing section's format is changed
VER_MINOR = 1


FILES = (
        'vaults',
        )


STRUCTURE = Struct((
    Field('desc',           String()),
    Field('pos',            Vector('H', 3)),
    ))


VAULT = Struct((
    Field('name',           String()),
    Field('size',           Vector('H', 2)),
    Field('structures',     Sequence(b'Structur', STRUCTURE)),
    Field('tree_mask',      Sequence(b'BitMasks', Scalar('B'))),
    Field('junk_mask',      Sequence(b'BitMasks', Scalar('B'))),
    ))


def convert(ctx, defs):
    ctx.init_intern_table(b'Strings\0', 1)
    ctx.init_intern_table(b'Structur', STRUCTURE.size())
    ctx.init_intern_table(b'BitMasks', 1)

    ctx.convert(b'Vaults\0\0', VAULT, defs['vaults'])

    ctx.build_index(b'Valt', (x['name'] for x in defs['vaults']))
