from outpost_data.gen.data.typed import *
from outpost_data.lib.consts import *
from outpost_data.lib import util


# Unlike most data definitions, UI data JSON files contain a dict mapping names
# to object data, instead of a list ordered by internal ID.  The assignment of
# IDs is handled later, since the IDs need to match enums in the Rust code.
def dict_by_name(xs):
    dct = {}
    for x in xs:
        dct[x.name] = x.process()
    return dct


class UICard:
    def __init__(self, dct):
        self.name = typed(dct, 'name', str)
        self.image = dct['image']

    def process(self):
        return {
                'pos': self.image['pos'],
                'size': self.image['size'],
                }

def conv_ui_card(dct):
    return dct['name'], UICard(dct)

def process_ui_cards(data):
    return dict_by_name(data.ui_card.objects())


class UIBorder:
    def __init__(self, dct):
        self.name = typed(dct, 'name', str)
        self.image = dct['image']
        self.inset = typed(dct, 'inset', (int, int, int, int))

    def process(self):
        return {
                'pos': self.image['pos'],
                'size': self.image['size'],
                'inset': self.inset,
                }

def conv_ui_border(dct):
    return dct['name'], UIBorder(dct)

def process_ui_borders(data):
    return dict_by_name(data.ui_border.objects())


class Font:
    def __init__(self, dct):
        self.name = typed(dct, 'name', str)
        raw_spans = typed(dct, 'spans', [{
            'first_char': int,
            'last_char': int,
            'xs': [int],
            'image': Any,
            }])
        self.spacing = typed(dct, 'spacing', int)
        self.space_width = typed(dct, 'space_width', int)

        self.height = 0
        self.spans = []
        for s in raw_spans:
            if self.height == 0:
                self.height = s['image']['size'][1]

            self.spans.append({
                'first_char': s['first_char'],
                'last_char': s['last_char'],
                'len': len(s['xs']) - 1,
                'xs': s['xs'],
                'pos': s['image']['pos'],
                })

    def process(self):
        return {
                'spans': self.spans,
                'height': self.height,
                'spacing': self.spacing,
                'space_width': self.space_width,
                }

def conv_font(dct):
    return dct['name'], Font(dct)

def process_fonts(data):
    return dict_by_name(data.font.objects())


class InventoryLayout:
    def __init__(self, dct):
        self.name = typed(dct, 'name', str)
        self.size = typed(dct, 'size', (int, int))
        slot_dcts = typed(dct, 'slots', [Any])
        self.slots = [InventoryLayoutSlot(dct) for dct in slot_dcts]

    def process(self):
        return {
                'size': self.size,
                'slots': [s.process() for s in self.slots],
                }

class InventoryLayoutSlot:
    def __init__(self, dct):
        self.image = dct['image']
        self.pos = typed(dct, 'pos', (int, int))

    def process(self):
        return {
                'display_pos': self.pos,
                'image_src': self.image['pos'],
                }

def conv_inventory_layout(dct):
    return dct['name'], InventoryLayout(dct)

def process_inventory_layouts(data):
    return dict_by_name(data.inventory_layout.objects())
