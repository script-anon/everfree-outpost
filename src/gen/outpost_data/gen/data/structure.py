from collections import namedtuple

from outpost_data.gen.data.typed import *
from outpost_data.lib.consts import *
from outpost_data.lib import util

TILE_SIZE = TILE_SIZE2


ExtBitmapParams = namedtuple('ExtBitmapParams', ('mode', 'w', 'h'))

def encode_ext_info(x):
    if isinstance(x, ExtBitmapParams):
        return [S_EXT_BITMAP_PARAMS, x.mode, x.w, x.h]
    else:
        raise TypeError('unknown extension info type: %s' % (type(x),))


class Structure:
    def __init__(self, dct):
        self.name = typed(dct, 'name', str)
        self.layer = typed(dct, 'layer', int)
        self.size = typed(dct, 'size', (int, int, int))
        self.shape = typed(dct, 'shape', [int])
        self.parts = tuple(StructurePart(p) for p in dct['parts'])
        self.flags = typed(dct, 'flags', int, 0)

        self.light = typed(dct, 'light',
                Optional({
                    'pos': (int, int, int),
                    'color': (int, int, int),
                    'radius': int,
                }))

        self.draw_mode = dct.get('draw_mode', DrawNormal())
        self.swizzle = typed(dct, 'swizzle', Optional([int]))

        self.depth_mask = typed(dct, 'depth_mask', Optional([int]))

        self.ext_info = [encode_ext_info(x) for x in dct.get('ext_info', ())]

    def draw_class(self):
        return getattr(self.draw_mode, 'cls', None)

    def calc_swizzle(self):
        if all(p.priority == 0 for p in self.parts):
            return None

        lst = [(p.priority, i) for i, p in enumerate(self.parts)]
        lst.sort()
        return [i for _, i in lst]

    def calc_depth_mask(self):
        # How depth works:
        # We assign a depth value to each half-block, divided diagonally:
        #
        #  z=2|  +--+--+--+
        #     |  | /| /|4/|     Numbered elements will all appear in the same
        #     |  \/ |/3|/ |     on-screen position.  The number indicates the
        #  z=1|  +--+--+--+     depth of the element.
        #     |  | /|2/| /|
        #     |  \/1|/ |/ |
        #  z=0|  +--+--+--+
        #     |
        #     +------------
        #      y=0  1  2  3
        #
        # Depth 0 is reserved for "not occupied".
        #
        # Depth is further modified by the structure's layer number.  Within a
        # given block, higher layers should be rendered above lower ones.  Since
        # there are less than four layer values used currently, we multiply the
        # depth by 4 and add the layer.

        sx, sy, sz = self.size
        mask = [0 for _ in range(sx * (sy + sz))]
        for z in range(sz):
            for y in range(sy):
                for x in range(sx):
                    idx = x + sx * (y + sy * (z))
                    if self.shape[idx] & B_OCCUPIED == 0:
                        continue

                    # Top side of the block
                    v0 = y - z + sz - 1
                    idx0 = x + sx * v0
                    depth = (2 * z + 2) * 4 + self.layer
                    mask[idx0] = max(mask[idx0], depth)

                    # Front side of the block
                    v1 = y - z + sz
                    idx1 = x + sx * v1
                    depth = (2 * z + 1) * 4 + self.layer
                    mask[idx1] = max(mask[idx1], depth)

        return mask

    def process(self, draw_class_map):
        mode, args = encode_draw_mode(self.draw_mode, draw_class_map)
        dct = {
                'name': self.name,
                'size': self.size,
                'layer': self.layer,
                'shape': self.shape,
                'parts': [p.process() for p in self.parts],
                'flags': self.flags,

                'draw_mode': mode,
                'draw_args': args,
                'depth_mask': self.calc_depth_mask(),
                }

        swizzle = self.calc_swizzle()
        if swizzle is not None:
            dct.update({
                'swizzle': swizzle,
                })

        if self.light is not None:
            dct.update({
                'light_pos': self.light['pos'],
                'light_color': self.light['color'],
                'light_radius': self.light['radius'],
                })
            dct['flags'] |= S_HAS_LIGHT

        return dct

class StructurePart:
    def __init__(self, dct):
        self.mesh = dct['mesh'].copy()
        self.image = dct['image']
        if 'mesh_bounds' in dct:
            self.mesh_bounds = typed(dct, 'mesh_bounds', (int,) * 6)
        else:
            b1, b2 = self.mesh.get_bounds()
            self.mesh_bounds = b1 + b2
        self.flags = typed(dct, 'flags', int, 0)
        self.priority = typed(dct, 'priority', int, 0)

        mx1, my1, mz1, mx2, my2, mz2 = self.mesh_bounds
        assert mx1 <= mx2 and my1 <= my2 and mz1 <= mz2
        mesh_size = mx2 - mx1, (my2 - my1) + (mz2 - mz1)

        if (self.flags & S_DRAW_BITMAP) == 0:
            assert mesh_size == tuple(self.image['orig_size']), \
                    'mesh bounds cover area of size %s, but image size is %s' % \
                    (mesh_size, self.image['orig_size'])

        ix, iy = self.image['crop_offset']
        iw, ih = self.image['size']
        mv1, mv2 = my1 - mz2, my2 - mz1
        # Adjust image bounds by `mx1, mv1` to align the image's original
        # bounds with the mesh's bounds.
        self.mesh.clip_xv(mx1 + ix, mv1 + iy, mx1 + ix + iw, mv1 + iy + ih)

    def process(self):
        if (self.flags & S_DRAW_BITMAP) == 0:
            # Rendering expects the part's `offset` to point to the pixel that
            # should appear at mesh coordinates (0, 0, 0).  There are two
            # adjustments to make:
            #  (1) `image['pos']` is the position of the top-left of the clipped
            #      image.  Subtract the clip offset to get the top-left of the
            #      original image.
            #  (2) The top-left of the original image appears at the (-x,-y,+z)
            #      corner of the mesh bounds.  Subtract out those coordinates to
            #      get the pixel appearing at (0, 0, 0) in the mesh.
            x, y = self.image['pos']
            ox, oy = self.image['crop_offset']
            mx1, my1, _, _, _, mz2 = self.mesh_bounds
            offset = (
                    x - ox - mx1,
                    y - oy - (my1 - mz2))
        else:
            # Offset is unused when S_DRAW_BITMAP is enabled.
            offset = (0, 0)

        dct = {
                'sheet': self.image['sheet'],
                'offset': offset,
                'verts': [v.pos for v in self.mesh.iter_tri_verts()],
                'flags': self.flags,
                }

        if self.image['anim'] is not None:
            anim = self.image['anim']
            dct.update({
                'anim_length': anim['length'],
                'anim_rate': anim['rate'],
                'anim_oneshot': anim['oneshot'],
                'anim_step': self.image['size'][0],
                })

        return dct




# Draw modes

DrawNormal = namedtuple('DrawNormal', ())
DrawFence = namedtuple('DrawFence', ('cls', 'fixed', 'use_corners'))
DrawFence.__new__.__defaults__ = (False,)
DrawTree = namedtuple('DrawTree', ('fixed',))
DrawTree.__new__.__defaults__ = (3,)
DrawFence16 = namedtuple('DrawFence16', ('cls',))
DrawDoor = namedtuple('DrawDoor', ('cls',))
DrawTerrain = namedtuple('DrawTerrain', ('index',))

def encode_draw_mode(mode, class_map):
    if isinstance(mode, DrawNormal):
        return (0, (0, 0, 0, 0))
    elif isinstance(mode, DrawFence):
        return (1, (class_map[mode.cls], mode.fixed, int(mode.use_corners), 0))
    elif isinstance(mode, DrawTree):
        return (2, (mode.fixed, 0, 0, 0))
    elif isinstance(mode, DrawFence16):
        return (3, (class_map[mode.cls], 0, 0, 0))
    elif isinstance(mode, DrawDoor):
        return (4, (class_map[mode.cls], 0, 0, 0))
    elif isinstance(mode, DrawTerrain):
        return (5, (mode.index, 0, 0, 0))
    else:
        assert False, 'bad draw mode: %r' % mode


def conv_structure(dct):
    return dct['name'], Structure(dct)

def process_structures(data, id_maps):
    structures = util.order_by_id(data.structure, id_maps.structure)

    draw_classes = set(s.draw_class() for s in structures)
    if None in draw_classes:
        draw_classes.remove(None)
    draw_class_map = util.assign_ids(draw_classes)

    return [s.process(draw_class_map) for s in structures]
