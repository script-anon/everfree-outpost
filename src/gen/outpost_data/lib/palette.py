from outpost_data.lib.image import Anim


def extract_palettes(img):
    '''
    Extract palettes from an image representation.  The image should consist of
    a grid of 2x2 color swatches, with one column per palette.
    '''
    def extract_palettes_inner(raw):
        w, h = raw.size
        pals = []
        for x in range(0, w, 2):
            pals.append([raw.getpixel((x, y)) for y in range(0, h, 2)])
        return pals
    return img.raw().compute(extract_palettes_inner)

def collect_mask_colors(img):
    if isinstance(img, Anim):
        img = img.flatten()

    def collect_mask_colors_inner(raw):
        # Converting to RGB before quantizing seems to give better results.  On
        # RGBA it unnecessarily reduced the number of colors in the image.
        quant = raw.convert('RGB').quantize()
        pal = quant.getpalette()
        levels = []
        for i in range(0, len(pal), 3):
            if pal[i] == pal[i + 2] and pal[i + 1] == 0 and pal[i] != 0:
                levels.append(pal[i])
        levels.sort(reverse=True)
        return [(x, 0, x, 255) for x in levels]
    return img.raw().compute(collect_mask_colors_inner)

def collect_all_colors(img):
    if isinstance(img, Anim):
        img = img.flatten()

    def collect_all_colors_inner(raw):
        # Converting to RGB before quantizing seems to give better results.  On
        # RGBA it unnecessarily reduced the number of colors in the image.
        quant = raw.convert('RGB').quantize()
        pal = quant.getpalette()
        # Build a list of all colors in the image, sorted by luminance.
        levels = set()
        for i in range(0, len(pal), 3):
            r, g, b = pal[i : i + 3]
            y = r * 0.2126 + g * 0.7152 + b * 0.0722
            levels.add((y, (r, g, b, 255)))
        return [c for _, c in sorted(levels)]
    return img.raw().compute(collect_all_colors_inner)

def recolor(img, palette, base_palette=None):
    if base_palette is None:
        base_palette = collect_mask_colors(img)
    palette = tuple(tuple(x[:3]) for x in palette)
    base_palette = tuple(tuple(x[:3]) for x in base_palette)

    dct = dict(zip(base_palette, palette))

    def recolor_inner(raw):
        if raw.mode == 'P':
            quant = raw.copy()
        else:
            quant = raw.convert('RGB').quantize()
        if raw.mode == 'RGBA':
            alpha = raw.split()[3]

        pal = list(quant.getpalette())
        for i in range(0, len(pal), 3):
            k = tuple(pal[i : i + 3])
            if k in dct:
                pal[i : i + 3] = dct[k]

        quant.putpalette(pal)
        new_img = quant.convert(raw.mode)
        if raw.mode == 'RGBA':
            new_img.putalpha(alpha)
        return new_img

    return img.modify(recolor_inner,
            desc=(__name__, 'recolor', palette, base_palette))
