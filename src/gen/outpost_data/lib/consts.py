TILE_SIZE = 32
TILE_SIZE2 = 16
ICON_SIZE = 16


SHAPE_ID = {
        'empty': 0,
        'floor': 1,
        'solid': 2,
        'ramp_e': 3,
        'ramp_w': 4,
        'ramp_s': 5,
        'ramp_n': 6,
        }


S_EMPTY =           0
S_FLOOR =           1
S_SOLID =           2
S_RAMP_E =          3
S_RAMP_W =          4
S_RAMP_S =          5
S_RAMP_N =          6


B_SUBFLOOR =        0x0001
B_FLOOR =           0x0002
B_SOLID =           0x0004
B_PART_MASK =       0x0007

B_SHAPE_MASK =      0xf000
B_SHAPE =           lambda s: s << 12

B_OPAQUE =          0x0008
B_NON_WALKABLE =    0x0010
B_OCCUPIED =        0x0020


I_USE_AT_POINT =        0x0001
I_USE_ON_SELF =         0x0002


# Structure flags
S_HAS_LIGHT =       0x04

# Structure part flags
S_DRAW_BITMAP =     0x01


S_EXT_BITMAP_PARAMS = 0x01

BITMAP_MODE_BRASS = 0
