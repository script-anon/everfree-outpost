

def parse_item_list(s):
    dct = {}
    for x in s.split(','):
        x = x.strip()

        count, _, name = x.strip().partition(' ')
        if name:
            count = int(count)
        else:
            name = count
            count = 1

        if name in dct:
            dct[name] += count
        else:
            dct[name] = count

    return list(dct.items())
