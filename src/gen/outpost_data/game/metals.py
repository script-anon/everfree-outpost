from outpost_data.lib.image import load
from outpost_data.lib.palette import extract_palettes


METALS = (
        'stone',
        'copper',
        'bronze',
        'iron',
        't1_earth',
        't2_earth',
        't1_pegasus',
        't2_pegasus',
        't1_unicorn',
        't2_unicorn',
        'silver',
        )

METAL_NAMES = {
        'stone': 'Stone',
        'copper': 'Copper',
        'bronze': 'Bronze',
        'iron': 'Iron',
        'silver': 'Silver',
        'gold': 'Gold',
        't1_earth': 'Titanium',
        't2_earth': 'Adamantium',
        't1_pegasus': 'Iridium',
        't2_pegasus': 'Aetherium',
        't1_unicorn': 'Palladium',
        't2_unicorn': 'Scintillium',
        }

METAL_STATIONS = {
        'stone': 'workbench',
        'copper': 'anvil',
        'iron': 'anvil',
        }

FURNACE_METALS = (
        'copper', 'iron', 'silver',
        )

def get_palettes():
    return dict(zip(
        ('_base',) + METALS,
        extract_palettes(load('misc/metal-palettes.png'))))
