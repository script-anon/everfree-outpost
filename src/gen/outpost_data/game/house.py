from outpost_data.lib.image import Image, Anim
from outpost_data import lib
from outpost_data.lib.consts import *


def assemble_2x2(a, b, c, d):
    x, y = a.px_size
    return Image.sheet((
        (a, (0, 0)),
        (b, (x, 0)),
        (c, (0, y)),
        (d, (x, y)),
        ))

def assemble_1x3(a, b, c):
    x, y = a.px_size
    return Image.sheet((
        (a, (0, 0)),
        (b, (0, y)),
        (c, (0, 2 * y)),
        ))

def load_house_parts(img, variant_idx):
    grid = img.chop_xy(unit=TILE_SIZE2)
    subgrid = img.chop_xy(unit=TILE_SIZE2 // 2)

    empty = Image.blank((TILE_SIZE2, TILE_SIZE2), unit=TILE_SIZE2)
    subempty = Image.blank((TILE_SIZE2 // 2, TILE_SIZE2 // 2), unit=TILE_SIZE2 // 2)

    base_top = assemble_2x2(subgrid[2][2], subgrid[2][7], subgrid[5][2], subgrid[5][7])
    if variant_idx is None:
        front1 = assemble_2x2(subgrid[6][2], subgrid[6][7], subgrid[7][2], subgrid[7][7])
        front2 = assemble_2x2(subgrid[8][2], subgrid[8][7], subgrid[9][2], subgrid[9][7])
        base = assemble_1x3(base_top, front1, front2)
    else:
        i = variant_idx
        base = assemble_1x3(base_top, grid[8][2 + i], grid[9][2 + i])

    # Order; N, E, S, W, NW, NE, SE, SW, door E, door W
    n = assemble_1x3(assemble_2x2(
        subgrid[4][2], subgrid[4][7], subempty, subempty), empty, empty)
    s = assemble_1x3(assemble_2x2(
        subempty, subempty, subgrid[3][2], subgrid[3][7]), empty, empty)

    if variant_idx is None:
        e = assemble_1x3(
                assemble_2x2(subempty, subgrid[2][5], subempty, subgrid[5][5]),
                assemble_2x2(subempty, subgrid[6][5], subempty, subgrid[7][5]),
                assemble_2x2(subempty, subgrid[8][5], subempty, subgrid[9][5]),
                )
        w = assemble_1x3(
                assemble_2x2(subgrid[2][4], subempty, subgrid[5][4], subempty),
                assemble_2x2(subgrid[6][4], subempty, subgrid[7][4], subempty),
                assemble_2x2(subgrid[8][4], subempty, subgrid[9][4], subempty),
                )
    else:
        e = assemble_1x3(
                assemble_2x2(subempty, subgrid[2][5], subempty, subgrid[5][5]),
                empty, empty)
        w = assemble_1x3(
                assemble_2x2(subgrid[2][4], subempty, subgrid[5][4], subempty),
                empty, empty)

    nw = assemble_1x3(assemble_2x2(
        subgrid[10][14], subempty, subempty, subempty), empty, empty)
    ne = assemble_1x3(assemble_2x2(
        subempty, subgrid[10][11], subempty, subempty), empty, empty)
    sw = assemble_1x3(assemble_2x2(
        subempty, subempty, subgrid[3][14], subempty), empty, empty)
    se = assemble_1x3(assemble_2x2(
        subempty, subempty, subempty, subgrid[3][11]), empty, empty)

    return [base, n, e, s, w, nw, ne, se, sw]

def load_floor_parts(img):
    subgrid = img.chop_xy(unit=TILE_SIZE2 // 2)
    subempty = Image.blank((TILE_SIZE2 // 2, TILE_SIZE2 // 2), unit=TILE_SIZE2 // 2)

    base = assemble_2x2(subgrid[2][0], subgrid[2][1], subgrid[3][0], subgrid[3][1])

    n = assemble_2x2(subgrid[0][2], subgrid[0][3], subempty, subempty)
    s = assemble_2x2(subempty, subempty, subgrid[1][2], subgrid[1][3])

    e = assemble_2x2(subempty, subgrid[2][3], subempty, subgrid[3][3])
    w = assemble_2x2(subgrid[2][2], subempty, subgrid[3][2], subempty)

    nw = assemble_2x2(subgrid[0][0], subempty, subempty, subempty)
    ne = assemble_2x2(subempty, subgrid[0][1], subempty, subempty)
    sw = assemble_2x2(subempty, subempty, subgrid[1][0], subempty)
    se = assemble_2x2(subempty, subempty, subempty, subgrid[1][1])

    return [base, n, e, s, w, nw, ne, se, sw]
