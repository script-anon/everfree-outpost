import configparser
import io
import json
import os
import tarfile
import time

from flask import Flask, request, jsonify, render_template, \
        flash, redirect, url_for, abort, session, make_response

def read_config():
    config = configparser.ConfigParser()
    config.read_file(open('outpost_bugreport.ini'))

    def get(k, default=None):
        return config['DEFAULT'].get(k, default)

    return {
            'flask_debug': bool(int(get('flask_debug', '0'))),
            'default_redir': get('default_redir'),
            'report_dir': get('report_dir', 'bug-reports'),
            'allowed_origin': get('allowed_origin'),
            }

cfg = read_config()

app = Flask(__name__)
app.debug = bool(cfg['flask_debug'])
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024


@app.route('/')
def index():
    url = cfg['default_redir']
    if url is None:
        return abort(404)
    return redirect(url)

@app.route('/report_bug', methods=['POST'])
def login():
    timestamp = time.strftime('%Y%m%d-%H%M%S')
    player_name = request.form.get('account_name')
    basename = 'report-%s%s' % (timestamp,
            '-%s' % player_name if player_name is not None else '')

    os.makedirs(cfg['report_dir'], exist_ok=True)

    if len(os.listdir(cfg['report_dir'])) >= 1000:
        raise RuntimeError('too many reports already')

    tar = tarfile.open(os.path.join(cfg['report_dir'], basename + '.tar.xz'), 'x:xz')

    def compress(name, content):
        if not isinstance(content, (str, bytes)):
            content = content.read()
        if isinstance(content, str):
            content = content.encode('utf-8')
        content_file = io.BytesIO(content)

        info = tarfile.TarInfo('%s/%s' % (basename, name))
        info.size = len(content)

        tar.addfile(info, content_file)


    info = {
            'time_submitted': time.time(),
            }
    if 'timestamp' in request.form:
        info['time_opened'] = request.form['timestamp']
    if 'account_name' in request.form:
        info['account_name'] = request.form['account_name']
    if 'account_uid' in request.form:
        info['account_uid'] = request.form['account_uid']
    if 'server_url' in request.form:
        info['server_url'] = request.form['server_url']
    compress('info.json', json.dumps(info, indent=4))

    if 'report' in request.form:
        compress('report.txt', request.form['report'])

    if 'screenshot' in request.files:
        compress('screenshot.png', request.files['screenshot'])

    if 'log' in request.files:
        compress('client.log', request.files['log'])

    tar.close()

    resp = make_response('')
    if cfg['allowed_origin'] is not None:
        resp.headers['Access-Control-Allow-Origin'] = cfg['allowed_origin']
    return resp


if __name__ == '__main__':
    app.run(port=5001)
