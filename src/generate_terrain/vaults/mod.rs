use common::util::BitSlice;
use common::util::RegionTree;
use server_types::*;
use terrain_gen_algo::cellular::CellularGrid;


pub mod data;

pub use self::data::{Data, VaultRef};

pub type VaultTree<'a, 'd> = RegionTree<(V2, VaultRef<'d>)>;


fn apply_tree_mask(v: VaultRef, pos: V2, grid: &mut CellularGrid, bounds: Region<V2>) {
    /*
    let v = data.vault(0);
    let pos = V2::new(1, 0) - (v.size() - V2::new(0, 3)).div_floor(scalar(2));
    */
    apply_mask(v, pos, v.tree_mask(), grid, bounds, false);
}

fn apply_junk_mask(v: VaultRef, pos: V2, grid: &mut CellularGrid, bounds: Region<V2>) {
    apply_mask(v, pos, v.junk_mask(), grid, bounds, true);
}

fn apply_mask(v: VaultRef,
              pos: V2,
              mask: &BitSlice,
              grid: &mut CellularGrid,
              bounds: Region<V2>,
              value: bool) {
    let vault_bounds = Region::sized(v.size()) + pos;
    let mask_size = V2::new((v.size().x + 7) & !7, v.size().y);
    let mask_bounds = Region::sized(mask_size) + pos;
    for p in vault_bounds.intersect(bounds).points() {
        if mask.get(mask_bounds.index(p)) {
            grid.set_fixed(p - bounds.min, value);
        }
    }
}

fn apply_structures_one<F: FnMut(V2, &str, V3)>(v: VaultRef,
                                                pos: V2,
                                                bounds: Region<V2>,
                                                mut f: F) {
    let vpos = pos + v.size().div_floor(2);
    for s_def in v.structures() {
        let p = s_def.pos() + pos.extend(0);
        if !bounds.contains(p.reduce()) {
            continue;
        }

        match s_def.name() {
            "torch" => f(vpos, "fence", p),
            "rock_small" => f(vpos, "rock_pile", p),
            "chest_treasure" => f(vpos, "chest", p),

            _ => f(vpos, s_def.name(), p),
        }
    }
}



pub fn apply_tree_masks(vaults: &VaultTree,
                        grid: &mut CellularGrid,
                        bounds: Region<V2>) {
    for (_, &(pos, v)) in vaults.iter_region(bounds) {
        apply_tree_mask(v, pos, grid, bounds);
    }
}

pub fn apply_junk_masks(vaults: &VaultTree,
                        grid: &mut CellularGrid,
                        bounds: Region<V2>) {
    for (_, &(pos, v)) in vaults.iter_region(bounds) {
        apply_junk_mask(v, pos, grid, bounds);
    }
}

pub fn apply_structures<F: FnMut(V2, &str, V3)>(vaults: &VaultTree,
                                                bounds: Region<V2>,
                                                mut f: F) {
    for (_, &(pos, v)) in vaults.iter_region(bounds) {
        apply_structures_one(v, pos, bounds, |vpos, name, pos| f(vpos, name, pos));
    }
}
