use std::collections::HashSet;
use std::hash::{Hash, Hasher};
#[allow(deprecated)] use std::hash::SipHasher;
use rand::{XorShiftRng, Rng, SeedableRng};
use linked_hash_map::LinkedHashMap;

use server_bundle::builder::Builder;
use server_bundle::types::Bundle;
use server_config::Data;
use server_extra::AnyValue;
use server_types::*;
use server_world_types::flags;
use terrain_gen_algo::cellular::CellularGrid;

use points;
use vaults::{self, VaultTree};


#[allow(dead_code)]
struct Cache<K: Hash+Eq, V> {
    map: LinkedHashMap<K, V>,
    limit: usize,
}

#[allow(dead_code)]
impl<K: Hash+Eq, V> Cache<K, V> {
    pub fn new(limit: usize) -> Cache<K, V> {
        Cache {
            map: LinkedHashMap::new(),
            limit: limit,
        }
    }

    fn calc<F>(&mut self, key: K, f: F) -> &V
            where K: Clone, F: Fn(&K) -> V {
        /* TODO: the obvious code gives borrow checker errors (???)
        if let Some(v) = self.map.get_refresh(&key) {
            return v;
        }
        */
        if self.map.contains_key(&key) {
            return self.map.get_refresh(&key).unwrap();
        }

        while self.map.len() >= self.limit {
            self.map.pop_front();
        }

        let value = f(&key);
        self.map.insert(key.clone(), value);
        self.map.get(&key).unwrap()
    }
}


pub struct Context<'d> {
    data: &'d Data,
    vaults: &'d vaults::Data,
    pid: Stable<PlaneId>,
    plane_seed: (u64, u64),
}

const S_TREE_GRID: u32 = 1;
const S_VAULT_POS: u32 = 2;
const S_VAULT_TYPE: u32 = 3;
const S_FOREST_JUNK_POS: u32 = 4;
const S_FOREST_JUNK_TYPE: u32 = 5;
const S_FOREST_LOOT: u32 = 6;

impl<'d> Context<'d> {
    pub fn new(data: &'d Data,
               vaults: &'d vaults::Data,
               pid: Stable<PlaneId>,
               seed: (u64, u64)) -> Context<'d> {
        Context {
            data: data,
            vaults: vaults,
            pid: pid,
            plane_seed: seed,
        }
    }

    #[allow(deprecated)]    // for SipHasher
    fn chunk_rng(&self, cpos: V2, seed: u32) -> XorShiftRng {
        let mut h = SipHasher::new_with_keys(self.plane_seed.0, self.plane_seed.1);
        (cpos, seed).hash(&mut h);
        let k = h.finish();
        XorShiftRng::from_seed([k as u32, 0x12345, (k >> 32) as u32, 0xfedcb])
    }

    fn gen_tree_grid(&self, cpos: V2, vaults: &VaultTree) -> CellularGrid {
        let mut grid = CellularGrid::new((CHUNK_SIZE * 2).into());
        let mut grid_rngs = [
            self.chunk_rng(cpos + V2::new(0, 0), S_TREE_GRID),
            self.chunk_rng(cpos + V2::new(1, 0), S_TREE_GRID),
            self.chunk_rng(cpos + V2::new(0, 1), S_TREE_GRID),
            self.chunk_rng(cpos + V2::new(1, 1), S_TREE_GRID),
        ];
        let quadrant_bounds = Region::<V2>::sized(2);
        grid.init(|p| {
            let quadrant = p.div_floor(CHUNK_SIZE);
            let idx = quadrant_bounds.index(quadrant);
            grid_rngs[idx].gen_range(0, 10) < 5
        });

        let chunk_bounds = Region::sized(CHUNK_SIZE) + cpos * CHUNK_SIZE;
        let grid_bounds = chunk_bounds.expand((CHUNK_SIZE / 2).into());
        vaults::apply_tree_masks(vaults, &mut grid, grid_bounds);

        for _ in 0 .. 5 {
            grid.step(|here, active, total| 2 * (here as u8 + active) > total);
        }

        grid
    }

    fn gen_no_junk_grid(&self, cpos: V2, vaults: &VaultTree) -> CellularGrid {
        let mut grid = CellularGrid::new(CHUNK_SIZE.into());
        grid.init(|_| false);

        let chunk_bounds = (Region::sized(1) + cpos) * CHUNK_SIZE;
        vaults::apply_junk_masks(vaults, &mut grid, chunk_bounds);

        grid
    }

    fn gen_vault_map(&self, cpos: V2) -> VaultTree {
        const SUPER_CHUNK_SIZE: i32 = 16;
        // Radius 5 makes sure we get all vaults that might be within compass range (4 chunks).
        let region = Region::around(cpos, 5);
        let region_super = region.div_round_signed(SUPER_CHUNK_SIZE);
        let mut tree = VaultTree::new();

        for super_pos in region_super.points() {
            let mut rng = self.chunk_rng(super_pos, S_VAULT_TYPE);
            let points = self.scatter_points(S_VAULT_POS,
                                             8 * CHUNK_SIZE,
                                             super_pos,
                                             SUPER_CHUNK_SIZE * CHUNK_SIZE,
                                             2);

            for p in points {
                // 30% chance of generating nothing
                if rng.gen_range(0, 10) < 3 {
                    continue;
                }
                if p.abs().max() <= 16 {
                    // Don't place anything too close to spawn
                    continue;
                }

                let name = "forest_loot";   // TODO: randomize
                info!("placed {} at {:?}", name, p);
                let id = self.vaults.vault_id(name);
                let v = self.vaults.vault(id);
                let bounds = Region::sized(v.size()) + p;
                tree.insert(bounds, (p, v));
            }
        }

        if region_super.contains((-1).into()) {
            let p = V2::new(-5, -4);
            let id = self.vaults.vault_id("forest_entry");
            let v = self.vaults.vault(id);
            let bounds = Region::sized(v.size()) + p;
            tree.insert(bounds, (p, v));
        }

        tree
    }

    fn scatter_points(&self,
                      seed: u32,
                      spacing: i32,
                      center: V2,
                      chunk_size: i32,
                      iters: usize) -> Vec<V2> {
        points::scatter_points(|cpos| self.chunk_rng(cpos, seed),
                               spacing,
                               center,
                               chunk_size,
                               iters)
    }

    pub fn generate(&self, cpos: V2) -> Bundle {
        // Compute tree placement
        let vaults = self.gen_vault_map(cpos);
        let tree_grid = self.gen_tree_grid(cpos, &vaults);
        let mut no_junk_grid = self.gen_no_junk_grid(cpos, &vaults);

        let mut b = Builder::new(self.data);

        let chunk_bounds = (Region::sized(1) + cpos) * CHUNK_SIZE;
        {
            let mut tc = b.terrain_chunk();
            tc.stable_plane(self.pid);
            tc.cpos(cpos);

            // Initialize base terrain
            for pos in Region::<V3>::sized(CHUNK_SIZE).points() {
                if pos.z == 0 {
                    tc.block(pos, "grass");
                } else {
                    tc.block(pos, "empty");
                }
            }

            // Place trees
            for pos in Region::<V2>::sized(CHUNK_SIZE).points() {
                // Place a tree at x,y if the tree mask bit for x+1,y is set.  Since the tree is
                // three tiles wide, this makes the tree trunk line up with the mask bit.
                if (pos.x + pos.y + 1) % 2 == 0 {
                    continue;
                }
                if !tree_grid.get(pos + V2::new(1, 0) + CHUNK_SIZE / 2) {
                    continue;
                }
                tc.structure(|s| {
                    s.stable_plane(self.pid)
                     .pos((pos + chunk_bounds.min).extend(0))
                     .template("tree");
                });
            }

            // Place vault structures
            let chunk_bounds = (Region::sized(1) + cpos) * CHUNK_SIZE;
            let mut loot_rng = self.chunk_rng(cpos, S_FOREST_LOOT);
            vaults::apply_structures(&vaults, chunk_bounds, |vpos, name, pos| {
                tc.structure(|s| {
                    s.stable_plane(self.pid)
                     .pos(pos)
                     .template(name);
                    if name == "chest" || name == "loot_chest" {
                        // Override structure type to loot_chest, which has special behavior
                        s.template("loot_chest");

                        let (item, count) = *loot_rng.choose(&[
                            ("wood", 50),
                            ("stone", 50),
                            //("ore/copper", 15),
                            //("ore/iron", 3),
                            //("hat/witch", 1),
                            //("hat/santa", 1),
                        ]).unwrap();
                        let glass_count = loot_rng.gen_range(2, 4);
                        info!("placing chest at {:?} with contents {} * {}", pos, item, count);
                        let iid = {
                            let mut i = s.inventory_();
                            i.size(30)
                             .item(0, item, count)
                             .item(1, "glass", glass_count)
                             .flags(flags::I_PUBLIC);
                            i.id()
                        };

                        s.extra_().set("contents", iid.into_value());
                        s.extra_().set("parent_vault", vpos.into_value());
                    } else if name == "workbench" {
                        let iid = {
                            let mut i = s.inventory_();
                            i.size(6)
                             .flags(flags::I_PUBLIC);
                            i.id()
                        };
                        s.extra_().set("contents", iid.into_value());
                    }
                });
            });

            // Record nearby vault positions
            {
                let compass_targets = vaults.iter()
                    .map(|(_, &(pos, ref v))| pos + v.size().div_floor(2))
                    .collect::<HashSet<_>>();
                let mut e = tc.extra_().set_array("compass_targets");
                for pos in compass_targets {
                    e.borrow().push(pos.into_value());
                }
            }

            // Place random junk
            let mut rng = self.chunk_rng(cpos, S_FOREST_JUNK_TYPE);
            for p in self.scatter_points(S_FOREST_JUNK_POS, 4, cpos, CHUNK_SIZE, 2) {
                let offset = p - chunk_bounds.min;

                if tree_grid.get(offset + CHUNK_SIZE / 2) ||
                   no_junk_grid.get(offset) {
                    continue;
                }
                // TODO: bit of a hack - we need this to avoid placing two structures in the same
                // place, since scatter_points with small spacing sometimes emits the same point
                // twice
                no_junk_grid.set(offset, true);

                let template = *rng.choose(&["rock", "rock_pile", "bush", "sapling"]).unwrap();

                tc.structure(|s| {
                    s.stable_plane(self.pid)
                     .pos(p.extend(0))
                     .template(template);
                });
            }
        }

        b.finish()
    }
}
