use std::cell::Cell;

use outpost_ui::event::{MouseEvent, UIResult};
use outpost_ui::geom::{Point, Rect};
use outpost_ui::widget::Widget;
use outpost_ui::widgets::container::{Grid, ChildWidget, GenWidgets};

use ui2::atlas::Border;
use ui2::context::Context;


pub type Color = (u8, u8, u8);

pub struct Swatch<'a> {
    palette: &'a [Color],
    index: usize,
}

impl<'a> Swatch<'a> {
    pub fn new(palette: &'a [Color],
               index: usize) -> Swatch<'a> {
        Swatch {
            palette: palette,
            index: index,
        }
    }
}

impl<'a, Ctx: Context> Widget<Ctx> for Swatch<'a> {
    type Event = ();

    fn min_size(&self, _ctx: &Ctx) -> Point {
        Point::new(12, 12)
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        let down = ctx.mouse_pressed_over() && ctx.mouse_over();
        ctx.draw_border(if down { Border::Frame } else { Border::FrameInv });

        let bounds = Rect::sized(ctx.cur_bounds().size()).inset(2, 2, 2, 2);
        ctx.with_bounds(bounds, |ctx| {
            ctx.draw_special_palette_color(self.palette, self.index);
        });
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        match evt {
            MouseEvent::Up(_) if ctx.mouse_pressed_over() => UIResult::Event(()),
            MouseEvent::Up(_) | MouseEvent::Down(_) => UIResult::NoEvent,
            _ => UIResult::Unhandled,
        }
    }
}


pub struct SwatchColorPicker<'a> {
    palette: &'a [Color],
}

impl<'a> SwatchColorPicker<'a> {
    pub fn new(palette: &'a [Color]) -> SwatchColorPicker<'a> {
        SwatchColorPicker {
            palette: palette,
        }
    }

    fn with_inner<Ctx, F, R>(&self, f: F) -> R
        where Ctx: Context,
              F: FnOnce(&Widget<Ctx, Event=usize>) -> R {
        let palette = self.palette;
        let contents = GenWidgets::new(0 .. palette.len(), move |idx| {
            ChildWidget::new(Swatch::new(palette, idx), move |()| idx)
        });
        let focus = Cell::new(0);

        let w = Grid::new(&focus, (palette.len() + 2) / 3, contents).spacing(2);
        f(&w)
    }
}

impl<'a, Ctx: Context> Widget<Ctx> for SwatchColorPicker<'a> {
    type Event = (u8, u8, u8);

    fn min_size(&self, ctx: &Ctx) -> Point {
        self.with_inner(|w| w.min_size(ctx))
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        self.with_inner(|w| w.on_paint(ctx));
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        self.with_inner(|w| w.on_mouse(ctx, evt)).map(|idx| self.palette[idx])
    }
}
