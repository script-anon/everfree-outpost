use common::types::*;
use std::u16;

use platform::{Config, ConfigKey};


pub const NUM_FRAMES: usize = 128;

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum DisplayMode {
    None = 0,
    Framerate = 1,
    Full = 2,
}

impl DisplayMode {
    pub fn from_primitive(x: i32) -> Option<DisplayMode> {
        Some(match x {
            0 => DisplayMode::None,
            1 => DisplayMode::Framerate,
            2 => DisplayMode::Full,
            _ => return None,
        })
    }

    pub fn next(self) -> DisplayMode {
        Self::from_primitive(self as i32 + 1).unwrap_or(DisplayMode::None)
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub struct FrameTimes {
    pub advance: u16,
    pub predict: u16,
    pub prepare: u16,
    pub render: u16,
    pub display: u16,
    pub total: u16,
    pub interval: u16,
}

impl FrameTimes {
    pub fn new() -> FrameTimes {
        FrameTimes {
            advance: 0,
            predict: 0,
            prepare: 0,
            render: 0,
            display: 0,
            total: 0,
            interval: 0,
        }
    }
}


pub struct Timer<'a> {
    dest: &'a mut u16,
    start: Time,
}

impl<'a> Timer<'a> {
    pub fn start(dest: &'a mut u16, now: Time) -> Timer<'a> {
        Timer { dest, start: now }
    }

    pub fn end(self, now: Time) {
        let interval = now - self.start;
        if interval > u16::MAX as Time {
            *self.dest = u16::MAX;
        } else {
            *self.dest = interval as u16;
        }
    }
}


pub struct Debug {
    pub mode: DisplayMode,

    pub frame_times: [FrameTimes; NUM_FRAMES],
    pub cur_frame: usize,
    pub total_interval: usize,

    pub last_time: Time,

    pub timing_low: Time,
    pub timing_mid: Time,
    pub timing_high: Time,
    pub timing_highest: Time,
    pub timing_samples: usize,

    pub timing_delta: Time,
    pub timing_skew_ticks: u32,

    pub pos: V3,
    pub day_time: u16,
    pub day_phase: u8,

    pub mouse_hit_pos: V3,
    pub mouse_hit_structure: Option<u32>,
    pub mouse_near_pawn: bool,

    pub mem_free: usize,
    pub mem_frags: usize,

    pub state_pending_count: usize,
    pub state_pending_time: Time,

    pub predict_pending: usize,
    pub predict_acknowledged: usize,
    pub predict_next_pending_delta: Time,
    pub predict_last_ack_delay: Time,
}

impl Debug {
    pub fn new() -> Debug {
        Debug {
            mode: DisplayMode::None,

            frame_times: [FrameTimes::new(); NUM_FRAMES],
            cur_frame: 0,
            total_interval: 0,

            last_time: 0,

            timing_low: 0,
            timing_mid: 0,
            timing_high: 0,
            timing_highest: 0,
            timing_samples: 0,

            timing_delta: 0,
            timing_skew_ticks: 0,

            pos: scalar(0),
            day_time: 0,
            day_phase: 0,

            mouse_hit_pos: scalar(-1),
            mouse_hit_structure: None,
            mouse_near_pawn: false,

            mem_free: 0,
            mem_frags: 0,

            state_pending_count: 0,
            state_pending_time: 0,

            predict_pending: 0,
            predict_acknowledged: 0,
            predict_next_pending_delta: 0,
            predict_last_ack_delay: 0,
        }
    }

    pub fn init<C: Config>(&mut self, cfg: &C) {
        self.mode = DisplayMode::from_primitive(cfg.get_int(ConfigKey::DebugShowPanel))
            .unwrap_or(DisplayMode::None);
    }

    /// Record preparation time for the current frame.
    pub fn record_frame_times(&mut self,
                              now: Time,
                              frame_times: FrameTimes) {
        let mut frame_times = frame_times;
        let interval = now - self.last_time;
        if interval > u16::MAX as Time {
            frame_times.interval = u16::MAX;
        } else {
            frame_times.interval = interval as u16;
        }

        self.total_interval -= self.frame_times[self.cur_frame].interval as usize;
        self.total_interval += frame_times.interval as usize;

        self.frame_times[self.cur_frame] = frame_times;
        self.cur_frame = (self.cur_frame + 1) % NUM_FRAMES;
        self.last_time = now;

    }

    pub fn average_frame_times(&self) -> FrameTimes {
        macro_rules! average {
            ($($field:ident),*) => {
                FrameTimes {
                    $( $field: {
                        let sum: u32 = self.frame_times.iter().map(|ft| ft.$field as u32).sum();
                        (sum / NUM_FRAMES as u32) as u16
                    }, )*
                }
            };
        }
        average!(advance, predict, prepare, render, display, total, interval)
    }

    #[cfg(asmjs)]
    pub fn update_memory_stats(&mut self) {
        /* FIXME
        extern "Rust" {
            fn asmmalloc_get_stats() -> (usize, usize);
        }
        let (free, frags) = unsafe { asmmalloc_get_stats() };
        self.mem_free = free;
        self.mem_frags = frags;
        */
    }

    #[cfg(not(asmjs))]
    pub fn update_memory_stats(&mut self) {
    }

    pub fn show_full(&self) -> bool {
        self.mode == DisplayMode::Full
    }
}

