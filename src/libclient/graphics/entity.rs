use common::types::*;
use common::{Activity, Appearance};
use common::appearance::{APP_WING, APP_HORN, APP_MALE};

use data::data;
use state::entity::{Entities, Entity};
use ui2::atlas::{Font, UIDataExt};
use util;

use graphics::GeometryGenerator;


#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct Vertex {
    // 0
    dest_pos: (u16, u16),
    src_pos: (u16, u16),
    sheet: i8,
    color: (u8, u8, u8),

    // 12
    ref_pos: (u16, u16, u16),
    ref_size_z: u16,

    // 20
    anim_length: u16,
    anim_rate: u16,
    anim_start: u16,
    anim_step: u16,

    // 28
}


#[derive(Clone)]
pub struct GeomGen<'a> {
    entities: &'a Entities,
    render_names: bool,
    bounds: Region<V2>,
    now: Time,
}

impl<'a> GeomGen<'a> {
    pub fn new(entities: &'a Entities,
               render_names: bool,
               chunk_bounds: Region<V2>,
               now: Time) -> GeomGen<'a> {
        let bounds = chunk_bounds * CHUNK_SIZE * TILE_SIZE;
        let bounds = Region::new(bounds.min - 128,
                                 bounds.max);

        GeomGen {
            entities: entities,
            render_names: render_names,

            bounds: bounds,
            now: now,
        }
    }
}

fn activity_anim(act: &Activity) -> AnimId {
    let data = data();

    match act {
        &Activity::Stand { dir, .. } => {
            data.physics_anim_table()[0][dir as usize]
        },
        &Activity::Walk { dir, .. } => {
            data.physics_anim_table()[1][dir as usize]
        },
    }
}

// Hacky adjustment.  `pos` is the raw entity position, but we need the center.
const ADJUST_X: i32 = 8;
const ADJUST_Y: i32 = 12;

// Note that NAME_Y/Z is the offset of the *top* of the text.
const NAME_Y: i32 = 0;
const NAME_Z: i32 = 34;

// How tall a sprite is considered to be in 3D space.
const SIZE_Z: i32 = 32;

pub fn render_entity_layers<F: FnMut(Vertex)>(e: &Entity,
                                              pos: V3,
                                              mut emit: F) {
    let data = data();
    let anim_id = activity_anim(&e.activity);

    let pos = pos + V3::new(ADJUST_X, ADJUST_Y, 0);
    let pos_2d = V2::new(pos.x, pos.y - pos.z);

    for_each_layer(&e.appearance, |layer_table_idx, color| {
        let layer_idx = data.pony_layer_table()[layer_table_idx];
        // TODO: remove this check once the layer list is computed correctly for new sprite data
        if layer_idx == 255 {
            return;
        }
        let l = data.pony_layer(layer_idx);
        let a = l.anim(anim_id);
        let s = a.sprite();

        for &(cx, cy) in &[(0, 0), (1, 0), (1, 1), (0, 0), (1, 1), (0, 1)] {
            let c = V2::new(cx, cy);
            let flip_vec = if a.flip() { V2::new(-1, 1) } else { V2::new(1, 1) };
            let dest_pos = pos_2d + (s.dest_pos() + c * s.size()) * flip_vec;
            let src_pos = s.src_pos() + c * s.size();

            emit(Vertex {
                dest_pos: (dest_pos.x as u16, dest_pos.y as u16),
                src_pos: (src_pos.x as u16, src_pos.y as u16),
                sheet: s.sheet as i8,
                color: color,

                ref_pos: (pos.x as u16, pos.y as u16, pos.z as u16),
                ref_size_z: SIZE_Z as u16,

                anim_length: s.anim_length() as u16,
                anim_rate: s.anim_rate as u16,
                anim_start: (e.activity_start % 55440) as u16,
                anim_step: s.anim_step,
            });
        }
    });
}

fn render_entity_name<F: FnMut(Vertex)>(e: &Entity, pos: V3, mut emit: F) {
    if e.name.len() == 0 {
        return;
    }

    let data = data();
    let font = data.font(Font::Default);

    let width = {
        let g = unwrap_or!(font.iter_glyphs(&e.name).last());
        g.x_offset + g.src.size().x
    };

    let pos = pos + V3::new(ADJUST_X, ADJUST_Y, 0);
    let pos_2d = V2::new(pos.x, (pos.y + NAME_Y) - (pos.z + NAME_Z));

    for g in font.iter_glyphs(&e.name) {
        let g_src = g.src.min;
        let g_dest = V2::new(g.x_offset - width / 2, 0);
        let g_size = g.src.size();

        for &(cx, cy) in &[(0, 0), (1, 0), (1, 1), (0, 0), (1, 1), (0, 1)] {
            let c = V2::new(cx, cy);
            let dest_pos = pos_2d + (g_dest + c * g_size);
            let src_pos = g_src + c * g_size;

            emit(Vertex {
                dest_pos: (dest_pos.x as u16, dest_pos.y as u16),
                src_pos: (src_pos.x as u16, src_pos.y as u16),
                sheet: -1,
                color: (255, 255, 255),

                ref_pos: (pos.x as u16, pos.y as u16, pos.z as u16),
                ref_size_z: SIZE_Z as u16,

                anim_length: 1,
                anim_rate: 1,
                anim_start: 0,
                anim_step: 0,
            });
        }
    }
}

impl<'a> GeometryGenerator for GeomGen<'a> {
    type Vertex = Vertex;

    fn generate<F: FnMut(Vertex)>(&mut self, mut emit: F) {
        const MASK: i32 = (1 << (TILE_BITS + CHUNK_BITS + LOCAL_BITS)) - 1;
        for e in self.entities.iter_z_order() {
            let pos = e.pos(self.now);
            let pos = util::wrap_base(pos, self.bounds.min.extend(0), scalar(MASK));
            if !self.bounds.contains(pos.reduce()) {
                // Not visible
                continue;
            }

            render_entity_layers(e, pos, |v| emit(v));

            if self.render_names {
                render_entity_name(e, pos, |v| emit(v));
            }
        }
    }
}


#[derive(Clone)]
pub struct SingleGeomGen<'a> {
    entity: &'a Entity,
    pos: V3,
}

impl<'a> SingleGeomGen<'a> {
    pub fn new(entity: &'a Entity,
               pos: V3) -> SingleGeomGen<'a> {
        SingleGeomGen {
            entity, pos,
        }
    }
}

impl<'a> GeometryGenerator for SingleGeomGen<'a> {
    type Vertex = Vertex;

    fn generate<F: FnMut(Vertex)>(&mut self, mut emit: F) {
        render_entity_layers(self.entity, self.pos, |v| emit(v));
    }
}


fn for_each_layer<F: FnMut(usize, (u8, u8, u8))>(app: &Appearance, mut f: F) {
    //let white = (0xff, 0xff, 0xff);
    let offset = if app.flags.contains(APP_MALE) { 1 } else { 0 };

    let mut go = |x, color| {
        f(x * 2 + offset, color)
    };

    if app.flags.contains(APP_WING) {
        go(3, app.coat_color);
    }
    go(0, app.coat_color); // base
    /* TODO
    if equip1 != 0 {
        go(15 + equip1 as usize - 1, white);    // socks
    }
    */
    if app.flags.contains(APP_WING) {
        go(2, app.coat_color);
    }
    // TODO: add eye color
    go(4, app.coat_color); // eyes
    go(8 + app.tail as usize, app.mane_color);
    go(5 + app.mane as usize, app.mane_color);
    if app.flags.contains(APP_HORN) {
        go(1, app.coat_color);
    }
    /* TODO
    if equip0 != 0 {
        go(11 + equip0 as usize - 1, white);    // hat
    }
    */
}
