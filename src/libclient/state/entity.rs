use common::types::*;
use std::collections::btree_map::{self, BTreeMap};
use std::collections::VecDeque;
use std::marker::PhantomData;
use std::mem;
use std::ops::Index;
use std::ptr;
use common::{Activity, Appearance};

use super::{Delta, Log};


#[derive(Clone)]
pub struct Entity {
    // Kind of redundant to include `id` here, but we need it to ensure stable sorting.
    pub id: EntityId,

    pub activity: Activity,
    pub activity_start: Time,
    pub appearance: Appearance,
    pub name: String,

    z_next: *mut Entity,
}

impl Entity {
    /// Constructor for building dummy entities.  This is useful for rendering entities that aren't
    /// actually present in the world state.
    pub fn dummy(activity: Activity,
                 activity_start: Time,
                 appearance: Appearance,
                 name: String) -> Entity {
        Entity {
            id: EntityId(0),
            activity: activity,
            activity_start: activity_start,
            appearance: appearance,
            name: name,
            z_next: ptr::null_mut(),
        }
    }

    pub fn pos(&self, now: Time) -> V3 {
        self.activity.pos(self.activity_start, now)
    }

    pub fn display_copy(&self) -> Entity {
        Entity {
            id: EntityId(0),

            activity: self.activity.clone(),
            activity_start: self.activity_start,
            appearance: self.appearance,
            name: String::new(),

            z_next: ptr::null_mut(),
        }
    }
}

#[derive(Clone)]
pub struct Entities {
    map: BTreeMap<EntityId, Entity>,
    queue: VecDeque<(EntityId, Box<Activity>, Time)>,

    /// We maintain an intrusive linked list that presents the entities sorted by `y + z` (in other
    /// words, rendering depth).
    z_list: *mut Entity,
}

impl Entities {
    pub fn new() -> Entities {
        Entities {
            map: BTreeMap::new(),
            queue: VecDeque::new(),

            z_list: ptr::null_mut(),
        }
    }

    pub fn len(&self) -> usize {
        self.map.len()
    }

    fn invalidate_z_list(&mut self) {
        self.z_list = ptr::null_mut();
    }

    pub fn clear(&mut self) {
        self.map.clear();
        self.queue.clear();
        self.invalidate_z_list();
    }

    pub fn appear<L>(&mut self,
                     id: EntityId,
                     appearance: Appearance,
                     name: String,
                     log: &mut L)
            where L: Log<Delta> {
        if self.map.contains_key(&id) {
            error!("desync: EntityAppear for existing {:?}", id);
            return;
        }

        // Insertions may cause elements to move around
        self.invalidate_z_list();

        self.map.insert(id, Entity {
            id: id,
            activity: Activity::default(),
            activity_start: 0,
            appearance: appearance,
            name: name,
            z_next: ptr::null_mut(),
        });
        log.record(Delta::EntityGone(id));
    }

    pub fn change<L>(&mut self,
                     id: EntityId,
                     appearance: Appearance,
                     name: String,
                     log: &mut L)
            where L: Log<Delta> {
        let e = unwrap_or_error!(self.map.get_mut(&id),
                                 "desync: EntityChange for nonexistent {:?}", id);

        let old_appearance = mem::replace(&mut e.appearance, appearance);
        let old_name = mem::replace(&mut e.name, name);
        log.record(Delta::EntityChange(id, Box::new(old_appearance), Box::new(old_name)));
    }

    pub fn gone<L>(&mut self,
                   id: EntityId,
                   log: &mut L)
            where L: Log<Delta> {
        // Removals may cause elements to move around
        self.invalidate_z_list();

        let old_e = unwrap_or_error!(self.map.remove(&id),
                                     "desync: EntityGone for nonexistent {:?}", id);

        log.record(Delta::EntityAppear(id, Box::new(old_e.appearance), Box::new(old_e.name)));
    }


    pub fn push_activity<L>(&mut self,
                            id: EntityId,
                            act: Box<Activity>,
                            start: Time,
                            log: &mut L)
            where L: Log<Delta> {
        self.queue.push_back((id, act, start));
        log.record(Delta::EntityActivityUnpush);
    }

    pub fn unpush_activity<L>(&mut self, log: &mut L)
            where L: Log<Delta> {
        let (id, act, start) = unwrap_or_error!(self.queue.pop_back(),
                                                "desync: unpush_activity with empty queue");
        log.record(Delta::EntityActivityPush(id, act, start));
    }

    pub fn pop_activity<L>(&mut self, log: &mut L)
            where L: Log<Delta> {
        let (id, act, start) = unwrap_or_error!(self.queue.pop_front(),
                                                "desync: pop_activity with empty queue");
        log.record(Delta::EntityActivityUnpop(id, act, start));
    }

    pub fn unpop_activity<L>(&mut self,
                             id: EntityId,
                             act: Box<Activity>,
                             start: Time,
                             log: &mut L)
            where L: Log<Delta> {
        self.queue.push_front((id, act, start));
        log.record(Delta::EntityActivityPop);
    }

    pub fn apply_activity<L>(&mut self, log: &mut L)
            where L: Log<Delta> {
        let (id, act, start) = unwrap_or_error!(self.queue.pop_front(),
                                                "desync: pop_activity with empty queue");
        self.set_activity(id, (*act).clone(), start, log);
        log.record(Delta::EntityActivityUnpop(id, act, start));
    }

    pub fn set_activity<L>(&mut self,
                           id: EntityId,
                           activity: Activity,
                           activity_start: Time,
                           log: &mut L)
            where L: Log<Delta> {
        self.invalidate_z_list();

        let e = unwrap_or_error!(self.map.get_mut(&id),
                                 "desync: EntityAct for nonexistent {:?}", id);

        let old_act = mem::replace(&mut e.activity, activity);
        let old_start = mem::replace(&mut e.activity_start, activity_start);

        log.record(Delta::EntityActivitySet(id, Box::new(old_act), old_start));
    }


    pub fn set_name(&mut self,
                    id: EntityId,
                    name: String) -> Option<String> {
        self.map.get_mut(&id).map(|e| mem::replace(&mut e.name, name))
    }


    pub fn advance<L>(&mut self, time: Time, log: &mut L)
            where L: Log<Delta> {
        while self.queue.front().map_or(false, |&(_,_,t)| t <= time) {
            self.apply_activity(log);
        }
    }


    pub fn get(&self, id: EntityId) -> Option<&Entity> {
        self.map.get(&id)
    }

    pub fn iter(&self) -> Iter {
        self.map.iter()
    }


    fn sort_full<F: Fn(&Entity) -> i32>(&mut self, calc_z: F) {
        let mut items = self.map.values().collect::<Vec<_>>();
        items.sort_by_key(|&e| (calc_z(e), e.id));
        unsafe {
            // The address of the `next` pointer of the previous entry.
            let mut last_ptr = &mut self.z_list;
            for e in items {
                let ptr = e as *const Entity as *mut Entity;
                *last_ptr = ptr;
                last_ptr = &mut (*ptr).z_next; 
            }
            *last_ptr = ptr::null_mut();
        }
    }

    fn check_sorted<F: Fn(&Entity) -> i32>(&mut self, calc_z: F) -> bool {
        if self.z_list.is_null() {
            if self.map.is_empty() {
                return true;
            } else {
                return false;
            }
        }

        unsafe {
            let mut last_key = (calc_z(&*self.z_list), (*self.z_list).id);
            let mut cur = (*self.z_list).z_next;
            while !cur.is_null() {
                let cur_key = (calc_z(&*cur), (*cur).id);
                if cur_key < last_key {
                    return false;
                }
                last_key = cur_key;
                cur = (*cur).z_next;
            }

            true
        }
    }

    pub fn update_z_order<F: Fn(&Entity) -> i32>(&mut self, calc_z: F) {
        if !self.check_sorted(|e| calc_z(e)) {
            self.sort_full(calc_z);
        }
    }

    pub fn iter_z_order(&self) -> ZOrderIter {
        ZOrderIter {
            cur: self.z_list,
            _marker: PhantomData,
        }
    }
}

pub type Iter<'a> = btree_map::Iter<'a, EntityId, Entity>;

impl Index<EntityId> for Entities {
    type Output = Entity;
    fn index(&self, idx: EntityId) -> &Entity {
        self.map.get(&idx).expect("no entry for given key")
    }
}

pub struct ZOrderIter<'a> {
    cur: *mut Entity,
    _marker: PhantomData<&'a Entity>,
}

impl<'a> Iterator for ZOrderIter<'a> {
    type Item = &'a Entity;

    fn next(&mut self) -> Option<&'a Entity> {
        if self.cur.is_null() {
            None
        } else {
            unsafe {
                let e = self.cur;
                self.cur = (*e).z_next;
                Some(&*e)
            }
        }
    }
}
