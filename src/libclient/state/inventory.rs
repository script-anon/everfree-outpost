use std::prelude::v1::*;
use common::types::*;
use std::collections::BTreeMap;
use std::mem;
use std::ops::Index;

use super::{Delta, Log};


#[derive(Clone, Copy, Debug, Hash)]
pub struct Item {
    pub id: ItemId,
    pub quantity: u8,
}

impl Item {
    pub fn new(id: ItemId, quantity: u8) -> Item {
        Item {
            id: id,
            quantity: quantity,
        }
    }
}

#[derive(Clone, Hash)]
pub struct Inventory {
    pub items: Box<[Item]>,
    pub id: InventoryId,
    pub flags: u32,
}

const I_PUBLIC: u32 = 0x00000004;

impl Inventory {
    // TODO: this is pub for UI testing only.  make private when testing is done.
    pub fn new(id: InventoryId, items: Box<[Item]>, flags: u32) -> Inventory {
        Inventory { items, id, flags }
    }

    pub fn len(&self) -> usize {
        self.items.len()
    }

    pub fn count(&self, item_id: ItemId) -> u16 {
        let mut count = 0;
        for i in &*self.items {
            if i.id == item_id {
                count += i.quantity as u16;
            }
        }
        count
    }

    pub fn is_public(&self) -> bool {
        self.flags & I_PUBLIC != 0
    }
}

#[derive(Clone)]
pub struct Inventories {
    map: BTreeMap<InventoryId, Inventory>,
    main_inv_id: Option<InventoryId>,
    ability_inv_id: Option<InventoryId>,
}

impl Inventories {
    pub fn new() -> Inventories {
        Inventories {
            map: BTreeMap::new(),
            main_inv_id: None,
            ability_inv_id: None,
        }
    }

    pub fn appear<L>(&mut self, id: InventoryId, items: Box<[Item]>, flags: u32, log: &mut L)
            where L: Log<Delta> {
        if self.map.contains_key(&id) {
            error!("desync: InventoryAppear for existing {:?}", id);
            return;
        }

        self.map.insert(id, Inventory::new(id, items, flags));
        log.record(Delta::InventoryGone(id));
    }

    pub fn gone<L>(&mut self, id: InventoryId, log: &mut L)
            where L: Log<Delta> {
        let inv = unwrap_or_error!(self.map.remove(&id),
                                   "desync: InventoryGone for nonexistent {:?}", id);
        log.record(Delta::InventoryAppear(id, inv.items, inv.flags));
    }

    pub fn contents<L>(&mut self, id: InventoryId, items: Box<[Item]>, log: &mut L)
            where L: Log<Delta> {
        let inv = unwrap_or_error!(self.map.get_mut(&id),
                                   "desync: InventoryContents for nonexistent {:?}", id);

        let old_items = mem::replace(&mut inv.items, items);
        log.record(Delta::InventoryContents(id, old_items));
    }

    pub fn flags<L>(&mut self, id: InventoryId, flags: u32, log: &mut L)
            where L: Log<Delta> {
        let inv = unwrap_or_error!(self.map.get_mut(&id),
                                   "desync: InventoryContents for nonexistent {:?}", id);

        let old_flags = mem::replace(&mut inv.flags, flags);
        log.record(Delta::InventoryFlags(id, old_flags));
    }

    pub fn update<L>(&mut self,
                     id: InventoryId,
                     slot: usize,
                     item: Item,
                     log: &mut L)
            where L: Log<Delta> {
        let inv = unwrap_or_error!(self.map.get_mut(&id),
                                   "desync: InventoryUpdate for nonexistent {:?}", id);
        let old_item = mem::replace(&mut inv.items[slot], item);
        log.record(Delta::InventoryUpdate(id, slot, old_item));
    }

    pub fn clear(&mut self) {
        self.map.clear();
        self.main_inv_id = None;
        self.ability_inv_id = None;
    }

    pub fn get(&self, id: InventoryId) -> Option<&Inventory> {
        self.map.get(&id)
    }


    pub fn main_inventory(&self) -> Option<&Inventory> {
        self.main_inv_id.map(|x| &self[x])
    }

    pub fn main_id(&self) -> Option<InventoryId> {
        self.main_inv_id
    }

    pub fn set_main_id<L>(&mut self, id: Option<InventoryId>, log: &mut L)
            where L: Log<Delta> {
        let old_id = mem::replace(&mut self.main_inv_id, id);
        log.record(Delta::InventorySetMainId(old_id));
    }

    pub fn ability_inventory(&self) -> Option<&Inventory> {
        self.ability_inv_id.map(|x| &self[x])
    }

    pub fn ability_id(&self) -> Option<InventoryId> {
        self.ability_inv_id
    }

    pub fn set_ability_id<L>(&mut self, id: Option<InventoryId>, log: &mut L)
            where L: Log<Delta> {
        let old_id = mem::replace(&mut self.ability_inv_id, id);
        log.record(Delta::InventorySetAbilityId(old_id));
    }
}

impl Index<InventoryId> for Inventories {
    type Output = Inventory;
    fn index(&self, idx: InventoryId) -> &Inventory {
        self.map.get(&idx).expect("no entry for given key")
    }
}
