use common::types::*;
use std::collections::HashSet;

use super::{Delta, Log};


pub const TERRAIN_BOUNDS: Region<V3> = Region {
    min: V3 {
        x: 0,
        y: 0,
        z: 0,
    },
    max: V3 {
        x: CHUNK_SIZE * LOCAL_SIZE,
        y: CHUNK_SIZE * LOCAL_SIZE,
        z: CHUNK_SIZE,
    },
};

const TOTAL_BLOCKS: usize = 1 << (2 * LOCAL_BITS + 3 * CHUNK_BITS);

pub struct TerrainChunks {
    blocks: Box<[BlockId; TOTAL_BLOCKS]>,
    change: HashSet<u8>,
}

impl TerrainChunks {
    pub fn new() -> TerrainChunks {
        let blocks = box [0; TOTAL_BLOCKS];
        TerrainChunks {
            blocks: blocks,
            change: HashSet::new(),
        }
    }

    pub fn decode_into<L>(&mut self, cpos_u8: (u8, u8), input: &[u16], log: &mut L)
            where L: Log<Delta> {
        let cpos = V2::new(cpos_u8.0 as i32,
                           cpos_u8.1 as i32);
        let cpos = cpos & LOCAL_MASK;
        let chunk_bounds = (Region::sized(1) + cpos.extend(0)) * CHUNK_SIZE;

        let encoded = chunk_bounds.points()
                                  .map(|p| self.blocks[TERRAIN_BOUNDS.index(p)])
                                  .collect::<Vec<_>>()
                                  .into_boxed_slice();
        let cpos_u8 = (cpos.x as u8, cpos.y as u8);
        log.record(Delta::TerrainChunkBlocks(cpos_u8, encoded));

        let mut decoded = rle16_decode(input.iter().map(|&x| x));

        for p in chunk_bounds.points() {
            let block = decoded.next().expect("not enough blocks in decoded chunk");
            let idx = TERRAIN_BOUNDS.index(p);
            self.blocks[idx] = block;
        }

        assert!(decoded.next().is_none(), "too many blocks in decoded chunk");

        self.change.insert(Region::sized(LOCAL_SIZE).index(cpos) as u8);
    }

    pub fn block(&self, pos: V3) -> BlockId {
        const MASK: i32 = (1 << (CHUNK_BITS + LOCAL_BITS)) - 1;
        let pos = pos & MASK;
        let idx = TERRAIN_BOUNDS.index(pos);
        self.blocks[idx]
    }

    pub fn changes(&self) -> &HashSet<u8> {
        &self.change
    }

    pub fn has_changes(&self) -> bool {
        self.change.len() > 0
    }

    pub fn clear_changes(&mut self) {
        self.change.clear()
    }
}

impl Clone for TerrainChunks {
    fn clone(&self) -> TerrainChunks {
        TerrainChunks {
            blocks: box *self.blocks,
            change: self.change.clone(),
        }
    }
}

struct Rle16Iter<I: Iterator<Item=u16>> {
    input: I,
    val: u16,
    count: u16,
}

fn rle16_decode<I: Iterator<Item=u16>>(input: I) -> Rle16Iter<I> {
    Rle16Iter {
        input: input,
        val: 0,
        count: 0,
    }
}

impl<I: Iterator<Item=u16>> Iterator for Rle16Iter<I> {
    type Item = u16;

    fn next(&mut self) -> Option<u16> {
        if self.count > 0 {
            self.count -= 1;
            return Some(self.val);
        }

        let x = match self.input.next() {
            Some(x) => x,
            None => return None,
        };

        if x & 0xf000 == 0 {
            Some(x)
        } else {
            self.count = x & 0x0fff;
            assert!(self.count > 0);
            self.val = self.input.next().expect("rle16: missing value after count");

            self.count -= 1;
            Some(self.val)
        }
    }
}
