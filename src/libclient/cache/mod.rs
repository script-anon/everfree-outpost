use common::types::*;

use data::data;

use state::entity::Entities;
use state::structure::Structures;
use state::terrain_chunk::TerrainChunks;
use util;


pub mod struct_pos;
pub mod draw_mode;
pub mod terrain_shape;
pub mod entity_pos;

pub struct Caches {
    struct_pos: struct_pos::Cache,
    draw_mode: draw_mode::Cache,
    terrain_shape: terrain_shape::Cache,
    entity_pos: entity_pos::Cache,
}

impl Caches {
    pub fn new() -> Caches {
        Caches {
            struct_pos: struct_pos::Cache::new(),
            draw_mode: draw_mode::Cache::new(),
            terrain_shape: terrain_shape::Cache::new(),
            entity_pos: entity_pos::Cache::new(),
        }
    }

    /// Update the cache.  This should be called exactly once on every frame, so as not to miss
    /// updates.
    ///
    /// The update happens in two parts, because some caches need to be updated before motion
    /// prediction runs, and others need to be updated after.  Specifically, `update1` runs before
    /// prediction and updates the structure and terrain shape caches, while `update2` runs after
    /// and updates the entity position and structure draw mode caches.
    pub fn update1(&mut self,
                   terrain_chunks: &TerrainChunks,
                   structures: &Structures) {
        let data = data();

        let (change_remove, change_add) = structures.changes();
        let change_terrain = terrain_chunks.changes();

        for (&id, &(pos, template_id)) in change_remove {
            let pos = util::unpack_v3(pos);
            let t = data.template(template_id);

            self.struct_pos.remove(id, pos, t.size());
            self.draw_mode.remove(id, pos, t.draw_mode());
            self.terrain_shape.remove_structure(pos, template_id);
        }

        for (&id, &(pos, template_id)) in change_add {
            let pos = util::unpack_v3(pos);
            let t = data.template(template_id);

            self.struct_pos.add(id, pos, t.size());
            self.draw_mode.add(id, pos, t.draw_mode());
            self.terrain_shape.add_structure(pos, template_id);
        }

        for &idx in change_terrain {
            let cpos: V2 = Region::sized(LOCAL_SIZE).from_index(idx as usize);
            let base = (cpos * CHUNK_SIZE).extend(0);
            self.terrain_shape.set_terrain(cpos, |offset| {
                terrain_chunks.block(base + offset)
            });
        }

        self.terrain_shape.refresh_structures(structures);
    }

    pub fn update2(&mut self,
                   now: Time,
                   entities: &Entities,
                   structures: &Structures) {
        self.entity_pos.update(now, entities);
        let change_entity_count = self.entity_pos.take_changed();

        for pos in change_entity_count {
            self.draw_mode.touch_doors(structures, &self.struct_pos, pos);
        }

        self.draw_mode.update(structures, &self.struct_pos, &self.entity_pos);
    }

    #[allow(dead_code)]
    pub fn struct_pos(&self) -> &struct_pos::Cache {
        &self.struct_pos
    }

    pub fn draw_mode(&self) -> &draw_mode::Cache {
        &self.draw_mode
    }

    pub fn terrain_shape(&self) -> &terrain_shape::Cache {
        &self.terrain_shape
    }
}
