#[macro_use] extern crate log;

use std::cell::RefCell;
use std::collections::HashMap;
use std::fmt::{self, Write};
use std::mem;
use std::slice;
use std::str::{self, FromStr};
use log::{Log, LogMetadata, LogLevel, LogLevelFilter, LogRecord};



extern {
    fn log_buffer_str(ptr: *const u8, len: u32);
    fn log_buffer_flush(level: u8);
}

struct ExternFormatWriter;

impl fmt::Write for ExternFormatWriter {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        unsafe { log_buffer_str(s.as_ptr(), s.len() as u32) };
        Ok(())
    }
}

#[no_mangle]
pub unsafe extern "C" fn asmlogger_set_config(ptr: *const u8, len: u32) {
    let slice = slice::from_raw_parts(ptr, len as usize);
    if let Ok(s) = str::from_utf8(slice) {
        set_instance(AsmLogger::from_config(s));
    } else {
        let _ = write!(ExternFormatWriter, "invalid logger config string");
        log_buffer_flush(LogLevel::Error as u8);
    }
}


static mut INSTANCE: *mut AsmLogger = 0 as *mut _;

fn instance() -> Option<&'static AsmLogger> {
    unsafe { INSTANCE.as_ref() }
}

unsafe fn set_instance(logger: AsmLogger) {
    let new = Box::into_raw(Box::new(logger));
    let old = mem::replace(&mut INSTANCE, new);
    if !old.is_null() {
        drop(Box::from_raw(old));
    }
}


pub struct AsmLogger {
    default_level: LogLevel,
    module_level: RefCell<HashMap<String, LogLevel>>,
}

impl AsmLogger {
    pub fn new() -> AsmLogger {
        AsmLogger {
            default_level: LogLevel::Info,
            module_level: RefCell::new(HashMap::new()),
        }
    }

    pub fn from_config(s: &str) -> AsmLogger {
        let mut default = LogLevel::Warn;
        let mut map = HashMap::new();
        for part in s.split(',') {
            if let Some(idx) = part.find('=') {
                let module = &part[..idx];
                let level_str = &part[idx + 1 ..];
                let level = LogLevel::from_str(level_str).unwrap();
                map.insert(module.to_owned(), level);
            } else {
                default = LogLevel::from_str(part).unwrap();
            }
        }

        info!("asmlogger config = {:?}", s);
        info!("parsed = {:?}, {:?}", default, map);

        AsmLogger {
            default_level: default,
            module_level: RefCell::new(map),
        }
    }

    fn module_level(&self, m: &str) -> LogLevel {
        if let Some(&level) = self.module_level.borrow().get(m) {
            return level;
        }

        // Compute a level based on settings for parent modules.

        // Find the position of every "::" in the module name.
        let mut idxs = Vec::new();
        let mut base = 0;
        let mut tail = m;
        while let Some(idx) = tail.find("::") {
            idxs.push(base + idx);
            tail = &tail[idx + 2 ..];
            base += idx + 2;
        }

        let mut level = None;
        for idx in idxs.into_iter().rev() {
            if let Some(&l) = self.module_level.borrow().get(&m[..idx]) {
                level = Some(l);
                break;
            }
        }

        let level = level.unwrap_or(self.default_level);
        self.module_level.borrow_mut().insert(m.to_owned(), level);
        level
    }
}

struct AsmLoggerHandle;

impl Log for AsmLoggerHandle {
    fn enabled(&self, metadata: &LogMetadata) -> bool {
        if let Some(inst) = instance() {
            metadata.level() <= inst.module_level(metadata.target())
        } else {
            metadata.level() <= LogLevel::Info
        }
    }

    fn log(&self, record: &LogRecord) {
        if self.enabled(record.metadata()) {
            let _ = write!(ExternFormatWriter,
                           "{}:{}: {}",
                           record.level(),
                           record.location().module_path(),
                           record.args());
            unsafe { log_buffer_flush(record.level() as u8) };
        }
    }
}


pub fn init() {
    unsafe {
        log::set_logger_raw(|max_log_level| {
            max_log_level.set(LogLevelFilter::Trace);
            &AsmLoggerHandle
        }).unwrap()
    }
}
