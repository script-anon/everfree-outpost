use server_types::*;
use engine::component::char_invs::CharInvs;
use engine::component::crafting::CraftingState;
use engine::component::dialog::{self, Dialog, ClientDialog};
use world::objects::{Activity, Item, Appearance};
use world::objects::InventoryFlags;


/// High-level request from a client.  Each request is associated with a ClientId.
#[derive(Clone, Debug)]
#[allow(dead_code)]
pub enum ClientRequest {
    InputStart(u16, u8, u8),
    InputChange(u16, u8, u8),

    Interact(V3),
    Destroy(V3),
    UseItem(V3, ItemId),

    ConnReady,
    Chat(String),

    DialogOpen(ClientDialog),
    DialogCancel,
    DialogAction(Box<dialog::Action>),
}

#[derive(Debug, Clone)]
#[repr(u8)]
#[allow(dead_code)]
pub enum SyncKind {
    Loading = 0,
    Ok = 1,
    Reset = 2,
    Refresh = 3,
}

/// High-level response to a client.  Each response is associated with a ClientId.
#[derive(Clone, Debug)]
#[allow(dead_code)]
pub enum ClientResponse {
    WorldInfo(Time, u32),

    EntityAppear(EntityId, Box<Appearance>, Box<str>),
    EntityChange(EntityId, Box<Appearance>, Box<str>),
    EntityActivity(EntityId, Time, Box<Activity>),
    EntityGone(EntityId),

    TerrainChunk(V2, Vec<u16>),
    UnloadChunk(V2),

    StructureAppear(StructureId, V3, TemplateId),
    StructureReplace(StructureId, TemplateId),
    StructureGone(StructureId),

    InventoryAppear(InventoryId, Box<[Item]>, InventoryFlags),
    InventoryContents(InventoryId, Box<[Item]>),
    InventoryFlags(InventoryId, InventoryFlags),
    InventoryGone(InventoryId),

    ChatUpdate(String),
    SyncStatus(SyncKind),
    KickReason(String),

    AckRequest(Time),
    NakRequest,

    CharInvs(CharInvs),

    DialogOpen(Box<Dialog>),
    DialogClose,

    CraftingState(StructureId, Option<Box<CraftingState>>),

    CameraFixed(V3),
    CameraPawn(EntityId),
}
