use server_types::*;
use std::fmt;

use common::{Activity, Appearance};
use server_extra::Extra;
use server_world_types::*;
use server_world_types::flags::*;


#[derive(Clone, Debug)]
pub struct World {
    pub now: Time,

    pub next_client: StableId,
    pub next_entity: StableId,
    pub next_inventory: StableId,
    pub next_plane: StableId,
    pub next_terrain_chunk: StableId,
    pub next_structure: StableId,

    pub extra: Extra,
}

impl Default for World {
    fn default() -> World {
        World {
            now: 0,

            next_client: 1,
            next_entity: 1,
            next_inventory: 1,
            next_plane: 1,
            next_terrain_chunk: 1,
            next_structure: 1,

            extra: Extra::new(),
        }
    }
}

#[derive(Clone, Debug)]
pub struct Client {
    pub name: String,
    pub pawn: Option<EntityId>,

    pub extra: Extra,
    pub stable_id: StableId,
}

impl Default for Client {
    fn default() -> Client {
        Client {
            name: String::new(),
            pawn: None,

            extra: Extra::new(),
            stable_id: NO_STABLE_ID,
        }
    }
}

#[derive(Clone, Debug)]
pub struct Entity {
    pub stable_plane: Stable<PlaneId>,
    // plane: transient

    // motion: time-dependent
    pub anim: AnimId,
    pub facing: V3,
    pub target_velocity: V3,
    pub appearance: Appearance,

    pub extra: Extra,
    pub stable_id: StableId,
    pub attachment: EntityAttachment,

    // Inputs for time-dependent fields
    pub activity: Activity,
    pub act_start: Time,
}

impl Default for Entity {
    fn default() -> Entity {
        Entity {
            stable_plane: STABLE_PLANE_FOREST,

            anim: 0,
            facing: V3::new(1, 0, 0),
            target_velocity: scalar(0),
            appearance: Appearance::default(),

            extra: Extra::new(),
            stable_id: NO_STABLE_ID,
            attachment: EntityAttachment::World,

            activity: Activity::default(),
            act_start: 0,
        }
    }
}

#[derive(Clone, Debug)]
pub struct Inventory {
    pub contents: Box<[Item]>,

    pub extra: Extra,
    pub stable_id: StableId,
    pub flags: InventoryFlags,
    pub attachment: InventoryAttachment,
}

impl Default for Inventory {
    fn default() -> Inventory {
        Inventory {
            contents: Vec::new().into_boxed_slice(),

            extra: Extra::new(),
            stable_id: NO_STABLE_ID,
            flags: InventoryFlags::empty(),
            attachment: InventoryAttachment::World,
        }
    }
}

#[derive(Clone, Debug)]
pub struct Plane {
    pub name: String,

    pub extra: Extra,
    pub stable_id: StableId,
}

impl Default for Plane {
    fn default() -> Plane {
        Plane {
            name: String::new(),

            extra: Extra::new(),
            stable_id: NO_STABLE_ID,
        }
    }
}

#[derive(Clone)]
pub struct TerrainChunk {
    pub stable_plane: Stable<PlaneId>,
    pub cpos: V2,
    pub blocks: Box<BlockChunk>,

    pub extra: Extra,
    pub stable_id: StableId,
    pub flags: TerrainChunkFlags,
}

impl Default for TerrainChunk {
    fn default() -> TerrainChunk {
        TerrainChunk {
            stable_plane: STABLE_PLANE_FOREST,
            cpos: scalar(0),
            blocks: Box::new(EMPTY_CHUNK),

            extra: Extra::new(),
            stable_id: NO_STABLE_ID,
            flags: TerrainChunkFlags::empty(),
        }
    }
}

impl fmt::Debug for TerrainChunk {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        let TerrainChunk {
            stable_plane, cpos, ref blocks,
            ref extra, stable_id, flags,
        } = *self;
        fmt.debug_struct("Stable")
            .field("stable_plane", &stable_plane)
            .field("cpos", &cpos)
            .field("blocks", &format_args!("{:?}", &**blocks as &[_]))
            .field("extra", extra)
            .field("stable_id", &stable_id)
            .field("flags", &flags)
            .finish()
    }
}


#[derive(Clone, Debug)]
pub struct Structure {
    pub stable_plane: Stable<PlaneId>,
    pub pos: V3,
    pub template: TemplateId,

    pub extra: Extra,
    pub stable_id: StableId,
    pub flags: StructureFlags,
    pub attachment: StructureAttachment,

    // Inputs for time-dependent fields

    /// Crafting progress parameters.  If crafting is in progress, this is `Some((start_time,
    /// start_progress))`.
    pub crafting_progress: Option<(Time, u32)>,
}

impl Default for Structure {
    fn default() -> Structure {
        Structure {
            stable_plane: STABLE_PLANE_FOREST,
            pos: scalar(0),
            template: 0,

            extra: Extra::new(),
            stable_id: NO_STABLE_ID,
            flags: StructureFlags::empty(),
            attachment: StructureAttachment::Plane,

            crafting_progress: None,
        }
    }
}
