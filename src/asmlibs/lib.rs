#![crate_name = "asmlibs"]

#![feature(
    alloc,
    allocator_api,
)]

extern crate alloc;

extern crate asmlogger;
extern crate client;
extern crate common;
extern crate physics;

use std::mem;
use std::ptr;
use std::ptr::NonNull;
use std::slice;
use alloc::alloc::{Alloc, Global, Layout};
use common::proto;
use common::proto::wire::ReadFrom;
use common::types::V2;

use client::Data;

use client::graphics::light;
use client::graphics::structure;
use client::graphics::terrain;

mod platform;
mod io;


pub type Client = client::Client<platform::Platform>;


// New API

// Init

#[no_mangle]
pub unsafe extern fn asmlibs_init() {
    // No-op
    asmlogger::init();
}

#[no_mangle]
pub unsafe extern fn data_init(blob_ptr: *mut u8,
                               blob_len: usize) {
    let blob = make_boxed_slice(blob_ptr, blob_len);
    client::data::set_data(Box::new(Data::new(blob)));
}

#[no_mangle]
pub unsafe extern fn client_init(out: *mut Client) {
    ptr::write(out, Client::new(platform::Platform::new()));
}

#[no_mangle]
pub unsafe extern fn handle_message(client: &mut Client,
                                    ptr: *const u8,
                                    len: usize) {
    let buf = make_slice(ptr, len);
    let msg = proto::game::Response::read_from(&mut io::Cursor::new(buf))
        .unwrap_or_else(|e| panic!("error parsing message ({:x}): {}", buf[0], e));
    client.handle_message(msg);
}

#[no_mangle]
pub unsafe extern fn client_reset(client: &mut Client) {
    client.reset_all();
}

#[no_mangle]
pub unsafe extern fn client_reset_renderer(client: &mut Client) {
    client.reset_renderer();
}

// Memory

#[no_mangle]
pub unsafe extern fn rust_alloc(size: usize, align: usize) -> *mut u8 {
    let layout = match Layout::from_size_align(size, align) {
        Ok(x) => x,
        Err(_) => return ptr::null_mut(),
    };
    Global.alloc(layout).ok().map_or(ptr::null_mut(), |nn| nn.as_ptr())
}

#[no_mangle]
pub unsafe extern fn rust_free(ptr: *mut u8, size: usize, align: usize) {
    let ptr = match NonNull::new(ptr) {
        Some(x) => x,
        None => return,
    };
    let layout = Layout::from_size_align(size, align).expect("dealloc: bad layout");
    Global.dealloc(ptr, layout);
}

// Inputs

#[no_mangle]
pub unsafe extern fn input_key_down(client: &mut Client,
                                    code: u8,
                                    shift: u8) -> u8 {
    let mods =
        if shift != 0 { 0x01 } else { 0 };
    client.input_key_down(code, mods) as u8
}

#[no_mangle]
pub unsafe extern fn input_key_up(client: &mut Client,
                                  code: u8,
                                  shift: u8) -> u8 {
    let mods =
        if shift != 0 { 0x01 } else { 0 };
    client.input_key_up(code, mods) as u8
}

#[no_mangle]
pub unsafe extern fn input_mouse_move(client: &mut Client,
                                      x: i32,
                                      y: i32) -> u8 {
    client.input_mouse_move(V2::new(x, y)) as u8
}

#[no_mangle]
pub unsafe extern fn input_mouse_down(client: &mut Client,
                                      x: i32,
                                      y: i32,
                                      button: u8,
                                      mods: u8) -> u8 {
    client.input_mouse_down(V2::new(x, y), button, mods) as u8
}

#[no_mangle]
pub unsafe extern fn input_mouse_up(client: &mut Client,
                                    x: i32,
                                    y: i32,
                                    button: u8,
                                    mods: u8) -> u8 {
    client.input_mouse_up(V2::new(x, y), button, mods) as u8
}


// Graphics

unsafe fn make_slice<T>(ptr: *const T, byte_len: usize) -> &'static [T] {
    slice::from_raw_parts(ptr, byte_len / mem::size_of::<T>())
}

unsafe fn make_boxed_slice<T>(ptr: *mut T, byte_len: usize) -> Box<[T]> {
    assert!(byte_len % mem::size_of::<T>() == 0);
    let raw: *mut [T] = slice::from_raw_parts_mut(ptr, byte_len / mem::size_of::<T>());
    Box::from_raw(raw)
}


#[no_mangle]
pub unsafe extern fn render_frame(client: &mut Client) {
    client.render_frame();
}

// Misc

#[no_mangle]
pub unsafe extern fn handle_pong(client: &mut Client,
                                 client_send: i32,
                                 client_recv: i32,
                                 server: u16) {
    client.handle_pong(client_send, client_recv, server);
}

#[no_mangle]
pub unsafe extern fn calc_scale(client: &Client,
                                size_x: u16,
                                size_y: u16) -> i16 {
    client.calc_scale((size_x, size_y))
}

#[no_mangle]
pub unsafe extern fn resize_window(client: &mut Client,
                                   width: u16,
                                   height: u16) {
    client.resize_window((width, height));
}


#[no_mangle]
pub unsafe extern fn client_bench(client: &mut Client) -> i32 {
    extern "C" {
        fn ap_get_time() -> i32;
    }

    let start = ap_get_time();
    client.bench();
    let end = ap_get_time();

    end - start
}


// SIZEOF

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct Sizes {
    client: usize,
    client_alignment: usize,

    terrain_vertex: usize,
    structure_vertex: usize,
    light_vertex: usize,

    scene: usize,

    item: usize,
}

#[export_name = "get_sizes"]
pub extern fn get_sizes(sizes: &mut Sizes) -> usize {
    use std::mem::size_of;
    use std::mem::align_of;

    sizes.client = size_of::<Client>();
    sizes.client_alignment = align_of::<Client>();

    sizes.terrain_vertex = size_of::<terrain::Vertex>();
    sizes.structure_vertex = size_of::<structure::Vertex>();
    sizes.light_vertex = size_of::<light::Vertex>();

    sizes.scene = size_of::<client::graphics::renderer::Scene>();

    sizes.item = size_of::<client::state::inventory::Item>();

    size_of::<Sizes>() / size_of::<usize>()
}
