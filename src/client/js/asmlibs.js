var config = require('./config');
var decodeUtf8 = require('./util/misc').decodeUtf8;
var LogServer = require('./debug_log').LogServer;

var AsmGl = require('./asmgl').AsmGl;


// External functions

var module_env = function(asm, gl) {
    var log_buffer = '';

    function flush_lines(callback) {
        var prev = 0;
        var idx = -1;
        while ((idx = log_buffer.indexOf('\n', prev)) != -1) {
            callback(log_buffer.substring(prev, idx));
            prev = idx + 1;
        }
        log_buffer = log_buffer.substring(prev);
    }

    function flush_lines_all(callback) {
        flush_lines(callback);
        if (log_buffer != '') {
            callback(log_buffer);
            log_buffer = '';
        }
    }

    return ({
        'abort': function() {
            console.assert(false, 'abort');
            throw 'abort';
        },

        'log_buffer_str': function(ptr, len) {
            log_buffer += asm._loadString(ptr, len);
        },

        'log_buffer_flush': function(level) {
            flush_lines_all(function(line) {
                if (level == 1) {
                    console.error(line);
                } else if (level == 2) {
                    console.warn(line);
                } else {
                    console.info(line);
                }

                LogServer.send(line);
            });
        },


        'rust_wasm_syscall': function(index, data) {
            if (index == 1) {   // Write
                var view = asm._makeView(Uint32Array, data, 12);
                var fd = view[0];
                var ptr = view[1];
                var len = view[2];
                log_buffer += asm._loadString(ptr, len);

                if (fd == 1) {
                    flush_lines(function(line) {
                        console.log(line);
                        LogServer.send(line);
                    });
                } else {
                    flush_lines(function(line) {
                        console.warn(line);
                        LogServer.send(line);
                    });
                }
                return 1;
            } else {
                return 0;
            }
        },


        'atan2f': Math.atan2,
        'cosf': Math.cos,
        'powf': Math.pow,
        'sinf': Math.sin,
        'sqrtf': Math.sqrt,
        'fmod': function(x, y) { return x % y; },

        'asmgl_has_draw_buffers': function() {
            return gl.hasDrawBuffers();
        },
        'asmgl_has_depth_texture': function() {
            return gl.hasDepthTexture();
        },

        'asmgl_gen_buffer': function() {
            return gl.genBuffer();
        },
        'asmgl_delete_buffer': function(name) {
            gl.deleteBuffer(name);
        },
        'asmgl_bind_buffer': function(target_idx, name) {
            gl.bindBuffer(target_idx, name);
        },
        'asmgl_bind_buffer_index': function(name) {
            gl.bindBufferIndex(name);
        },
        'asmgl_buffer_data_alloc': function(target, len) {
            gl.bufferDataAlloc(target, len);
        },
        'asmgl_buffer_subdata': function(target, offset, ptr, len) {
            var data = asm._makeView(Uint8Array, ptr, len);
            gl.bufferSubdata(target, offset, data);
        },

        'asmgl_load_shader': function(
                vert_name_ptr, vert_name_len,
                frag_name_ptr, frag_name_len,
                defs_ptr, defs_len) {
            var vert_name = asm._loadString(vert_name_ptr, vert_name_len);
            var frag_name = asm._loadString(frag_name_ptr, frag_name_len);
            var defs = asm._loadString(defs_ptr, defs_len);
            return gl.loadShader(vert_name, frag_name, defs);
        },
        'asmgl_delete_shader': function(name) {
            gl.deleteShader(name);
        },
        'asmgl_bind_shader': function(name) {
            gl.bindShader(name);
        },
        'asmgl_get_uniform_location': function(shader_name, name_ptr, name_len) {
            var var_name = asm._loadString(name_ptr, name_len);
            return gl.getUniformLocation(shader_name, var_name);
        },
        'asmgl_get_attrib_location': function(shader_name, name_ptr, name_len) {
            var var_name = asm._loadString(name_ptr, name_len);
            return gl.getAttribLocation(shader_name, var_name);
        },
        'asmgl_set_uniform_1i': function(loc, value) {
            gl.setUniform1i(loc, value);
        },
        'asmgl_set_uniform_1f': function(loc, value) {
            gl.setUniform1f(loc, value);
        },
        'asmgl_set_uniform_2f': function(loc, ptr) {
            var view = asm._makeView(Float32Array, ptr, 8);
            gl.setUniform2f(loc, view);
        },
        'asmgl_set_uniform_3f': function(loc, ptr) {
            var view = asm._makeView(Float32Array, ptr, 12);
            gl.setUniform3f(loc, view);
        },
        'asmgl_set_uniform_4f': function(loc, ptr) {
            var view = asm._makeView(Float32Array, ptr, 16);
            gl.setUniform4f(loc, view);
        },

        'asmgl_load_texture': function(name_ptr, name_len, size_ptr) {
            var name = asm._loadString(name_ptr, name_len);
            var size_view = asm._makeView(Uint16Array, size_ptr, 4);
            return gl.loadTexture(name, size_view);
        },
        'asmgl_gen_texture': function(width, height, kind) {
            return gl.genTexture(width, height, kind);
        },
        'asmgl_delete_texture': function(name) {
            gl.deleteTexture(name);
        },
        'asmgl_active_texture': function(unit) {
            gl.activeTexture(unit);
        },
        'asmgl_bind_texture': function(name) {
            gl.bindTexture(name);
        },
        'asmgl_texture_image': function(width, height, kind, data_ptr, data_len) {
            var view = null;
            if (data_ptr != 0) {
                view = asm._makeView(Uint8Array, data_ptr, data_len);
            }
            gl.textureImage(width, height, kind, view);
        },
        'asmgl_texture_subimage': function(x, y, width, height, kind, data_ptr, data_len) {
            var view = null;
            if (data_ptr != 0) {
                view = asm._makeView(Uint8Array, data_ptr, data_len);
            }
            gl.textureSubimage(x, y, width, height, kind, view);
        },

        'asmgl_gen_framebuffer': function() {
            return gl.genFramebuffer();
        },
        'asmgl_delete_framebuffer': function(name) {
            gl.deleteFramebuffer(name);
        },
        'asmgl_bind_framebuffer': function(name) {
            gl.bindFramebuffer(name);
        },
        'asmgl_gen_renderbuffer': function(width, height, is_depth) {
            return gl.genRenderbuffer(width, height, is_depth);
        },
        'asmgl_delete_renderbuffer': function(name) {
            gl.deleteRenderbuffer(name);
        },
        'asmgl_framebuffer_texture': function(tex_name, attachment) {
            gl.framebufferTexture(tex_name, attachment);
        },
        'asmgl_framebuffer_renderbuffer': function(rb_name, attachment) {
            gl.framebufferRenderbuffer(rb_name, attachment);
        },
        'asmgl_check_framebuffer_status': function() {
            return gl.checkFramebufferStatus();
        },
        'asmgl_draw_buffers': function(num_attachments) {
            gl.drawBuffers(num_attachments);
        },

        'asmgl_viewport': function(x, y, w, h) {
            gl.viewport(x, y, w, h);
        },
        'asmgl_scissor': function(x, y, w, h) {
            gl.scissor(x, y, w, h);
        },
        'asmgl_scissor_disable': function() {
            gl.scissorDisable();
        },
        'asmgl_clear_color': function(r, g, b, a) {
            gl.clearColor(r, g, b, a);
        },
        'asmgl_clear_depth': function(d) {
            gl.clearDepth(d);
        },
        'asmgl_clear': function() {
            gl.clear();
        },
        'asmgl_set_depth_test': function(enable) {
            gl.setDepthTest(enable);
        },
        'asmgl_set_blend_mode': function(mode) {
            gl.setBlendMode(mode);
        },
        'asmgl_enable_vertex_attrib_array': function(index) {
            gl.enableVertexAttribArray(index);
        },
        'asmgl_disable_vertex_attrib_array': function(index) {
            gl.disableVertexAttribArray(index);
        },
        'asmgl_vertex_attrib_pointer': function(loc, count, ty, normalize, stride, offset) {
            gl.vertexAttribPointer(loc, count, ty, normalize, stride, offset);
        },
        'asmgl_draw_arrays_triangles': function(start, count) {
            gl.drawArraysTriangles(start, count);
        },

        'ap_config_get': function(key_ptr, key_len, value_len_p) {
            var key = asm._loadString(key_ptr, key_len);
            var value = config.rawGet(key);

            var value_view = asm._allocString(value);

            var value_len_p_view = asm._makeView(Uint32Array, value_len_p, 4);
            value_len_p_view[0] = value_view.length;
            return value_view.ptr;
        },

        'ap_config_get_int': function(key_ptr, key_len) {
            var key = asm._loadString(key_ptr, key_len);
            var value = config.rawGet(key);
            return value|0;
        },

        'ap_config_set': function(key_ptr, key_len, value_ptr, value_len) {
            var key = asm._loadString(key_ptr, key_len);
            var value = asm._loadString(value_ptr, value_len);

            config.rawSet(key, value);
        },

        'ap_config_clear': function(key_ptr, key_len) {
            var key = asm._loadString(key_ptr, key_len);

            config.rawClear(key);
        },

        'ap_set_cursor': function(cursor) {
            var str;
            switch (cursor) {
                case 0: str = 'auto'; break;
                case 1: str = 'grabbing'; break;
                case 2: str = 'not-allowed'; break;
                default: throw 'bad cursor value: ' + cursor;
            }
            document.body.style.cursor = str;
        },

        'ap_send_message': function(ptr, len) {
            var view = asm._makeView(Uint8Array, ptr, len);
            asm.conn._send(view);
        },

        'ap_get_time': function() {
            return (Date.now() - asm._time_base) & 0x7fffffff;
        },
    });
};


// Helper functions

function memcpy(dest_buffer, dest_offset, src_buffer, src_offset, len) {
    var dest = new Int8Array(dest_buffer, dest_offset, len);
    var src = new Int8Array(src_buffer, src_offset, len);
    dest.set(src);
}

function store_vec(view, offset, vec) {
    view[offset + 0] = vec.x;
    view[offset + 1] = vec.y;
    view[offset + 2] = vec.z;
}


/** @constructor */
function Allocation(mem, ptr, type, length) {
    this.mem = mem;
    this.ptr = ptr;
    this.type = type;
    this.length = length;
    this._array = new type(this.mem.buffer, ptr, length);
}

// Returns a valid TypedArray view of the allocated memory.
//
// If the WebAssembly memory is resized after calling this function, the
// returned TypedArray will be invalidated.  Call this function again to get a
// fresh, valid TypedArray.
Allocation.prototype.array = function() {
    // `byteLength == 0` indicates that the array was invalidated.
    if (this._array.byteLength == 0 && this.length != 0) {
        this._array = new this.type(this.mem.buffer, this.ptr, this.length);
    }
    return this._array;
};


/** @constructor */
function DynAsm() {
    this.asmgl = new AsmGl();
    this.conn = null;   // Will be set later, in main.js
    this._time_base = Date.now();
    this.mem = null;
    this.inst = null;
    this._raw = null;

    this.client = null;
    this.SIZEOF = null;
}
exports.DynAsm = DynAsm;

DynAsm.prototype.init = function(m) {
    var imports = { 'env': module_env(this, this.asmgl) };
    var this_ = this;
    return WebAssembly.instantiate(m, imports).then(function(inst) {
        this_.inst = inst;
        this_._raw = inst.exports;
        this_.mem = inst.exports['memory'];

        // TODO: figure out how to make LLVM allocate less stack (currently it
        // alocates about 1MB)
        this_._raw['asmlibs_init']();

        this_.SIZEOF = this_._calcSizeof();

        LogServer.sendConfig(this_);
    });
};

DynAsm.prototype._calcSizeof = function() {
    var EXPECT_SIZES = 7;
    var sizes = this._heapAlloc(Int32Array, EXPECT_SIZES);

    var num_sizes = this._raw['get_sizes'](sizes.ptr);
    console.assert(num_sizes == EXPECT_SIZES,
            'expected sizes for ' + EXPECT_SIZES + ' types, but got ' + num_sizes);

    var index = 0;
    var next = function() { return sizes.array()[index++]; };
    var result = {};

    result.Client = next();
    result.ClientAlignment = next();

    result.TerrainVertex = next();
    result.StructureVertex = next();
    result.LightVertex = next();

    result.Scene = next();

    result.Item = next();

    console.assert(index == EXPECT_SIZES,
            'some items were left over after building sizeof', index, EXPECT_SIZES);

    this._heapFree(sizes);

    return result;
};

DynAsm.prototype._heapAlloc = function(type, count) {
    var size = count * type.BYTES_PER_ELEMENT;
    var ptr = this._raw['rust_alloc'](size, type.BYTES_PER_ELEMENT);
    return new Allocation(this.mem, ptr, type, count);
};

DynAsm.prototype._heapFree = function(alloc) {
    var align = alloc.type.BYTES_PER_ELEMENT;
    var size = alloc.length * align;
    this._raw['rust_free'](alloc.ptr, size, align);
};

DynAsm.prototype._makeView = function(type, offset, bytes) {
    return new type(this.mem.buffer, offset, bytes / type.BYTES_PER_ELEMENT);
};

DynAsm.prototype._memcpy = function(dest_offset, data) {
    if (data.constructor !== ArrayBuffer) {
        memcpy(this.mem.buffer, dest_offset, data.buffer, data.byteOffset, data.byteLength);
    } else {
        memcpy(this.mem.buffer, dest_offset, data, 0, data.byteLength);
    }
};

DynAsm.prototype._loadString = function(ptr, len) {
    var view = this._makeView(Uint8Array, ptr, len);
    return decodeUtf8(view);
};

DynAsm.prototype._allocString = function(s) {
    var utf8 = unescape(encodeURIComponent('' + s));
    var len = utf8.length;
    var alloc = this._heapAlloc(Uint8Array, len);

    var view = alloc.array();
    for (var i = 0; i < len; ++i) {
        view[i] = utf8.charCodeAt(i);
    }

    return alloc;
};

DynAsm.prototype.initClient = function(gl, assets) {
    // AsmGl must be initialized before calling `client_init`.
    this.asmgl.init(gl, assets);

    var blob = assets['client_data'];
    var len = blob.byteLength;
    var ptr = this._raw['rust_alloc'](len, 8);
    this._memcpy(ptr, blob);

    // NB: takes ownership of `ptr`
    this._raw['data_init'](ptr, len);

    this.client = this._raw['rust_alloc'](this.SIZEOF.Client, this.SIZEOF.ClientAlignment);
    console.log('client addr = ' + this.client.toString(16));
    this._raw['client_init'](this.client);

    console.log(' -- CLIENT INIT -- ');
};

DynAsm.prototype.resetClient = function() {
    this._raw['client_reset'](this.client);
};

DynAsm.prototype.handleMessage = function(msg) {
    var buf = this._heapAlloc(Uint8Array, msg.length);
    buf.array().set(msg);
    this._raw['handle_message'](this.client, buf.ptr, buf.length);
    this._heapFree(buf);
};

DynAsm.prototype.inputKeyDown = function(code, shift) {
    return this._raw['input_key_down'](this.client, code, shift);
};

DynAsm.prototype.inputKeyUp = function(code, shift) {
    return this._raw['input_key_up'](this.client, code, shift);
};

DynAsm.prototype.inputMouseMove = function(x, y) {
    return this._raw['input_mouse_move'](this.client, x, y);
};

DynAsm.prototype.inputMouseDown = function(x, y, button, shift) {
    return this._raw['input_mouse_down'](this.client, x, y, button, shift);
};

DynAsm.prototype.inputMouseUp = function(x, y, button, shift) {
    return this._raw['input_mouse_up'](this.client, x, y, button, shift);
};

DynAsm.prototype.renderFrame = function() {
    this._raw['render_frame'](this.client);
};

DynAsm.prototype.handlePong = function(client_send, client_recv, server_now) {
    client_send -= this._time_base;
    client_recv -= this._time_base;
    this._raw['handle_pong'](this.client, client_send, client_recv, server_now);
};

DynAsm.prototype.calcScale = function(width, height) {
    return this._raw['calc_scale'](this.client, width, height);
};

DynAsm.prototype.resizeWindow = function(width, height) {
    this._raw['resize_window'](this.client, width, height);
};

DynAsm.prototype.bench = function() {
    return this._raw['client_bench'](this.client);
};


DynAsm.prototype.setLogConfig = function(s) {
    console.log('set log config:', s);
    var str_view = this._allocString(s);
    console.log(str_view);
    this._raw['asmlogger_set_config'](str_view.ptr, str_view.length);
    this._heapFree(str_view);
};


function downloadArray(arr, name) {
    var b = new Blob([arr]);
    var url = window.URL.createObjectURL(b);

    var a = document.createElement('a');
    console.log(url);
    a.setAttribute('href', url);
    a.setAttribute('download', name);
    console.log('clicking...');
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
}

DynAsm.prototype.debugExport = function() {
    downloadArray(this.mem.buffer, 'outpost_heap.dat');
};

DynAsm.prototype.debugImport = function() {
    var input = document.createElement('input');
    input.setAttribute('type', 'file');

    var this_ = this;

    input.onchange = function(evt) {
        if (input.files.length != 1) {
            return;
        }
        var f = input.files[0];
        var reader = new FileReader();
        reader.onloadend = function(evt) {
            var src = new Uint8Array(reader.result);
            var dest = new Uint8Array(this_.mem.buffer);
            dest.set(src);

            window['ASMGL_LOG'] = true;

            this_._raw['client_reset_renderer'](this_.client);
            this_._raw['resize_window'](this_.client, window.innerWidth, window.innerHeight);
        };
        reader.readAsArrayBuffer(f);
    };

    document.body.appendChild(input);
    input.click();
    document.body.removeChild(input);
};


/** @constructor */
function AsmClientInput(asm) {
    this._asm = asm;
}
exports.AsmClientInput = AsmClientInput;

AsmClientInput.prototype.handleMouseMove = function(evt) {
    var ret = this._asm.inputMouseMove(evt.x, evt.y);
    if (!ret) {
        evt.forward();
    }
    return ret;
};

AsmClientInput.prototype.handleMouseDown = function(evt) {
    // JS numbers buttons 0,1,2, but libclient numbers them 1,2,3.
    var ret = this._asm.inputMouseDown(evt.x, evt.y, evt.button + 1, evt.shift);
    if (!ret) {
        evt.forward();
    }
    return ret;
};

AsmClientInput.prototype.handleMouseUp = function(evt) {
    var ret = this._asm.inputMouseUp(evt.x, evt.y, evt.button + 1, evt.shift);
    if (!ret) {
        evt.forward();
    }
    return ret;
};

AsmClientInput.prototype.handleWheel = function(evt) {
    var button = evt.raw.deltaY < 0 ? 4 : 5;
    var ret1 = this._asm.inputMouseDown(evt.x, evt.y, button, evt.shift);
    var ret2 = this._asm.inputMouseUp(evt.x, evt.y, button, evt.shift);
    var ret = ret1 || ret2;
    if (!ret) {
        evt.forward();
    }
    return ret;
};
