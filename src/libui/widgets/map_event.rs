use std::marker::PhantomData;
use std::ops::{Deref, DerefMut};

use context::{Context, Void};
use event::{KeyEvent, MouseEvent, UIResult};
use geom::{Point, Rect};
use widget::Widget;


pub trait EventFn<Ctx, Input> {
    type Output;
    fn call(&self, ctx: &mut Ctx, e: UIResult<Input>) -> UIResult<Self::Output>;
}


pub struct MapEventFn<I, O, F> {
    f: F,
    _marker: PhantomData<fn(I) -> O>,
}

impl<Ctx, I, O, F: Fn(&mut Ctx, I) -> O> EventFn<Ctx, I> for MapEventFn<I, O, F> {
    type Output = O;
    fn call(&self, ctx: &mut Ctx, e: UIResult<I>) -> UIResult<O> {
        e.map(|i| (self.f)(ctx, i))
    }
}


pub struct AndThenEventFn<I, O, F> {
    f: F,
    _marker: PhantomData<fn(I) -> O>,
}

impl<Ctx, I, O, F: Fn(&mut Ctx, I) -> UIResult<O>> EventFn<Ctx, I> for AndThenEventFn<I, O, F> {
    type Output = O;
    fn call(&self, ctx: &mut Ctx, e: UIResult<I>) -> UIResult<O> {
        e.and_then(|i| (self.f)(ctx, i))
    }
}


pub struct OrElseEventFn<E, F> {
    f: F,
    _marker: PhantomData<fn() -> E>,
}

impl<Ctx, E, F: Fn(&mut Ctx) -> UIResult<E>> EventFn<Ctx, E> for OrElseEventFn<E, F> {
    type Output = E;
    fn call(&self, ctx: &mut Ctx, e: UIResult<E>) -> UIResult<E> {
        e.or_else(|| (self.f)(ctx))
    }
}


pub struct IgnoreEventFn;

impl<Ctx, I> EventFn<Ctx, I> for IgnoreEventFn {
    type Output = Void;
    fn call(&self, _ctx: &mut Ctx, e: UIResult<I>) -> UIResult<Void> {
        e.and_then(|_| UIResult::NoEvent)
    }
}


pub struct HandleEventFn<E, F> {
    f: F,
    _marker: PhantomData<fn(E)>,
}

impl<Ctx, E, F: Fn(&mut Ctx, E)> EventFn<Ctx, E> for HandleEventFn<E, F> {
    type Output = Void;
    fn call(&self, ctx: &mut Ctx, e: UIResult<E>) -> UIResult<Void> {
        e.and_then(|e| {
            (self.f)(ctx, e);
            UIResult::NoEvent
        })
    }
}


// `Ctx` and the `where` clause aren't strictly necessary, but they make type inference behave a
// little better.  Without them, `rustc` sometimes fails to infer the right `Ctx` for `map_event`.
pub struct EventMapped<Ctx, W, F>
        where Ctx: Context, W: Widget<Ctx>, F: EventFn<Ctx, W::Event> {
    w: W,
    f: F,
    _marker: PhantomData<fn(&mut Ctx)>,
}

impl<Ctx, W, F> EventMapped<Ctx, W, F>
        where Ctx: Context, W: Widget<Ctx>, F: EventFn<Ctx, W::Event> {
    pub fn new(w: W, f: F) -> EventMapped<Ctx, W, F> {
        EventMapped {
            w: w,
            f: f,
            _marker: PhantomData,
        }
    }
}

impl<Ctx, W, F> Widget<Ctx> for EventMapped<Ctx, W, F>
    where Ctx: Context,
          W: Widget<Ctx>,
          F: EventFn<Ctx, W::Event> {
    type Event = F::Output;

    fn min_size(&self, ctx: &Ctx) -> Point {
        self.w.min_size(ctx)
    }

    fn requested_visibility(&self, ctx: &Ctx) -> Option<Rect> {
        self.w.requested_visibility(ctx)
    }


    fn on_paint(&self,
                ctx: &mut Ctx) {
        self.w.on_paint(ctx);
    }

    fn on_key(&self,
              ctx: &mut Ctx,
              evt: KeyEvent<Ctx::Key>) -> UIResult<Self::Event> {
        let e = self.w.on_key(ctx, evt);
        self.f.call(ctx, e)
    }

    fn on_mouse(&self,
                ctx: &mut Ctx,
                evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        let e = self.w.on_mouse(ctx, evt);
        self.f.call(ctx, e)
    }

    fn check_drop(&self,
                  ctx: &mut Ctx,
                  data: &Ctx::DropData) -> bool {
        self.w.check_drop(ctx, data)
    }

    fn on_drop(&self,
               ctx: &mut Ctx,
               data: &Ctx::DropData) -> UIResult<Self::Event> {
        let e = self.w.on_drop(ctx, data);
        self.f.call(ctx, e)
    }
}


pub trait MapEvent<Ctx: Context>: Widget<Ctx>+Sized {
    fn map_event<F, R>(self, f: F) -> EventMapped<Ctx, Self, MapEventFn<Self::Event, R, F>>
        where F: Fn(&mut Ctx, Self::Event) -> R;

    fn and_then_event<F, R>(self, f: F) -> EventMapped<Ctx, Self, AndThenEventFn<Self::Event, R, F>>
        where F: Fn(&mut Ctx, Self::Event) -> UIResult<R>;

    fn or_else_event<F>(self, f: F) -> EventMapped<Ctx, Self, OrElseEventFn<Self::Event, F>>
        where F: Fn(&mut Ctx) -> UIResult<Self::Event>;

    fn ignore_event(self) -> EventMapped<Ctx, Self, IgnoreEventFn>;

    fn handle_event<F>(self, f: F) -> EventMapped<Ctx, Self, HandleEventFn<Self::Event, F>>
        where F: Fn(&mut Ctx, Self::Event);
}

impl<Ctx, W> MapEvent<Ctx> for W
        where Ctx: Context, W: Widget<Ctx> {
    fn map_event<F, R>(self, f: F) -> EventMapped<Ctx, Self, MapEventFn<Self::Event, R, F>>
            where F: Fn(&mut Ctx, Self::Event) -> R {
        EventMapped::new(self, MapEventFn {
            f: f,
            _marker: PhantomData,
        })
    }

    fn and_then_event<F, R>(self, f: F) -> EventMapped<Ctx, Self, AndThenEventFn<Self::Event, R, F>>
            where F: Fn(&mut Ctx, Self::Event) -> UIResult<R> {
        EventMapped::new(self, AndThenEventFn {
            f: f,
            _marker: PhantomData,
        })
    }

    fn or_else_event<F>(self, f: F) -> EventMapped<Ctx, Self, OrElseEventFn<Self::Event, F>>
            where F: Fn(&mut Ctx) -> UIResult<Self::Event> {
        EventMapped::new(self, OrElseEventFn {
            f: f,
            _marker: PhantomData,
        })
    }

    fn ignore_event(self) -> EventMapped<Ctx, Self, IgnoreEventFn> {
        EventMapped::new(self, IgnoreEventFn)
    }

    fn handle_event<F>(self, f: F) -> EventMapped<Ctx, Self, HandleEventFn<Self::Event, F>>
            where F: Fn(&mut Ctx, Self::Event) {
        EventMapped::new(self, HandleEventFn {
            f: f,
            _marker: PhantomData,
        })
    }
}


impl<Ctx, W, F> Deref for EventMapped<Ctx, W, F>
        where Ctx: Context, W: Widget<Ctx>, F: EventFn<Ctx, W::Event> {
    type Target = W;
    fn deref(&self) -> &W {
        &self.w
    }
}

impl<Ctx, W, F> DerefMut for EventMapped<Ctx, W, F>
        where Ctx: Context, W: Widget<Ctx>, F: EventFn<Ctx, W::Event> {
    fn deref_mut(&mut self) -> &mut W {
        &mut self.w
    }
}
