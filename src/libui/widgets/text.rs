use context::Context;
use event::{MouseEvent, UIResult};
use geom::*;
use widget::Widget;
use widgets::container::Align;


#[derive(Clone, Debug)]
pub struct Label<'a, Ctx: Context> {
    text: &'a str,
    style: Ctx::TextStyle,
}

impl<'a, Ctx: Context> Label<'a, Ctx> {
    pub fn new(text: &'a str) -> Label<'a, Ctx> {
        Label {
            text: text,
            style: Ctx::TextStyle::default(),
        }
    }

    pub fn text(self, text: &'a str) -> Self {
        Label {
            text: text,
            .. self
        }
    }

    pub fn style(self, style: Ctx::TextStyle) -> Self {
        Label {
            style: style,
            .. self
        }
    }
}

impl<'a, Ctx: Context> Widget<Ctx> for Label<'a, Ctx> {
    type Event = ();

    fn min_size(&self, ctx: &Ctx) -> Point {
        ctx.text_size(self.text, self.style)
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        ctx.draw_text(self.text, self.style);
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        match evt {
            MouseEvent::Down(_) => UIResult::NoEvent,
            MouseEvent::Up(_) => {
                if ctx.mouse_pressed_over() {
                    UIResult::Event(())
                } else {
                    UIResult::NoEvent
                }
            },
            _ => UIResult::Unhandled,
        }
    }
}


#[derive(Clone, Debug)]
pub struct FixedLabel<'a, Ctx: Context> {
    text: &'a str,
    width: i32,
    align: Align,
    style: Ctx::TextStyle,
}

impl<'a, Ctx: Context> FixedLabel<'a, Ctx> {
    pub fn new(text: &'a str, width: i32) -> FixedLabel<'a, Ctx> {
        FixedLabel {
            text: text,
            width,
            align: Align::Start,
            style: Ctx::TextStyle::default(),
        }
    }

    pub fn text(self, text: &'a str) -> Self {
        FixedLabel {
            text: text,
            .. self
        }
    }

    pub fn width(self, width: i32) -> Self {
        FixedLabel {
            width: width,
            .. self
        }
    }

    pub fn align(self, align: Align) -> Self {
        FixedLabel {
            align: align,
            .. self
        }
    }

    pub fn style(self, style: Ctx::TextStyle) -> Self {
        FixedLabel {
            style: style,
            .. self
        }
    }
}

impl<'a, Ctx: Context> Widget<Ctx> for FixedLabel<'a, Ctx> {
    type Event = ();

    fn min_size(&self, ctx: &Ctx) -> Point {
        let height = ctx.text_size(self.text, self.style).y;
        Point::new(self.width, height)
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        let text_w = ctx.text_size(self.text, self.style).x;
        let bounds = ctx.cur_bounds().size();
        let x = match self.align {
            Align::Start => 0,
            Align::Center => (bounds.x - text_w) / 2,
            Align::End => bounds.x - text_w,
            Align::Stretch => (bounds.x - text_w) / 2,
        };

        ctx.with_bounds(Rect::new(x, 0, bounds.x, bounds.y), |ctx| {
            ctx.draw_text(self.text, self.style);
        });
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        match evt {
            MouseEvent::Down(_) => UIResult::NoEvent,
            MouseEvent::Up(_) => {
                if ctx.mouse_pressed_over() {
                    UIResult::Event(())
                } else {
                    UIResult::NoEvent
                }
            },
            _ => UIResult::Unhandled,
        }
    }
}


#[derive(Clone, Debug)]
pub struct WrappedLabel<'a, Ctx: Context> {
    text: &'a str,
    width: i32,
    style: Ctx::TextStyle,
}

impl<'a, Ctx: Context> WrappedLabel<'a, Ctx> {
    pub fn new(text: &'a str, width: i32) -> WrappedLabel<'a, Ctx> {
        WrappedLabel {
            text: text,
            width: width,
            style: Ctx::TextStyle::default(),
        }
    }

    pub fn text(self, text: &'a str) -> Self {
        WrappedLabel {
            text: text,
            .. self
        }
    }

    pub fn width(self, width: i32) -> Self {
        WrappedLabel {
            width: width,
            .. self
        }
    }

    pub fn style(self, style: Ctx::TextStyle) -> Self {
        WrappedLabel {
            style: style,
            .. self
        }
    }


    fn iter_lines(&self) -> WrappedLines<'a, Ctx> {
        WrappedLines {
            text: self.text,
            width: self.width,
            style: self.style,
        }
    }
}

struct WrappedLines<'a, Ctx: Context> {
    text: &'a str,
    width: i32,
    style: Ctx::TextStyle,
}

impl<'a, Ctx: Context> WrappedLines<'a, Ctx> {
    fn next(&mut self, ctx: &Ctx) -> Option<&'a str> {
        if self.text.len() == 0 {
            return None;
        }

        let mut x = 0;
        let mut word_start = None;
        for (i, b) in self.text.bytes().enumerate() {
            if b == b' ' {
                if let Some(start) = word_start {
                    word_start = None;

                    // Try adding the current word to the line
                    let word = &self.text[start .. i];
                    x += ctx.text_size(word, self.style).x;

                    // Push this word onto the next line?  The `start != 0` check prevents an
                    // infinite loop if a single word is too long for a line.
                    if x > self.width && start != 0 {
                        // We include the spaces in the preceding line.  Note that the spaces may
                        // extend past EOL (we don't check for this), but this should be okay
                        // because spaces are invisible.
                        let (line, rest) = self.text.split_at(start);
                        self.text = rest;
                        return Some(line);
                    }
                }
                x += ctx.text_space_width(self.style);
            } else {
                if word_start.is_none() {
                    word_start = Some(i);
                }
            }
        }

        let split_idx;
        if let Some(start) = word_start {
            let word = &self.text[start ..];
            x += ctx.text_size(word, self.style).x;

            // If the last word put the width over the limit, then we stop after the previous word.
            // Otherwise, we take the entire remainder of the text.
            if x > self.width && start != 0 {
                split_idx = start;
            } else {
                split_idx = self.text.len();
            }
        } else {
            // The text ended with some spaces.  The previous word was under the limit, and spaces
            // don't count, so emit the whole text.
            split_idx = self.text.len();
        };

        let (line, rest) = self.text.split_at(split_idx);
        self.text = rest;
        Some(line)
    }
}

impl<'a, Ctx: Context> Widget<Ctx> for WrappedLabel<'a, Ctx> {
    type Event = ();

    fn min_size(&self, ctx: &Ctx) -> Point {
        let mut lines = 0;
        let mut iter = self.iter_lines();
        while let Some(_) = iter.next(ctx) {
            lines += 1;
        }

        Point { x: self.width, y: lines as i32 * ctx.text_line_height(self.style) }
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        let mut y = 0;
        let h = ctx.text_line_height(self.style);

        let mut iter = self.iter_lines();
        while let Some(line) = iter.next(ctx) {
            let bounds = Rect::new(0, y, self.width, y + h);
            ctx.with_bounds(bounds, |ctx| ctx.draw_text(line, self.style));
            y += h;
        }
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        match evt {
            MouseEvent::Down(_) => UIResult::NoEvent,
            MouseEvent::Up(_) => {
                if ctx.mouse_pressed_over() {
                    UIResult::Event(())
                } else {
                    UIResult::NoEvent
                }
            },
            _ => UIResult::Unhandled,
        }
    }
}
