use context::Context;
use event::{KeyEvent, MouseEvent, UIResult};
use geom::*;
use widget::Widget;


pub struct Dialog<'a, Ctx: Context, W> {
    child: W,
    title: &'a str,
    style: Ctx::DialogStyle,
}

impl<'a, Ctx: Context, W> Dialog<'a, Ctx, W> {
    pub fn new(child: W, title: &'a str) -> Dialog<'a, Ctx, W> {
        Dialog {
            child: child,
            title: title,
            style: Ctx::DialogStyle::default(),
        }
    }

    pub fn style(self, style: Ctx::DialogStyle) -> Self {
        Self {
            style: style,
            .. self
        }
    }

    fn with_child_bounds<F, R>(&self, ctx: &mut Ctx, f: F) -> R
            where F: FnOnce(&mut Ctx) -> R {
        let (nw, se) = ctx.dialog_border_size(self.style);
        let bounds = Rect::sized(ctx.cur_bounds().size()).inset(nw.x, se.x, nw.y, se.y);
        ctx.with_bounds(bounds, f)
    }
}

impl<'a, Ctx: Context, W: Widget<Ctx>> Widget<Ctx> for Dialog<'a, Ctx, W> {
    type Event = W::Event;

    fn min_size(&self, ctx: &Ctx) -> Point {
        let (nw, se) = ctx.dialog_border_size(self.style);
        self.child.min_size(ctx) + nw + se
    }

    fn on_paint(&self, ctx: &mut Ctx) {
        ctx.draw_dialog(self.title, self.style);
        self.with_child_bounds(ctx, |ctx| self.child.on_paint(ctx));
    }

    fn on_key(&self, ctx: &mut Ctx, evt: KeyEvent<Ctx::Key>) -> UIResult<Self::Event> {
        self.with_child_bounds(ctx, |ctx| self.child.on_key(ctx, evt))
    }

    fn on_mouse(&self, ctx: &mut Ctx, evt: MouseEvent<Ctx::Button>) -> UIResult<Self::Event> {
        self.with_child_bounds(ctx, |ctx| {
            if !ctx.mouse_target() {
                return UIResult::Unhandled;
            }
            self.child.on_mouse(ctx, evt)
        })
    }

    fn check_drop(&self, ctx: &mut Ctx, data: &Ctx::DropData) -> bool {
        self.with_child_bounds(ctx, |ctx| {
            if !ctx.mouse_target() {
                return false;
            }
            self.child.check_drop(ctx, data)
        })
    }

    fn on_drop(&self, ctx: &mut Ctx, data: &Ctx::DropData) -> UIResult<Self::Event> {
        self.with_child_bounds(ctx, |ctx| {
            if !ctx.mouse_target() {
                return UIResult::Unhandled;
            }
            self.child.on_drop(ctx, data)
        })
    }
}
