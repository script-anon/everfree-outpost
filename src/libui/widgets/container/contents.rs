use std::marker::PhantomData;
use std::ops::Range;

use context::Context;
use widget::Widget;

use super::ChildWidget;


pub trait Contents<Ctx: Context, R> {
    /// Return the number of child widgets in this sequence.
    fn len(&self) -> usize;
    /// Apply a visitor to each child widget in this sequence.
    fn accept<V: Visitor<Ctx, R>>(&self, v: &mut V);
    /// Apply a visitor to each child widget in this sequence, in reverse order.
    fn accept_rev<V: Visitor<Ctx, R>>(&self, v: &mut V);
    /// Apply a visitor to the child widget at index `n`.
    fn accept_nth<V: Visitor<Ctx, R>>(&self, v: &mut V, n: usize);
}

pub trait Visitor<Ctx: Context, R> {
    fn visit<W, F>(&mut self, cw: &ChildWidget<W, F>)
        where W: Widget<Ctx>, F: Fn(W::Event) -> R;
}


impl<Ctx, R, W1, F1, C2> Contents<Ctx, R> for (ChildWidget<W1, F1>, C2)
        where Ctx: Context,
              W1: Widget<Ctx>,
              F1: Fn(W1::Event) -> R,
              C2: Contents<Ctx, R> {
    #[inline]
    fn len(&self) -> usize {
        1 + self.1.len()
    }

    fn accept<V: Visitor<Ctx, R>>(&self, v: &mut V) {
        v.visit(&self.0);
        self.1.accept(v);
    }

    fn accept_rev<V: Visitor<Ctx, R>>(&self, v: &mut V) {
        self.1.accept_rev(v);
        v.visit(&self.0);
    }

    fn accept_nth<V: Visitor<Ctx, R>>(&self, v: &mut V, n: usize) {
        if n == 0 {
            v.visit(&self.0);
        } else {
            self.1.accept_nth(v, n - 1);
        }
    }
}

#[allow(unused_variables)]
impl<Ctx: Context, R> Contents<Ctx, R> for () {
    #[inline]
    fn len(&self) -> usize { 0 }
    fn accept<V: Visitor<Ctx, R>>(&self, v: &mut V) {}
    fn accept_rev<V: Visitor<Ctx, R>>(&self, v: &mut V) {}
    fn accept_nth<V: Visitor<Ctx, R>>(&self, v: &mut V, n: usize) {}
}


pub struct GenWidgets<W, F, G> {
    range: Range<usize>,
    gen: G,
    _marker: PhantomData<fn() -> ChildWidget<W, F>>,
}

impl<W, F, G> GenWidgets<W, F, G>
        where G: Fn(usize) -> ChildWidget<W, F> {
    pub fn new(range: Range<usize>, gen: G) -> GenWidgets<W, F, G> {
        GenWidgets {
            range: range,
            gen: gen,
            _marker: PhantomData,
        }
    }
}

impl<Ctx, R, W, F, G> Contents<Ctx, R> for GenWidgets<W, F, G>
        where Ctx: Context,
              W: Widget<Ctx>,
              F: Fn(W::Event) -> R,
              G: Fn(usize) -> ChildWidget<W, F> {
    fn len(&self) -> usize {
        self.range.end - self.range.start
    }

    fn accept<V: Visitor<Ctx, R>>(&self, v: &mut V) {
        for idx in self.range.clone() {
            let cw = (self.gen)(idx);
            v.visit(&cw);
        }
    }

    fn accept_rev<V: Visitor<Ctx, R>>(&self, v: &mut V) {
        for idx in self.range.clone().rev() {
            let cw = (self.gen)(idx);
            v.visit(&cw);
        }
    }

    fn accept_nth<V: Visitor<Ctx, R>>(&self, v: &mut V, n: usize) {
        let idx = self.range.start + n;
        if idx < self.range.end {
            let cw = (self.gen)(idx);
            v.visit(&cw);
        }
    }
}


impl<Ctx, R, W, F> Contents<Ctx, R> for [ChildWidget<W, F>]
        where Ctx: Context,
              W: Widget<Ctx>,
              F: Fn(W::Event) -> R {
    fn len(&self) -> usize {
        self.len()
    }

    fn accept<V: Visitor<Ctx, R>>(&self, v: &mut V) {
        for cw in self {
            v.visit(cw);
        }
    }

    fn accept_rev<V: Visitor<Ctx, R>>(&self, v: &mut V) {
        for cw in self.iter().rev() {
            v.visit(cw);
        }
    }

    fn accept_nth<V: Visitor<Ctx, R>>(&self, v: &mut V, n: usize) {
        v.visit(&self[n]);
    }
}

impl<Ctx, R, W, F> Contents<Ctx, R> for Vec<ChildWidget<W, F>>
        where Ctx: Context,
              W: Widget<Ctx>,
              F: Fn(W::Event) -> R {
    fn len(&self) -> usize {
        self.len()
    }

    fn accept<V: Visitor<Ctx, R>>(&self, v: &mut V) {
        (**self).accept(v);
    }

    fn accept_rev<V: Visitor<Ctx, R>>(&self, v: &mut V) {
        (**self).accept_rev(v);
    }

    fn accept_nth<V: Visitor<Ctx, R>>(&self, v: &mut V, n: usize) {
        v.visit(&self[n]);
    }
}


#[macro_export]
macro_rules! contents {
    () => { () };
    ($a:expr $(, $rest:expr)*) => { ($a, contents!($($rest),*)) };
    ($($a:expr,)*) => { contents!($($a),*) };
}
