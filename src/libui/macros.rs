#[macro_export]
macro_rules! try_handle {
    ($e:expr) => {
        match $e {
            $crate::event::UIResult::Unhandled => {},
            r => { return r; },
        }
    };
}
