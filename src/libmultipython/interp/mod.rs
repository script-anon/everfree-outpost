use std::any::Any;
use std::collections::HashSet;
use std::env;
use std::mem;
use std::ops::Deref;
use std::panic::{self, AssertUnwindSafe};
use std::ptr;
use python3_sys::*;

use builtin::BuiltinTypes;
use marker::NonPy;

use self::type_map::TypeMap;

mod lock;
mod type_map;


pub struct Interp {
    tstate: *mut PyThreadState,
    state_map: TypeMap,
}

impl Interp {
    pub fn new() -> Interp {
        // Set up the Python thread state first
        let tstate = unsafe { interp_init() };
        let mut i = Interp {
            tstate: tstate,
            state_map: TypeMap::new(),
        };

        i.add_state(BuiltinTypes::new);
        i
    }

    /// Run some code with the GIL held inside this interpreter's context.
    ///
    /// The `NonPy` bound helps prevent leaking Python objects to outside code, where their
    /// destructors might (unsafely) run without an active interpreter.
    pub fn enter<F: FnOnce() -> R + NonPy, R: NonPy>(&mut self, f: F) -> R {
        unsafe { self.enter_unchecked(f) }
    }

    /// Like `enter` but without the `NonPy` bound.
    pub unsafe fn enter_unchecked<F: FnOnce() -> R, R>(&mut self, f: F) -> R {
        PyEval_AcquireThread(self.tstate);
        // Modify CUR_INTERP only while the GIL is held
        let old_interp = mem::replace(&mut CUR_INTERP, self);

        let r = panic::catch_unwind(AssertUnwindSafe(f));

        CUR_INTERP = old_interp;
        PyEval_ReleaseThread(self.tstate);

        match r {
            Ok(x) => x,
            Err(e) => panic!(e),
        }
    }

    /// Run some code with the GIL released.
    ///
    /// The `NonPy` bound helps prevent leaking Python objects into the closure, where it might
    /// manipulate or destruct them while the interpreter is not active.
    pub fn leave<F: FnOnce() -> R + NonPy, R: NonPy>(f: F) -> R {
        unsafe {
            let old_interp = mem::replace(&mut CUR_INTERP, ptr::null());
            let tstate = PyEval_SaveThread();

            // FIXME: handle panics in `f`
            let r = f();

            PyEval_RestoreThread(tstate);
            CUR_INTERP = old_interp;
            r
        }
    }

    pub fn add_state<T: Any, F: FnOnce() -> T>(&mut self, f: F) {
        unsafe {
            let map = &mut self.state_map as *mut TypeMap;
            self.enter_unchecked(|| {
                let s = f();
                // Need to do the insert inside an interpreter context, because if it fails, the
                // inserted element will be dropped.
                (*map).insert(Box::new(s));
            });
        }
    }

    /// Get a reference to an interpreter state object, if one exists with the given type.
    pub fn get_state_direct<T: Any>(&self) -> Option<&T> {
        self.state_map.get()
    }

    /// Get a reference to an interpreter state object, if one exists with the given type.
    pub fn get_state<T: Any>() -> Option<StateGuard<T>> {
        unsafe {
            assert!(!CUR_INTERP.is_null());
            (*CUR_INTERP).state_map.get().map(|r| StateGuard(r))
        }
    }
}

impl Drop for Interp {
    fn drop(&mut self) {
        // Tear down Rust state first.  The destructor must run in an interpreter context.
        let state_map = mem::replace(&mut self.state_map, TypeMap::new());
        unsafe { self.enter_unchecked(|| drop(state_map)) };

        // Now tear down remaining Python state
        unsafe { interp_fini(self.tstate) };
    }
}

static mut CUR_INTERP: *const Interp = 0 as *const _;


/// Wrap a static ref and limit its lifetime to that of a guard object.  When working with pointers
/// obtained through global variables (which naturally, but unsoundly, have `'static` lifetime),
/// this makes it harder to accidentally leak `'static` refs into other data structures.
pub struct StateGuard<T: 'static>(&'static T);

impl<T> Deref for StateGuard<T> {
    type Target = T;

    fn deref(&self) -> &T {
        self.0
    }
}

impl<T> !NonPy for StateGuard<T> {}


// Low-level interpreter initialization

static mut INTERP_COUNT: usize = 0;

/// "Main" Python thread state, which is created by `Py_InitializeEx`.  No `Interp` uses this
/// thread state for normal execution - its only purpose is to provide a "current thread" for
/// calling `Py_NewInterpreter`.  (Note that while the `Py_NewInterpreter` claims that you don't
/// need a current thread state on entry, you do need to be holding the GIL, and acquiring the GIL
/// once it's been initialized requires a current thread state.)
static mut MAIN_THREAD: *mut PyThreadState = 0 as *mut _;

unsafe fn interp_init() -> *mut PyThreadState {
    let g = lock::lock();
    let tstate;
    {   // Interp lock critical section

        // Acquire the GIL and enter the main thread state.
        if INTERP_COUNT == 0 {
            assert!(Py_IsInitialized() == 0);
            fix_pythonpath();
            Py_InitializeEx(0);
            PyEval_InitThreads();
            MAIN_THREAD = PyThreadState_Get();
        } else {
            PyEval_AcquireThread(MAIN_THREAD);
        }

        tstate = Py_NewInterpreter();
        PyEval_ReleaseThread(tstate);

        INTERP_COUNT += 1;
    }
    mem::drop(g);

    tstate
}

unsafe fn interp_fini(tstate: *mut PyThreadState) {
    let g = lock::lock();
    {   // Interp lock critical section

        {   // GIL critical section
            PyEval_AcquireThread(tstate);
            Py_EndInterpreter(tstate);
            let _ = PyThreadState_Swap(MAIN_THREAD);
            PyEval_ReleaseThread(MAIN_THREAD);
        }

        INTERP_COUNT -= 1;
        
        if INTERP_COUNT == 0 {
            PyEval_AcquireThread(MAIN_THREAD);
            Py_Finalize();
            MAIN_THREAD = ptr::null_mut();
        }
    }
    mem::drop(g);
}

/// This is a workaround for a bug in Python 3.7.  When `$PYTHONPATH` contains `setuptools` along
/// with three pairs of duplicated entries, `Py_NewInterpreter` crashes while initializing the
/// `site` module.  We deduplicate `$PYTHONPATH` on startup to avoid the problem.
fn fix_pythonpath() {
    if let Some(pp) = env::var_os("PYTHONPATH") {
        let mut seen = HashSet::new();
        let mut parts = Vec::new();
        for p in env::split_paths(&pp) {
            if !seen.contains(&p) {
                seen.insert(p.clone());
                parts.push(p);
            }
        }

        let new_pp = env::join_paths(parts).unwrap();
        env::set_var("PYTHONPATH", &new_pp);
    }
}
