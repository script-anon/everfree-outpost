//! A simple static lock.  `Interp` instances use this to synchronize initialization and
//! finalization of Python interpreter states.
//!
//! # Design
//!
//! The lock is implemented using a queue of `Thread`s, where the thread at the front of the queue
//! holds the lock, and all others are waiting.  The queue itself is protected by a simple
//! spinlock.

use std::ptr;
use std::sync::atomic::{AtomicBool, ATOMIC_BOOL_INIT};
use std::sync::atomic::Ordering;
use std::thread::{self, Thread};


// Spinlock

static SPINLOCK: AtomicBool = ATOMIC_BOOL_INIT;

unsafe fn spin_lock() {
    while SPINLOCK.compare_and_swap(false, true, Ordering::Acquire) != false {
        thread::yield_now();
    }
}

unsafe fn spin_unlock() {
    SPINLOCK.store(false, Ordering::Release);
}


// Queue

// Access to `HEAD`, `TAIL`, and `node.next` fields is allowed only while the spinlock is held.

struct Node {
    thread: Thread,
    next: *mut Node,
}

static mut HEAD: *mut Node = 0 as *mut _;
static mut TAIL: *mut Node = 0 as *mut _;

unsafe fn acquire() -> Box<Node> {
    let mut node = Box::new(Node {
        thread: thread::current(),
        next: ptr::null_mut(),
    });
    let ptr: *mut Node = &mut *node;


    spin_lock();

    if HEAD.is_null() {
        HEAD = ptr;
        TAIL = ptr;
    } else {
        (*TAIL).next = ptr;
        TAIL = ptr;

        while HEAD != ptr {
            spin_unlock();
            thread::park();
            spin_lock();
        }
    }

    spin_unlock();


    node
}

unsafe fn release(node: Box<Node>) {
    let mut wake = None;


    spin_lock();

    HEAD = node.next;
    if HEAD.is_null() {
        TAIL = ptr::null_mut();
    } else {
        // Clone the thread handle.  We don't want to be holding the spinlock when we
        // `unpark()` the waiting thread, but it's not safe to look at `HEAD` or `node.next`
        // after we release it.
        wake = Some((*HEAD).thread.clone());
    }

    spin_unlock();


    if let Some(thread) = wake {
        thread.unpark();
    }

    drop(node);
}


// Public API

pub struct Guard(Option<Box<Node>>);

pub fn lock() -> Guard {
    let node = unsafe { acquire() };
    Guard(Some(node))
}

impl Drop for Guard {
    fn drop(&mut self) {
        if let Some(node) = self.0.take() {
            unsafe { release(node) };
        }
    }
}
