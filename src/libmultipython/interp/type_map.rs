use std::collections::HashMap;
use std::any::{TypeId, Any};

pub struct TypeMap {
    // No fancy space-saving tricks here.  Each entry needs a pointer to the actual value and
    // either a pointer to a vtable or a pointer to the `drop()` function.  We might consider using
    // the vtable/drop pointer as the key, expecting it to be distinct for every type, but this is
    // actually wrong: vtables and `drop()` functions are both marked `unnamed_addr` at the LLVM
    // level, which means the optimizer can coalesce these items even across types if their bodies
    // are identical.
    map: HashMap<TypeId, Box<Any>>,
}

impl TypeMap {
    pub fn new() -> TypeMap {
        TypeMap {
            map: HashMap::new(),
        }
    }

    pub fn insert<T: Any>(&mut self, v: Box<T>) {
        let k = TypeId::of::<T>();
        assert!(!self.map.contains_key(&k));
        self.map.insert(k, v as Box<Any>);
    }

    #[allow(dead_code)]
    pub fn remove<T: Any>(&mut self) -> Option<Box<T>> {
        let k = TypeId::of::<T>();
        self.map.remove(&k).map(|v| v.downcast().unwrap())
    }

    pub fn get<T: Any>(&self) -> Option<&T> {
        let k = TypeId::of::<T>();
        self.map.get(&k).map(|v| v.downcast_ref().unwrap())
    }

    #[allow(dead_code)]
    pub fn get_mut<T: Any>(&mut self) -> Option<&mut T> {
        let k = TypeId::of::<T>();
        self.map.get_mut(&k).map(|v| v.downcast_mut().unwrap())
    }

    #[allow(dead_code)]
    pub fn contains<T: Any>(&self) -> bool {
        let k = TypeId::of::<T>();
        self.map.contains_key(&k)
    }
}
