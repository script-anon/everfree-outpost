//! A replacement for `PyModule` that doesn't require a separately allocated `PyModuleDef`.

use std::any::TypeId;
use std::mem;
use std::ptr;
use std::raw::TraitObject;
use libc::{c_char, c_int, c_uint, c_void};
use python3_sys::*;

use api as py;
use builtin::BuiltinTypes;
use exc::{self, PyResult};
use ptr::{PyBox, PyRef};


//
// Public API
//

/// Construct a new `Module` object.
pub fn new<T: ModuleData>(name: &'static str, data: T) -> PyResult<PyBox> {
    let bt = BuiltinTypes::get();
    unsafe { new_typed(bt.module.borrow(), name, data) }
}

/// Construct a new `Module` object.  The `ty` argument must be the result of a previous
/// `module::register()` call.
///
/// This function is unsafe because passing the wrong type can cause undefined behavior.
pub unsafe fn new_typed<T: ModuleData>(ty: PyRef,
                                       name: &'static str,
                                       data: T) -> PyResult<PyBox> {
    let dct = py::dict::new()?;

    let obj = py::type_::instantiate_var(ty, mem::size_of::<T>())?;
    // Nothing may fail between here and the end of the block.  This ensures we don't end up with a
    // partially initialized `Module`.
    {
        let f = obj.as_ptr() as *mut Module<T>;
        ptr::write(&mut (*f).dict, dct);
        ptr::write(&mut (*f).name, name);
        ptr::write(&mut (*f).vtable, get_vtable::<T>());
        ptr::write(&mut (*f).data, data);
    }

    py::object::set_attr_str(obj.borrow(),
                             "__name__",
                             py::unicode::from_str(name)?.borrow())?;

    Ok(obj)
}

/// Try to get module data of type `T` from the module object `obj`.  Returns `None` if `obj` is
/// not a module, `obj`'s module data has already been dropped, or `obj`'s module data does not
/// have type `T`.
pub fn get<'a, T: 'static>(obj: PyRef<'a>) -> Option<&'a T> {
    if check::<T>(obj) {
        Some(unsafe { get_raw(obj) })
    } else {
        None
    }
}

/// Try to get a mutable reference to module data of type `T` from `obj`.  This fails under the
/// same conditions as `get`.
///
/// `get` and `get_mut` perform no run-time borrow checking.  Therefore it is the caller's
/// responsibility to ensure that references returned from `get_mut` don't overlap in lifetime with
/// other references to the same object.
pub unsafe fn get_mut<'a, T: 'static>(obj: PyRef<'a>) -> Option<&'a mut T> {
    if check::<T>(obj) {
        Some(get_raw_mut(obj))
    } else {
        None
    }
}

/// Like `get`, but only checks that `obj`'s module data has not been dropped.
///
/// Memory-unsafe if `obj` is not a module or `obj`'s module data does not have type `T`.
pub unsafe fn get_unchecked<'a, T: 'static>(obj: PyRef<'a>) -> Option<&'a T> {
    let m = &*(obj.as_ptr() as *const Module<T>);
    if m.vtable.is_null() {
        return None;
    }
    Some(get_raw(obj))
}

/// Like `get_mut`, but only checks that `obj`'s module data has not been dropped.
///
/// Memory-unsafe for the same reasons as both `get_mut` and `get_unchecked`.
pub unsafe fn get_mut_unchecked<'a, T: 'static>(obj: PyRef<'a>) -> Option<&'a mut T> {
    let m = &*(obj.as_ptr() as *const Module<T>);
    if m.vtable.is_null() {
        return None;
    }
    Some(get_raw_mut(obj))
}

unsafe fn get_raw<'a, T: 'static>(obj: PyRef<'a>) -> &'a T {
    let m = &*(obj.as_ptr() as *const Module<T>);
    &m.data
}

unsafe fn get_raw_mut<'a, T: 'static>(obj: PyRef<'a>) -> &'a mut T {
    let m = &mut *(obj.as_ptr() as *mut Module<T>);
    &mut m.data
}

/// Check whether a Python object is a module with a certain data type.
///
/// On modules that were initialized with type `T` but whose data has since been dropped, this
/// function returns `false`.
pub fn check<T: 'static>(obj: PyRef) -> bool {
    if !py::object::is_instance(obj, BuiltinTypes::get().module.borrow()) {
        return false;
    }

    unsafe {
        let m = &*(obj.as_ptr() as *const Module<()>);
        if m.vtable.is_null() {
            return false;
        }

        let v = use_vtable(m.vtable);
        if v.type_id() != TypeId::of::<T>() {
            return false;
        }

        true
    }
}


pub trait ModuleData: 'static {
    fn traverse<F>(&self, visit: F) -> Result<(), c_int>
        where F: Fn(PyRef) -> Result<(), c_int>;
}

impl ModuleData for () {
    fn traverse<F>(&self, _visit: F) -> Result<(), c_int>
            where F: Fn(PyRef) -> Result<(), c_int> {
        Ok(())
    }
}


//
// Implementation
//

#[repr(C)]
struct Module<T: 'static> {
    #[allow(dead_code)]
    base: PyVarObject,
    dict: PyBox,
    name: &'static str,
    /// A vtable pointer for `<VtableMarker as ModuleDataVtable<T>>`.  If null, then the module
    /// data has already been dropped.
    vtable: *mut (),
    data: T,
}


/// Helper trait for manipulating module data.  `Module<T>` keeps a `&'static ModuleDataVtable<T>`,
/// which provides methods for manipulating module data without knowing its concrete type.  We
/// can't just keep the vtable pointer for `T as ModuleData` because some of the methods we want to
/// call are static.
///
/// It's important that all methods here take `T` by pointer or reference, since we will later lose
/// the precise type information (replacing `T` with `()`).
trait ModuleDataVtable<T> {
    unsafe fn drop_in_place(&self, obj: *mut T);
    fn traverse(&self, obj: &T, visit: &Fn(PyRef) -> Result<(), c_int>) -> Result<(), c_int>;
    fn type_id(&self) -> TypeId;
}

struct VtableMarker;
static VTABLE_MARKER: VtableMarker = VtableMarker;

impl<T: ModuleData> ModuleDataVtable<T> for VtableMarker {
    unsafe fn drop_in_place(&self, obj: *mut T) {
        ptr::drop_in_place(obj as *mut T);
    }

    fn traverse(&self, obj: &T, visit: &Fn(PyRef) -> Result<(), c_int>) -> Result<(), c_int> {
        obj.traverse(visit)
    }

    fn type_id(&self) -> TypeId {
        TypeId::of::<T>()
    }

    // FIXME: need align_of here, to deal with ModuleData types that are aligned to more than
    // pointer width.  Then we need adjustments everywhere m.data is accessed.
}


unsafe fn get_vtable<T: ModuleData>() -> *mut () {
    let v = &VtableMarker as &ModuleDataVtable<T>;
    let raw: TraitObject = mem::transmute(v);
    raw.vtable
}

unsafe fn use_vtable(vtable: *mut ()) -> &'static ModuleDataVtable<()> {
    let raw = TraitObject {
        data: &VTABLE_MARKER as *const VtableMarker as *mut (),
        vtable: vtable,
    };
    mem::transmute(raw)
}


unsafe extern "C" fn repr(obj: *mut PyObject) -> *mut PyObject {
    let m = &*(obj as *mut Module<()>);
    let r = py::unicode::from_str(&format!("<built-in module {}>", m.name));
    exc::return_result(r)
}

unsafe extern "C" fn dealloc(obj: *mut PyObject) {
    clear(obj);

    // `clear` dropped `m.data`, now let's drop the rest of it.
    let m = &mut *(obj as *mut Module<()>);
    ptr::drop_in_place(&mut m.dict);
    ptr::drop_in_place(&mut m.name);
    ptr::drop_in_place(&mut m.vtable);
    // `m.data` was already dropped.

    let ty = Py_TYPE(obj);
    let free: freefunc = mem::transmute(PyType_GetSlot(ty, Py_tp_free));
    free(obj as *mut c_void);
}

unsafe extern "C" fn traverse(obj: *mut PyObject, visit: visitproc, arg: *mut c_void) -> c_int {
    let m = &*(obj as *mut Module<()>);
    if m.vtable.is_null() {
        return 0;
    }
    let v = use_vtable(m.vtable);
    let r = v.traverse(&m.data, &|obj| {
        let r = visit(obj.as_ptr(), arg);
        if r == 0 { Ok(()) } else { Err(r) }
    });

    if let Err(r) = r { r } else { 0 }
}

unsafe extern "C" fn clear(obj: *mut PyObject) {
    let m = &mut *(obj as *mut Module<()>);

    // `clear` may get called from both the cycle collector and `dealloc`.
    if !m.vtable.is_null() {
        // It's important to clear `m.vtable` first.  Dropping `m.data` may run arbitrary python
        // code while dropping, and we shouldn't let it observe `m` in an inconsistent state.
        let vtable = use_vtable(mem::replace(&mut m.vtable, ptr::null_mut()));
        vtable.drop_in_place(&mut m.data);
    }

    py::dict::clear(m.dict.borrow());
}

unsafe extern "C" fn getattro(obj: *mut PyObject, key: *mut PyObject) -> *mut PyObject {
    let m = &*(obj as *mut Module<()>);
    let key = PyRef::new_non_null(key);
    exc::return_result(getattro_inner(m.dict.borrow(), key))
}

unsafe fn getattro_inner(dct: PyRef, key: PyRef) -> PyResult<PyBox> {
    if py::unicode::eq_ascii(key, "__dict__\0") {
        return Ok(dct.to_box());
    }

    if let Some(val) = py::dict::get_item(dct, key)? {
        Ok(val.to_box())
    } else {
        pyraise!(attribute_error, "no such attribute");
    }
}

unsafe extern "C" fn setattro(obj: *mut PyObject,
                              key: *mut PyObject,
                              value: *mut PyObject) -> c_int {
    let m = &*(obj as *mut Module<()>);
    let key = PyRef::new_non_null(key);
    let value = PyRef::new_opt(value);
    exc::return_result_int(setattro_inner(m.dict.borrow(), key, value))
}

unsafe fn setattro_inner(dct: PyRef,
                         key: PyRef,
                         value: Option<PyRef>) -> PyResult<()> {
    if py::unicode::eq_ascii(key, "__dict__\0") {
        pyraise!(attribute_error, "cannot set __dict__ of module");
    }

    if let Some(value) = value {
        py::dict::set_item(dct, key, value)
    } else {
        py::dict::del_item(dct, key)
    }
}

pub fn register() -> PyResult<PyBox> {
    unsafe {
        macro_rules! slot {
            ($slot:expr, $val:expr) => {
                PyType_Slot { slot: $slot, pfunc: mem::transmute($val) }
            };
        }

        let mut slots = [
            slot!(Py_tp_repr, repr as reprfunc),
            slot!(Py_tp_dealloc, dealloc as destructor),
            slot!(Py_tp_traverse, traverse as traverseproc),
            slot!(Py_tp_clear, clear as destructor),
            slot!(Py_tp_getattro, getattro as getattrofunc),
            slot!(Py_tp_setattro, setattro as setattrofunc),
            slot!(0, 0_u64),
        ];

        let mut spec = PyType_Spec {
            name: "module\0".as_ptr() as *const c_char,
            basicsize: mem::size_of::<Module<()>>() as c_int,
            itemsize: 1,
            flags: (Py_TPFLAGS_DEFAULT | Py_TPFLAGS_HAVE_GC) as c_uint,
            slots: slots.as_mut_ptr(),
        };

        let raw = PyType_FromSpec(&mut spec);
        PyBox::new(raw)
    }
}



//
// Tests
//

#[cfg(test)]
mod test {
    use std::cell::RefCell;
    use super::*;
    use interp::Interp;
    use util;

    #[test]
    fn basic() {
        let mut i = Interp::new();
        let s = i.enter(|| {
            let m = new("m", ()).unwrap();
            assert!(get::<()>(m.borrow()).is_some());
            assert!(get::<u32>(m.borrow()).is_none());
            py::object::repr(m.borrow()).unwrap()
        });
        assert_eq!(&s, "<built-in module m>")
    }

    #[test]
    fn cyclic_gc() {
        struct MyState {
            m: RefCell<Option<PyBox>>,
        }

        impl ModuleData for MyState {
            fn traverse<F>(&self, visit: F) -> Result<(), c_int>
                    where F: Fn(PyRef) -> Result<(), c_int> {
                if let Some(ref m) = *self.m.borrow() {
                    visit(m.borrow())?;
                }
                Ok(())
            }
        }

        static mut DROPPED: bool = false;
        impl Drop for MyState {
            fn drop(&mut self) {
                unsafe { DROPPED = true };
            }
        }


        let mut i = Interp::new();
        i.enter(|| {
            // Create a reference cycle.  `m`'s `data` field refers back to `m`.
            let m = new("m", MyState {
                m: RefCell::new(None),
            }).unwrap();
            {
                let data = get::<MyState>(m.borrow()).unwrap();
                *data.m.borrow_mut() = Some(m.clone());
            }

            // Now make sure `m` can be collected without tearing down the whole interpreter.
            assert!(unsafe { !DROPPED });
            drop(m);
            util::exec(r"import gc; gc.collect()").unwrap();
            assert!(unsafe { DROPPED });
        });
    }
}
