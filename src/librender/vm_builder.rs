use std::borrow::Borrow;
use std::cmp;
use std::collections::HashMap;
use std::hash::Hash;
use std::mem;
use std::ops::Index;

use ir::{self, VarId, PrimTy, IntSize};
use vm::*;


#[derive(Clone, Copy, Debug)]
pub struct VarInfo {
    addr: Addr,
    pty: PrimTy,
    len: u8,
}

pub struct VmBuilder {
    insns: Vec<Insn>,
    vars: HashMap<VarId, VarInfo>,
    geom_layouts: HashMap<VarId, HashMap<String, VarInfo>>,
}

#[allow(dead_code)]
impl VmBuilder {
    pub fn new() -> VmBuilder {
        VmBuilder {
            insns: Vec::new(),
            vars: HashMap::new(),
            geom_layouts: HashMap::new(),
        }
    }

    pub fn add_var(&mut self, v: VarId, addr: Addr, pty: PrimTy, len: u8) {
        self.vars.insert(v, VarInfo { addr, pty, len });
    }

    pub fn add_var_layout(&mut self, buf_idx: u8, layout: &Layout<VarId>) {
        for (name, field) in &layout.fields {
            let info = VarInfo {
                addr: Addr::new(buf_idx, field.offset),
                pty: field.pty,
                len: field.len,
            };
            self.vars.insert(name.clone(), info);
        }
    }

    pub fn add_geom_field(&mut self,
                          geom: VarId,
                          name: String,
                          addr: Addr,
                          pty: PrimTy,
                          len: u8) {
        self.geom_layouts.entry(geom).or_insert_with(HashMap::new)
            .insert(name, VarInfo { addr, pty, len });
    }

    pub fn add_geom_layout(&mut self, buf_var: VarId, buf_idx: u8, layout: &Layout<String>) {
        assert!(!self.geom_layouts.contains_key(&buf_var));
        let mut vars = HashMap::with_capacity(layout.fields.len());
        for (name, field) in &layout.fields {
            let info = VarInfo {
                addr: Addr::new(buf_idx, field.offset),
                pty: field.pty,
                len: field.len,
            };
            vars.insert(name.clone(), info);
        }
        self.geom_layouts.insert(buf_var, vars);
    }


    pub fn emit(&mut self, insn: Insn) {
        self.insns.push(insn);
    }

    pub fn emit_copy_var(&mut self, src: VarId, dest: Addr) {
        let info = self.vars[&src];
        self.emit_copy(dest, info);
    }

    fn emit_copy(&mut self, dest: Addr, info: VarInfo) {
        for i in 0 .. info.len {
            let off = i as u16 * prim_size(info.pty);
            let src = info.addr.add(off);
            let dest = dest.add(off);
            self.insns.push(copy_insn(prim_int_size(info.pty), src, dest));
        }
    }

    pub fn emit_ir(&mut self, dest: Addr, insn: &ir::Insn) {
        match *insn {
            ir::Insn::ConstI(sz, x) => self.insns.push(int_insn(sz, dest, x)),

            ir::Insn::ConstU(sz, x) => self.insns.push(uint_insn(sz, dest, x)),

            ir::Insn::ConstF(x) => self.insns.push(Insn::ConstF(dest, x)),

            ir::Insn::Copy(sv) => {
                let info = self.vars[&sv];
                self.emit_copy(dest, info);
            },

            ir::Insn::VecIndex(sv, idx) => {
                let info = &self.vars[&sv];
                let off = idx as u16 * prim_size(info.pty);
                let src = info.addr.add(off);
                self.insns.push(copy_insn(prim_int_size(info.pty), src, dest));
            },

            ir::Insn::VecSwizzle(sv, idxs) => {
                let info = &self.vars[&sv];
                for (j, &i) in idxs.iter().enumerate() {
                    let src = info.addr.add(i as u16 * prim_size(info.pty));
                    let dest = dest.add(j as u16 * prim_size(info.pty));
                    self.insns.push(copy_insn(prim_int_size(info.pty), src, dest));
                }
            },

            ir::Insn::VecLen(sv) => {
                let info = &self.vars[&sv];
                self.insns.push(uint_insn(IntSize::I32, dest, info.len as u32));
            },

            ir::Insn::TextureSize(sv) => {
                // Textures are represented as a pair of u32, `(width, height)`.  This is the same
                // as the output type, so we just emit a copy.
                let info = self.vars[&sv];
                self.emit_copy(dest, info);
            },

            ir::Insn::GeomField(sv, name) => {
                let info = self.geom_layouts[&sv][*name.0];
                self.emit_copy(dest, info);
            },

            ir::Insn::VecCtor(pty, svs) => {
                for (i, &sv) in svs.iter().enumerate() {
                    let info = &self.vars[&sv];
                    assert!(info.pty == pty);
                    let src = info.addr;
                    let dest = dest.add(i as u16 * prim_size(pty));
                    self.insns.push(copy_insn(prim_int_size(pty), src, dest));
                }
            },

            ir::Insn::TextureCtor(_fmt, svs) => {
                for (i, &sv) in svs.iter().enumerate() {
                    let info = &self.vars[&sv];
                    assert!(info.pty == PrimTy::Uint(IntSize::I32));
                    let src = info.addr;
                    let dest = dest.add(i as u16 * 4);
                    self.insns.push(copy_insn(IntSize::I32, src, dest));
                }
            },

            ir::Insn::Coerce(sv, pty) => {
                let info = self.vars[&sv];
                self.insns.push(coerce_insn(info.pty, pty, info.addr, dest));
            },
        }
    }

    /// Take the final list of instructions from this builder.  Afterward, the builder can be
    /// reused to build another program using the same variable and geometry layouts.
    pub fn finish(&mut self) -> Vec<Insn> {
        mem::replace(&mut self.insns, Vec::new())
    }
}

fn int_insn(sz: IntSize, dest: Addr, x: i32) -> Insn {
    match sz {
        IntSize::I8 => Insn::ConstI_8(dest, x),
        IntSize::I16 => Insn::ConstI_16(dest, x),
        IntSize::I32 => Insn::ConstI_32(dest, x),
    }
}

fn uint_insn(sz: IntSize, dest: Addr, x: u32) -> Insn {
    match sz {
        IntSize::I8 => Insn::ConstU_8(dest, x),
        IntSize::I16 => Insn::ConstU_16(dest, x),
        IntSize::I32 => Insn::ConstU_32(dest, x),
    }
}

fn copy_insn(sz: IntSize, src: Addr, dest: Addr) -> Insn {
    match sz {
        IntSize::I8 => Insn::Copy_8(src, dest),
        IntSize::I16 => Insn::Copy_16(src, dest),
        IntSize::I32 => Insn::Copy_32(src, dest),
    }
}

fn coerce_insn(pty1: PrimTy, pty2: PrimTy, src: Addr, dest: Addr) -> Insn {
    use vm::Insn::*;

    match (pty1, pty2) {
        // Int/Uint -> Float
        (PrimTy::Int(sz), PrimTy::Float) => match sz {
            IntSize::I8 => IntToFloat_8(src, dest),
            IntSize::I16 => IntToFloat_16(src, dest),
            IntSize::I32 => IntToFloat_32(src, dest),
        },
        (PrimTy::Uint(sz), PrimTy::Float) => match sz {
            IntSize::I8 => UintToFloat_8(src, dest),
            IntSize::I16 => UintToFloat_16(src, dest),
            IntSize::I32 => UintToFloat_32(src, dest),
        },

        // Float -> Int/Uint
        (PrimTy::Float, PrimTy::Int(sz)) => match sz {
            IntSize::I8 => FloatToInt_8(src, dest),
            IntSize::I16 => FloatToInt_16(src, dest),
            IntSize::I32 => FloatToInt_32(src, dest),
        },
        (PrimTy::Float, PrimTy::Uint(sz)) => match sz {
            IntSize::I8 => FloatToUint_8(src, dest),
            IntSize::I16 => FloatToUint_16(src, dest),
            IntSize::I32 => FloatToUint_32(src, dest),
        },

        // Float -> Float
        (PrimTy::Float, PrimTy::Float) => Copy_32(src, dest),

        // Int/Uint -> Int/Uint copy
        (PrimTy::Int(IntSize::I8), PrimTy::Int(IntSize::I8)) |
        (PrimTy::Int(IntSize::I8), PrimTy::Uint(IntSize::I8)) |
        (PrimTy::Uint(IntSize::I8), PrimTy::Int(IntSize::I8)) |
        (PrimTy::Uint(IntSize::I8), PrimTy::Uint(IntSize::I8)) =>
            Copy_8(src, dest),
        (PrimTy::Int(IntSize::I16), PrimTy::Int(IntSize::I16)) |
        (PrimTy::Int(IntSize::I16), PrimTy::Uint(IntSize::I16)) |
        (PrimTy::Uint(IntSize::I16), PrimTy::Int(IntSize::I16)) |
        (PrimTy::Uint(IntSize::I16), PrimTy::Uint(IntSize::I16)) =>
            Copy_16(src, dest),
        (PrimTy::Int(IntSize::I32), PrimTy::Int(IntSize::I32)) |
        (PrimTy::Int(IntSize::I32), PrimTy::Uint(IntSize::I32)) |
        (PrimTy::Uint(IntSize::I32), PrimTy::Int(IntSize::I32)) |
        (PrimTy::Uint(IntSize::I32), PrimTy::Uint(IntSize::I32)) =>
            Copy_32(src, dest),

        // Int/Uint -> Int/Uint truncation
        (PrimTy::Int(IntSize::I32), PrimTy::Int(IntSize::I8)) |
        (PrimTy::Int(IntSize::I32), PrimTy::Uint(IntSize::I8)) |
        (PrimTy::Uint(IntSize::I32), PrimTy::Int(IntSize::I8)) |
        (PrimTy::Uint(IntSize::I32), PrimTy::Uint(IntSize::I8)) =>
            Trunc_32_8(src, dest),
        (PrimTy::Int(IntSize::I32), PrimTy::Int(IntSize::I16)) |
        (PrimTy::Int(IntSize::I32), PrimTy::Uint(IntSize::I16)) |
        (PrimTy::Uint(IntSize::I32), PrimTy::Int(IntSize::I16)) |
        (PrimTy::Uint(IntSize::I32), PrimTy::Uint(IntSize::I16)) =>
            Trunc_32_16(src, dest),
        (PrimTy::Int(IntSize::I16), PrimTy::Int(IntSize::I8)) |
        (PrimTy::Int(IntSize::I16), PrimTy::Uint(IntSize::I8)) |
        (PrimTy::Uint(IntSize::I16), PrimTy::Int(IntSize::I8)) |
        (PrimTy::Uint(IntSize::I16), PrimTy::Uint(IntSize::I8)) =>
            Trunc_16_8(src, dest),

        // Uint -> Int/Uint zero extension
        (PrimTy::Uint(IntSize::I8), PrimTy::Int(IntSize::I16)) |
        (PrimTy::Uint(IntSize::I8), PrimTy::Uint(IntSize::I16)) =>
            ZeroExt_8_16(src, dest),
        (PrimTy::Uint(IntSize::I8), PrimTy::Int(IntSize::I32)) |
        (PrimTy::Uint(IntSize::I8), PrimTy::Uint(IntSize::I32)) =>
            ZeroExt_8_32(src, dest),
        (PrimTy::Uint(IntSize::I16), PrimTy::Int(IntSize::I32)) |
        (PrimTy::Uint(IntSize::I16), PrimTy::Uint(IntSize::I32)) =>
            ZeroExt_16_32(src, dest),

        // Int -> Int/Uint sign extension
        (PrimTy::Int(IntSize::I8), PrimTy::Int(IntSize::I16)) |
        (PrimTy::Int(IntSize::I8), PrimTy::Uint(IntSize::I16)) =>
            SignExt_8_16(src, dest),
        (PrimTy::Int(IntSize::I8), PrimTy::Int(IntSize::I32)) |
        (PrimTy::Int(IntSize::I8), PrimTy::Uint(IntSize::I32)) =>
            SignExt_8_32(src, dest),
        (PrimTy::Int(IntSize::I16), PrimTy::Int(IntSize::I32)) |
        (PrimTy::Int(IntSize::I16), PrimTy::Uint(IntSize::I32)) =>
            SignExt_16_32(src, dest),
    }
}

fn prim_int_size(pty: PrimTy) -> IntSize {
    match pty {
        PrimTy::Int(sz) | PrimTy::Uint(sz) => sz,
        PrimTy::Float => IntSize::I32,
    }
}

fn prim_size(pty: PrimTy) -> u16 {
    prim_int_size(pty).size() as u16
}


#[derive(Clone)]
pub struct Layout<K> {
    fields: HashMap<K, Field>,
    size: u16,
    align: u16,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub struct Field {
    pub offset: u16,
    pub pty: PrimTy,
    pub len: u8,
}

impl<K> Layout<K> {
    pub fn new() -> Layout<K>
            where K: Hash+Eq {
        Layout {
            fields: HashMap::new(),
            size: 0,
            align: 1,
        }
    }

    pub fn add(&mut self, name: K, pty: PrimTy, len: u8) -> u16
            where K: Hash+Eq {
        let field_size = pty.size() as u16 * len as u16;
        let field_align = pty.size() as u16;
        let offset = align_to(self.size, field_align);
        self.fields.insert(name, Field { offset, pty, len });
        self.size = offset + field_size;
        self.align = cmp::max(self.align, field_align);
        offset
    }

    pub fn get<Q>(&self, key: &Q) -> Option<&Field>
            where K: Hash+Eq+Borrow<Q>, Q: Hash+Eq {
        self.fields.get(key)
    }

    pub fn size(&self) -> u16 {
        (self.size + self.align - 1) & !(self.align - 1)
    }

    pub fn align(&self) -> u16 {
        self.align
    }
}

impl<'a, K, Q> Index<&'a Q> for Layout<K>
        where K: Hash+Eq+Borrow<Q>, Q: Hash+Eq {
    type Output = Field;
    fn index(&self, key: &'a Q) -> &Field {
        &self.fields[key]
    }
}

fn align_to(pos: u16, align: u16) -> u16 {
    (pos + align - 1) & !(align - 1)
}

