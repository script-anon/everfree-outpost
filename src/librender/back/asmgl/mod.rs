use std::cell::RefCell;
use std::cmp;
use std::collections::HashMap;
use std::mem;
use std::rc::Rc;
use std::usize;

use rustc_arena::DroplessArena;

use asmgl as ffi;

use ast::{self, IntSize, PrimTy, TexFormat, RenderConfig};
use front;
use intern::Interners;
use ir::{self, VarId};
use traits;
use traits::Geometry as GeometryTrait;
use traits::Texture as TextureTrait;
use transform;
use vm;
use vm_builder::{Layout, Field};
use util::AlignedBytes;

use self::gl_objects::{Framebuffer, Buffer, Shader, Texture};

mod compile;
mod gl_objects;



pub struct Backend {
    output_size: (u16, u16),
}

impl Backend {
    pub fn new() -> Backend {
        Backend {
            output_size: (800, 600),
        }
    }
}

impl traits::Backend for Backend {
    type Pass = Pass;
    type Texture = TextureHandle;
    type Geometry = GeometryHandle;

    fn compile(&mut self, pipeline: &ast::Pipeline) -> HashMap<String, Self::Pass> {
        let arena = DroplessArena::new();
        let intern = Interners::new(&arena);
        let p = front::lower_pipeline(&pipeline, &intern);

        let has_draw_buffers = unsafe { ffi::asmgl_has_draw_buffers() != 0 };
        // TODO: also check that GL_MAX_DRAW_BUFFERS > 1
        let has_depth_texture = unsafe { ffi::asmgl_has_depth_texture() != 0 };

        let mut passes = HashMap::new();
        for (id, pass) in p.passes.iter() {
            if !pass.public {
                continue;
            }

            let mut pass = transform::inline::flatten_pass(&intern, p, id);
            pass = transform::copy_propagate(&intern, pass);
            if !has_depth_texture {
                pass = transform::replace_depth::replace_depth(&intern, pass);
            }
            // Order matters: the depth_texture workaround adds an additional color output, which
            // the draw_buffers workaround may need to turn into an additional render step.
            if !has_draw_buffers {
                pass = transform::split_buffers::split_buffers(&intern, pass);
            }
            passes.insert((**pass.name.0).to_owned(), Pass::new(pass));
        }
        passes
    }

    fn new_texture(&self, fmt: TexFormat, size: (u16, u16)) -> TextureHandle {
        TextureHandle(Rc::new(Texture::new(fmt, size)))
    }

    fn texture_from_image(&self, path: &str) -> TextureHandle {
        TextureHandle(Rc::new(Texture::from_image(path)))
    }

    fn output_texture(&self) -> TextureHandle {
        TextureHandle(Rc::new(Texture::output(self.output_size)))
    }

    fn geometry_from_layout(&self, size: usize, align: usize) -> Self::Geometry {
        GeometryHandle(Rc::new(RefCell::new(Geometry::new(size as u16, align as u16))))
    }
}


pub struct Pass {
    uniform_prog: vm::Program,

    l_args: Layout<VarId>,
    l_uniforms: Layout<VarId>,

    b_args: AlignedBytes,
    b_uniforms: AlignedBytes,

    name_map: HashMap<String, VarId>,
    geometry: HashMap<VarId, GeometryHandle>,
    textures: HashMap<VarId, TextureHandle>,

    shaders: Vec<Shader>,
    buffers: Vec<BufferInfo>,
    framebuffers: Vec<Framebuffer>,

    buffer_updates: Vec<BufferUpdate>,
    ops: Vec<Op>,
}

enum Op {
    Clear(ClearInfo),
    Render(RenderInfo),
}

struct ClearInfo {
    framebuffer: usize,
    color: Option<(PassBuffer, Field)>,
    depth: Option<(PassBuffer, Field)>,
    texture_attachments: Vec<TextureAttachment>,
    viewport: Viewport,
}

struct RenderInfo {
    shader: usize,
    framebuffer: usize,

    uniform_bindings: Vec<UniformBinding>,
    attrib_bindings: Vec<AttribBinding>,
    texture_bindings: Vec<TextureBinding>,
    texture_attachments: Vec<TextureAttachment>,
    viewport: Viewport,

    config: RenderConfig,
}

enum Viewport {
    /// Set viewport automatically based on the size of texture `.0`.
    AutoSize(VarId),
    /// Get an explicit viewport size from buffer `.0`, field `.1`.
    Explicit(PassBuffer, Field),
}

struct UniformBinding {
    loc: i32,
    buf: PassBuffer,
    field: Field,
}

struct AttribBinding {
    loc: i32,
    /// Index of the OpenGL buffer in `Pass::buffers` to use for this attribute.
    buf: usize,
    field: Field,
    kind: AttribKind,
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
enum AttribKind {
    Vertex,
    Instance,
}

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
enum PassBuffer {
    Args,
    Uniforms,
}

struct TextureBinding {
    /// Location of the texture to bind.
    src: VarId,
    /// Texture unit to bind the texture to.
    unit: usize,
}

struct TextureAttachment {
    /// Location of the texture to attach.
    src: VarId,
    /// Which framebuffer attachment point to attach it to.
    attach: i8,
}

struct BufferInfo {
    buffer: Buffer,

    /// Number of bytes between consecutive elements.  This is derived from the `TyKind::Geometry`
    /// field list, and doesn't change when a new `Geometry` object is assigned (rather, the
    /// geometry object is checked to ensure its entry size matches this number).
    stride: usize,
    /// Number of elements currently in the buffer.  This changes as the `Geometry` object does.
    len: usize,
}

struct BufferUpdate {
    src: VarId,
    dest: usize,
}


impl Pass {
    fn new(ir: ir::Pass) -> Pass {
        compile::compile_pass(ir)
    }

    fn read_array<T>(&self,
                     src: PassBuffer,
                     field: &Field,
                     mut f: impl FnMut(u32) -> T) -> (usize, [T; 4])
            where T: Default {
        let src_bytes = match src {
            PassBuffer::Args => &self.b_args,
            PassBuffer::Uniforms => &self.b_uniforms,
        };

        let mut i = 0;
        let mut buf = [T::default(), T::default(), T::default(), T::default()];
        for x in read_field(src_bytes, field) {
            assert!(i < 4, "too many values for field");
            buf[i] = f(x);
            i += 1;
        }
        (i, buf)
    }

    fn run_clear(&self, info: &ClearInfo) {
        // Update framebuffer attachments
        let fb = &self.framebuffers[info.framebuffer];
        if info.texture_attachments.len() == 1 &&
           info.texture_attachments[0].attach == 0 &&
           self.textures[&info.texture_attachments[0].src].is_output() {
            Framebuffer::bind_default();
        } else {
            fb.bind();
            for att in &info.texture_attachments {
                let tex = &self.textures[&att.src];
                fb.attach(&tex.0, att.attach);
            }
        }

        let must_clear_scissor = self.set_viewport_scissor(&info.viewport);

        unsafe {
            if let Some((buf, field)) = info.color {
                let (len, [r,g,b,a]) = self.read_array(buf, &field, f32::from_bits);
                assert!(len == 4);
                ffi::asmgl_clear_color(r, g, b, a);
            }
            if let Some((buf, field)) = info.depth {
                let (len, [d,_,_,_]) = self.read_array(buf, &field, f32::from_bits);
                assert!(len == 1);
                ffi::asmgl_clear_depth(d);
            }
            // TODO: set only the appropriate color/depth bits in clear()
            ffi::asmgl_clear();

            if must_clear_scissor {
                ffi::asmgl_scissor_disable();
            }
        }
    }

    fn run_render(&self, info: &RenderInfo) {
        // Create the shader
        let shader = &self.shaders[info.shader];

        // Set shader uniforms
        for bnd in &info.uniform_bindings {
            let (len, vals) = self.read_array(bnd.buf, &bnd.field, f32::from_bits);
            shader.set_uniform_f(bnd.loc, &vals[..len]);
        }

        // Set up texture units
        for bnd in &info.texture_bindings {
            let tex = &self.textures[&bnd.src];
            tex.bind(bnd.unit);
            // No need to set uniforms - the shader is already configured to expect the texture to
            // appear on `bnd.unit`.
        }

        // Set up attribute bindings
        let mut vert_count = None;
        let mut inst_count = None;
        for bnd in &info.attrib_bindings {
            let buf = &self.buffers[bnd.buf];

            buf.buffer.bind();
            unsafe {
                ffi::asmgl_enable_vertex_attrib_array(bnd.loc);
                ffi::asmgl_vertex_attrib_pointer(
                    bnd.loc,
                    bnd.field.len as usize,
                    ffi_data_type(bnd.field.pty) as u8,
                    0,  // don't normalize
                    buf.stride,
                    bnd.field.offset as usize);
            }

            let count =
                if bnd.kind == AttribKind::Vertex { &mut vert_count }
                else { &mut inst_count };
            let count = count.get_or_insert(buf.len);
            *count = cmp::min(*count, buf.len);
        }
        assert!(inst_count.is_none(), "instanced rendering nyi");
        let vert_count = vert_count.expect("render stmt has no vertex bindings?");

        // Update framebuffer attachments
        let fb = &self.framebuffers[info.framebuffer];
        if info.texture_attachments.len() == 1 &&
           info.texture_attachments[0].attach == 0 &&
           self.textures[&info.texture_attachments[0].src].is_output() {
            Framebuffer::bind_default();
        } else {
            fb.bind();
            for att in &info.texture_attachments {
                let tex = &self.textures[&att.src];
                fb.attach(&tex.0, att.attach);
            }
        }

        // Set viewport
        self.set_viewport(&info.viewport);

        // Set up render config
        unsafe {
            ffi::asmgl_set_depth_test(ffi_depth_test(info.config.depth_test) as u8);
            ffi::asmgl_set_blend_mode(ffi_blend_mode(info.config.blend_mode) as u8);
        }

        // Actually render
        shader.bind();
        unsafe { ffi::asmgl_draw_arrays_triangles(0, vert_count) };

        // Disable vertex arrays
        for bnd in &info.attrib_bindings {
            unsafe {
                ffi::asmgl_disable_vertex_attrib_array(bnd.loc);
            }
        }
    }

    fn set_field(&mut self,
                 name: &str,
                 expect_ty: PrimTy,
                 len: usize,
                 vals: impl Iterator<Item=u32>) {
        let v = self.name_map[name];
        let f = self.l_args.get(&v)
            .unwrap_or_else(|| panic!("no such argument `{}`", name));

        // Note we only look at the discriminant of `expect_ty`, not its fields.
        match expect_ty {
            PrimTy::Int(_) => match f.pty {
                PrimTy::Int(_) => {},
                pty => panic!("argument `{}` is {:?}, not Int", name, pty),
            },
            PrimTy::Uint(_) => match f.pty {
                PrimTy::Uint(_) => {},
                pty => panic!("argument `{}` is {:?}, not Uint", name, pty),
            },
            PrimTy::Float => match f.pty {
                PrimTy::Float => {},
                pty => panic!("argument `{}` is {:?}, not Float", name, pty),
            },
        };

        assert!(len == f.len as usize,
                "field `{}` expected {} elements, not {}", name, f.len, len);
        write_field(&mut self.b_args, f, vals);
    }

    fn set_viewport(&self, vp: &Viewport) {
        let (x, y, w, h) = match *vp {
            Viewport::AutoSize(v) => {
                let (w, h) = self.textures[&v].size();
                (0, 0, w as i32, h as i32)
            },
            Viewport::Explicit(buf, field) => {
                let (len, [x, y, w, h]) = self.read_array(buf, &field, |x| x as i32);
                assert!(len == 4);
                (x, y, w, h)
            },
        };

        unsafe {
            ffi::asmgl_viewport(x, y, w, h);
        }
    }

    /// Enable the scissor test, clipped to `vp`, if `vp` doesn't cover the entire output buffer.
    /// Returns `true` if the caller should disable the viewport when finished.
    fn set_viewport_scissor(&self, vp: &Viewport) -> bool {
        match *vp {
            Viewport::AutoSize(_) => false,
            Viewport::Explicit(buf, field) => {
                let (len, [x, y, w, h]) = self.read_array(buf, &field, |x| x as i32);
                assert!(len == 4, "Viewport::Explicit expected 4 elements, not {}", len);
                unsafe {
                    ffi::asmgl_scissor(x, y, w, h);
                }
                true
            },
        }
    }
}

fn ffi_data_type(p: PrimTy) -> ffi::DataType {
    match p {
        PrimTy::Int(IntSize::I8) => ffi::DataType::I8,
        PrimTy::Int(IntSize::I16) => ffi::DataType::I16,
        PrimTy::Int(IntSize::I32) => ffi::DataType::I32,
        PrimTy::Uint(IntSize::I8) => ffi::DataType::U8,
        PrimTy::Uint(IntSize::I16) => ffi::DataType::U16,
        PrimTy::Uint(IntSize::I32) => ffi::DataType::U32,
        PrimTy::Float => ffi::DataType::F32,
    }
}

fn ffi_blend_mode(m: ir::BlendMode) -> ffi::BlendMode {
    match m {
        ir::BlendMode::None => ffi::BlendMode::None,
        ir::BlendMode::Alpha => ffi::BlendMode::Alpha,
        ir::BlendMode::Add => ffi::BlendMode::Add,
        ir::BlendMode::MultiplyInv => ffi::BlendMode::MultiplyInv,
    }
}

fn ffi_depth_test(m: ir::DepthTest) -> ffi::DepthMode {
    match m {
        ir::DepthTest::Disable => ffi::DepthMode::Disable,
        ir::DepthTest::GEqual => ffi::DepthMode::GEqual,
        ir::DepthTest::Equal => ffi::DepthMode::Equal,
        ir::DepthTest::Always => ffi::DepthMode::Always,
    }
}

fn write_field(buf: &mut [u8],
               f: &Field,
               vals: impl Iterator<Item=u32>) {
    let sz = match f.pty {
        PrimTy::Int(sz) => sz,
        PrimTy::Uint(sz) => sz,
        PrimTy::Float => IntSize::I32,
    };

    unsafe {
        assert!(f.offset as usize + sz.size() * f.len as usize <= buf.len(),
                "bad offset/size for field {:?}?", f);
        let ptr: *mut u8 = buf.as_mut_ptr().offset(f.offset as isize);
        assert!(ptr as usize % sz.size() == 0,
                "bad align for field {:?}?", f);

        // The I8 and I16 cases simply truncate the values, which is correct for both unsigned
        // and signed ints.  Floats always fall into the I32 case, which passes the value
        // through unchanged.
        match sz {
            IntSize::I8 => {
                for (i, x) in vals.take(f.len as usize).enumerate() {
                    *(ptr as *mut u8).offset(i as isize) = x as u8;
                }
            },
            IntSize::I16 => {
                for (i, x) in vals.take(f.len as usize).enumerate() {
                    *(ptr as *mut u16).offset(i as isize) = x as u16;
                }
            },
            IntSize::I32 => {
                for (i, x) in vals.take(f.len as usize).enumerate() {
                    *(ptr as *mut u32).offset(i as isize) = x;
                }
            },
        }
    }
}

fn read_field<'a>(buf: &'a [u8],
                  f: &'a Field) -> impl Iterator<Item=u32>+'a {
    let sz = match f.pty {
        PrimTy::Int(sz) => sz,
        PrimTy::Uint(sz) => sz,
        PrimTy::Float => IntSize::I32,
    };

    unsafe {
        assert!(f.offset as usize + sz.size() * f.len as usize <= buf.len(),
                "bad offset/size for field {:?}?", f);
        let ptr: *const u8 = buf.as_ptr().offset(f.offset as isize);
        assert!(ptr as usize % sz.size() == 0,
                "bad align for field {:?}?", f);

        // We sign-extend in `Int` cases so that `result as i32` will do the right thing.
        (0 .. f.len).map(move |i| match f.pty {
            PrimTy::Int(IntSize::I8) =>
                *(ptr as *const i8).offset(i as isize) as i32 as u32,
            PrimTy::Int(IntSize::I16) =>
                *(ptr as *const i16).offset(i as isize) as i32 as u32,
            PrimTy::Int(IntSize::I32) =>
                *(ptr as *const i32).offset(i as isize) as i32 as u32,
            PrimTy::Uint(IntSize::I8) =>
                *(ptr as *const u8).offset(i as isize) as u32,
            PrimTy::Uint(IntSize::I16) =>
                *(ptr as *const u16).offset(i as isize) as u32,
            PrimTy::Uint(IntSize::I32) =>
                *(ptr as *const u32).offset(i as isize) as u32,
            PrimTy::Float =>
                (*(ptr as *const f32).offset(i as isize)).to_bits(),
        })
    }
}

impl traits::Pass for Pass {
    type Geometry = GeometryHandle;
    type Texture = TextureHandle;

    fn set_i(&mut self, name: &str, val: i32) {
        self.set_iv(name, &[val]);
    }

    fn set_u(&mut self, name: &str, val: u32) {
        self.set_uv(name, &[val]);
    }

    fn set_f(&mut self, name: &str, val: f32) {
        self.set_fv(name, &[val]);
    }

    fn set_iv(&mut self, name: &str, val: &[i32]) {
        self.set_field(name, PrimTy::Int(IntSize::I32), val.len(),
                       val.iter().map(|&x| x as u32));
    }

    fn set_uv(&mut self, name: &str, val: &[u32]) {
        self.set_field(name, PrimTy::Uint(IntSize::I32), val.len(),
                       val.iter().map(|&x| x));
    }

    fn set_fv(&mut self, name: &str, val: &[f32]) {
        self.set_field(name, PrimTy::Float, val.len(),
                       val.iter().map(|&x| x.to_bits()));
    }

    fn set_texture(&mut self, name: &str, tex: &TextureHandle) {
        let v = self.name_map[name];
        self.textures.insert(v, tex.clone());
    }

    fn set_geometry(&mut self, name: &str, geom: &GeometryHandle) {
        let v = self.name_map[name];
        self.geometry.insert(v, geom.clone());
    }

    fn run(&mut self) {
        // Copy texture sizes into arg buffer
        for (&v, tex) in &self.textures {
            if let Some(f) = self.l_args.get(&v) {
                let (w, h) = tex.size();
                let vals = [w as u32, h as u32];
                assert!(f.len == 2);
                write_field(&mut self.b_args, f, vals.iter().cloned());
            }
        }

        // Evaluate uniforms
        self.uniform_prog.run(&mut [
            vm::Buffer::Imm(&self.b_args),
            vm::Buffer::Mut(&mut self.b_uniforms),
        ]);

        // Update temporary textures
        for (&v, tex) in &mut self.textures {
            if let Some(f) = self.l_uniforms.get(&v) {
                assert!(f.len == 2);
                let mut it = read_field(&self.b_uniforms, f);
                let w = it.next().unwrap();
                let h = it.next().unwrap();
                assert!(it.next().is_none());
                tex.resize((w as u16, h as u16));
            }
        }

        // Update attrib buffers
        for update in &self.buffer_updates {
            let geom = self.geometry[&update.src].0.borrow();
            let buf = &mut self.buffers[update.dest];
            assert!(geom.size as usize == buf.stride,
                    "bad geometry size for buffer: {:?}", update.src);
            buf.len = geom.len();
            buf.buffer.set(&geom.bytes);
        }

        // Run rendering stmts
        for op in &self.ops {
            match *op {
                Op::Clear(ref c) => self.run_clear(c),
                Op::Render(ref r) => self.run_render(r),
            }
        }
    }
}


#[derive(Debug)]
pub struct Geometry {
    bytes: AlignedBytes,
    size: u16,
    align: u16,
}

impl Geometry {
    fn new(size: u16, align: u16) -> Geometry {
        Geometry {
            bytes: AlignedBytes::new(),
            size: size as u16,
            align: align as u16,
        }
    }
}

impl traits::Geometry for Geometry {
    fn set<T: Copy>(&mut self, idx: usize, x: &T) {
        assert!(mem::size_of::<T>() == self.size as usize);
        assert!(mem::align_of::<T>() <= self.align as usize);
        let pos = idx * self.size as usize;
        let slice = &mut self.bytes[pos .. pos + self.size as usize];
        unsafe { *(slice.as_mut_ptr() as *mut T) = *x; }
    }

    fn len(&self) -> usize {
        if self.size == 0 {
            return 0;
        }
        self.bytes.len() / self.size as usize
    }

    fn set_len(&mut self, len: usize) {
        let byte_len = len * self.size as usize;
        let old_byte_len = self.bytes.len();
        if byte_len > old_byte_len {
            self.bytes.extend_zero(byte_len - old_byte_len);
        } else if byte_len < old_byte_len {
            self.bytes.reduce(old_byte_len - byte_len);
        }
    }
}

#[derive(Clone, Debug)]
pub struct GeometryHandle(pub Rc<RefCell<Geometry>>);

impl traits::Geometry for GeometryHandle {
    fn set<T: Copy>(&mut self, idx: usize, x: &T) {
        self.0.borrow_mut().set(idx, x);
    }

    fn len(&self) -> usize {
        self.0.borrow().len()
    }

    fn set_len(&mut self, len: usize) {
        self.0.borrow_mut().set_len(len);
    }
}


#[derive(Clone, Debug)]
pub struct TextureHandle(pub Rc<Texture>);

impl TextureHandle {
    fn bind(&self, unit: usize) {
        self.0.bind(unit);
    }

    #[allow(unused)]
    fn name(&self) -> u32 {
        self.0.name()
    }
}

impl traits::Texture for TextureHandle {
    fn size(&self) -> (u16, u16) { self.0.size() }
    fn format(&self) -> TexFormat { self.0.format() }
    fn is_output(&self) -> bool { self.0.is_output() }

    /// Change the size of the texture, invalidating all image data it contains.
    fn resize(&mut self, size: (u16, u16)) { self.0.resize(size) }

    fn load_image(&mut self, path: &str) {
        if let Some(tex) = Rc::get_mut(&mut self.0) {
            tex.load_image(path);
            return;
        }
        self.0 = Rc::new(Texture::from_image(path));
    }

    fn load_data(&mut self, b: &[u8]) { self.0.load_data(b) }
    fn load_subdata(&mut self, offset: (u16, u16), size: (u16, u16), b: &[u8]) {
        self.0.load_subdata(offset, size, b);
    }
}
