use std::fmt;
use std::marker::PhantomData;
use std::{u8, u16, u32};


#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub struct Addr(u16);

impl Addr {
    pub fn new(buf: u8, offset: u16) -> Addr {
        assert!(buf < (1 << 5), "buffer index out of range");
        assert!(offset < (1 << 11), "offset out of range");
        Addr(((buf as u16) << 11) | offset)
    }

    pub fn buffer(self) -> u8 {
        (self.0 >> 11) as u8
    }

    pub fn offset(self) -> u16 {
        self.0 & ((1 << 11) - 1)
    }

    pub fn unpack(self) -> (u8, u16) {
        (self.buffer(), self.offset())
    }

    pub fn add(self, off: u16) -> Addr {
        Addr::new(self.buffer(), self.offset() + off)
    }
}

impl fmt::Debug for Addr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "(buf {}, +{})", self.buffer(), self.offset())
    }
}


#[derive(Clone, Copy, Debug)]
#[allow(non_camel_case_types, dead_code)]
pub enum Insn {
    ConstI_8(Addr, i32),
    ConstI_16(Addr, i32),
    ConstI_32(Addr, i32),
    ConstU_8(Addr, u32),
    ConstU_16(Addr, u32),
    ConstU_32(Addr, u32),
    ConstF(Addr, f32),

    Copy_8(Addr, Addr),
    Copy_16(Addr, Addr),
    Copy_32(Addr, Addr),

    ZeroExt_8_16(Addr, Addr),
    ZeroExt_8_32(Addr, Addr),
    ZeroExt_16_32(Addr, Addr),
    SignExt_8_16(Addr, Addr),
    SignExt_8_32(Addr, Addr),
    SignExt_16_32(Addr, Addr),
    Trunc_32_8(Addr, Addr),
    Trunc_32_16(Addr, Addr),
    Trunc_16_8(Addr, Addr),

    /// Convert signed integer to float
    IntToFloat_8(Addr, Addr),
    IntToFloat_16(Addr, Addr),
    IntToFloat_32(Addr, Addr),
    UintToFloat_8(Addr, Addr),
    UintToFloat_16(Addr, Addr),
    UintToFloat_32(Addr, Addr),
    /// Convert float to signed integer
    FloatToInt_8(Addr, Addr),
    FloatToInt_16(Addr, Addr),
    FloatToInt_32(Addr, Addr),
    FloatToUint_8(Addr, Addr),
    FloatToUint_16(Addr, Addr),
    FloatToUint_32(Addr, Addr),

    /// Convert unsigned int to normalized float.
    UintToFloatNorm_8(Addr, Addr),
    UintToFloatNorm_16(Addr, Addr),
    UintToFloatNorm_32(Addr, Addr),
}

impl Insn {
    /// Get the address and required alignment of this instruction's read access, if any.
    fn access_read(self) -> Option<(Addr, u8)> {
        use self::Insn::*;
        Some(match self {
            ConstI_8(_, _) |
            ConstU_8(_, _) |
            ConstI_16(_, _) |
            ConstU_16(_, _) |
            ConstI_32(_, _) |
            ConstU_32(_, _) |
            ConstF(_, _) => return None,

            Copy_8(a, _) |
            ZeroExt_8_16(a, _) |
            ZeroExt_8_32(a, _) |
            SignExt_8_16(a, _) |
            SignExt_8_32(a, _) |
            IntToFloat_8(a, _) |
            UintToFloat_8(a, _) |
            UintToFloatNorm_8(a, _) => (a, 1),

            Copy_16(a, _) |
            ZeroExt_16_32(a, _) |
            SignExt_16_32(a, _) |
            Trunc_16_8(a, _) |
            IntToFloat_16(a, _) |
            UintToFloat_16(a, _) |
            UintToFloatNorm_16(a, _) => (a, 2),

            Copy_32(a, _) |
            Trunc_32_8(a, _) |
            Trunc_32_16(a, _) |
            IntToFloat_32(a, _) |
            UintToFloat_32(a, _) |
            FloatToInt_8(a, _) |
            FloatToInt_16(a, _) |
            FloatToInt_32(a, _) |
            FloatToUint_8(a, _) |
            FloatToUint_16(a, _) |
            FloatToUint_32(a, _) |
            UintToFloatNorm_32(a, _) => (a, 4),
        })
    }

    /// Get the address and required alignment of this instruction's write access.
    fn access_write(self) -> (Addr, u8) {
        use self::Insn::*;
        match self {
            ConstI_8(b, _) |
            ConstU_8(b, _) |
            Copy_8(_, b) |
            Trunc_16_8(_, b) |
            Trunc_32_8(_, b) |
            FloatToInt_8(_, b) |
            FloatToUint_8(_, b) => (b, 1),

            ConstI_16(b, _) |
            ConstU_16(b, _) |
            Copy_16(_, b) |
            ZeroExt_8_16(_, b) |
            SignExt_8_16(_, b) |
            Trunc_32_16(_, b) |
            FloatToInt_16(_, b) |
            FloatToUint_16(_, b) => (b, 2),

            ConstI_32(b, _) |
            ConstU_32(b, _) |
            ConstF(b, _) |
            Copy_32(_, b) |
            ZeroExt_8_32(_, b) |
            ZeroExt_16_32(_, b) |
            SignExt_8_32(_, b) |
            SignExt_16_32(_, b) |
            IntToFloat_8(_, b) |
            IntToFloat_16(_, b) |
            IntToFloat_32(_, b) |
            UintToFloat_8(_, b) |
            UintToFloat_16(_, b) |
            UintToFloat_32(_, b) |
            FloatToInt_32(_, b) |
            FloatToUint_32(_, b) |
            UintToFloatNorm_8(_, b) |
            UintToFloatNorm_16(_, b) |
            UintToFloatNorm_32(_, b) => (b, 4),
        }
    }
}


#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Debug, Hash)]
pub enum Perm {
    Read,
    ReadWrite,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug, Hash)]
pub struct BufferSpec {
    pub size: u16,
    pub align: u8,
    pub perm: Perm,
}

impl BufferSpec {
    pub fn new(size: u16, align: u8, perm: Perm) -> BufferSpec {
        BufferSpec { size, align, perm }
    }
}

pub enum Buffer<'a> {
    Imm(&'a [u8]),
    Mut(&'a mut [u8]),
}

/// Invariant: every memory access in `insns` that accesses a buffer `b` is in-bounds and
/// well-aligned on any buffer whose size and alignment match `specs[b]`.
#[derive(Clone, Debug)]
pub struct Program {
    insns: Vec<Insn>,
    specs: Vec<BufferSpec>,
}

impl Program {
    pub fn validate(insns: Vec<Insn>, specs: Vec<BufferSpec>) -> Result<Program, String> {
        {
            let check_access = |i: usize, addr: Addr, align: u8, perm: Perm| -> Result<(), String> {
                let (buf, off) = addr.unpack();
                let spec = specs.get(buf as usize)
                    .ok_or_else(|| format!("insn {}: no such buffer {}", i, buf))?;
                // NB: we assume `align` == access size.
                if off + align as u16 > spec.size {
                    return Err(format!("insn {}: out of range for buffer {}: {} > {}",
                                       i, buf, off + align as u16, spec.size));
                }
                if align > spec.align {
                    // Required alignment exceeds buffer alignment, so this access could be misaligned
                    // in some buffers.
                    return Err(format!("insn {}: bad alignment for buffer {}: {} > {}",
                                       i, buf, align, spec.align));
                }
                if off % align as u16 != 0 {
                    return Err(format!("insn {}: misaligned access: offset {}, align {}",
                                       i, off, align));
                }
                if perm > spec.perm {
                    return Err(format!("insn {}: not enough permissions: {:?} > {:?}",
                                       i, perm, spec.perm));
                }
                Ok(())
            };

            for (i, insn) in insns.iter().enumerate() {
                let (addr, align) = insn.access_write();
                check_access(i, addr, align, Perm::ReadWrite)?;

                if let Some((addr, align)) = insn.access_read() {
                    check_access(i, addr, align, Perm::Read)?;
                }
            }
        }

        Ok(Program { insns, specs })
    }

    /// Check that `bufs` satisfy the requirements of `specs`.
    fn check_bufs(&self, bufs: &[Buffer]) {
        assert!(bufs.len() == self.specs.len(),
                "wrong number of bufs");
        for (i, (buf, spec)) in bufs.iter().zip(self.specs.iter()).enumerate() {
            // Check permissions, and get byte array as &[u8].
            let b = match buf {
                Buffer::Imm(b) => {
                    assert!(spec.perm <= Perm::Read);
                    b
                },
                Buffer::Mut(b) => {
                    assert!(spec.perm <= Perm::ReadWrite);
                    b as &[u8]
                },
            };

            // Check size and alignment
            assert!(b.len() >= spec.size as usize,
                    "buf {} too small: {} < {}", i, b.len(), spec.size);
            let addr = b.as_ptr() as usize;
            assert!(addr & (spec.align as usize - 1) == 0,
                    "buf {} is misaligned: expected {}", i, spec.align);
        }
    }

    pub fn run(&self, bufs: &mut [Buffer]) {
        unsafe {
            self.check_bufs(bufs);

            for insn in &self.insns {
                interp_insn(RawBuffers { bufs }, insn);
            }
        }
    }
}


struct RawBuffer<'a> {
    ptr: *mut u8,
    _marker: PhantomData<&'a mut ()>,
}

impl<'a> RawBuffer<'a> {
    unsafe fn load_u8(&self, off: u16) -> u8 {
        *(self.ptr.offset(off as isize) as *const u8)
    }

    unsafe fn load_u16(&self, off: u16) -> u16 {
        *(self.ptr.offset(off as isize) as *const u16)
    }

    unsafe fn load_u32(&self, off: u16) -> u32 {
        *(self.ptr.offset(off as isize) as *const u32)
    }

    unsafe fn load_i8(&self, off: u16) -> i8 {
        *(self.ptr.offset(off as isize) as *const i8)
    }

    unsafe fn load_i16(&self, off: u16) -> i16 {
        *(self.ptr.offset(off as isize) as *const i16)
    }

    unsafe fn load_i32(&self, off: u16) -> i32 {
        *(self.ptr.offset(off as isize) as *const i32)
    }

    unsafe fn load_f32(&self, off: u16) -> f32 {
        *(self.ptr.offset(off as isize) as *const f32)
    }


    unsafe fn store_u8(&mut self, off: u16, x: u8) {
        *(self.ptr.offset(off as isize) as *mut u8) = x;
    }

    unsafe fn store_u16(&mut self, off: u16, x: u16) {
        *(self.ptr.offset(off as isize) as *mut u16) = x;
    }

    unsafe fn store_u32(&mut self, off: u16, x: u32) {
        *(self.ptr.offset(off as isize) as *mut u32) = x;
    }

    unsafe fn store_i8(&mut self, off: u16, x: i8) {
        *(self.ptr.offset(off as isize) as *mut i8) = x;
    }

    unsafe fn store_i16(&mut self, off: u16, x: i16) {
        *(self.ptr.offset(off as isize) as *mut i16) = x;
    }

    unsafe fn store_i32(&mut self, off: u16, x: i32) {
        *(self.ptr.offset(off as isize) as *mut i32) = x;
    }

    unsafe fn store_f32(&mut self, off: u16, x: f32) {
        *(self.ptr.offset(off as isize) as *mut f32) = x;
    }
}

struct RawBuffers<'a, 'b: 'a> {
    bufs: &'a mut [Buffer<'b>],
}

impl<'a, 'b> RawBuffers<'a, 'b> {
    unsafe fn buffer(&self, idx: u8) -> RawBuffer {
        let ptr = match *self.bufs.get_unchecked(idx as usize) {
            Buffer::Imm(ref b) => b.as_ptr(),
            Buffer::Mut(ref b) => b.as_ptr(),
        };
        RawBuffer {
            ptr: ptr as *mut u8,
            _marker: PhantomData,
        }
    }


    unsafe fn load_u8(&self, addr: Addr) -> u8 {
        self.buffer(addr.buffer()).load_u8(addr.offset())
    }

    unsafe fn load_u16(&self, addr: Addr) -> u16 {
        self.buffer(addr.buffer()).load_u16(addr.offset())
    }

    unsafe fn load_u32(&self, addr: Addr) -> u32 {
        self.buffer(addr.buffer()).load_u32(addr.offset())
    }

    unsafe fn load_i8(&self, addr: Addr) -> i8 {
        self.buffer(addr.buffer()).load_i8(addr.offset())
    }

    unsafe fn load_i16(&self, addr: Addr) -> i16 {
        self.buffer(addr.buffer()).load_i16(addr.offset())
    }

    unsafe fn load_i32(&self, addr: Addr) -> i32 {
        self.buffer(addr.buffer()).load_i32(addr.offset())
    }

    unsafe fn load_f32(&self, addr: Addr) -> f32 {
        self.buffer(addr.buffer()).load_f32(addr.offset())
    }


    unsafe fn store_u8(&mut self, addr: Addr, x: u8) {
        self.buffer(addr.buffer()).store_u8(addr.offset(), x);
    }

    unsafe fn store_u16(&mut self, addr: Addr, x: u16) {
        self.buffer(addr.buffer()).store_u16(addr.offset(), x);
    }

    unsafe fn store_u32(&mut self, addr: Addr, x: u32) {
        self.buffer(addr.buffer()).store_u32(addr.offset(), x);
    }

    unsafe fn store_i8(&mut self, addr: Addr, x: i8) {
        self.buffer(addr.buffer()).store_i8(addr.offset(), x);
    }

    unsafe fn store_i16(&mut self, addr: Addr, x: i16) {
        self.buffer(addr.buffer()).store_i16(addr.offset(), x);
    }

    unsafe fn store_i32(&mut self, addr: Addr, x: i32) {
        self.buffer(addr.buffer()).store_i32(addr.offset(), x);
    }

    unsafe fn store_f32(&mut self, addr: Addr, x: f32) {
        self.buffer(addr.buffer()).store_f32(addr.offset(), x);
    }
}


unsafe fn interp_insn(mut raw: RawBuffers, insn: &Insn) {
    use self::Insn::*;
    match *insn {
        ConstI_8(dest, x) => raw.store_i8(dest, x as i8),
        ConstI_16(dest, x) => raw.store_i16(dest, x as i16),
        ConstI_32(dest, x) => raw.store_i32(dest, x),
        ConstU_8(dest, x) => raw.store_u8(dest, x as u8),
        ConstU_16(dest, x) => raw.store_u16(dest, x as u16),
        ConstU_32(dest, x) => raw.store_u32(dest, x),
        ConstF(dest, x) => raw.store_f32(dest, x),

        Copy_8(src, dest) => {
            let x = raw.load_u8(src);
            raw.store_u8(dest, x);
        },
        Copy_16(src, dest) => {
            let x = raw.load_u16(src);
            raw.store_u16(dest, x);
        },
        Copy_32(src, dest) => {
            let x = raw.load_u32(src);
            raw.store_u32(dest, x);
        },

        ZeroExt_8_16(src, dest) => {
            let x = raw.load_u8(src);
            raw.store_u16(dest, x as u16);
        },
        ZeroExt_8_32(src, dest) => {
            let x = raw.load_u8(src);
            raw.store_u32(dest, x as u32);
        },
        ZeroExt_16_32(src, dest) => {
            let x = raw.load_u16(src);
            raw.store_u32(dest, x as u32);
        },

        SignExt_8_16(src, dest) => {
            let x = raw.load_i8(src);
            raw.store_i16(dest, x as i16);
        },
        SignExt_8_32(src, dest) => {
            let x = raw.load_i8(src);
            raw.store_i32(dest, x as i32);
        },
        SignExt_16_32(src, dest) => {
            let x = raw.load_i16(src);
            raw.store_i32(dest, x as i32);
        },

        Trunc_32_8(src, dest) => {
            let x = raw.load_u32(src);
            raw.store_u8(dest, x as u8);
        },
        Trunc_32_16(src, dest) => {
            let x = raw.load_u32(src);
            raw.store_u16(dest, x as u16);
        },
        Trunc_16_8(src, dest) => {
            let x = raw.load_u16(src);
            raw.store_u8(dest, x as u8);
        },

        IntToFloat_8(src, dest) => {
            let x = raw.load_i8(src);
            raw.store_f32(dest, x as f32);
        },
        IntToFloat_16(src, dest) => {
            let x = raw.load_i16(src);
            raw.store_f32(dest, x as f32);
        },
        IntToFloat_32(src, dest) => {
            let x = raw.load_i32(src);
            raw.store_f32(dest, x as f32);
        },

        UintToFloat_8(src, dest) => {
            let x = raw.load_u8(src);
            raw.store_f32(dest, x as f32);
        },
        UintToFloat_16(src, dest) => {
            let x = raw.load_u16(src);
            raw.store_f32(dest, x as f32);
        },
        UintToFloat_32(src, dest) => {
            let x = raw.load_u32(src);
            raw.store_f32(dest, x as f32);
        },

        FloatToInt_8(src, dest) => {
            let x = raw.load_f32(src);
            raw.store_i8(dest, x as i8);
        },
        FloatToInt_16(src, dest) => {
            let x = raw.load_f32(src);
            raw.store_i16(dest, x as i16);
        },
        FloatToInt_32(src, dest) => {
            let x = raw.load_f32(src);
            raw.store_i32(dest, x as i32);
        },

        FloatToUint_8(src, dest) => {
            let x = raw.load_f32(src);
            raw.store_u8(dest, x as u8);
        },
        FloatToUint_16(src, dest) => {
            let x = raw.load_f32(src);
            raw.store_u16(dest, x as u16);
        },
        FloatToUint_32(src, dest) => {
            let x = raw.load_f32(src);
            raw.store_u32(dest, x as u32);
        },

        UintToFloatNorm_8(src, dest) => {
            let x = raw.load_u8(src);
            raw.store_f32(dest, x as f32 / u8::MAX as f32);
        },
        UintToFloatNorm_16(src, dest) => {
            let x = raw.load_u16(src);
            raw.store_f32(dest, x as f32 / u16::MAX as f32);
        },
        UintToFloatNorm_32(src, dest) => {
            let x = raw.load_u32(src);
            raw.store_f32(dest, x as f32 / u32::MAX as f32);
        },
    }
}
