use std::iter::FromIterator;
use std::marker::PhantomData;
use std::mem;
use std::ops::{Deref, DerefMut, Index, IndexMut};
use std::slice;


macro_rules! define_id {
    ($Id:ident($T:ty)) => {
        #[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
        pub struct $Id(pub $T);

        impl From<usize> for $Id {
            fn from(x: usize) -> $Id {
                $Id(x as $T)
            }
        }

        impl From<$Id> for usize {
            fn from(x: $Id) -> usize {
                x.0 as usize
            }
        }
    };
}


#[derive(Clone, Debug)]
pub struct IndexVec<K, V> {
    vec: Vec<V>,
    _marker: PhantomData<K>,
}

#[allow(dead_code)]
impl<K: From<usize>+Into<usize>, V> IndexVec<K, V> {
    pub fn new() -> IndexVec<K, V> {
        IndexVec {
            vec: Vec::new(),
            _marker: PhantomData,
        }
    }

    pub fn with_capacity(cap: usize) -> IndexVec<K, V> {
        IndexVec {
            vec: Vec::with_capacity(cap),
            _marker: PhantomData,
        }
    }

    pub fn push(&mut self, v: V) -> K {
        let k = self.vec.len().into();
        self.vec.push(v);
        k
    }

    pub fn pop(&mut self) -> Option<(K, V)> {
        self.vec.pop()
            .map(|v| (self.vec.len().into(), v))
    }

    pub fn len(&self) -> usize {
        self.vec.len()
    }

    pub fn iter<'a>(&'a self) -> impl Iterator<Item=(K, &'a V)> {
        self.vec.iter().enumerate().map(|(i, v)| (i.into(), v))
    }

    pub fn iter_mut<'a>(&'a mut self) -> impl Iterator<Item=(K, &'a mut V)> {
        self.vec.iter_mut().enumerate().map(|(i, v)| (i.into(), v))
    }
}

impl<K: From<usize>+Into<usize>, V> Deref for IndexVec<K, V> {
    type Target = IndexSlice<K, V>;
    fn deref(&self) -> &IndexSlice<K, V> {
        IndexSlice::new(&self.vec)
    }
}

impl<K: From<usize>+Into<usize>, V> DerefMut for IndexVec<K, V> {
    fn deref_mut(&mut self) -> &mut IndexSlice<K, V> {
        IndexSlice::new_mut(&mut self.vec)
    }
}

impl<K: From<usize>+Into<usize>, V> FromIterator<V> for IndexVec<K, V> {
    fn from_iter<T>(iter: T) -> Self
            where T: IntoIterator<Item=V> {
        let iter = iter.into_iter();
        let (len, _) = iter.size_hint();
        let mut vec = IndexVec::with_capacity(len);
        for v in iter {
            vec.push(v);
        }
        vec
    }
}

#[derive(Debug)]
pub struct IndexSlice<K, V> {
    _marker: PhantomData<K>,
    vec: [V],
}

#[allow(dead_code)]
impl<K: From<usize>+Into<usize>, V> IndexSlice<K, V> {
    pub fn new<'a>(slice: &'a [V]) -> &'a IndexSlice<K, V> {
        unsafe { mem::transmute(slice) }
    }

    pub fn new_mut<'a>(slice: &'a mut [V]) -> &'a mut IndexSlice<K, V> {
        unsafe { mem::transmute(slice) }
    }

    pub fn len(&self) -> usize {
        self.vec.len()
    }

    pub fn iter<'a>(&'a self) -> impl Iterator<Item=(K, &'a V)> {
        self.vec.iter().enumerate().map(|(i, v)| (i.into(), v))
    }

    pub fn iter_mut<'a>(&'a mut self) -> impl Iterator<Item=(K, &'a mut V)> {
        self.vec.iter_mut().enumerate().map(|(i, v)| (i.into(), v))
    }

    pub fn as_slice(&self) -> &[V] {
        &self.vec
    }
}

impl<K: From<usize>+Into<usize>, V> Index<K> for IndexSlice<K, V> {
    type Output = V;
    fn index(&self, k: K) -> &V {
        &self.vec[k.into()]
    }
}

impl<K: From<usize>+Into<usize>, V> IndexMut<K> for IndexSlice<K, V> {
    fn index_mut(&mut self, k: K) -> &mut V {
        &mut self.vec[k.into()]
    }
}


type Word = u64;
const WORD_SIZE: usize = mem::size_of::<Word>();

#[derive(Debug)]
pub struct AlignedBytes {
    vec: Vec<Word>,
    byte_len: usize,
}

#[allow(dead_code)]
impl AlignedBytes {
    pub fn new() -> AlignedBytes {
        AlignedBytes {
            vec: Vec::new(),
            byte_len: 0,
        }
    }

    pub fn with_capacity(cap: usize) -> AlignedBytes {
        AlignedBytes {
            vec: Vec::with_capacity(cap),
            byte_len: 0,
        }
    }

    pub fn with_len(len: usize) -> AlignedBytes {
        let mut ab = AlignedBytes::with_capacity(len);
        ab.extend_zero(len);
        ab
    }

    pub fn len(&self) -> usize {
        self.byte_len
    }

    pub fn capacity(&self) -> usize {
        WORD_SIZE * self.vec.capacity()
    }

    pub fn extend_zero(&mut self, amt: usize) {
        self.byte_len += amt;
        let word_len = (self.byte_len + WORD_SIZE - 1) / WORD_SIZE;
        self.vec.resize(word_len, 0 as Word);
    }

    pub fn reduce(&mut self, amt: usize) {
        self.byte_len -= amt;
        let word_len = (self.byte_len + WORD_SIZE - 1) / WORD_SIZE;
        self.vec.truncate(word_len);
    }

    /// Pad with zeroes until the length is a multiple of `align`.
    pub fn align(&mut self, align: usize) {
        let adj = (align - self.byte_len % align) % align;
        self.extend_zero(adj);
    }

    pub fn push(&mut self, b: u8) {
        let idx = self.byte_len;
        self.extend_zero(1);
        self[idx] = b;
    }
}

impl Deref for AlignedBytes {
    type Target = [u8];
    fn deref(&self) -> &[u8] {
        let words = &self.vec as &[Word];
        unsafe { slice::from_raw_parts(words.as_ptr() as *const u8,
                                       words.len() * WORD_SIZE) }
    }
}

impl DerefMut for AlignedBytes {
    fn deref_mut(&mut self) -> &mut [u8] {
        let words = &mut self.vec as &mut [Word];
        unsafe { slice::from_raw_parts_mut(words.as_mut_ptr() as *mut u8,
                                           words.len() * WORD_SIZE) }
    }
}
