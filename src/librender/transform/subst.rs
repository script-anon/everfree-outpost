use std::collections::{HashMap, HashSet};
use intern::Interners;
use ir::*;

enum Default {
    Remove,
    Identity,
}

pub struct Substitution<'a: 'i, 'i> {
    intern: &'i Interners<'a>,
    renamed: HashMap<VarId, VarId>,
    removed: HashSet<VarId>,
    default: Default,
}

impl<'a, 'i> Substitution<'a, 'i> {
    pub fn new(intern: &'i Interners<'a>) -> Substitution<'a, 'i> {
        Substitution {
            intern,
            renamed: HashMap::new(),
            removed: HashSet::new(),
            default: Default::Remove,
        }
    }

    pub fn rename(&mut self, old: VarId, new: VarId) {
        self.renamed.insert(old, new);
        self.removed.remove(&old);
    }

    pub fn remove(&mut self, old: VarId) {
        self.renamed.remove(&old);
        self.removed.insert(old);
    }

    /// For vars that aren't explicitly renamed or removed, default to the identity mapping.
    #[allow(dead_code)]
    pub fn default_identity(&mut self) {
        self.default = Default::Identity;
    }

    pub fn subst_var_id(&self, old: VarId) -> VarId {
        if let Some(&new) = self.renamed.get(&old) {
            new
        } else if self.removed.contains(&old) {
            panic!("can't subst {:?} - var was explicitly removed");
        } else {
            match self.default {
                Default::Remove => panic!("can't subst {:?} - no entry for var", old),
                Default::Identity => old,
            }
        }
    }

    pub fn subst_stmt(&self, old: Stmt<'a>) -> Stmt<'a> {
        match old {
            Stmt::Local(s) =>
                Stmt::Local(self.subst_local_stmt(s)),
            Stmt::Clear(s) =>
                Stmt::Clear(self.subst_clear_stmt(s)),
            Stmt::Render(s) =>
                Stmt::Render(self.subst_render_stmt(s)),
            Stmt::Call(s) =>
                Stmt::Call(self.subst_call_stmt(s)),
        }
    }

    pub fn subst_local_stmt(&self, old: LocalStmt<'a>) -> LocalStmt<'a> {
        LocalStmt {
            var: old.var,
            insn: self.subst_insn(old.insn),
        }
    }

    pub fn subst_clear_stmt(&self, old: ClearStmt<'a>) -> ClearStmt<'a> {
        ClearStmt {
            color: old.color.map(|v| self.subst_var_id(v)),
            depth: old.depth.map(|v| self.subst_var_id(v)),
            outputs: self.subst_draw_outputs(old.outputs),
        }
    }

    pub fn subst_call_stmt(&self, old: CallStmt<'a>) -> CallStmt<'a> {
        let inputs = old.inputs.iter().cloned().map(|v| self.subst_var_id(v))
            .collect::<Vec<_>>();
        let outputs = old.outputs.iter().cloned().map(|v| self.subst_var_id(v))
            .collect::<Vec<_>>();
        CallStmt {
            pass: old.pass,
            inputs: self.intern.slice(&inputs),
            outputs: self.intern.slice(&outputs),
        }
    }

    pub fn subst_render_stmt(&self, old: RenderStmt<'a>) -> RenderStmt<'a> {
        let uniforms = old.uniforms.iter().map(|&u| self.subst_uniform(u))
            .collect::<Vec<_>>();
        let vert_attribs = old.vert_attribs.iter().map(|&a| self.subst_attrib(a))
            .collect::<Vec<_>>();
        let inst_attribs = old.inst_attribs.iter().map(|&a| self.subst_attrib(a))
            .collect::<Vec<_>>();
        RenderStmt {
            shader: old.shader,
            uniforms: self.intern.slice(&uniforms),
            vert_attribs: self.intern.slice(&vert_attribs),
            inst_attribs: self.intern.slice(&inst_attribs),
            outputs: self.subst_draw_outputs(old.outputs),
            config: old.config,
        }
    }

    pub fn subst_uniform(&self, old: UniformBinding<'a>) -> UniformBinding<'a> {
        UniformBinding {
            name: old.name,
            ty: old.ty,
            var: self.subst_var_id(old.var),
        }
    }

    pub fn subst_attrib(&self, old: AttribBinding<'a>) -> AttribBinding<'a> {
        AttribBinding {
            name: old.name,
            ty: old.ty,
            var: self.subst_var_id(old.var),
            field: old.field,
        }
    }

    pub fn subst_draw_outputs(&self, old: DrawOutputs<'a>) -> DrawOutputs<'a> {
        let color = old.color.iter().cloned().map(|v| self.subst_var_id(v))
            .collect::<Vec<_>>();
        DrawOutputs {
            color: self.intern.slice(&color),
            depth: old.depth.map(|v| self.subst_var_id(v)),
            viewport: old.viewport.map(|v| self.subst_var_id(v)),
        }
    }

    pub fn subst_insn(&self, old: Insn<'a>) -> Insn<'a> {
        match old {
            Insn::ConstI(sz, x) =>
                Insn::ConstI(sz, x),
            Insn::ConstU(sz, x) =>
                Insn::ConstU(sz, x),
            Insn::ConstF(x) =>
                Insn::ConstF(x),
            Insn::Copy(v) =>
                Insn::Copy(self.subst_var_id(v)),
            Insn::VecIndex(v, idx) =>
                Insn::VecIndex(self.subst_var_id(v), idx),
            Insn::VecSwizzle(v, idxs) =>
                Insn::VecSwizzle(self.subst_var_id(v), idxs),
            Insn::VecLen(v) =>
                Insn::VecLen(self.subst_var_id(v)),
            Insn::TextureSize(v) =>
                Insn::TextureSize(self.subst_var_id(v)),
            Insn::GeomField(v, f) =>
                Insn::GeomField(self.subst_var_id(v), f),
            Insn::VecCtor(p, vs) => {
                let vs = vs.iter().cloned().map(|v| self.subst_var_id(v)).collect::<Vec<_>>();
                Insn::VecCtor(p, self.intern.slice(&vs))
            },
            Insn::TextureCtor(t, [v0, v1]) =>
                Insn::TextureCtor(t, [self.subst_var_id(v0), self.subst_var_id(v1)]),
            Insn::Coerce(v, p) =>
                Insn::Coerce(self.subst_var_id(v), p),
        }
    }
}
