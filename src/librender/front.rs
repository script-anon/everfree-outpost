use std::collections::HashMap;
use std::ops::Deref;
use std::str::{self, FromStr};
use std::u8;

use ast;
use intern::Interners;
use ir::{self, Symbol, Ty, PassId, VarId};
use util::IndexVec;


pub fn lower_pipeline<'a>(p: &ast::Pipeline, intern: &Interners<'a>) -> ir::Pipeline<'a> {
    IrLowering::new(intern).lower_pipeline(p)
}

struct IrLowering<'a: 'i, 'i> {
    intern: &'i Interners<'a>,

    type_aliases: HashMap<Symbol<'a>, Ty<'a>>,
    pass_sigs: IndexVec<PassId, PassSig<'a>>,
    passes_by_name: HashMap<String, PassId>,
}

impl<'a, 'i> IrLowering<'a, 'i> {
    pub fn new(intern: &'i Interners<'a>) -> IrLowering<'a, 'i> {
        IrLowering {
            intern: intern,

            type_aliases: HashMap::new(),
            pass_sigs: IndexVec::new(),
            passes_by_name: HashMap::new(),
        }
    }

    pub fn lower_pipeline(&mut self, p: &ast::Pipeline) -> ir::Pipeline<'a> {
        self.lower_decls(&p.items);
        let passes = self.lower_passes(&p.items);
        ir::Pipeline {
            passes: self.intern.index_slice(&passes),
        }
    }

    fn try_lower_ty(&self, ty: &ast::Ty) -> Result<ir::Ty<'a>, ir::Symbol<'a>> {
        match ty {
            &ast::Ty::Prim(p) => Ok(self.intern.ty(&ir::TyKind::Prim(p))),
            &ast::Ty::Vec(p, l) => Ok(self.intern.ty(&ir::TyKind::Vec(p, l))),
            &ast::Ty::Texture(fmt) => Ok(self.intern.ty(&ir::TyKind::Texture(fmt))),
            &ast::Ty::Geometry(ref fs) => {
                let mut ir_fs = Vec::with_capacity(fs.len());
                for f in fs {
                    ir_fs.push(ir::Field {
                        name: self.intern.symbol(&f.name),
                        ty: self.try_lower_ty(&f.ty)?,
                    });
                }
                let ir_fs = self.intern.slice(&ir_fs);
                Ok(self.intern.ty(&ir::TyKind::Geometry(ir_fs)))
            },
            &ast::Ty::Alias(ref name) => {
                let name = self.intern.symbol(name);
                self.type_aliases.get(&name).cloned().ok_or(name)
            },
        }
    }

    fn lower_ty(&self, ty: &ast::Ty) -> ir::Ty<'a> {
        match self.try_lower_ty(ty) {
            Ok(x) => x,
            Err(name) => panic!("unknown type alias {:?}", name),
        }
    }

    fn lower_decls(&mut self, items: &[ast::Item]) {
        let mut passes = Vec::new();
        let mut aliases = Vec::new();
        for i in items {
            match i {
                &ast::Item::Pass(ref p) => passes.push(p),
                &ast::Item::TypeAlias(ref name, ref ty) =>
                    aliases.push((self.intern.symbol(name), ty)),
            }
        }

        // Process all type aliases.
        while aliases.len() > 0 {
            let old_len = aliases.len();
            // Intern and record all the type aliases we can, and remove those type aliases from
            // `aliases`.  The ones where interning fails must depend on type aliases that haven't
            // been recorded yet.
            aliases.retain(|&(name, ty)| {
                if let Ok(ty) = self.try_lower_ty(ty) {
                    assert!(!self.type_aliases.contains_key(&name),
                            "duplicate type alias {:?}", name);
                    let alias_ty = self.intern.ty(&ir::TyKind::Alias(name, ty));
                    self.type_aliases.insert(name, alias_ty);
                    false
                } else {
                    true
                }
            });

            // If we stop making progress, there must be a cycle among aliases.  Bail out.
            if aliases.len() == old_len {
                let bad_alias_names = aliases.into_iter().map(|(n, _)| n).collect::<Vec<_>>();
                panic!("cyclic references among type aliases: {:?}", bad_alias_names);
            }
        }

        // Collect pass signatures.
        for p in passes {
            let mut vars = IndexVec::with_capacity(p.inputs.len() + p.outputs.len());
            let name = self.intern.symbol(&p.name);

            let inputs = p.inputs.iter().enumerate().map(|(idx, param)| {
                vars.push(ir::Var {
                    name: self.intern.symbol(&param.name),
                    ty: self.lower_ty(&param.ty),
                    origin: ir::VarOrigin::Input(idx),
                })
            }).collect::<Vec<_>>();

            let outputs = p.outputs.iter().enumerate().map(|(idx, param)| {
                vars.push(ir::Var {
                    name: self.intern.symbol(&param.name),
                    ty: self.lower_ty(&param.ty),
                    origin: ir::VarOrigin::Output(idx),
                })
            }).collect::<Vec<_>>();

            let pass_id = self.pass_sigs.push(PassSig { name, inputs, outputs, vars });
            self.passes_by_name.insert(p.name.clone(), pass_id);
        }
    }

    fn lower_passes(&self, items: &[ast::Item]) -> IndexVec<PassId, ir::Pass<'a>> {
        let mut ir_passes = IndexVec::with_capacity(self.pass_sigs.len());
        for i in items {
            let p = match i {
                &ast::Item::Pass(ref p) => p,
                _ => continue,
            };

            let id: PassId = ir_passes.len().into();
            let sig = &self.pass_sigs[id];

            let mut sl = StmtLowering::new(self, sig.name, sig.vars.clone());
            for s in &p.stmts {
                sl.lower_stmt(s);
            }
            let (vars, stmts) = sl.into_inner();

            ir_passes.push(ir::Pass {
                name: sig.name,
                public: p.public,
                inputs: self.intern.slice(&sig.inputs),
                outputs: self.intern.slice(&sig.outputs),
                stmts: self.intern.slice(&stmts),
                vars: self.intern.index_slice(&vars),
            });
        }
        ir_passes
    }
}

struct PassSig<'a> {
    name: Symbol<'a>,
    inputs: Vec<VarId>,
    outputs: Vec<VarId>,
    vars: IndexVec<VarId, ir::Var<'a>>,
}


struct StmtLowering<'a: 'b+'i, 'i: 'b, 'b> {
    ir: &'b IrLowering<'a, 'i>,

    pass_name: Symbol<'a>,

    scope: HashMap<Symbol<'a>, VarId>,
    vars: IndexVec<VarId, ir::Var<'a>>,
    stmts: Vec<ir::Stmt<'a>>,
}

impl<'a, 'i, 'b> StmtLowering<'a, 'i, 'b> {
    pub fn new(ir: &'b IrLowering<'a, 'i>,
               pass_name: Symbol<'a>,
               vars: IndexVec<VarId, ir::Var<'a>>) -> StmtLowering<'a, 'i, 'b> {
        let mut scope = HashMap::with_capacity(vars.len());
        for (v_id, var) in vars.iter() {
            assert!(!scope.contains_key(&var.name),
                    "pass {}: duplicate parameter name {:?}", pass_name, var.name);
            scope.insert(var.name, v_id);
        }

        StmtLowering {
            ir,
            pass_name,
            scope,
            vars,
            stmts: Vec::new(),
        }
    }

    pub fn into_inner(self) -> (IndexVec<VarId, ir::Var<'a>>, Vec<ir::Stmt<'a>>) {
        (self.vars, self.stmts)
    }

    pub fn lower_stmt(&mut self, stmt: &ast::Stmt) {
        match *stmt {
            ast::Stmt::Local(ref l) => {
                let name = self.intern.symbol(&l.name);
                let ty = self.lower_ty(&l.ty);
                let insn = self.lower_expr_init(&l.init, ty);
                self.add_local(name, ty, insn);
            },

            ast::Stmt::Clear(ref c) => {
                let color_ty = self.intern.ty(&COLOR_TY_KIND);
                let depth_ty = self.intern.ty(&DEPTH_TY_KIND);
                let c = ir::ClearStmt {
                    color: c.color.as_ref()
                        .map(|e| self.lower_expr_as_var(e, color_ty)),

                    depth: c.depth.as_ref()
                        .map(|e| self.lower_expr_as_var(e, depth_ty)),
                    outputs: self.lower_outputs(&c.outputs),
                };
                self.stmts.push(ir::Stmt::Clear(c));
            },

            ast::Stmt::Render(ref r) => {
                let shader = self.lower_shader(&r.shader);
                let outputs = self.lower_outputs(&r.outputs);
                let (uniforms, vert_attribs, inst_attribs) = self.lower_bindings(&r.bindings);
                let config = r.config.clone();

                self.stmts.push(ir::Stmt::Render(ir::RenderStmt {
                    shader, outputs, uniforms, vert_attribs, inst_attribs, config
                }));
            },

            ast::Stmt::Call(ref c) => {
                let pass_id = self.passes_by_name.get(&c.pass).cloned()
                    .unwrap_or_else(|| panic!("unknown pass {:?}", c.pass));
                let ir = self.ir;
                let sig = &ir.pass_sigs[pass_id];

                let inputs = self.lower_call_args(&c.inputs, &sig.inputs, &sig.vars);
                let outputs = self.lower_call_args(&c.outputs, &sig.outputs, &sig.vars);

                self.stmts.push(ir::Stmt::Call(ir::CallStmt {
                    pass: pass_id, inputs, outputs
                }));
            },
        }
    }


    fn add_local(&mut self, name: Symbol<'a>, ty: Ty<'a>, insn: ir::Insn<'a>) {
        let idx = self.stmts.len();
        let var = self.vars.push(ir::Var {
            name, ty,
            origin: ir::VarOrigin::Stmt(idx),
        });
        self.stmts.push(ir::Stmt::Local(ir::LocalStmt { var, insn }));
        self.scope.insert(name, var);
    }

    fn add_temp(&mut self, ty: Ty<'a>, insn: ir::Insn<'a>) -> VarId {
        let name = self.intern.symbol("_tmp");
        let idx = self.stmts.len();
        let var = self.vars.push(ir::Var {
            name, ty,
            origin: ir::VarOrigin::Stmt(idx),
        });
        self.stmts.push(ir::Stmt::Local(ir::LocalStmt { var, insn }));
        var
    }


    fn lower_outputs(&mut self, o: &ast::DrawOutputs) -> ir::DrawOutputs<'a> {
        let color_ty = self.intern.ty(&COLOR_TEXTURE_TY_KIND);
        let color = o.color.iter()
                .map(|e| self.lower_expr_as_var(e, color_ty))
                .collect::<Vec<_>>();

        let depth_ty = self.intern.ty(&DEPTH_TEXTURE_TY_KIND);
        let depth = o.depth.as_ref()
                .map(|e| self.lower_expr_as_var(e, depth_ty));

        let viewport_ty = self.intern.ty(&VIEWPORT_TY_KIND);
        let viewport = o.viewport.as_ref()
                .map(|e| self.lower_expr_as_var(e, viewport_ty));

        ir::DrawOutputs {
            color: self.intern.slice(&color),
            depth: depth,
            viewport: viewport,
        }
    }

    fn lower_call_args(&mut self,
                       ast: &[ast::Arg],
                       ir: &[VarId],
                       vars: &IndexVec<VarId, ir::Var<'a>>) -> &'a [VarId] {
        let mut index_map = ast.iter().enumerate()
            .map(|(i, arg)| (self.intern.symbol(&arg.name), i))
            .collect::<HashMap<_, _>>();

        let mut result = Vec::with_capacity(ir.len());
        for &ir_id in ir {
            let ir_var = &vars[ir_id];
            let ast_idx = index_map.remove(&ir_var.name)
                .unwrap_or_else(|| panic!("missing argument {:?}", ir_var.name));
            let ast_arg = &ast[ast_idx];

            let insn = self.lower_expr_as(&ast_arg.expr, ir_var.ty);
            let var = self.add_temp(ir_var.ty, insn);
            result.push(var);
        }

        self.intern.slice(&result)
    }

    fn lower_shader(&mut self, s: &ast::Shader) -> ir::Shader<'a> {
        ir::Shader {
            vertex: self.intern.symbol(&s.vertex),
            fragment: self.intern.symbol(&s.fragment),
            defs: self.intern.symbol(""),
        }
    }

    fn lower_bindings(&mut self,
                      bs: &[ast::Binding]) -> (&'a [ir::UniformBinding<'a>],
                                               &'a [ir::AttribBinding<'a>],
                                               &'a [ir::AttribBinding<'a>]) {
        let mut us = Vec::new();
        let mut vs = Vec::new();
        let mut is = Vec::new();
        for b in bs {
            let name = self.intern.symbol(&b.name);
            let ty = self.lower_ty(&b.ty);
            if b.scope == ast::Scope::Uniform {
                let insn = self.lower_expr_as(&b.expr, ty);
                let var = self.add_temp(ty, insn);
                us.push(ir::UniformBinding { name, ty, var });
            } else {
                let (e_geom, field_name) = match b.expr {
                    ast::Expr::Field(ref e, ref f) => (e, f),
                    _ => panic!("{:?} `{}` binding initializer must be a geometry field",
                                b.scope, b.name),
                };
                let (insn, geom_ty) = self.lower_expr(e_geom);
                let var = self.add_temp(geom_ty, insn);
                let fields = match *geom_ty.resolve() {
                    ir::TyKind::Geometry(fs) => fs,
                    _ => panic!("{:?} `{}` binding initializer must be a geometry field",
                                b.scope, b.name),
                };

                let field = fields.iter().position(|f| f.name.0 == field_name)
                    .unwrap_or_else(|| panic!("no such field `{}` for {:?}", field_name, e_geom));

                let bnd = ir::AttribBinding { name, ty, var, field };
                match b.scope {
                    ast::Scope::Uniform => unreachable!(),
                    ast::Scope::Attrib => vs.push(bnd),
                    ast::Scope::Instance => is.push(bnd),
                }
            }
        }

        (self.intern.slice(&us),
         self.intern.slice(&vs),
         self.intern.slice(&is))
    }


    fn lower_expr(&mut self, e: &ast::Expr) -> (ir::Insn<'a>, Ty<'a>) {
        match *e {
            ast::Expr::ConstI(sz, x) =>
                (ir::Insn::ConstI(sz, x),
                 self.intern.ty(&ir::TyKind::Prim(ir::PrimTy::Int(sz)))),
            ast::Expr::ConstU(sz, x) =>
                (ir::Insn::ConstU(sz, x),
                 self.intern.ty(&ir::TyKind::Prim(ir::PrimTy::Uint(sz)))),
            ast::Expr::ConstF(x) =>
                (ir::Insn::ConstF(x),
                 self.intern.ty(&ir::TyKind::Prim(ir::PrimTy::Float))),

            ast::Expr::Var(ref name) => {
                let name = self.intern.symbol(name);
                let var = self.scope.get(&name).cloned()
                    .unwrap_or_else(|| panic!("unknown variable `{}`", name));
                (ir::Insn::Copy(var), self.vars[var].ty)
            },

            ast::Expr::Field(ref e, ref field) => {
                let (insn, ty) = self.lower_expr(e);
                let no_field = || panic!("unknown field {:?} for {:?}", field, ty);
                let var = self.add_temp(ty, insn);
                match *ty.resolve() {
                    ir::TyKind::Vec(p, l) => {
                        if field == "len" {
                            (ir::Insn::VecLen(var), self.intern.ty(&U32_TY_KIND))
                        } else if let Some(swiz) = parse_vec_swizzle(l, field) {
                            assert!(
                                swiz.len() <= u8::MAX as usize,
                                "pass {}: swizzle result length for {:?} exceeds maximum (255)",
                                self.pass_name, field);
                            if swiz.len() == 1 {
                                (ir::Insn::VecIndex(var, swiz[0]),
                                 self.intern.ty(&ir::TyKind::Prim(p)))
                            } else {
                                (ir::Insn::VecSwizzle(var, self.intern.slice(&swiz)),
                                 self.intern.ty(&ir::TyKind::Vec(p, swiz.len() as u8)))
                            }
                        } else {
                            no_field()
                        }
                    },

                    ir::TyKind::Texture(_) => {
                        if field == "size" {
                            (ir::Insn::TextureSize(var), self.intern.ty(&U32_X2_TY_KIND))
                        } else {
                            no_field()
                        }
                    },

                    ir::TyKind::Geometry(fs) => {
                        if let Some(f) = fs.iter().find(|f| f.name.0 == field) {
                            (ir::Insn::GeomField(var, f.name), f.ty)
                        } else {
                            no_field()
                        }
                    },

                    _ => no_field(),
                }
            },

            ast::Expr::Ctor(ref ty, ref init) => {
                let ty = self.lower_ty(ty);
                (self.lower_expr_init(init, ty), ty)
            },
        }
    }

    fn lower_expr_as(&mut self, e: &ast::Expr, ty: Ty<'a>) -> ir::Insn<'a> {
        let (insn, actual_ty) = self.lower_expr(e);
        if actual_ty.equiv(ty) {
            insn
        } else {
            self.coerce(insn, actual_ty, ty)
        }
   }

    fn lower_expr_as_var(&mut self, e: &ast::Expr, ty: Ty<'a>) -> VarId {
        let insn = self.lower_expr_as(e, ty);
        self.add_temp(ty, insn)
   }

    fn lower_expr_init(&mut self, init: &[ast::Expr], ty: Ty<'a>) -> ir::Insn<'a> {
        match *ty.resolve() {
            ir::TyKind::Vec(p, l) => {
                let mut vars = Vec::with_capacity(l as usize);
                let expand_len = if init.len() == 1 { Some(l as usize) } else { None };
                for e in init {
                    let (insn, ty) = self.lower_expr(e);
                    self.coerce_init_prims(insn, ty, p, &mut vars, expand_len);
                }
                assert!(vars.len() == l as usize,
                        "pass {}: not enough elements in vector initializer",
                        self.pass_name);
                ir::Insn::VecCtor(p, self.intern.slice(&vars))
            },

            ir::TyKind::Texture(fmt) => {
                let mut vars = Vec::with_capacity(2);
                for e in init {
                    let (insn, ty) = self.lower_expr(e);
                    self.coerce_init_prims(insn, ty, U32_PRIM_TY, &mut vars, None);
                }
                assert!(vars.len() == 2 as usize,
                        "pass {}: not enough elements in texture initializer",
                        self.pass_name);
                ir::Insn::TextureCtor(fmt, [vars[0], vars[1]])
            },

            _ => panic!("type {:?} is not constructible", ty),
        }
    }


    /// Coerce the result of `insn` (which has type `ty`) to a sequence of primitive values of type
    /// `prim`.  Store the results in temporary variables, and push those variables onto `vars`.
    /// If `expand_prim` is set and the input is a primitive value, it will be expanded into
    /// `expand_prim` copies of itself.
    fn coerce_init_prims(&mut self,
                         insn: ir::Insn<'a>,
                         ty: Ty<'a>,
                         prim: ir::PrimTy,
                         vars: &mut Vec<VarId>,
                         expand_prim: Option<usize>) {
        let prim_ty = self.intern.ty(&ir::TyKind::Prim(prim));
        match *ty.resolve() {
            ir::TyKind::Prim(p) => {
                let insn = self.coerce_prim(insn, p, prim);
                let var = self.add_temp(prim_ty, insn);
                if let Some(len) = expand_prim {
                    for _ in 0 .. len {
                        vars.push(var);
                    }
                } else {
                    vars.push(var);
                }
            },

            ir::TyKind::Vec(p, l) => {
                let vec_var = self.add_temp(ty, insn);
                for i in 0 .. l {
                    let insn = ir::Insn::VecIndex(vec_var, i);
                    let insn = self.coerce_prim(insn, p, prim);
                    let var = self.add_temp(prim_ty, insn);
                    vars.push(var);
                }
            },

            _ => panic!("expected prim or vec, but got {:?}", ty),
        }
    }

    fn coerce(&mut self, insn: ir::Insn<'a>, old_ty: Ty<'a>, new_ty: Ty<'a>) -> ir::Insn<'a> {
        if old_ty.equiv(new_ty) {
            return insn;
        }

        match (old_ty.resolve(), new_ty.resolve()) {
            (&ir::TyKind::Prim(p1),
             &ir::TyKind::Prim(p2)) => {
                self.coerce_prim(insn, p1, p2)
            },

            (&ir::TyKind::Prim(_),
             &ir::TyKind::Vec(p2, l)) => {
                let mut vars = Vec::with_capacity(l as usize);
                self.coerce_init_prims(insn, old_ty, p2, &mut vars, Some(l as usize));
                ir::Insn::VecCtor(p2, self.intern.slice(&vars))
            },

            (&ir::TyKind::Vec(p1, l1),
             &ir::TyKind::Vec(p2, l2)) => {
                assert!(l1 == l2,
                        "pass {}: can't coerce {:?} to different-length vector {:?}",
                        self.pass_name, old_ty, new_ty);
                let var = self.add_temp(old_ty, insn);
                let p2_ty = self.intern.ty(&ir::TyKind::Prim(p2));
                let mut vars = Vec::with_capacity(l2 as usize);
                for i in 0 .. l2 {
                    let insn = self.coerce_prim(ir::Insn::VecIndex(var, i), p1, p2);
                    vars.push(self.add_temp(p2_ty, insn));
                }
                ir::Insn::VecCtor(p2, self.intern.slice(&vars))
            },

            _ => panic!("{:?} cannot be coerced to {:?}", old_ty, new_ty),
        }
    }

    fn coerce_prim(&mut self,
                   insn: ir::Insn<'a>,
                   old_prim: ir::PrimTy,
                   new_prim: ir::PrimTy) -> ir::Insn<'a> {
        if old_prim == new_prim {
            insn
        } else {
            let old_ty = self.intern.ty(&ir::TyKind::Prim(old_prim));
            let var = self.add_temp(old_ty, insn);
            ir::Insn::Coerce(var, new_prim)
        }
    }
}

impl<'a, 'i, 'b> Deref for StmtLowering<'a, 'i, 'b> {
    type Target = IrLowering<'a, 'i>;
    fn deref(&self) -> &IrLowering<'a, 'i> {
        &self.ir
    }
}


const U32_PRIM_TY: ir::PrimTy = ir::PrimTy::Uint(ir::IntSize::I32);
const U32_TY_KIND: ir::TyKind = ir::TyKind::Prim(U32_PRIM_TY);
const U32_X2_TY_KIND: ir::TyKind = ir::TyKind::Vec(U32_PRIM_TY, 2);
const COLOR_TY_KIND: ir::TyKind = ir::TyKind::Vec(ir::PrimTy::Float, 4);
const DEPTH_TY_KIND: ir::TyKind = ir::TyKind::Prim(ir::PrimTy::Float);
const COLOR_TEXTURE_TY_KIND: ir::TyKind = ir::TyKind::Texture(ir::TexFormat::RGBA);
const DEPTH_TEXTURE_TY_KIND: ir::TyKind = ir::TyKind::Texture(ir::TexFormat::Depth);
const VIEWPORT_TY_KIND: ir::TyKind = ir::TyKind::Vec(U32_PRIM_TY, 4);

fn parse_vec_swizzle(len: u8, field: &str) -> Option<Box<[u8]>> {
    let mut idxs = Vec::with_capacity(field.len());

    if field.len() == 0 {
        return None;

    // GLSL-style swizzles (v.xxyy, v.abgr, etc).  Note GLSL doesn't allow mixing characters from
    // different alias groups (v.xxrr, for example, is invalid).
    } else if field.chars().all(|c| c.is_ascii_alphabetic()) {
        let pattern = match field.chars().next().unwrap() {
            'x' | 'y' | 'z' | 'w' => "xyzw",
            'r' | 'g' | 'b' | 'a' => "rgba",
            's' | 't' | 'p' | 'q' => "stpq",
            _ => return None,
        };

        for c in field.chars() {
            let idx = pattern.find(c)? as u8;
            if idx >= len {
                return None;
            }
            idxs.push(idx);
        }

    // Shorthand numeric swizzle, v.0123
    } else if len <= 10 && field.chars().all(|c| c.is_ascii_digit()) {
        for c in field.chars() {
            let idx = c as u8 - b'0';
            if idx >= len {
                return None;
            }
            idxs.push(idx);
        }

    // General numeric swizzle, v.0_1_10_17
    } else {
        for s in field.split("_") {
            let idx = u8::from_str(s).ok()?;
            if idx >= len {
                return None;
            }
            idxs.push(idx);
        }

    }

    Some(idxs.into_boxed_slice())
}
