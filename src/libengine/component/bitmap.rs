//! Component for associating a bitmap with a structure.  Currently used for player-editable sign
//! displays.
use server_types::*;
use std::collections::HashMap;
use std::iter;
use common::util::Bytes;
use syntax_exts::for_each_obj_named;
use world::objects::*;
use world::object_ref::{ObjectRef, ObjectMut};

use engine::Engine;
use ext::{Ext, WorldExt};
use update::UpdateBuilder;


#[derive(Clone, PartialEq, Eq, Debug)]
pub struct Bitmap {
    pub size: V2,
    pub data: Bytes,
}

pub struct Bitmaps {
    map: HashMap<StructureId, Bitmap>,
}

impl Bitmaps {
    pub fn new() -> Bitmaps {
        Bitmaps {
            map: HashMap::new(),
        }
    }

    fn insert(&mut self, id: StructureId, bitmap: Bitmap, ub: &UpdateBuilder) {
        if let Some(old_bitmap) = self.map.remove(&id) {
            error!("{:?} already has a bitmap", id);
            ub.structure_bitmap_move(id, Some(old_bitmap));
        } else {
            ub.structure_bitmap(id, None);
        }

        self.map.insert(id, bitmap);
    }

    fn init(&mut self, id: StructureId, size: V2, ub: &UpdateBuilder) {
        if size.x < 0 || size.y < 0 {
            error!("invalid bitmap size for {:?}: {:?}", id, size);
            return;
        }

        let data = Bytes(iter::repeat(0).take(data_len(size)).collect());

        self.insert(id, Bitmap { size, data }, ub);
    }

    fn set_data(&mut self, id: StructureId, data: Bytes, ub: &UpdateBuilder) {
        let bitmap = unwrap_or_error!(self.map.get_mut(&id),
                                      "no bitmap for {:?}", id);
        if data.len() != bitmap.data.len() {
            error!("new bitmap data size does not match old ({} != {})",
                   data.len(), bitmap.data.len());
            return;
        }

        ub.structure_bitmap(id, Some(bitmap));
        bitmap.data = data;
    }

    fn remove(&mut self, id: StructureId, ub: &UpdateBuilder) {
        if let Some(old_bitmap) = self.map.remove(&id) {
            ub.structure_bitmap_move(id, Some(old_bitmap));
        }
        // Otherwise, the value didn't actually change.
    }

    pub fn contains(&self, id: StructureId) -> bool {
        self.map.contains_key(&id)
    }

    pub fn get(&self, id: StructureId) -> Option<&Bitmap> {
        self.map.get(&id)
    }

    pub fn handle_destroy_structure(&mut self, id: StructureId, ub: &UpdateBuilder) {
        ub.structure_bitmap_destroyed(id, self.map.remove(&id));
    }
}

pub fn data_len(size: V2) -> usize {
    if size.x < 0 || size.y < 0 {
        return 0;
    }
    let bits = size.x as usize * size.y as usize;
    (bits + 7) / 8
}

pub fn add<E: Ext>(eng: &mut Engine<E>,
                   id: StructureId,
                   size: V2) {
    eng.w.bitmaps.init(id, size, &eng.update);
}

pub fn remove<E: Ext>(eng: &mut Engine<E>,
                      id: StructureId) {
    eng.w.bitmaps.remove(id, &eng.update);
}

pub fn update<E: Ext>(eng: &mut Engine<E>,
                      id: StructureId,
                      data: Bytes) {
    eng.w.bitmaps.set_data(id, data, &eng.update);
}

/// Set the component value without recording an update.  Call this only when applying a `Delta`.
pub fn apply_silent<E: Ext>(eng: &mut Engine<E>,
                            id: StructureId,
                            bitmap: Option<Bitmap>) {
    if let Some(bitmap) = bitmap {
        eng.w.bitmaps.map.insert(id, bitmap);
    } else {
        eng.w.bitmaps.map.remove(&id);
    }
}



pub trait BitmapExt {
    fn has_bitmap(&self) -> bool;
    fn bitmap(&self) -> &Bitmap;
    fn get_bitmap(&self) -> Option<&Bitmap>;
}

pub trait BitmapExtMut {
    fn create_bitmap(&mut self, size: V2);
    fn destroy_bitmap(&mut self);
    fn set_bitmap_data(&mut self, data: Bytes);
}

for_each_obj_named! {
    { object_ref; object_mut; }

    impl<'a, 'd> BitmapExt for $Obj<'a, WorldExt, Structure> {
        fn has_bitmap(&self) -> bool {
            self.world().bitmaps.contains(self.id())
        }

        fn bitmap(&self) -> &Bitmap {
            let id = self.id();
            self.get_bitmap()
                .unwrap_or_else(|| panic!("{:?} has no bitmap", id))
        }

        fn get_bitmap(&self) -> Option<&Bitmap> {
            let id = self.id();
            self.world().bitmaps.get(id)
        }
    }

}

impl<'a, 'd> BitmapExtMut for ObjectMut<'a, WorldExt, Structure> {
    fn create_bitmap(&mut self, size: V2) {
        let id = self.id();
        let w = self.world_mut();
        w.components.bitmaps.init(id, size, &w.update);
    }

    fn destroy_bitmap(&mut self) {
        let id = self.id();
        let w = self.world_mut();
        w.components.bitmaps.remove(id, &w.update);
    }

    fn set_bitmap_data(&mut self, data: Bytes) {
        let id = self.id();
        let w = self.world_mut();
        w.components.bitmaps.set_data(id, data, &w.update);
    }
}
