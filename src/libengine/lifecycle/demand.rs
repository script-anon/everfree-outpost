use server_types::*;
use std::collections::HashSet;
use std::collections::hash_map::{HashMap, Entry};
use std::mem;

use engine::Engine;
use ext::Ext;
use lifecycle::import_fsm::{self, ImportFsmId};
use lifecycle::login_fsm;


#[derive(Clone, Copy, PartialEq, Eq, Debug, Hash)]
pub enum Source {
    /// Login state machine, keyed on user ID.
    LoginFsm(u32),
    TeleportFsm(u32),
    ImportFsm(ImportFsmId),

    /// Active connection (login FSM complete), keyed on user ID.
    Conn(u32),
    ClientCamera(ClientId),
    EntityPos(EntityId),
    TerrainChunk(TerrainChunkId),
}

#[derive(Clone, Copy, PartialEq, Eq, Debug, Hash)]
pub enum Target {
    Client(Stable<ClientId>),
    Plane(Stable<PlaneId>),
    TerrainChunk(PlaneId, V2),
}


pub struct Demand {
    /// For each outstanding load request, track the set of sources that depend on the object.
    pending: HashMap<Target, HashSet<Source>>,
    /// For each loaded object, track the number of sources.
    loaded: HashMap<Target, usize>,

    /// Set of loaded targets whose refcount is currently zero.
    zeroed: HashSet<Target>,
    /// Set of pending targets that were added (and not returned to zero) since the last check.
    added: HashSet<Target>,
}


impl Demand {
    pub fn new() -> Demand {
        Demand {
            pending: HashMap::new(),
            loaded: HashMap::new(),
            zeroed: HashSet::new(),
            added: HashSet::new(),
        }
    }

    pub fn loaded(&self, target: Target) -> bool{
        self.loaded.contains_key(&target)
    }

    /// Add a reference from `source` to `target`.  Returns `true` if the target is already loaded.
    pub fn inc_ref(&mut self, source: Source, target: Target) -> bool {
        if let Some(ref_count) = self.loaded.get_mut(&target) {
            *ref_count += 1;
            trace!("inc_ref: {:?} -> {:?}, loaded, now {}", source, target, *ref_count);
            if *ref_count == 1 {
                self.zeroed.remove(&target);
                trace!("inc_ref: {:?} no longer zeroed", target);
            }
            true
        } else {
            match self.pending.entry(target) {
                Entry::Vacant(e) => {
                    let watchers = e.insert(HashSet::new());
                    watchers.insert(source);
                    self.added.insert(target);
                    trace!("inc_ref: {:?} -> {:?}, NEW", source, target);
                },
                Entry::Occupied(e) => {
                    let watchers = e.into_mut();
                    watchers.insert(source);
                    trace!("inc_ref: {:?} -> {:?}, pending, now {}",
                           source, target, watchers.len());
                },
            }
            false
        }
    }

    pub fn dec_ref(&mut self, source: Source, target: Target) {
        if let Some(ref_count) = self.loaded.get_mut(&target) {
            *ref_count -= 1;
            trace!("dec_ref: {:?} -> {:?}, loaded, now {}", source, target, *ref_count);
            if *ref_count == 0 {
                self.zeroed.insert(target);
                trace!("dec_ref: {:?} now zeroed", target);
            }
        } else {
            match self.pending.entry(target) {
                Entry::Vacant(_) => {
                    error!("dec_ref: {:?} -> {:?}: target is neither loaded nor pending?",
                           source, target);
                },
                Entry::Occupied(mut e) => {
                    e.get_mut().remove(&source);
                    // If refcount is zero and the target is still in `added`, that means no
                    // external observer has seen the initial request yet, and we can silently
                    // cancel it.
                    //
                    // We can't cancel requests that have been published to the outside world, so
                    // in those cases, we keep the `pending` entry even for targets with no refs.
                    if e.get().len() == 0 && self.added.remove(&target) {
                        trace!("dec_ref: {:?} -> {:?}, CANCEL", source, target);
                        e.remove();
                    } else {
                        trace!("dec_ref: {:?} -> {:?}, pending, now {}",
                               source, target, e.get().len());
                    }
                },
            }
        }
    }

    /// Get the set of targets whose refcount became (and remained) zero since the last check.
    pub fn take_zeroed(&mut self) -> HashSet<Target> {
        let zeroed = mem::replace(&mut self.zeroed, HashSet::new());
        // Remove all entries from `loaded` that have zero refcount.  This simplifies the invariant
        // on `zeroed`: it's always the set of targets in `loaded` that currently have zero
        // refcount.
        for target in &zeroed {
            self.loaded.remove(target);
        }
        zeroed
    }

    /// Get the set of new targets that were requested since the last check.
    pub fn take_added(&mut self) -> HashSet<Target> {
        mem::replace(&mut self.added, HashSet::new())
    }

    fn mark_loaded_and_get_watchers(&mut self, target: Target) -> HashSet<Source> {
        let watchers = unwrap_or!(self.pending.remove(&target), {
            error!("target_loaded: target {:?} is not pending", target);
            return HashSet::new();
        });
        self.loaded.insert(target, watchers.len());
        if watchers.len() == 0 {
            self.zeroed.insert(target);
        }
        watchers
    }
}

/// Mark `target` as loaded.  Should be called only after `target` has actually been loaded (so
/// handlers can access the target object), and only once per target (to keep internal state
/// consistent).
pub fn mark_loaded<E: Ext>(eng: &mut Engine<E>, target: Target) {
    trace!("calling load handler for {:?}", target);
    let watchers = eng.demand.mark_loaded_and_get_watchers(target);
    for w in watchers {
        match w {
            Source::LoginFsm(id) => {
                login_fsm::advance(eng, id, target);
            },
            Source::TeleportFsm(_id) => {
                // TODO
            },
            Source::ImportFsm(id) => {
                import_fsm::advance(eng, id, target);
            },

            // No callbacks for these sources.
            Source::Conn(_) => {},
            Source::ClientCamera(_) => {},
            Source::EntityPos(_) => {},
            Source::TerrainChunk(_) => {},
        }
    }
}
