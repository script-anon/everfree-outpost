//! Macros describing field and component data.  These are used to generate `DataMap`, `Delta`, and
//! `UpdateBuilder` implementation code.
use syntax_exts::{
    define_expand_objs_list,
    expand_objs_named,
    expand_objs_named_macro_safe,
};


// Field definitions for each object type.
//
// The name and flag entries are self-explanatory.  As for the others:
// - `field`: The name of the field on the object struct.  This is used in lookups when the full
//   object is available.
// - `DM`: `DataMode` instance, describing the data associated with this field, and how it should
//   be represented in various forms (base type, owned type, reference type).
// - `store_old`: Should `Delta` store the old value when the field changes?  For efficiency, this
//   is set only when there is a consumer that actually uses the old value for something.  It's
//   safe to set it in more places if there's a new consumer for the data.
// - `store_new`: Should `Delta` store the new value when the field changes?  This is set
//   essentially everywhere, since either clients or save files need to monitor updates to all the
//   fields listed in this file.
//
// There is an additional entry, `base`, which is added automatically to the derived `obj_fields`
// and the global `core_fields` list.  It contains the name of the base object (`client` for
// `client_pawn`, etc).


define_expand_objs_list! {
    client_custom_fields = {
        client_pawn,
            FLAG = C_PAWN, Tag = (::update::fields::ClientPawnTag),
            store_old = false, store_new = true;
        client_name,
            FLAG = C_NAME, Tag = (::update::fields::ClientNameTag),
            store_old = false, store_new = true;
    }
}

define_expand_objs_list! {
    entity_custom_fields = {
        entity_plane,
            FLAG = E_PLANE, Tag = (::update::fields::EntityPlaneTag),
            store_old = true, store_new = true;
        entity_activity,
            FLAG = E_ACTIVITY, Tag = (::update::fields::EntityActivityTag),
            store_old = true, store_new = true;
        entity_appearance,
            FLAG = E_APPEARANCE, Tag = (::update::fields::EntityAppearanceTag),
            store_old = false, store_new = true;
        entity_attachment,
            FLAG = E_ATTACHMENT, Tag = (::update::fields::EntityAttachmentTag),
            store_old = false, store_new = true;
    }
}

define_expand_objs_list! {
    inventory_custom_fields = {
        inventory_contents,
            FLAG = I_CONTENTS, Tag = (::update::fields::InventoryContentsTag),
            store_old = false, store_new = true;
        inventory_attachment,
            FLAG = I_ATTACHMENT, Tag = (::update::fields::InventoryAttachmentTag),
            store_old = false, store_new = true;
    }
}

define_expand_objs_list! {
    plane_custom_fields = {
    }
}

define_expand_objs_list! {
    terrain_chunk_custom_fields = {
        terrain_chunk_blocks,
            FLAG = TC_BLOCKS, Tag = (::update::fields::TerrainChunkBlocksTag),
            store_old = false, store_new = true;
    }
}

define_expand_objs_list! {
    structure_custom_fields = {
        structure_template,
            FLAG = S_TEMPLATE, Tag = (::update::fields::StructureTemplateTag),
            store_old = true, store_new = true;
        structure_attachment,
            FLAG = S_ATTACHMENT, Tag = (::update::fields::StructureAttachmentTag),
            store_old = false, store_new = true;
    }
}


// For each object type, produce a list `obj_fields` which consists of everything from
// `obj_custom_fields` plus the auto-generated common fields (`obj_world_flags` and
// `obj_stable_id`).  This step also adds the entry `base` to each custom field, containing the
// name of the base object type.
for_each_obj! {
    expand_objs_named_macro_safe! { [[$obj_custom_fields]]
        define_expand_objs_list! {
            $obj_fields = {
                #(
                    #obj, base = $obj, Base = $Obj, Id = $ObjId,
                        FLAG = #FLAG, Tag = #Tag,
                        DM = (<#Tag as ::update::traits::Field<::world::objects::$Obj>>::DataMode),
                        store_old = #store_old, store_new = #store_new;
                )*
                // This is called `obj_world_flags` instead of `obj_flags` to avoid a method name
                // conflict in `Delta`.
                $obj_world_flags, base = $obj, Base = $Obj, Id = $ObjId,
                    FLAG = $O_FLAGS, Tag = (::update::fields::FlagsTag),
                    DM = (::update::traits::Small<$ObjFlags>),
                    store_old = false, store_new = true;
                $obj_stable_id, base = $obj, Base = $Obj, Id = $ObjId,
                    FLAG = $O_STABLE_ID, Tag = (::update::fields::StableIdTag),
                    DM = (::update::traits::Small<StableId>),
                    store_old = false, store_new = true;
            }
        }
    }
}

// Concatenate all `obj_fields` lists into a single list called `core_fields`.
expand_objs_named! { [[world_objs]]
    expand_objs_named_macro_safe! { {}
        define_expand_objs_list! {
            core_fields = {
                $( #( [[$obj_fields]]
                    #obj, base = #base, Base = #Base, Id = #Id,
                        FLAG = #FLAG, Tag = #Tag, DM = #DM,
                        store_old = #store_old, store_new = #store_new;
                )* )*
            }
        }
    }
}


// Component definitions for each object type.
//
// These have the same entries as fields, except that there is no `field` entry.

define_expand_objs_list! {
    world_components = {
        world_ward_permits,
            FLAG = W_WARD_PERMITS, Tag = (::update::components::WardPermitsTag),
            store_old = false, store_new = true;
    }
}

define_expand_objs_list! {
    client_components = {
        client_camera_pos,
            FLAG = C_CAMERA_POS, Tag = (::update::components::CameraPosTag),
            store_old = true, store_new = true;
        client_dialog,
            FLAG = C_DIALOG, Tag = (::update::components::DialogTag),
            store_old = false, store_new = true;
        client_key_value,
            FLAG = C_KEY_VALUE, Tag = (::update::components::KeyValueTag),
            store_old = false, store_new = true;
    }
}

define_expand_objs_list! {
    entity_components = {
        entity_char_invs,
            FLAG = E_CHAR_INVS, Tag = (::update::components::CharInvsTag),
            store_old = false, store_new = true;
        entity_key_value,
            FLAG = E_KEY_VALUE, Tag = (::update::components::KeyValueTag),
            store_old = false, store_new = true;
    }
}

define_expand_objs_list! {
    inventory_components = {
        inventory_key_value,
            FLAG = I_KEY_VALUE, Tag = (::update::components::KeyValueTag),
            store_old = false, store_new = true;
    }
}

define_expand_objs_list! {
    plane_components = {
        plane_ward_map,
            FLAG = P_WARD_MAP, Tag = (::update::components::WardMapTag),
            store_old = false, store_new = true;
        plane_key_value,
            FLAG = P_KEY_VALUE, Tag = (::update::components::KeyValueTag),
            store_old = false, store_new = true;
        plane_looted_vaults,
            FLAG = P_LOOTED_VAULTS, Tag = (::update::components::LootedVaultsTag),
            store_old = false, store_new = true;
    }
}

define_expand_objs_list! {
    terrain_chunk_components = {
        terrain_chunk_key_value,
            FLAG = TC_KEY_VALUE, Tag = (::update::components::KeyValueTag),
            store_old = false, store_new = true;
        terrain_chunk_compass_targets,
            FLAG = TC_COMPASS_TARGETS, Tag = (::update::components::CompassTargetsTag),
            store_old = false, store_new = true;
    }
}

define_expand_objs_list! {
    structure_components = {
        structure_bitmap,
            FLAG = S_BITMAP, Tag = (::update::components::BitmapTag),
            store_old = false, store_new = true;
        structure_contents,
            FLAG = S_CONTENTS, Tag = (::update::components::ContentsTag),
            store_old = false, store_new = true;
        structure_crafting,
            FLAG = S_CRAFTING, Tag = (::update::components::CraftingTag),
            store_old = false, store_new = true;
        structure_key_value,
            FLAG = S_KEY_VALUE, Tag = (::update::components::KeyValueTag),
            store_old = false, store_new = true;
        structure_parent_vault,
            FLAG = S_PARENT_VAULT, Tag = (::update::components::ParentVaultTag),
            store_old = false, store_new = true;
    }
}

// Combine all `obj_components` lists into a single `components` list.  Also add `base` entries for
// each component.
expand_objs_named! { [[world_objs]]
    expand_objs_named_macro_safe! { {}
        define_expand_objs_list! {
            components = {
                #( [[world_components]]
                    #obj, base = world, Id = (),
                        FLAG = #FLAG, Tag = #Tag,
                        DM = (<#Tag as ::update::traits::Component<()>>::DataMode),
                        store_old = #store_old, store_new = #store_new;
                )*

                $( #( [[$obj_components]]
                    #obj, base = $obj, Id = $ObjId,
                        FLAG = #FLAG, Tag = #Tag,
                        DM = (<#Tag as ::update::traits::Component<$ObjId>>::DataMode),
                        store_old = #store_old, store_new = #store_new;
                )* )*
            }
        }
    }
}


// Events

define_expand_objs_list! {
    update_events = {
        ward_add, args = {
            plane, Ty = (PlaneId);
            owner, Ty = (Stable<ClientId>);
            owner_name, Ty = (String);
            region, Ty = (Region<V2>);
        };
        ward_remove, args = {
            plane, Ty = (PlaneId);
            owner, Ty = (Stable<ClientId>);
        };

        permit_add, args = {
            owner, Ty = (Stable<ClientId>);
            user, Ty = (String);
        };
        permit_remove, args = {
            owner, Ty = (Stable<ClientId>);
            user, Ty = (String);
        };

        key_value_set, args = {
            id, Ty = AnyId;
            key, Ty = String;
            value, Ty = Value;
        };
        key_value_clear, args = {
            id, Ty = AnyId;
            key, Ty = String;
        };

        looted_vaults_add, args = {
            plane, Ty = (PlaneId);
            vault, Ty = (::component::compass_targets::VaultId);
        };
    }
}


// Misc. shorthand macros

macro_rules! for_each_core_field {
    ($($args:tt)*) => { ::syntax_exts::for_each_obj_named! { [[core_fields]] $($args)* } };
}

macro_rules! for_each_component {
    ($($args:tt)*) => { ::syntax_exts::for_each_obj_named! { [[components]] $($args)* } };
}

macro_rules! expand {
    ($base:ident => $($ts:tt)*) => {
        ::syntax_exts::for_each_obj_named_macro_safe!( { $base; } $($ts)* )
    };
}


// Mode dispatchers
//
// The various fields and components have different requirements regarding what things get passed
// by value, by reference, by Box<T>, etc.  These macros handle those differences by defining a
// "mode" for each passing style.  We use a slightly ugly dispatch scheme so that we can define
// each mode in its own macro, rather than having each mode's definition spread across cases of
// several different macros.

/// `ref_ty!(DM)`: Get the reference type from a `DataMode`.  This is used for non-moving inserts
/// and for the return values of lookup methods.
macro_rules! ref_ty {
    ($DM:ty) => { <$DM as ::update::traits::DataMode>::Ref };
}

/// `owned_ty!(DM)`: Get the owned type from a `DataMode`.  This is used for moving inserts.
macro_rules! owned_ty {
    ($DM:ty) => { <$DM as ::update::traits::DataMode<'static>>::Owned };
}

/// `ref_to_owned!(DM, x)`: Convert `x: ref_ty!(DM)` to `owned_ty!(DM)`, typically by cloning.
macro_rules! ref_to_owned {
    ($DM:ty, $e:expr) => { <$DM as ::update::traits::DataMode>::ref_to_owned($e) };
}

/// `owned_ref_to_ref!(DM, x)`: Convert `x: &owned_ty!(DM)` to `ref_ty!(DM)`.  This is used for the
/// return values of lookup methods.
macro_rules! owned_ref_to_ref {
    ($DM:ty, $e:expr) => { <$DM as ::update::traits::DataMode>::owned_ref_to_ref($e) };
}

/// `field_ref!(Tag, Obj, x)`: Access the field of `x` identified by `Tag`, and return a reference
/// of type `ref_ty!(Tag::DataMode)`.  This is used for core-field lookup methods in `Delta`.
macro_rules! field_ref {
    ($Tag:ty, $Obj:ty, $e:expr) => {
        <$Tag as ::update::traits::Field<$Obj>>::get_ref($e)
    };
}
