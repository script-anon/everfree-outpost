#[allow(unused)] use server_types::*;
use std::marker::PhantomData;
use common::util::StringResult;
use world::import::IdRemapper;

use engine::Engine;
use ext::Ext;


pub trait Field<Obj> {
    type DataMode: for<'a> DataMode<'a>;

    fn get_ref<'a>(obj: &'a Obj) -> <Self::DataMode as DataMode<'a>>::Ref;
}

#[allow(unused_variables)]
pub trait Component<Id> {
    type DataMode: for<'a> DataMode<'a>;

    /// Get the current component value for `id` from the engine.
    fn get<E>(eng: &Engine<E>, id: Id)
              -> Option<<Self::DataMode as DataMode<'static>>::Owned>;

    /// Run pre-apply checks and rewrite any transient IDs contained in component data.
    fn pre_apply(component: &mut <Self::DataMode as DataMode>::Owned,
                 mapper: &IdRemapper) -> StringResult<()> {
        Ok(())
    }

    /// Set the value of this component for object `id` to `component`.
    fn apply<E: Ext>(eng: &mut Engine<E>,
                     id: Id,
                     component: Option<<Self::DataMode as DataMode>::Owned>) {}
}


/// Defines multiple views of a type of data, along with functions to convert between them.
pub trait DataMode<'a> {
    type Base: ?Sized;
    type Ref: Copy;
    type Owned;

    fn ref_to_owned(r: Self::Ref) -> Self::Owned;
    fn owned_ref_to_ref(r: &'a Self::Owned) -> Self::Ref;
    fn base_ref_to_ref(r: &'a Self::Base) -> Self::Ref;
}

pub struct Small<T>(PhantomData<T>);
impl<'a, T: Copy> DataMode<'a> for Small<T> {
    type Base = T;
    type Ref = T;
    type Owned = T;

    fn ref_to_owned(r: T) -> T { r }
    fn owned_ref_to_ref(r: &'a T) -> T { *r }
    fn base_ref_to_ref(r: &'a T) -> T { *r }
}

pub struct Medium<T>(PhantomData<T>);
impl<'a, T: Clone+'a> DataMode<'a> for Medium<T> {
    type Base = T;
    type Ref = &'a T;
    type Owned = T;

    fn ref_to_owned(r: &'a T) -> T { (*r).clone() }
    fn owned_ref_to_ref(r: &'a T) -> &'a T { r }
    fn base_ref_to_ref(r: &'a T) -> &'a T { r }
}

pub struct Large<T>(PhantomData<T>);
impl<'a, T: Clone+'a> DataMode<'a> for Large<T> {
    type Base = T;
    type Ref = &'a T;
    type Owned = Box<T>;

    fn ref_to_owned(r: &'a T) -> Box<T> { Box::new((*r).clone()) }
    fn owned_ref_to_ref(r: &'a Box<T>) -> &'a T { r }
    fn base_ref_to_ref(r: &'a T) -> &'a T { r }
}

pub struct LargeSlice<T>(PhantomData<T>);
impl<'a, T: Clone+'a> DataMode<'a> for LargeSlice<T> {
    type Base = [T];
    type Ref = &'a [T];
    type Owned = Box<[T]>;

    fn ref_to_owned(r: &'a [T]) -> Box<[T]> { (*r).to_owned().into_boxed_slice() }
    fn owned_ref_to_ref(r: &'a Box<[T]>) -> &'a [T] { &**r }
    fn base_ref_to_ref(r: &'a [T]) -> &'a [T] { r }
}

pub struct MediumSmall<T, U>(PhantomData<(T, U)>);
impl<'a, T: Clone+'a, U: Copy> DataMode<'a> for MediumSmall<T, U> {
    type Base = (T, U);
    type Ref = (&'a T, U);
    type Owned = (T, U);

    fn ref_to_owned(r: (&'a T, U)) -> (T, U) { ((*r.0).clone(), r.1) }
    fn owned_ref_to_ref(r: &'a (T, U)) -> (&'a T, U) { (&r.0, r.1) }
    fn base_ref_to_ref(r: &'a (T, U)) -> (&'a T, U) { (&r.0, r.1) }
}

