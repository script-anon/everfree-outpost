use server_types::*;
use std::fmt;
use std::mem;
use std::ptr;
use std::raw::TraitObject;
use server_extra::Value;
use syntax_exts::{expand_objs_named, for_each_obj_named, expand_objs_named_macro_safe};


for_each_obj_named! { [[update_events]]
    expand_objs_named_macro_safe! { $args
        #[derive(Debug)]
        pub struct $Obj {
            #(pub #obj: #Ty,)*
        }
    }
}


pub trait Event {
    fn as_event(&self) -> EventRef;
    fn as_debug(&self) -> &dyn fmt::Debug;
}

expand_objs_named! { [[update_events]]
    #[derive(Clone, Copy, Debug)]
    pub enum EventRef<'a> {
        $( $Obj(&'a $Obj), )*
    }
}

for_each_obj_named! { [[update_events]]
    impl Event for $Obj {
        fn as_event(&self) -> EventRef {
            EventRef::$Obj(self)
        }

        fn as_debug(&self) -> &dyn fmt::Debug {
            self
        }
    }
}


pub struct EventVec {
    idxs: Vec<usize>,
    data: Vec<usize>,
}

impl EventVec {
    pub fn new() -> EventVec {
        EventVec {
            idxs: Vec::new(),
            data: Vec::new(),
        }
    }

    pub fn len(&self) -> usize {
        self.idxs.len()
    }

    pub fn push<T: Event>(&mut self, x: T) {
        use std::mem::{size_of, align_of};
        assert!(align_of::<T>() <= align_of::<usize>());

        let idx = self.data.len();
        unsafe {
            // Extend `data` without initializing.  Safe because nothing reads the memory until
            // after we copy the new value into it.
            let words = 1 + (size_of::<T>() + size_of::<usize>() - 1) / size_of::<usize>();
            self.data.reserve(words);
            let new_len = self.data.len() + words;
            assert!(new_len <= self.data.capacity());
            self.data.set_len(new_len);

            // If this push succeeds, no panics are allowed until the end of the function.  If we
            // were to exit this function after the push but before copying `x` into `data`, the
            // item at `data[idx]` could be dropped while uninitialized.
            self.idxs.push(idx);

            // No panics allowed from this point onward.

            let storage = self.data.as_mut_ptr().add(idx);
            // Store the vtable pointer at `idx`.
            let vtable_ptr: *mut () = mem::transmute::<_, TraitObject>(&x as &Event).vtable;
            ptr::write(storage as *mut *mut (), vtable_ptr);
            // Store the object itself. at `idx + 1`.
            ptr::write(storage.add(1) as *mut T, x);
        }
    }

    fn iter_dyn<'a>(&'a self) -> impl Iterator<Item=&'a dyn Event>+'a {
        let data = &self.data;
        self.idxs.iter().map(move |&idx| {
            unsafe {
                let header_ptr: *const usize = &data[idx];
                let value_ptr = header_ptr.offset(1) as *mut ();
                let ptr: *const Event = mem::transmute(TraitObject {
                    vtable: *header_ptr as *mut (),
                    data: value_ptr,
                });
                &*ptr
            }
        })
    }

    pub fn iter<'a>(&'a self) -> impl Iterator<Item=EventRef<'a>>+'a {
        self.iter_dyn().map(|e| e.as_event())
    }
}

impl fmt::Debug for EventVec {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut fl = f.debug_list();
        for e in self.iter_dyn() {
            fl.entry(e.as_debug());
        }
        fl.finish()
    }
}

impl Drop for EventVec {
    fn drop(&mut self) {
        unsafe {
            for idx in mem::replace(&mut self.idxs, Vec::new()) {
                let header_ptr: *mut usize = &mut self.data[idx];
                let value_ptr = header_ptr.offset(1) as *mut ();
                let ptr: *mut Event = mem::transmute(TraitObject {
                    vtable: *header_ptr as *mut (),
                    data: value_ptr,
                });
                ptr::drop_in_place(ptr);
            }
        }
    }
}
