use server_types::*;
use multipython::{PyBox, PyResult};
use multipython::util;
use server_config::data;
use server_python_conv::Pack;
use syntax_exts::python_module_builder;


python_module_builder! {
    name = _outpost_data;
    builder_name = build_module;
    data: ();
    conv_module = ::server_python_conv;


    fn block_count() -> usize {
        data().blocks().len()
    }

    fn block_by_name(name: String) -> PyResult<BlockId> {
        Ok(pyunwrap!(data().get_block_id(&name),
                     key_error, "no such block: {:?}", name))
    }

    fn get_block_by_name(name: String) -> Option<BlockId> {
        data().get_block_id(&name)
    }

    fn block_name(id: BlockId) -> PyResult<PyBox> {
        let b = pyunwrap!(data().get_block(id),
                          runtime_error, "no block with that ID");
        Pack::pack(b.name())
    }

    fn block_shape(id: BlockId) -> PyResult<&'static str> {
        let b = pyunwrap!(data().get_block(id),
                          runtime_error, "no block with that ID");
        let desc = match b.flags().shape() {
            Shape::Empty => "empty",
            Shape::Floor => "floor",
            Shape::Solid => "solid",
            //Shape::RampE => "ramp_e",
            //Shape::RampW => "ramp_w",
            //Shape::RampS => "ramp_s",
            Shape::RampN => "ramp_n",
        };
        Ok(desc)
    }


    fn item_count() -> usize {
        data().items().len()
    }

    fn item_by_name(name: String) -> PyResult<ItemId> {
        Ok(pyunwrap!(data().get_item_id(&name),
                     key_error, "no such item: {:?}", name))
    }

    fn get_item_by_name(name: String) -> Option<ItemId> {
        data().get_item_id(&name)
    }

    fn item_name(id: ItemId) -> PyResult<PyBox> {
        let i = pyunwrap!(data().get_item(id),
                          runtime_error, "no item with that ID");
        Pack::pack(i.name())
    }


    fn recipe_count() -> usize {
        data().recipes().len()
    }

    fn recipe_by_name(name: String) -> PyResult<RecipeId> {
        Ok(pyunwrap!(data().get_recipe_id(&name),
                     key_error, "no such recipe: {:?}", name))
    }

    fn get_recipe_by_name(name: String) -> Option<RecipeId> {
        data().get_recipe_id(&name)
    }

    fn recipe_name(id: RecipeId) -> PyResult<PyBox> {
        let r = pyunwrap!(data().get_recipe(id),
                          runtime_error, "no recipe with that ID");
        Pack::pack(r.name())
    }

    fn recipe_inputs(id: RecipeId) -> PyResult<PyBox> {
        let r = pyunwrap!(data().get_recipe(id),
                          runtime_error, "no recipe with that ID");
        Pack::pack(r.inputs())
    }

    fn recipe_outputs(id: RecipeId) -> PyResult<PyBox> {
        let r = pyunwrap!(data().get_recipe(id),
                          runtime_error, "no recipe with that ID");
        Pack::pack(r.outputs())
    }

    fn recipe_crafting_class(id: RecipeId) -> PyResult<u8> {
        let r = pyunwrap!(data().get_recipe(id),
                          runtime_error, "no recipe with that ID");
        Ok(r.crafting_class)
    }


    fn crafting_class_count() -> usize {
        data().crafting_classes().len()
    }

    fn crafting_class_by_name(name: String) -> PyResult<u8> {
        Ok(pyunwrap!(data().get_crafting_class_id(&name),
                     key_error, "no such crafting class: {:?}", name))
    }

    fn get_crafting_class_by_name(name: String) -> Option<u8> {
        data().get_crafting_class_id(&name)
    }

    fn crafting_class_name(cls: u8) -> PyResult<PyBox> {
        let s = pyunwrap!(data().get_crafting_class_name(cls),
                          runtime_error, "no crafting class with that ID");
        Pack::pack(s)
    }


    // TODO: clean up Option<PyResult<_>> methods above to work more like template_name &
    // template_layer below


    fn template_count() -> usize {
        data().templates().len()
    }

    fn template_by_name(name: String) -> PyResult<TemplateId> {
        Ok(pyunwrap!(data().get_template_id(&name),
                     key_error, "no such template: {:?}", name))
    }

    fn get_template_by_name(name: String) -> Option<TemplateId> {
        data().get_template_id(&name)
    }

    fn template_name(id: TemplateId) -> PyResult<PyBox> {
        let t = pyunwrap!(data().get_template(id),
                          runtime_error, "no template with that ID");
        Pack::pack(t.name())
    }

    fn template_layer(id: TemplateId) -> PyResult<u8> {
        let t = pyunwrap!(data().get_template(id),
                          runtime_error, "no template with that ID");
        Ok(t.layer)
    }

    fn template_size(id: TemplateId) -> PyResult<V3> {
        let t = pyunwrap!(data().get_template(id),
                          runtime_error, "no template with that ID");
        Ok(t.size)
    }
}

pub fn register() -> PyResult<PyBox> {
    let m = build_module(())?;
    util::register_module(m.borrow())?;
    Ok(m)
}
