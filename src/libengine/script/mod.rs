//! This module provides the interface between the engine and the Python script interpreter.  It
//! sets up the Python modules for calling into native engine code, and also imports some Python
//! functions that should be callable from the engine.
//!
//! This code is probably memory-unsafe under certain weird conditions, such as when replacing and
//! dropping/forgetting the `Engine`'s `Scripting` object while inside a callback from Python code.
//! Just don't do anything stupid and you'll probably be fine.

use std::marker::PhantomData;
use std::path::Path;
use common::util::StringResult;
use multipython::{self, PyBox, PyResult};
use multipython::Interp;
use server_python_conv;

use engine::Engine;
use ext::Ext;

use self::handlers::Handlers;


mod data;
mod engine;
mod handlers;
mod types;


struct Modules {
    #[allow(dead_code)]
    data: PyBox,
    engine: PyBox,
    #[allow(dead_code)]
    types: PyBox,
}


pub struct Scripting<E> {
    interp: Interp,
    _marker: PhantomData<E>,
}

impl<E: Ext> Scripting<E> {
    pub fn new(boot_path: &Path) -> Scripting<E> {
        let mut interp = Interp::new();

        // Set up built-in types and modules
        interp.add_state(server_python_conv::ServerTypes::new);
        interp.add_state(|| {
            let data = data::register().unwrap();
            let engine = engine::register::<E>().unwrap();
            let types = types::register().unwrap();
            Modules {
                data: data,
                engine: engine,
                types: types,
            }
        });

        // Run the bootstrap script
        interp.enter(|| {
            multipython::util::run_file(boot_path).unwrap();
        });

        // Collect event handler functions
        interp.add_state(|| {
            let module = multipython::util::import("outpost.core.handlers").unwrap();
            Handlers::new(module.borrow()).unwrap()
        });


        Scripting {
            interp: interp,
            _marker: PhantomData,
        }
    }

    pub fn call<F, R>(eng: &mut Engine<E>, f: F) -> StringResult<R>
            where F: FnOnce(&Handlers) -> PyResult<R> {
        let eng_ptr = eng as *mut Engine<E>;

        unsafe {
            let g = || {
                let ms = pyunwrap!(Interp::get_state::<Modules>(),
                                   runtime_error, "engine modules not initialized");
                let handlers = pyunwrap!(Interp::get_state::<Handlers>(),
                                         runtime_error, "handlers not initialized");

                let _eng_guard = engine::set_ref(ms.engine.borrow(), eng_ptr);
                f(&handlers)
            };

            eng.scripting.interp.enter_unchecked(|| g().map_err(|e| e.unwrap_rust().msg.into()))
        }
    }

    pub fn handle<F: FnOnce(&Handlers) -> PyResult<()>>(eng: &mut Engine<E>, f: F) {
        match Scripting::call(eng, f) {
            Ok(()) => {},
            Err(e) => error!("python exception in handler: {}", e),
        }
    }
}


pub fn on_ipython<E: Ext>(eng: &mut Engine<E>, req: String) -> String {
    match Scripting::call(eng, |h| h.on_ipython_request(req)) {
        Ok(s) => s,
        Err(e) => {
            warn!("python exception in ipython handler: {}", e);
            // Dummy, but valid, response
            "{ \
                \"ok\": false, \
                \"exc_type\": \"engine error\", \
                \"exc_msg\": \"\", \
                \"backtrace\": \"\", \
                \"print\": [] \
            }".to_owned()
        },
    }
}
