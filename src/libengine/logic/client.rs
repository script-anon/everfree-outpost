use server_types::*;
use server_config::data;
use world::objects::*;

use component::camera;
use component::char_invs::{self, CharInvs};
use component::dialog;
use engine::Engine;
use ext::Ext;
use script::Scripting;


pub fn client_loaded<E: Ext>(eng: &mut Engine<E>, cid: ClientId) {
    trace!("client_loaded: {:?}", cid);
    if eng.w.client(cid).pawn_id().is_none() {
        let name = eng.w.client(cid).name().to_owned();
        trace!("client_loaded: {:?} has no pawn! (opening dialog)", cid);
        dialog::open(eng, cid, dialog::pony_edit::new(name));
    }
}

pub fn on_ready<E: Ext>(eng: &mut Engine<E>,
                        cid: ClientId) {
    let now = eng.w.now;
    // TODO: handle world_info (should really be plane_info) inside Delta or vision somewhere
    eng.ext.client_world_info(cid, now, DAY_NIGHT_CYCLE_MS as u32);
    eng.ext.client_readiness(cid, true);
    Scripting::handle(eng, |h| h.on_client_join(cid));
}

pub fn kick<E: Ext>(eng: &mut Engine<E>, id: ClientId, reason: &str) {
    let c = unwrap_or_error!(eng.w.get_client(id), "kick: unknown {:?}", id);
    eng.ext.kick_user(c.uid(), reason);
    // TODO: mark the user as kicked, so they can't take further actions between now and whenever
    // we finally get the `UserDisconnect` back from `conn`
}

pub fn create_pawn<E: Ext>(eng: &mut Engine<E>,
                           cid: ClientId,
                           appearance: Appearance) -> bool {
    if eng.w.get_client(cid).is_none() {
        error!("create_pawn: nonexistent {:?}", cid);
        return false;
    }

    if let Err(e) = check_appearance(&appearance) {
        warn!("{:?}: illegal pawn appearance: {}", cid, e);
        return false;
    }

    let plane = unwrap_or!(eng.w.transient_plane_id(STABLE_PLANE_FOREST), {
        // The client camera points at the forest plane, so it should get loaded eventually, but it
        // might not be done yet if the client was exceptionally quick.
        warn!("create_pawn: forest plane does not exist yet");
        return false;
    });

    let eid = {
        let mut e = eng.w.create_entity(Entity {
            plane: plane,
            activity: Activity::default(),
            appearance: appearance,
            ..Entity::default()
        });
        e.set_attachment(EntityAttachment::Client(cid));
        e.id()
    };

    eng.w.client_mut(cid).set_pawn(eid);
    camera::clear(eng, cid);

    // Add character inventories
    let invs = {
        let mut mk = |size, give| {
            let mut i = eng.w.create_inventory(Inventory::new(size));
            i.set_attachment(InventoryAttachment::Entity(eid));
            if give {
                i.modify_contents(|c| {
                    c[0] = Item::new(data().item_id("wood"), 20);
                    c[1] = Item::new(data().item_id("stone"), 20);
                });
            }
            i.id()
        };
        CharInvs {
            main: mk(30, true),
            ability: mk(30, false),
            equip: mk(13, false),
        }
    };
    char_invs::add(eng, eid, invs);

    trace!("created pawn {:?} for {:?}", eid, cid);
    true
}

fn check_appearance(a: &Appearance) -> Result<(), String> {
    if a.flags.contains(APP_WING | APP_HORN) {
        return Err("bad flags: both WING and HORN are set".to_owned());
    }

    if a.flags.contains(APP_LIGHT) {
        return Err("bad flags: LIGHT is set".to_owned());
    }

    // TODO: consult server data to know how many manes/tails are valid
    if a.mane >= 3 {
        return Err(format!("bad mane: {} >= 3", a.mane));
    }

    if a.tail >= 2 {
        return Err(format!("bad tail: {} >= 2", a.tail));
    }

    // TODO: check that all equipment slots are 0 (not present)
    Ok(())
}
