//! High-level crafting logic.
//!
//! The crafting system is built in three parts.
//!
//!  - `component::crafting` handles the lowest-level details, such as tracking progress, updating
//!    periodically, and running the recipe.  It doesn't include any checks - a call to `start` can
//!    start crafting any recipe, into any inventory.
//!
//!  - `component::dialog::crafting` takes input from the user, checks it against its internal
//!    configuration, and then calls `component::crafting` methods.  But the event handlers here
//!    assume the dialog's configuration is reasonable - once the dialog is open, the player can
//!    craft anything permitted by the dialog's `class_mask` and `char_ability` inventory, into its
//!    `contents` inventory.
//!
//!  - `logic::crafting` has the main entry point for crafting, `build_structure_dialog`.  This
//!    function builds an appropriately-configured dialog for doing crafting with a particular
//!    crafting station, and is responsible for upholding the "normal" properties of crafting, such
//!    as the crafting inventory being the contents of the crafting station structure.

use server_types::*;
use server_config::data;

use component::crafting::StationId;
use component::dialog::{self, Dialog};
use engine::Engine;
use ext::Ext;
use logic::inventory;


pub fn can_craft<E: Ext>(eng: &Engine<E>,
                         recipe_id: RecipeId,
                         classes: u32,
                         ability_inv: Option<InventoryId>) -> bool {
    let recipe = unwrap_or!(data().get_recipe(recipe_id), {
        error!("invalid recipe id {}", recipe_id);
        return false;
    });

    if classes & (1 << recipe.crafting_class) == 0 {
        return false;
    }

    if recipe.ability != NO_ITEM {
        let ability_inv = unwrap_or!(ability_inv, return false);
        let inv = unwrap_or!(eng.w.get_inventory(ability_inv), {
            error!("nonexistent ability inventory {:?}", ability_inv);
            return false;
        });
        if inventory::count(inv, recipe.ability) == 0 {
            return false;
        }
    }

    true
}

pub fn build_structure_dialog<E: Ext>(eng: &mut Engine<E>,
                                      cid: ClientId,
                                      sid: StructureId,
                                      classes: u32) -> Option<Dialog> {
    let c = unwrap_or!(eng.w.get_client(cid), return None);
    let e = unwrap_or!(c.pawn(), return None);
    let invs = unwrap_or!(eng.char_invs.get(e.id()), return None);
    let contents = unwrap_or!(eng.contents.get(sid), return None);

    Some(dialog::crafting::crafting(StationId::Structure(sid),
                                    classes,
                                    contents,
                                    invs.main,
                                    invs.ability))
}

pub fn build_celestial_dialog<E: Ext>(eng: &mut Engine<E>,
                                      cid: ClientId,
                                      sid: StructureId,
                                      iid: InventoryId,
                                      classes: u32) -> Option<Dialog> {
    let c = unwrap_or!(eng.w.get_client(cid), return None);
    let e = unwrap_or!(c.pawn(), return None);
    let invs = unwrap_or!(eng.char_invs.get(e.id()), return None);

    Some(dialog::crafting::crafting(StationId::Structure(sid),
                                    classes,
                                    iid,
                                    invs.main,
                                    invs.ability))
}
