use server_types::*;
use std::collections::HashSet;
use world::object_ref::ObjectRef;
use world::objects::*;

use component::inv_update;
use engine::Engine;
use ext::{Ext, WorldExt};
use lifecycle::demand::Target;
use logic;
use movement;
use timer;
use update::flags;
use update::{Update, UpdateItem, UpdateBuilder};

pub const TICK_MS: Time = 32;

pub fn on_tick<E: Ext>(eng: &mut Engine<E>) {
    // Earlier: all client inputs are processed immediately upon arrival.

    timer::run(eng);
    movement::update(eng);

    update_entity_cpos(eng);

    inv_update::tick(eng);

    let mut delta = eng.update.take(eng);
    for it in delta.items() {
        expand_objs! {
            match it {
                UpdateItem::World(_) => {},
                $( UpdateItem::$Obj(id, f) => {
                    logic::auto_refs::$update_obj_refs(
                        id, f, &mut eng.demand, &eng.w, &delta);
                }, )*
            }
        }
    }


    // Unload objects that have zero refs.  This runs in a loop because unloading one object might
    // dec_ref other objects, possibly to zero.  The number of iterations is O(1), limited by the
    // structure of the reference graph, which can be stratified by source/target kind (for
    // example, `Source::Conn`s reference only `Target::Client`, and `Client` bundles contain only
    // `Source::ClientCamera` and `Source::EntityPos` references, which refer only to planes and
    // terrain chunks).

    loop {
        let zeroed = eng.demand.take_zeroed();
        if zeroed.len() == 0 {
            break;
        }

        for target in zeroed {
            // This operation only unloads objects, nothing else.
            unload_bundle(eng, target);
        }

        {
            let new_delta = eng.update.take(eng);
            for it in new_delta.items() {
                expand_objs! {
                    match it {
                        $( UpdateItem::$Obj(id, f)
                                if f.contains(flags::$O_DESTROYED) &&
                                   !f.contains(flags::$O_CREATED) => {
                            logic::auto_refs::$update_obj_refs(
                                id, f, &mut eng.demand, &eng.w, &new_delta);
                        }, )*
                        _ => {
                            error!("unload: discarding unexpected change {:?}", it);
                        },
                    }
                }
            }
            delta.merge(new_delta);
        }
    }

    let u: Update = delta;

    eng.terrain_cache.update(&eng.w, &u);
    eng.structure_cache.update(&eng.w, &u);
    eng.movement.update(&eng.w, &u);
    eng.inv_updates.update(&u);
    eng.w.crafting.update(&mut eng.timers, &mut eng.inv_updates);

    dispatch_update(eng, u);

    let added_targets = eng.demand.take_added();
    request_bundles(eng, added_targets);

    // This must happen last, to ensure that removed objects' IDs are not reused until after all
    // updates referencing them have been processed.
    eng.w.recycle_ids();

    eng.w.now += TICK_MS;
}

/// Make sure entities' activity start positions always give a reasonable approximation of their
/// current chunk position.  We do this by periodically bumping up moving entities' starting
/// position and time, keeping them on the same trajectory but moving their starting position
/// closer to their current one.
fn update_entity_cpos<E: Ext>(eng: &mut Engine<E>) {
    let now = eng.w.now;
    for eid in eng.movement.entity_ids() {
        let mut e = unwrap_or!(eng.w.get_entity_mut(eid), continue);
        let delta = e.activity_start() - now;
        if delta < 1000 || e.activity().velocity() == 0 {
            // We can only bump in increments of 1 second.
            continue;
        }
        let steps = (delta / 1000) as i32;

        let new_start = e.activity_start() + steps as Time * 1000;
        match *e.activity() {
            Activity::Stand { .. } => {},
            Activity::Walk { pos, velocity, dir } => {
                let pos = pos + velocity * steps;
                e.set_activity(Activity::Walk { pos, velocity, dir }, new_start);
            },
        }
    }
}

fn dispatch_update<E: Ext>(eng: &mut Engine<E>, u: Update) {
    //info!("{}", ::update::debug::dump(&u).unwrap());
    eng.ext.handle_update(&eng.w, u);
}

fn request_bundles<E: Ext>(eng: &mut Engine<E>, targets: HashSet<Target>) {
    for target in targets {
        trace!("request bundle for {:?}", target);
        match target {
            Target::Client(id) => {
                let uid = id.unwrap() as u32;
                let name = unwrap_or!(eng.conn_map.get_name(uid), {
                    error!("no such connection for uid 0x{:x}", uid);
                    continue;
                });
                eng.ext.request_client_bundle(uid, name);
            },

            Target::Plane(id) => eng.ext.request_plane_bundle(id),

            Target::TerrainChunk(pid, cpos) => {
                let stable_pid = unwrap_or!(eng.w.get_plane_mut(pid), {
                    error!("no such plane {:?} for chunk at {:?}", pid, cpos);
                    continue;
                }).pin();
                if stable_pid == STABLE_PLANE_LIMBO {
                    continue;
                }
                eng.ext.request_terrain_chunk_bundle(stable_pid, cpos);
            },
        }
    }
}

fn unload_bundle<E: Ext>(eng: &mut Engine<E>, target: Target) {
    trace!("unload unused {:?}", target);
    match target {
        Target::Client(stable_id) => {
            let id = unwrap_or_error!(eng.w.transient_client_id(stable_id),
                                      "unload: missing client {:?}", stable_id);
            {
                let c = unwrap_or_error!(eng.w.get_client(id),
                                         "unload: missing {:?}", id);
                mark_unloaded_client(c, &eng.update);
            }
            eng.w.destroy_client(id);
        },

        Target::Plane(stable_id) => {
            let id = unwrap_or_error!(eng.w.transient_plane_id(stable_id),
                                      "unload: missing plane {:?}", stable_id);
            {
                let p = unwrap_or_error!(eng.w.get_plane(id),
                                         "unload: missing {:?}", id);
                mark_unloaded_plane(p, &eng.update);
            }
            eng.w.destroy_plane(id);
        },

        Target::TerrainChunk(pid, cpos) => {
            let tcid = {
                let p = unwrap_or_error!(
                    eng.w.get_plane(pid),
                    "unload: missing plane {:?} for terrain chunk",
                    pid);
                unwrap_or_error!(p.chunk_id(cpos),
                                 "unload: missing chunk at {:?} in plane {:?}",
                                 cpos, p.id())
            };
            {
                let tc = unwrap_or_error!(eng.w.get_terrain_chunk(tcid),
                                          "unload: missing {:?}", tcid);
                mark_unloaded_terrain_chunk(tc, &eng.update);
            }
            eng.w.destroy_terrain_chunk(tcid);
        },
    }
}

fn mark_unloaded_client(c: ObjectRef<WorldExt, Client>, ub: &UpdateBuilder) {
    for e in c.child_entities() {
        mark_unloaded_entity(e, ub);
    }
    for i in c.child_inventories() {
        mark_unloaded_inventory(i, ub);
    }
    ub.client_unloaded(c.id());
}

fn mark_unloaded_entity(e: ObjectRef<WorldExt, Entity>, ub: &UpdateBuilder) {
    for i in e.child_inventories() {
        mark_unloaded_inventory(i, ub);
    }
    ub.entity_unloaded(e.id());
}

fn mark_unloaded_inventory(i: ObjectRef<WorldExt, Inventory>, ub: &UpdateBuilder) {
    ub.inventory_unloaded(i.id());
}

fn mark_unloaded_plane(p: ObjectRef<WorldExt, Plane>, ub: &UpdateBuilder) {
    ub.plane_unloaded(p.id());
}

fn mark_unloaded_terrain_chunk(tc: ObjectRef<WorldExt, TerrainChunk>, ub: &UpdateBuilder) {
    for s in tc.child_structures() {
        mark_unloaded_structure(s, ub);
    }
    ub.terrain_chunk_unloaded(tc.id());
}

fn mark_unloaded_structure(s: ObjectRef<WorldExt, Structure>, ub: &UpdateBuilder) {
    for i in s.child_inventories() {
        mark_unloaded_inventory(i, ub);
    }
    ub.structure_unloaded(s.id());
}
