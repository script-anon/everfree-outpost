#![crate_name = "engine"]

#![feature(
    fnbox,
    proc_macro_hygiene,
    raw,
)]

#[macro_use] extern crate bitflags;
extern crate libc;
#[macro_use] extern crate log;

extern crate common;
extern crate physics;
#[macro_use] extern crate multipython;
extern crate server_bundle;
extern crate server_config;
extern crate server_extra;
extern crate server_python_conv;
extern crate server_types;
extern crate syntax_exts;
#[macro_use] extern crate world;

// Import `expand_objs` name list
define_obj_list_world!();


pub mod engine;
pub mod ext;

pub mod update;

pub mod script;

// Ordering matters due to with_X_ext_methods! macros
#[macro_use] pub mod component;
#[macro_use] pub mod cache;

pub mod chat;
pub mod input;
pub mod lifecycle;
pub mod movement;
pub mod tick;
pub mod timer;

// Exposed only for testing - engine_thread shouldn't call into `logic` directly.
pub mod logic;

pub use self::engine::Engine;
