import _outpost_engine as ENG
from _outpost_types import *
from outpost.core.data import DATA, ItemProxy


def can_add(i, item, count):
    return ENG.logic_inventory_can_add(i.id, DATA.item(item).id, count)

def can_add_multi(i, items):
    return ENG.logic_inventory_can_add_multi(i.id,
            [(DATA.item(item).id, count) for item, count in items])

def add(i, item, count):
    return ENG.logic_inventory_add(i.id, DATA.item(item).id, count)

def can_remove(i, item, count):
    return ENG.logic_inventory_can_remove(i.id, DATA.item(item).id, count)

def remove(i, item, count):
    return ENG.logic_inventory_remove(i.id, DATA.item(item).id, count)

