import shlex

from _outpost_types import *
from outpost.core import engine


class Handler:
    """Chat command handler.  This is a wrapped function that also contains
    some metadata about the command."""

    def __init__(self, func, doc=None, name=None, cond=None):
        self.func = func
        self.cond = cond

        # `doc` can be a string, a list of strings, or None
        if doc is None:
            if func.__doc__ is not None:
                self.doc = reflow(func.__doc__)
            else:
                self.doc = None
        elif isinstance(doc, str):
            self.doc = [doc]
        else:
            self.doc = doc

        self.name = name or func.__name__

    def __call__(self, client, args):
        self.func(client, args)

    def usable_by(self, client):
        if self.cond is None:
            return True
        return self.cond(client)

def is_superuser(c):
    return c.get('superuser', False)

def reflow(s):
    lines = []
    buf = ''

    for line in s.splitlines():
        line = line.strip()
        if line == '':
            if buf != '':
                lines.append(buf)
                buf = ''
        else:
            if buf != '':
                buf = '%s %s' % (buf, line)
            else:
                buf = line

    if buf != '':
        lines.append(buf)

    return lines


_HANDLERS = {}

def get_handler(cmd, client):
    h = _HANDLERS.get(cmd)
    if h is None or not h.usable_by(client):
        return None
    return h

def handle_command(client, cmd, args):
    h = get_handler(cmd, client)
    if h is None:
        client.log_msg('Unknown command /%s' % cmd)
        return
    h(client, args)

def list_commands(client):
    cmds = list(k for k, v in _HANDLERS.items() if v.usable_by(client))
    cmds.sort()
    return cmds

def register_command(handler):
    """Add a Handler to _HANDLERS, checking for duplicate entries."""
    cmd = handler.name
    assert cmd not in _HANDLERS, \
            'duplicate registration for chat command %r' % cmd
    _HANDLERS[cmd] = handler


def command(*args, **kwargs):
    """Decorator for registering chat command handlers.  Arguments are passed
    through to the `Handler` constructor.

    Usage:
        @command                            # Default options
        @command(doc='/cmd: Blah blah')     # Set documentation for /cmd
    """

    # Support no-argument usage
    if len(args) == 1 and len(kwargs) == 0 and hasattr(args[0], '__call__'):
        return command()(*args)

    def decorator(f):
        h = Handler(f, *args, **kwargs)
        register_command(h)
        return h
    return decorator

def su_command(*args, **kwargs):
    if len(args) == 1 and len(kwargs) == 0 and hasattr(args[0], '__call__'):
        return su_command()(*args)

    old_cond = kwargs.get('cond')
    if old_cond is not None:
        kwargs['cond'] = lambda c: old_cond(c) and is_superuser(c)
    else:
        kwargs['cond'] = is_superuser

    return command(*args, **kwargs)


@command
def help(client, args):
    '''
    /help: Show a list of commands

    /help <command>: Show detailed info about <command>
    '''

    cmd = args.strip()
    if cmd == '':
        cmds = list_commands(client)
        client.log_msg('Commands: %s' % ', '.join(cmds))
        client.log_msg('Use "/help <command>" for more information')
        return

    if cmd.startswith('/'):
        cmd = cmd[1:]

    h = get_handler(cmd, client)
    if h is None:
        client.log_msg('Unknown command /%s' % cmd)
        return

    if h.doc is None:
        client.log_msg('No help available for /%s' % cmd)
        return

    for line in h.doc:
        client.log_msg(line)


@command
def l(client, args):
    '''/l <msg>: Send <msg> to nearby players only (local chat)'''
    assert False, '/l should be handled in native code'


@command('''
    /permit <name>: Give <name> permission to bypass your ward
    /revoke <name>: Revoke <name>'s permission to bypass your ward
''')
def permit(client, args):
    if engine.ward_permit_add(client.stable_id(), args):
        client.log_msg('Granted permission to %r' % args)
    else:
        client.log_msg('%r already has permission' % args)

@command(permit.doc)
def revoke(client, args):
    if engine.ward_permit_remove(client.stable_id(), args):
        client.log_msg('Revoked permission from %r' % args)
    else:
        client.log_msg('No permission to revoke from %r')


def parse_coords(s):
    xyz = s.split(',')
    if len(xyz) != 3:
        raise ValueError('Expected 3 coordinates, got %d (%r)' % (len(xyz), s))
    x, y, z = xyz
    return V3(int(x), int(y), int(z))

@su_command('''
        /tp [<who>] <where>: Teleport someone (default: you) to a location
        Options for <where>:
        - `[<plane_id>:]<x>,<y>,<z>`: absolute location, optionally on a specific plane
        - `+<x>,<y>,<z>`: offset from the current location
        - `<player_name>`: a player's current location
        Player names containing spaces should be quoted.
''')
def tp(client, args):
    args = shlex.split(args)

    def parse_args(args):
        if len(args) == 1:
            return client.pawn(), args[0]
        elif len(args) == 2:
            # by_name raises an exception on failure
            who = engine.Client.by_name(args[0])
            return who.pawn(), args[1]
        else:
            raise ValueError('Expected 1 or 2 arguments, got %d' % len(args))

    try:
        e, dest_str = parse_args(args)

        plane_id = None
        if ',' in dest_str:
            dest = parse_coords(dest_str)
        elif dest_str.startswith('+'):
            dest = e.pos() + parse_coords(dest_str[1:])
        elif ':' in dest_str:
            plane_str, _, pos_str = dest_str.partition(':')
            plane_id = StablePlaneId(int(plane_str))
            dest = parse_coords(pos_str)
        else:
            dest_client = engine.Client.by_name(dest_str)
            dest = dest_client.pawn().pos()
            plane_id = dest_client.pawn().plane().stable_id()

        if plane_id is None:
            e.teleport(dest)
            client.log_msg('Teleported %r to %s' % (e.controller().name(), dest))
        else:
            e.teleport_plane(plane_id, dest)
            client.log_msg('Teleported %r to %s on plane %d' %
                    (e.controller().name(), dest, plane_id.raw))

    except Exception as e:
        client.log_msg('Error: %r' % e)
