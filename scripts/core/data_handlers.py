def no_op(*args, **kwargs):
    print('no op', args, kwargs)
    pass

def return_false(*args, **kwargs):
    print('return false', args, kwargs)
    return False


class ItemHandlers:
    def __init__(self):
        self.can_use = return_false
        self.on_use = no_op

class TemplateHandlers:
    def __init__(self):
        self.can_interact = return_false
        self.on_interact = no_op

        self.can_destroy = return_false
        self.on_destroy = no_op
