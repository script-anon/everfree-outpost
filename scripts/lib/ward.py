from _outpost_types import *
from outpost.core.data import DATA
from outpost.core import engine, logic
from outpost.lib import behavior, permission, util
from outpost.lib.util import handle

WARD_SIZE = 27
WARD_OFFSET = 13

WARD_MARGIN = 12

def register_ward(name, item_name):
    struct = DATA.template(name)
    item = DATA.item(item_name)

    def can_create(e, pos):
        if not e.plane().has_ward_map():
            e.log_msg('You cannot place wards in this part of the world.')
            return False

        if e.controller().name().startswith('Anon:'):
            e.log_msg('Guest users cannot place wards.  Register an account first.')
            return False

        owner = e.controller().stable_id()
        if e.plane().has_ward(owner):
            cur_region = e.plane().ward_region(owner)
            cur_pos = (cur_region.min + cur_region.max) / 2
            where = util.vector_desc(pos.reduce() - cur_pos)
            e.log_msg('You may only have one ward at a time.  '
                'Your current ward is %s.' % where)
            return False

        margin_region_min = pos - WARD_OFFSET - WARD_MARGIN
        margin_region_max = margin_region_min + WARD_SIZE + 2 * WARD_MARGIN
        margin_region = Region3(margin_region_min, margin_region_max)
        blocker = permission.get_blocker(e, margin_region, 'ward_overlap')
        if blocker is not None:
            e.log_msg('This area is too close to land belonging to %s.' % blocker)
            return False

        return True

    def on_create(e, s):
        owner = e.controller().stable_id()
        s['owner'] = owner
        r_min = s.pos().reduce() - WARD_OFFSET
        r_max = r_min + WARD_SIZE
        region = Region2(r_min, r_max)
        e.plane().add_ward(region, owner, e.controller().name())


    def can_destroy(e, s):
        if e.controller().stable_id() != s['owner']:
            owner_name = s.plane().ward_owner_name(owner)
            e.log_msg('Only the owner (%s) can destroy this ward.' % owner_name)
            return False

        return True

    def on_destroy(e, s):
        s.plane().remove_ward(s['owner'])


    @handle(struct.handlers, 'can_interact')
    def struct_can_interact(s, e):
        return True

    @handle(struct.handlers, 'on_interact')
    def struct_on_interact(s, e):
        owner = s['owner']
        if owner == e.controller().stable_id():
            permit_list = engine.ward_permit_list(owner)
            permit_list.sort()
            permits = ', '.join(permit_list) if permit_list else '(nobody)'
            e.log_msg('You own this ward.  Permits: %s' % permits)
        else:
            owner_name = s.plane().ward_owner_name(owner)
            has_perm = engine.ward_permit_granted(owner, e.controller().name())

            e.log_msg('This ward belongs to %s%s.' % (owner_name,
                ', but you have permission to build' if has_perm else ''))


    behavior.item_use_place(item_name, name,
            can_create=can_create, on_create=on_create)
    behavior.structure_destroy_take(name, item_name,
            can_destroy=can_destroy, on_destroy=on_destroy)
