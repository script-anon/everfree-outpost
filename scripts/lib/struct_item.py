from outpost.core.data import DATA
from outpost.core import logic
from outpost.lib import behavior
from outpost.lib.util import handle


def register_basic(name, item_name=None):
    '''Register create and destroy handlers for a basic structure/item.'''
    if item_name is None:
        item_name = name
    behavior.item_use_place(item_name, name)
    behavior.structure_destroy_take(name, item_name)

def register_container(name, item_name=None, size=None):
    '''Register create, destroy, and interact handlers for a container.'''
    if item_name is None:
        item_name = name
    assert isinstance(size, int) and size > 0, 'size must be a positive integer'

    def on_create(e, s):
        i = s.create_child_inventory(size)
        i.set_public(True)
        s.set_contents(i)

    def can_destroy(e, s):
        return s.contents().is_empty()

    behavior.item_use_place(item_name, name, on_create=on_create)
    behavior.structure_destroy_take(name, item_name, can_destroy=can_destroy)
    behavior.structure_interact_container(name)

def register_container_loot(name, item_name=None):
    '''Register destroy and interact handlers for a loot container, which marks
    its containing vault as "looted" when opened.'''
    if item_name is None:
        item_name = name

    def can_destroy(e, s):
        return s.contents().is_empty()

    behavior.structure_destroy_take(name, item_name, can_destroy=can_destroy)
    behavior.structure_interact_container_loot(name)

def register_crafting(name, item_name=None, size=6, classes=None):
    '''Register create, destroy, and interact handlers for a crafting station.'''
    if item_name is None:
        item_name = name
    assert isinstance(size, int) and size > 0, 'size must be a positive integer'

    def on_create(e, s):
        i = s.create_child_inventory(size)
        i.set_public(True)
        s.set_contents(i)

    def can_destroy(e, s):
        return s.contents().is_empty()

    behavior.item_use_place(item_name, name, on_create=on_create)
    behavior.structure_destroy_take(name, item_name, can_destroy=can_destroy)
    behavior.structure_interact_crafting(name, classes)

def register_crafting_celestial(name, item_name=None, classes=None):
    '''Register create, destroy, and interact handlers for a crafting station.'''
    if item_name is None:
        item_name = name

    behavior.item_use_place(item_name, name)
    behavior.structure_destroy_take(name, item_name)
    behavior.structure_interact_crafting_celestial(name, classes)
