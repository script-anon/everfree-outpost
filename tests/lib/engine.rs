use server_types::*;
use std::fs::File;
use std::io::Read;
use std::mem;
use std::path::Path;
use engine::chat;
use engine::Engine;
use engine::component::char_invs::CharInvs;
use engine::ext::{Ext, FullWorld};
use engine::lifecycle;
use engine::update::{Delta, UpdateItem, flags};
use engine::update::delta::DeltaInternals;
use server_config::{data, data_tls, Data};
use world::World;
use world::objects::*;


pub enum Event {
    Update(Delta),
    RequestClient(u32, String),
    RequestPlane(Stable<PlaneId>),
    RequestTerrainChunk(Stable<PlaneId>, V2),
}

pub struct EventTracker {
    events: Vec<Event>,
}

impl EventTracker {
    fn take_events(&mut self) -> Vec<Event> {
        mem::replace(&mut self.events, Vec::new())
    }
}

impl Ext for EventTracker {
    fn handle_update(&mut self, w: &FullWorld, u: Delta) {
        self.events.push(Event::Update(u));
    }

    fn request_client_bundle(&mut self, uid: u32, name: &str) {
        self.events.push(Event::RequestClient(uid, name.to_owned()))
    }
    fn request_plane_bundle(&mut self, stable_id: Stable<PlaneId>) {
        self.events.push(Event::RequestPlane(stable_id))
    }
    fn request_terrain_chunk_bundle(&mut self, plane: Stable<PlaneId>, cpos: V2) {
        self.events.push(Event::RequestTerrainChunk(plane, cpos))
    }

    fn client_world_info(&mut self,
                         id: ClientId,
                         now: Time,
                         day_night_cycle_ms: u32) {}
    fn client_readiness(&mut self, id: ClientId, readiness: bool) {}

    fn client_loaded(&mut self, uid: u32, cid: ClientId) {}
    fn client_unloaded(&mut self, uid: u32) {}

    fn kick_user(&mut self, uid: u32, reason: &str) {}

    fn chat_message(&mut self, channel: chat::Channel, name: &str, text: &str) {}

    fn ack_request(&mut self, cid: ClientId, apply_time: Time) {}
    fn nak_request(&mut self, cid: ClientId) {}
}

pub fn init_data(full: bool) {
    let mut buf = Vec::new();
    let path = if full { test_paths::FULL_SERVER_DATA } else { test_paths::TEST_SERVER_DATA };
    File::open(path).unwrap()
        .read_to_end(&mut buf).unwrap();
    let d = Box::new(Data::new(buf.into_boxed_slice()));
    unsafe { data_tls::set_data(&d as &Data as *const Data) };
    mem::forget(d);
}

pub fn init_engine(full: bool) -> Engine<EventTracker> {
    init_data(full);
    // TODO: turns out multithreaded scripting doesn't actually work yet
    //unsafe { engine::enable_multithreading() };
    let path = if full { test_paths::FULL_BOOT_PY } else { test_paths::TEST_BOOT_PY };
    let mut eng = Engine::new(Path::new(path),
                              Delta::new(),
                              EventTracker { events: Vec::new() });

    eng
}

pub trait BundleProvider {
    fn provide_client(&mut self, uid: u32, name: &str) -> DeltaInternals;
    fn provide_plane(&mut self, stable_id: Stable<PlaneId>) -> DeltaInternals;
    fn provide_terrain_chunk(&mut self, stable_plane: Stable<PlaneId>, cpos: V2) -> DeltaInternals;
}

pub struct DefaultBundleProvider;

pub const CID0: ClientId = ClientId(0);
pub const EID0: EntityId = EntityId(0);
pub const TCID0: TerrainChunkId = TerrainChunkId(0);
pub const PID0: PlaneId = PlaneId(0);

impl BundleProvider for DefaultBundleProvider {
    fn provide_client(&mut self, uid: u32, name: &str) -> DeltaInternals {
        let mut d = DeltaInternals::new();

        d.flags.client_mut(CID0).insert(flags::C_CREATED);
        d.new.insert_client(CID0, Client {
            pawn: Some(EID0),
            .. Client::new(uid, &name)
        });

        d.flags.entity_mut(EID0).insert(flags::E_CREATED | flags::E_CHAR_INVS);
        d.new.insert_entity(EID0, Entity {
            attachment: EntityAttachment::Client(CID0),
            .. Entity::new(PID0, scalar(0), 0, Appearance::default())
        });
        d.new.insert_entity_char_invs(EID0, Some(CharInvs {
            main: InventoryId(0),
            ability: InventoryId(1),
            equip: InventoryId(2),
        }));

        for i in 0 .. 3 {
            d.flags.inventory_mut(InventoryId(i)).insert(flags::I_CREATED);
            d.new.insert_inventory(InventoryId(i), Inventory {
                attachment: InventoryAttachment::Entity(EID0),
                .. Inventory::new(30)
            });
        }

        d.new.insert_plane_ref(PID0, STABLE_PLANE_FOREST);

        d
    }

    fn provide_plane(&mut self, stable_id: Stable<PlaneId>) -> DeltaInternals {
        let mut d = DeltaInternals::new();
        d.flags.plane_mut(PID0).insert(flags::P_CREATED);
        d.new.insert_plane(PID0, Plane {
            stable_id: stable_id.unwrap(),
            .. Plane::new()
        });
        d
    }

    fn provide_terrain_chunk(&mut self, stable_plane: Stable<PlaneId>, cpos: V2) -> DeltaInternals {
        let data = data();
        let ground = data.get_block_id("ground")
            .or_else(|| data.get_block_id("grass"))
            .expect("couldn't find ground or grass block");
        let mut blocks = Box::new(EMPTY_CHUNK);
        for i in 0 .. 16 * 16 {
            blocks[i] = ground;
        }

        let mut d = DeltaInternals::new();
        d.flags.terrain_chunk_mut(TCID0).insert(flags::TC_CREATED);
        d.new.insert_terrain_chunk(TCID0, TerrainChunk {
            blocks,
            .. TerrainChunk::new(PID0, cpos)
        });
        d.new.insert_plane_ref(PID0, stable_plane);
        d
    }
}

/// Run `on_tick`, automatically provide any required bundles, and return the tick `Delta`.
pub fn tick_delta(eng: &mut Engine<EventTracker>,
                  provider: &mut impl BundleProvider) -> Delta {
    engine::tick::on_tick(eng);

    let mut delta = None;
    for e in eng.ext.take_events() {
        match e {
            Event::Update(d) => {
                assert!(delta.is_none());
                delta = Some(d);
            },
            Event::RequestClient(uid, name) => {
                let delta = provider.provide_client(uid, &name).into();
                lifecycle::start_import(eng, delta);
            },
            Event::RequestPlane(stable_id) => {
                let delta = provider.provide_plane(stable_id).into();
                lifecycle::start_import(eng, delta);
            },
            Event::RequestTerrainChunk(stable_plane, cpos) => {
                let delta = provider.provide_terrain_chunk(stable_plane, cpos).into();
                lifecycle::start_import(eng, delta);
            },
        }
    }

    delta.unwrap_or_else(Delta::new)
}

