extern crate common;

use common::types::*;
use common::physics::collide_2d::{self, ShapeSource, MoveResult};


struct TileShapeSource {
    tiles: [u8; 16],
}

impl ShapeSource for TileShapeSource {
    fn can_pass(&mut self, _start: V2, end: V2) -> bool {
        eprintln!("check pass: {:?} -> {:?}", _start, end);
        let bounds = Region2::sized(4);
        if !bounds.contains(end) {
            return false;
        }
        let tile = self.tiles[bounds.index(end)];
        tile == b'.'
    }
}


const SIZE: V2 = V2 { x: TILE_SIZE, y: TILE_SIZE };

#[test]
fn unobstructed() {
    // Entity moves around an unobstructed area.
    let mut tss = TileShapeSource {
        tiles: *b"....\
                  ....\
                  ....\
                  ....",
    };

    for &y in [-1, 0, 1].iter() {
        for &x in [-1, 0, 1].iter() {
            let dir = V2::new(x, y);
            let mr = collide_2d::try_move(TILE_SIZE.into(), SIZE, dir, &mut tss);
            if x != 0 || y != 0 {
                assert_eq!(mr, MoveResult::Steps(dir, TILE_SIZE as u8));
            } else {
                assert_eq!(mr, MoveResult::Blocked);
            }
        }
    }
}

#[test]
fn unobstructed_midtile() {
    // Entity walks around an unobstructed region, starting from a mid-tile position
    let mut tss = TileShapeSource {
        tiles: *b"....\
                  ....\
                  ....\
                  ....",
    };

    for &y in [-1, 0, 1].iter() {
        for &x in [-1, 0, 1].iter() {
            let dir = V2::new(x, y);
            let mr = collide_2d::try_move((TILE_SIZE * 3 / 2).into(), SIZE, dir, &mut tss);
            if x != 0 || y != 0 {
                assert_eq!(mr, MoveResult::Steps(dir, TILE_SIZE as u8 / 2));
            } else {
                assert_eq!(mr, MoveResult::Blocked);
            }
        }
    }
}

#[test]
fn unobstructed_midtile2() {
    // Entity walks around an unobstructed region, starting from a mid-tile position
    let mut tss = TileShapeSource {
        tiles: *b"....\
                  ....\
                  ....\
                  ....",
    };

    // This starting position means X and Y movement hits tile boundaries at different times.
    let pos = V2::new(TILE_SIZE + 5, TILE_SIZE + 7);
    for &y in [-1, 0, 1].iter() {
        for &x in [-1, 0, 1].iter() {
            let dir = V2::new(x, y);
            let mr = collide_2d::try_move(pos, SIZE, dir, &mut tss);
            if (x, y) == (0, 0) {
                assert_eq!(mr, MoveResult::Blocked);
            } else {
                match mr {
                    MoveResult::Steps(d, s) => {
                        assert_eq!(d, dir);
                        let end_pos = pos + d * s as i32;
                        assert!((end_pos % TILE_SIZE).is_zero().max() == 1);
                    },
                    mr => panic!("expected Steps, got {:?}", mr),
                }
            }
        }
    }
}

#[test]
fn wall() {
    // Entity walks into a wall at various angles
    let mut tss = TileShapeSource {
        tiles: *b"..#.\
                  ..#.\
                  ..#.\
                  ..#.",
    };

    for &y in [-1, 0, 1].iter() {
        for &x in [-1, 0, 1].iter() {
            let dir = V2::new(x, y);
            let mr = collide_2d::try_move(TILE_SIZE.into(), SIZE, dir, &mut tss);

            if (x, y) == (1, 0) || (x, y) == (0, 0) {
                assert_eq!(mr, MoveResult::Blocked);
            } else if x == 1 {
                assert_eq!(mr, MoveResult::Steps(V2::new(0, y), TILE_SIZE as u8));
            } else {
                assert_eq!(mr, MoveResult::Steps(dir, TILE_SIZE as u8));
            }
        }
    }
}

#[test]
fn wall_midtile() {
    // Entity walks into a wall from a mid-tile position.
    let mut tss = TileShapeSource {
        tiles: *b"...#\
                  ...#\
                  ...#\
                  ...#",
    };

    // Starting position lets us also test hitting obstacles on one axis while the other could keep
    // moving.
    let pos = V2::new(TILE_SIZE * 3 / 2, TILE_SIZE);

    for &y in [-1, 0, 1].iter() {
        let dir = V2::new(1, y);
        let mr = collide_2d::try_move(pos, SIZE, dir, &mut tss);

        assert_eq!(mr, MoveResult::Steps(dir, TILE_SIZE as u8 / 2));
    }
}

#[test]
fn wall_clip() {
    // Entity moves around while already inside a wall.
    let mut tss = TileShapeSource {
        tiles: *b".#..\
                  .#..\
                  .#..\
                  .#..",
    };

    for &y in [-1, 0, 1].iter() {
        for &x in [-1, 0, 1].iter() {
            let dir = V2::new(x, y);
            let mr = collide_2d::try_move(TILE_SIZE.into(), SIZE, dir, &mut tss);

            if x == 0 {
                assert_eq!(mr, MoveResult::Blocked);
            } else {
                // Should be able to move horizontally, just not vertically.
                assert_eq!(mr, MoveResult::Steps(V2::new(x, 0), TILE_SIZE as u8));
            }
        }
    }
}

#[test]
fn wall_clip_midtile() {
    // Entity moves around while already inside a wall, starting at a mid-tile position.  The
    // entity should be able to move freely, up to the boundaries of the adjacent walls.
    let mut tss = TileShapeSource {
        tiles: *b".#..\
                  .#..\
                  .#..\
                  .#..",
    };

    let pos = V2::new(TILE_SIZE, TILE_SIZE * 3 / 2);

    for &y in [-1, 0, 1].iter() {
        for &x in [-1, 0, 1].iter() {
            let dir = V2::new(x, y);
            let mr = collide_2d::try_move(pos, SIZE, dir, &mut tss);

            if (x, y) == (0, 0) {
                assert_eq!(mr, MoveResult::Blocked);
            } else if y == 0 {
                // Moving directly left/right is unrestricted.
                assert_eq!(mr, MoveResult::Steps(dir, TILE_SIZE as u8));
            } else {
                // Moving up/down, the entity will hit the adjacent wall halfway through the tile.
                assert_eq!(mr, MoveResult::Steps(dir, TILE_SIZE as u8 / 2));
            }
        }
    }
}

#[test]
fn cross_clip() {
    // Tricky case that came up at one point while designing the algorithm.  Entity is clipped
    // through four different walls, each requiring a different movement direction.  In the current
    // design, we actually ignore all clipped walls, so the entity should be able to move freely.
    let mut tss = TileShapeSource {
        tiles: *b"....\
                  .#..\
                  ..#.\
                  ....",
    };

    let pos = V2::new(TILE_SIZE * 3 / 2, TILE_SIZE * 3 / 2);

    for &y in [-1, 0, 1].iter() {
        for &x in [-1, 0, 1].iter() {
            let dir = V2::new(x, y);
            let mr = collide_2d::try_move(pos, SIZE, dir, &mut tss);

            if (x, y) == (0, 0) {
                assert_eq!(mr, MoveResult::Blocked);
            } else {
                assert_eq!(mr, MoveResult::Steps(dir, TILE_SIZE as u8 / 2));
            }
        }
    }
}

#[test]
fn corner_check() {
    // Moving directly into a corner should be blocked.  This is a special case in the
    // implementation.
    let mut tss = TileShapeSource {
        tiles: *b"#.#.\
                  ....\
                  #.#.\
                  ....",
    };

    for &y in [-1, 0, 1].iter() {
        for &x in [-1, 0, 1].iter() {
            let dir = V2::new(x, y);
            let mr = collide_2d::try_move(TILE_SIZE.into(), SIZE, dir, &mut tss);

            if (x, y) == (0, 0) || (x != 0 && y != 0) {
                // Not moving, or moving directly into a corner.
                assert_eq!(mr, MoveResult::Blocked);
            } else {
                // Moving strictly orthogonally is okay.
                assert_eq!(mr, MoveResult::Steps(dir, TILE_SIZE as u8));
            }
        }
    }
}

#[test]
fn corner_assist() {
    // Moving directly into a corner should be blocked.  This is a special case in the
    // implementation.
    let mut tss = TileShapeSource {
        tiles: *b"#.#.\
                  ....\
                  #.#.\
                  ....",
    };

    {
        let pos = V2::new(TILE_SIZE, TILE_SIZE - 2);

        // Sliding up
        let mr = collide_2d::try_move(pos, SIZE, V2::new(1, -1), &mut tss);
        assert_eq!(mr, MoveResult::Steps(V2::new(0, -1), TILE_SIZE as u8 - 2));
        // Sliding down
        let mr = collide_2d::try_move(pos, SIZE, V2::new(1, 1), &mut tss);
        assert_eq!(mr, MoveResult::Steps(V2::new(0, 1), 2));
        // Corner-assist down (same as sliding down)
        let mr = collide_2d::try_move(pos, SIZE, V2::new(1, 0), &mut tss);
        assert_eq!(mr, MoveResult::Steps(V2::new(0, 1), 2));

        // Sliding up
        let mr = collide_2d::try_move(pos, SIZE, V2::new(-1, -1), &mut tss);
        assert_eq!(mr, MoveResult::Steps(V2::new(0, -1), TILE_SIZE as u8 - 2));
        // Sliding down
        let mr = collide_2d::try_move(pos, SIZE, V2::new(-1, 1), &mut tss);
        assert_eq!(mr, MoveResult::Steps(V2::new(0, 1), 2));
        // Corner-assist down (same as sliding down)
        let mr = collide_2d::try_move(pos, SIZE, V2::new(-1, 0), &mut tss);
        assert_eq!(mr, MoveResult::Steps(V2::new(0, 1), 2));
    }

    {
        let pos = V2::new(TILE_SIZE - 2, TILE_SIZE);

        // Sliding left
        let mr = collide_2d::try_move(pos, SIZE, V2::new(-1, 1), &mut tss);
        assert_eq!(mr, MoveResult::Steps(V2::new(-1, 0), TILE_SIZE as u8 - 2));
        // Sliding right
        let mr = collide_2d::try_move(pos, SIZE, V2::new(1, 1), &mut tss);
        assert_eq!(mr, MoveResult::Steps(V2::new(1, 0), 2));
        // Corner-assist right (same as sliding right)
        let mr = collide_2d::try_move(pos, SIZE, V2::new(0, 1), &mut tss);
        assert_eq!(mr, MoveResult::Steps(V2::new(1, 0), 2));

        // Sliding left
        let mr = collide_2d::try_move(pos, SIZE, V2::new(-1, -1), &mut tss);
        assert_eq!(mr, MoveResult::Steps(V2::new(-1, 0), TILE_SIZE as u8 - 2));
        // Sliding right
        let mr = collide_2d::try_move(pos, SIZE, V2::new(1, -1), &mut tss);
        assert_eq!(mr, MoveResult::Steps(V2::new(1, 0), 2));
        // Corner-assist right (same as sliding right)
        let mr = collide_2d::try_move(pos, SIZE, V2::new(0, -1), &mut tss);
        assert_eq!(mr, MoveResult::Steps(V2::new(1, 0), 2));
    }
}

// TODO: test exotic ShapeSources with partial walls
