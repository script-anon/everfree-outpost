extern crate common;

use common::util::SmallSet;

fn insert_remove_n(n: u32) {
    let mut v: SmallSet<u32> = SmallSet::new();
    for i in 0 .. n {
        v.insert(i);
    }
    assert_eq!(v.len(), n as usize);
    for i in 0 .. n {
        v.insert(i);
    }
    assert_eq!(v.len(), n as usize);
    for i in 0 .. n {
        assert!(v.contains(&i));
    }
    for i in 0 .. n {
        v.remove(&i);
    }
    assert_eq!(v.len(), 0);
}

#[test]
fn insert_remove() {
    for n in 1 .. 11 {
        insert_remove_n(n);
    }
}
