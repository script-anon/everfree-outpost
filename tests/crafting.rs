extern crate env_logger;
#[macro_use] extern crate log;

extern crate common;
extern crate engine;
extern crate server_bundle;
extern crate server_config;
extern crate server_types;
extern crate test_paths;
extern crate world;

use std::collections::VecDeque;
use std::fs::File;
use std::io::Read;
use std::mem;
use std::path::Path;

use server_types::*;
use engine::Engine;
use engine::chat;
use engine::component::crafting::StationId;
use engine::ext::{Ext, FullWorld};
use engine::lifecycle;
use engine::update::{Update, UpdateItem, flags};
use engine::update::delta::{Delta, DeltaInternals};
use server_bundle::builder::Builder;
use server_config::{data, data_tls, Data};
use server_types::Stable;
use world::World;
use world::objects::*;


struct InvUpdateTracker {
    updates: VecDeque<(Time, Vec<Item>)>,
    imports: Vec<Delta>,
}

fn describe_items(items: &[Item]) -> String {
    let mut desc = String::new();
    for (i, item) in items.iter().enumerate() {
        if i > 0 {
            desc.push_str(", ");
        }
        let name = data().item(item.id).name();
        desc.push_str(&format!("{} {}", item.count, name));
    }
    desc
}

impl InvUpdateTracker {
    pub fn ignore_inv_update(&mut self) {
        if self.updates.len() == 0 {
            panic!("expected ignorable update, but found no more events");
        }

        self.updates.pop_front();
    }

    pub fn expect_inv_update(&mut self, time: Time, items: Vec<(&'static str, u8)>) {
        let mut desc = String::new();
        for (i, &(name, count)) in items.iter().enumerate() {
            if i > 0 {
                desc.push_str(", ");
            }
            desc.push_str(&format!("{} {}", count, name));
        }

        if self.updates.len() == 0 {
            panic!("expected {} at {}, but found no more events", desc, time);
        }

        let (u_time, u_items) = self.updates.pop_front().unwrap();
        let mut u_desc = describe_items(&u_items);
        let mut ok = true;
        for (&(n, c), u_item) in items.iter().zip(u_items.iter()) {
            let u_name = data().item(u_item.id).name();
            if n != u_name || c != u_item.count {
                ok = false;
            }
        }

        if u_time != time {
            panic!("expected {} at {}, but saw an event at {} instead ({})",
                   desc, time, u_time, u_desc);
        }

        if !ok {
            panic!("expected {} (at {}), but saw {} instead",
                   desc, time, u_desc);
        }
    }

    pub fn expect_end_of_inv_updates(&self) {
        if self.updates.len() > 0 {
            let time = self.updates.front().unwrap().0;
            panic!("expected end of updates, but got {} more starting at {}",
                   self.updates.len(), time);
        }
    }

    pub fn dump_inv_updates(&self) {
        info!("dumping {} inv updates:", self.updates.len());
        for &(time, ref items) in &self.updates {
            info!("  {}: {}", time, describe_items(items));
        }
    }

    pub fn process_imports(eng: &mut Engine<Self>) {
        let imports = mem::replace(&mut eng.ext.imports, Vec::new());
        for d in imports {
            lifecycle::start_import(eng, d);
        }
    }
}

impl Ext for InvUpdateTracker {
    fn handle_update(&mut self, w: &FullWorld, u: Update) {
        for it in u.items() {
            match it {
                UpdateItem::Inventory(id, f) => {
                    if f.contains(flags::I_CONTENTS) {
                        self.updates.push_back((w.now, w.inventory(id).contents().to_owned()));
                    }
                },

                _ => {},
            }
        }
    }

    fn request_client_bundle(&mut self, uid: u32, name: &str) {
        let mut d = DeltaInternals::new();

        d.flags.client_mut(CID).insert(flags::C_CREATED);
        d.new.insert_client(CID, Client {
            pawn: Some(EID),
            .. Client::new(uid, &name)
        });

        d.flags.entity_mut(EID).insert(flags::E_CREATED);
        d.new.insert_entity(EID, Entity {
            attachment: EntityAttachment::Client(CID),
            .. Entity::new(PID, scalar(0), 0, Appearance::default())
        });

        for i in 0 .. 3 {
            d.flags.inventory_mut(InventoryId(i)).insert(flags::I_CREATED);
            d.new.insert_inventory(InventoryId(i), Inventory {
                attachment: InventoryAttachment::Entity(EID),
                .. Inventory::new(30)
            });
        }

        d.new.insert_plane_ref(PID, STABLE_PLANE_FOREST);

        self.imports.push(d.into());
    }

    fn request_plane_bundle(&mut self, stable_id: Stable<PlaneId>) {
        let mut d = DeltaInternals::new();
        d.flags.plane_mut(PID).insert(flags::P_CREATED);
        d.new.insert_plane(PID, Plane {
            stable_id: stable_id.unwrap(),
            .. Plane::new()
        });
        self.imports.push(d.into());
    }

    fn request_terrain_chunk_bundle(&mut self, stable_plane: Stable<PlaneId>, cpos: V2) {
        let ground = data().block_id("ground");
        let mut blocks = Box::new(EMPTY_CHUNK);
        for i in 0 .. 16 * 16 {
            blocks[i] = ground;
        }

        let mut d = DeltaInternals::new();
        d.flags.terrain_chunk_mut(TCID).insert(flags::TC_CREATED);
        d.new.insert_terrain_chunk(TCID, TerrainChunk {
            blocks,
            .. TerrainChunk::new(PID, cpos)
        });
        d.new.insert_plane_ref(PID, stable_plane);
        self.imports.push(d.into());
    }

    fn client_world_info(&mut self,
                         id: ClientId,
                         now: Time,
                         day_night_cycle_ms: u32) {}
    fn client_readiness(&mut self, id: ClientId, readiness: bool) {}

    fn client_loaded(&mut self, uid: u32, cid: ClientId) {}
    fn client_unloaded(&mut self, uid: u32) {}

    fn kick_user(&mut self, uid: u32, reason: &str) {}

    fn chat_message(&mut self, channel: chat::Channel, name: &str, text: &str) {}

    fn ack_request(&mut self, cid: ClientId, apply_time: Time) {}
    fn nak_request(&mut self, cid: ClientId) {}
}

const CID: ClientId = ClientId(0);
const EID: EntityId = EntityId(0);
const TCID: TerrainChunkId = TerrainChunkId(0);
const PID: PlaneId = PlaneId(0);
const P_SID: Stable<PlaneId> = STABLE_PLANE_FOREST;

const CRAFTING_IID: InventoryId = InventoryId(3);

fn init_data() {
    let mut buf = Vec::new();
    File::open(test_paths::TEST_SERVER_DATA).unwrap()
        .read_to_end(&mut buf).unwrap();
    let d = Box::new(Data::new(buf.into_boxed_slice()));
    unsafe { data_tls::set_data(&d as &Data as *const Data) };
    mem::forget(d);
}

fn init_engine() -> Engine<InvUpdateTracker> {
    // TODO: turns out multithreaded scripting doesn't actually work yet
    //unsafe { engine::enable_multithreading() };
    init_data();
    let mut eng = Engine::new(Path::new(test_paths::TEST_BOOT_PY),
                              Update::new(),
                              InvUpdateTracker {
                                  updates: VecDeque::new(),
                                  imports: Vec::new(),
                              });

    lifecycle::user_connect(&mut eng, 1, "Me".to_owned());
    for _ in 0 .. 3 {
        engine::tick::on_tick(&mut eng);
        InvUpdateTracker::process_imports(&mut eng);
    }
    eng.now = 0;

    let pid = eng.w.transient_plane_id(STABLE_PLANE_FOREST).unwrap();
    assert_eq!(pid, PlaneId(0));

    let template = data().template_id("box");
    let sid = eng.w.create_structure(Structure::new(pid, scalar(0), template)).id();
    assert_eq!(sid, StructureId(0));

    let iid = eng.w.create_inventory(Inventory::new(3)).id();
    assert_eq!(iid, CRAFTING_IID);

    eng
}

fn run_engine(eng: &mut Engine<InvUpdateTracker>, until: Time) {
    while eng.now < until {
        engine::tick::on_tick(eng);
        InvUpdateTracker::process_imports(eng);
    }
}


#[test]
fn simple_craft() {
    let _ = env_logger::init();
    let mut eng = init_engine();

    eng.inventory_mut(CRAFTING_IID).modify_contents(|contents| {
        contents[0] = Item::new(data().item_id("item1"), 3);
    });

    engine::component::crafting::start(&mut eng,
                                       StationId::Structure(StructureId(0)),
                                       CRAFTING_IID,
                                       data().recipe_id("recipe_1x1_1x2"),
                                       3);
    run_engine(&mut eng, 2500);

    eng.ext.ignore_inv_update();    // inventory init
    eng.ext.expect_inv_update(1024, vec![("item1", 2), ("item2", 1), ("none", 0)]);
    eng.ext.expect_inv_update(2016, vec![("item1", 1), ("item2", 2), ("none", 0)]);
    eng.ext.expect_end_of_inv_updates();
}

#[test]
fn stop_count() {
    let _ = env_logger::init();
    let mut eng = init_engine();

    eng.inventory_mut(CRAFTING_IID).modify_contents(|contents| {
        contents[0] = Item::new(data().item_id("item1"), 3);
    });

    engine::component::crafting::start(&mut eng,
                                       StationId::Structure(StructureId(0)),
                                       CRAFTING_IID,
                                       data().recipe_id("recipe_1x1_1x2"),
                                       2);
    run_engine(&mut eng, 5000);

    eng.ext.ignore_inv_update();    // inventory init
    eng.ext.expect_inv_update(1024, vec![("item1", 2), ("item2", 1), ("none", 0)]);
    eng.ext.expect_inv_update(2016, vec![("item1", 1), ("item2", 2), ("none", 0)]);
    eng.ext.expect_end_of_inv_updates();
}

#[test]
fn stop_inputs() {
    let _ = env_logger::init();
    let mut eng = init_engine();


    eng.inventory_mut(CRAFTING_IID).modify_contents(|contents| {
        contents[0] = Item::new(data().item_id("item1"), 2);
    });

    engine::component::crafting::start(&mut eng,
                                       StationId::Structure(StructureId(0)),
                                       CRAFTING_IID,
                                       data().recipe_id("recipe_1x1_1x2"),
                                       3);
    run_engine(&mut eng, 3000);

    eng.ext.ignore_inv_update();    // inventory init
    eng.ext.dump_inv_updates();
    eng.ext.expect_inv_update(1024, vec![("item1", 1), ("item2", 1), ("none", 0)]);
    eng.ext.expect_inv_update(2016, vec![("none", 0), ("item2", 2), ("none", 0)]);


    eng.inventory_mut(CRAFTING_IID).modify_contents(|contents| {
        contents[0] = Item::new(data().item_id("item1"), 1);
    });

    engine::component::crafting::update_state(&mut eng, StationId::Structure(StructureId(0)));
    run_engine(&mut eng, 5000);

    eng.ext.ignore_inv_update();    // direct inventory modification
    eng.ext.expect_inv_update(4032, vec![("none", 0), ("item2", 3), ("none", 0)]);
    eng.ext.expect_end_of_inv_updates();
}

#[test]
fn stop_outputs() {
    let _ = env_logger::init();
    let mut eng = init_engine();


    eng.inventory_mut(CRAFTING_IID).modify_contents(|contents| {
        contents[0] = Item::new(data().item_id("item1"), 2);
        contents[1] = Item::new(data().item_id("item3"), 1);
        contents[2] = Item::new(data().item_id("item3"), 1);
    });

    engine::component::crafting::start(&mut eng,
                                       StationId::Structure(StructureId(0)),
                                       CRAFTING_IID,
                                       data().recipe_id("recipe_1x1_1x2"),
                                       3);
    run_engine(&mut eng, 3000);
    eng.ext.ignore_inv_update();    // inventory init
    eng.ext.expect_end_of_inv_updates();

    eng.inventory_mut(CRAFTING_IID).modify_contents(|contents| {
        contents[1] = Item::none();
    });

    engine::component::crafting::update_state(&mut eng, StationId::Structure(StructureId(0)));
    run_engine(&mut eng, 4000);

    // No `ignore_inv_update()` here.  The recipe execution gets merged with the explicit inventory
    // update because both occur in the same tick.
    eng.ext.dump_inv_updates();
    eng.ext.expect_inv_update(3008, vec![("item1", 1), ("item2", 1), ("item3", 1)]);
    eng.ext.expect_end_of_inv_updates();
}
