extern crate env_logger;
#[macro_use] extern crate log;

extern crate common;
extern crate engine;
extern crate server_config;
extern crate server_types;
extern crate test_paths;
extern crate world;

use server_types::*;
use engine::Engine;
use engine::component::ward_map::WardMap;
use engine::input;
use engine::lifecycle;
use engine::logic;
use engine::update::{Delta, UpdateItem, flags};
use engine::update::delta::DeltaInternals;
use server_config::data;
use server_types::Stable;
use world::World;
use world::objects::*;


// TODO: make this into a proper library
#[path = "lib/engine.rs"]
mod test_engine;

use self::test_engine::*;

pub fn tick_delta(eng: &mut Engine<EventTracker>) -> Delta {
    test_engine::tick_delta(eng, &mut WardBundleProvider)
}


struct WardBundleProvider;

impl BundleProvider for WardBundleProvider {
    fn provide_client(&mut self, uid: u32, name: &str) -> DeltaInternals {
        DefaultBundleProvider.provide_client(uid, name)
    }

    fn provide_plane(&mut self, stable_id: Stable<PlaneId>) -> DeltaInternals {
        let mut d = DefaultBundleProvider.provide_plane(stable_id);
        d.flags.plane_mut(PID0).insert(flags::P_WARD_MAP);
        d.new.insert_plane_ward_map(PID, Some(WardMap::new()));
        d
    }

    fn provide_terrain_chunk(&mut self, stable_plane: Stable<PlaneId>, cpos: V2) -> DeltaInternals {
        DefaultBundleProvider.provide_terrain_chunk(stable_plane, cpos)
    }
}


const CID: ClientId = ClientId(0);
const EID: EntityId = EntityId(0);
const TCID: TerrainChunkId = TerrainChunkId(0);
const PID: PlaneId = PlaneId(0);
const P_SID: Stable<PlaneId> = STABLE_PLANE_FOREST;

fn give_item(eng: &mut Engine<EventTracker>, cid: ClientId, item: ItemId, count: u16) {
    let iid = {
        let eid = eng.w.client(cid).pawn().unwrap().id();
        eng.w.char_invs.get(eid).unwrap().main
    };
    let i = eng.w.inventory_mut(iid);
    logic::inventory::add(i, item, count);
}

fn assert_structure_placed(d: &Delta, t_ward: TemplateId, pos: V3) {
    for it in d.items() {
        match it {
            UpdateItem::Structure(id, f) => {
                if !f.contains(flags::S_CREATED) {
                    continue;
                }
                if f.intersects(flags::S_LOADED | flags::S_DESTROYED) {
                    continue;
                }
                let s = d.new_structure(id);
                if s.template != t_ward || s.pos != pos {
                    continue;
                }
                return;
            },
            _ => {},
        }
    }
    panic!("no ward placed at {:?}", pos);
}

fn assert_structure_destroyed(d: &Delta, t_ward: TemplateId, pos: V3) {
    for it in d.items() {
        match it {
            UpdateItem::Structure(id, f) => {
                if !f.contains(flags::S_DESTROYED) {
                    continue;
                }
                if f.intersects(flags::S_UNLOADED | flags::S_CREATED) {
                    continue;
                }
                let s = d.new_structure(id);
                if s.template != t_ward || s.pos != pos {
                    continue;
                }
                return;
            },
            _ => {},
        }
    }
    panic!("no ward destroyed at {:?}", pos);
}

fn assert_no_structures_changed(d: &Delta) {
    for it in d.items() {
        match it {
            UpdateItem::Structure(id, f) =>
                panic!("expected no structure changes, but got {:?} {:?}", id, f),
            _ => {},
        }
    }
}


#[test]
fn wards() {
    let _ = env_logger::init();
    let mut eng = init_engine(true);


    lifecycle::user_connect(&mut eng, 1, "Me".to_owned());
    lifecycle::user_connect(&mut eng, 2, "Friend".to_owned());
    lifecycle::user_connect(&mut eng, 3, "Stranger".to_owned());

    tick_delta(&mut eng);
    tick_delta(&mut eng);
    tick_delta(&mut eng);

    let me = eng.w.transient_client_id(Stable::new(1)).unwrap();
    let friend = eng.w.transient_client_id(Stable::new(2)).unwrap();
    let stranger = eng.w.transient_client_id(Stable::new(3)).unwrap();


    let i_ward = data().item_id("ward");
    let t_ward = data().template_id("ward");

    give_item(&mut eng, me, i_ward, 2);
    give_item(&mut eng, friend, i_ward, 2);
    give_item(&mut eng, stranger, i_ward, 2);

    let i_fence = data().item_id("fence");
    let t_fence = data().template_id("fence");

    give_item(&mut eng, me, i_fence, 2);
    give_item(&mut eng, friend, i_fence, 2);
    give_item(&mut eng, stranger, i_fence, 2);


    // Place a ward
    input::on_point_use_item(&mut eng, me, V3::new(1, 0, 0), i_ward);
    let d = tick_delta(&mut eng);
    assert_structure_placed(&d, t_ward, V3::new(1, 0, 0));

    // TODO: add permit for friend


    let fence_pos = V3::new(1, 2, 0);

    // Build under ward
    input::on_point_use_item(&mut eng, me, fence_pos, i_fence);
    let d = tick_delta(&mut eng);
    assert_structure_placed(&d, t_fence, fence_pos);
    input::on_point_destroy(&mut eng, me, fence_pos);
    let d = tick_delta(&mut eng);
    assert_structure_destroyed(&d, t_fence, fence_pos);

    // Friend: build under ward
    input::on_point_use_item(&mut eng, friend, fence_pos, i_fence);
    let d = tick_delta(&mut eng);
    // TODO: this one should succeed
    //assert_structure_placed(&d, t_fence, fence_pos);
    //input::on_point_destroy(&mut eng, friend, fence_pos);
    //let d = tick_delta(&mut eng);
    //assert_structure_destroyed(&d, t_fence, fence_pos);

    // Stranger: build under ward
    input::on_point_use_item(&mut eng, stranger, fence_pos, i_ward);
    let d = tick_delta(&mut eng);
    assert_no_structures_changed(&d);


    let ward_overlap_pos = V3::new(1, 1, 0);

    // Place another ward overlapping
    input::on_point_use_item(&mut eng, me, ward_overlap_pos, i_ward);
    let d = tick_delta(&mut eng);
    assert_no_structures_changed(&d);

    // Friend: place a ward overlapping
    input::on_point_use_item(&mut eng, friend, ward_overlap_pos, i_ward);
    let d = tick_delta(&mut eng);
    assert_no_structures_changed(&d);

    // Stranger: place a ward overlapping
    input::on_point_use_item(&mut eng, stranger, ward_overlap_pos, i_ward);
    let d = tick_delta(&mut eng);
    assert_no_structures_changed(&d);


    // Ward diameter is 23 blocks, so teleport that far.
    let act = Activity::Stand {
        pos: V3::new(23 * TILE_SIZE, 0, 0),
        dir: 0,
    };
    let start = eng.w.now;
    eng.w.client_mut(me).pawn_mut().unwrap().set_activity(act.clone(), start);
    eng.w.client_mut(friend).pawn_mut().unwrap().set_activity(act.clone(), start);
    eng.w.client_mut(stranger).pawn_mut().unwrap().set_activity(act.clone(), start);

    tick_delta(&mut eng);
    tick_delta(&mut eng);
    tick_delta(&mut eng);


    // Remember, the original ward is at 1,0, not 0,0.
    let ward_adjacent_pos = V3::new(24, 1, 0);

    // Place ward adjacent
    input::on_point_use_item(&mut eng, me, ward_adjacent_pos, i_ward);
    let d = tick_delta(&mut eng);
    assert_no_structures_changed(&d);

    // Friend: place a ward adjacent
    input::on_point_use_item(&mut eng, friend, ward_adjacent_pos, i_ward);
    let d = tick_delta(&mut eng);
    // TODO: this one should succeed
    //assert_structure_placed(&d, t_ward, ward_adjacent_pos);
    //input::on_point_destroy(&mut eng, friend, ward_adjacent_pos);
    //let d = tick_delta(&mut eng);
    //assert_structure_destroyed(&d, t_ward, ward_adjacent_pos);

    // Stranger: place a ward adjacent
    input::on_point_use_item(&mut eng, stranger, ward_adjacent_pos, i_ward);
    let d = tick_delta(&mut eng);
    assert_no_structures_changed(&d);


    // Ward diameter + 2*margin is 23 + 2*12 = 47 blocks.
    let act = Activity::Stand {
        pos: V3::new(47 * TILE_SIZE, 0, 0),
        dir: 0,
    };
    let start = eng.w.now;
    eng.w.client_mut(me).pawn_mut().unwrap().set_activity(act.clone(), start);
    eng.w.client_mut(friend).pawn_mut().unwrap().set_activity(act.clone(), start);
    eng.w.client_mut(stranger).pawn_mut().unwrap().set_activity(act.clone(), start);

    tick_delta(&mut eng);
    tick_delta(&mut eng);
    tick_delta(&mut eng);


    let ward_distant_pos = V3::new(48, 1, 0);

    // Place ward far away
    input::on_point_use_item(&mut eng, me, ward_distant_pos, i_ward);
    let d = tick_delta(&mut eng);
    assert_no_structures_changed(&d);

    // Friend: place a ward far away
    input::on_point_use_item(&mut eng, friend, ward_distant_pos, i_ward);
    let d = tick_delta(&mut eng);
    assert_structure_placed(&d, t_ward, ward_distant_pos);
    input::on_point_destroy(&mut eng, friend, ward_distant_pos);
    let d = tick_delta(&mut eng);
    assert_structure_destroyed(&d, t_ward, ward_distant_pos);

    // Stranger: place a ward far away
    input::on_point_use_item(&mut eng, stranger, ward_distant_pos, i_ward);
    let d = tick_delta(&mut eng);
    assert_structure_placed(&d, t_ward, ward_distant_pos);
    input::on_point_destroy(&mut eng, stranger, ward_distant_pos);
    let d = tick_delta(&mut eng);
    assert_structure_destroyed(&d, t_ward, ward_distant_pos);

    eprintln!("all done!");
}
